(eval-and-compile
  (setq-default
    init-bb (or (member (getenv "USERDOMAIN") '("BBVPN" "CORP"))
                (equal  (system-name)         "PF2YPJMM"))))
;; ---------------------------------------------------------------------------
(setq-default
  init-gc-high-cons-threshold          (eval-when-compile (* 64 1024 1024))
  init-gc-low-cons-threshold           (eval-when-compile (* 16 1024 1024))
  init-indent-width                    (if init-bb 4 2)
  init-magit-after-save-refresh-status nil
  init-user-name                       "Alexander Shukaev")
;; ---------------------------------------------------------------------------
(let ((fzf-default-command (getenv "FZF_DEFAULT_COMMAND")))
  (when (zerop (length fzf-default-command))
    (setq fzf-default-command
      (or (when (executable-find "rg")
            (eval-when-compile
              (mapconcat #'identity
                         `("rg"
                           "--files"
                           ;; ------------------------------------------------
                           ;; CAUTION:
                           ;;
                           ;; Using option
                           ;;
                           ;;   "--follow"
                           ;;
                           ;; and running `fzf' from the root '/' of the file
                           ;; system produces infinite output, i.e. the search
                           ;; never ends (most probably because of symbolic
                           ;; link cycles).
                           ;; ------------------------------------------------
                           "--hidden"
                           "--no-ignore"
                           "--no-messages"
                           ;; ------------------------------------------------
                           ;; CAUTION:
                           ;;
                           ;; Using option
                           ;;
                           ;;   "--one-file-system"
                           ;;
                           ;; can feel too limiting at times.
                           ;; ------------------------------------------------
                           "-g" ,(shell-quote-argument "!.git")
                           ,@(when (bound-and-true-p init-bb)
                               `("-g" ,(shell-quote-argument "!mbig*/*")))
                           ,@(unless (memq system-type '(cygwin windows-nt))
                               `("-g" ,(shell-quote-argument "!dev/*")
                                 "-g" ,(shell-quote-argument "!proc/*")
                                 "-g" ,(shell-quote-argument "!sys/*"))))
                         " ")))
          (when (executable-find "fd")
            (eval-when-compile
              (mapconcat #'identity
                         `("fd"
                           "--type" "file"
                           ;; ------------------------------------------------
                           ;; CAUTION:
                           ;;
                           ;; Using option
                           ;;
                           ;;   "--follow"
                           ;;
                           ;; and running `fzf' from the root '/' of the file
                           ;; system produces infinite output, i.e. the search
                           ;; never ends (most probably because of symbolic
                           ;; link cycles).
                           ;; ------------------------------------------------
                           "--hidden"
                           "--no-ignore"
                           ;; ------------------------------------------------
                           ;; CAUTION:
                           ;;
                           ;; Using option
                           ;;
                           ;;   "--same-file-system"
                           ;;
                           ;; can feel too limiting at times.
                           ;; ------------------------------------------------
                           "-E" ,(shell-quote-argument ".git")
                           ,@(when (bound-and-true-p init-bb)
                               `("-E" ,(shell-quote-argument "mbig*/*")))
                           ,@(unless (memq system-type '(cygwin windows-nt))
                               `("-E" ,(shell-quote-argument "dev/*")
                                 "-E" ,(shell-quote-argument "proc/*")
                                 "-E" ,(shell-quote-argument "sys/*"))))
                         " ")))
          (when (executable-find find-program)
            (eval-when-compile
              (mapconcat #'identity
                         `(,find-program
                           ;; ------------------------------------------------
                           ;; CAUTION:
                           ;;
                           ;; Using option
                           ;;
                           ;;   "-L"
                           ;;
                           ;; and running `fzf' from the root '/' of the file
                           ;; system produces infinite output, i.e. the search
                           ;; never ends (most probably because of symbolic
                           ;; link cycles).
                           ;; ------------------------------------------------
                           ,@(unless (memq system-type '(cygwin windows-nt))
                               '("-O3"))
                           "."
                           ;; ------------------------------------------------
                           ;; PORTABILITY:
                           ;;
                           ;;   "-mindepth" "1"
                           ;; ------------------------------------------------
                           ;; CAUTION:
                           ;;
                           ;; Using option
                           ;;
                           ;;   "-xdev"
                           ;;
                           ;; can feel too limiting at times.
                           ;; ------------------------------------------------
                           ,(shell-quote-argument "(")
                           ;; ------------------------------------------------
                           ;; CAUTION:
                           ;;
                           ;; Using option
                           ;;
                           ;;   "-name" ,(shell-quote-argument ".?*")
                           ;;
                           ;; would skip hidden files, while the opposite is
                           ;; desired.
                           ;; ------------------------------------------------
                           "-name" ,(shell-quote-argument ".git")
                           ,(shell-quote-argument ")")
                           "-prune"
                           "-o"
                           ,@(when (bound-and-true-p init-bb)
                               `("-type" "d"
                                 ,(shell-quote-argument "(")
                                 "-name" ,(shell-quote-argument "mbig*")
                                 ,(shell-quote-argument ")")
                                 "-prune"
                                 "-o"))
                           ,@(unless (memq system-type '(cygwin windows-nt))
                               `("-type" "d"
                                 ,(shell-quote-argument "(")
                                 "-name" ,(shell-quote-argument "dev")
                                 "-o"
                                 "-name" ,(shell-quote-argument "proc")
                                 "-o"
                                 "-name" ,(shell-quote-argument "sys")
                                 ,(shell-quote-argument ")")
                                 "-prune"
                                 "-o"
                                 ,(shell-quote-argument "(")
                                 "-fstype" "devfs"
                                 "-o"
                                 "-fstype" "devtmpfs"
                                 "-o"
                                 "-fstype" "proc"
                                 "-o"
                                 "-fstype" "sysfs"
                                 ,(shell-quote-argument ")")
                                 "-prune"
                                 "-o"))
                           ,(shell-quote-argument "(")
                           "-type" "f"
                           ;; ------------------------------------------------
                           ;; PORTABILITY:
                           ;;
                           ;;   "-type" "l"
                           ;; ------------------------------------------------
                           ,(shell-quote-argument ")")
                           "-print"
                           "2>" ,(shell-quote-argument null-device)
                           "|"
                           "cut"
                           "-c" "3-")
                         " ")))))
    (unless (zerop (length fzf-default-command))
      (prog1
          (setq fzf-default-command
            (concat (eval-when-compile
                      (mapconcat #'identity
                                 `(,@(unless (memq system-type '(cygwin
                                                                 windows-nt))
                                       '("set"
                                         "-o" "pipefail"
                                         ";"))
                                   "command")
                                 " "))
                    " "
                    fzf-default-command))
        (setenv "FZF_DEFAULT_COMMAND" fzf-default-command)))))
;; ---------------------------------------------------------------------------
;;

;;; File Local Variables {{{
;;  ==========================================================================
;;  Local Variables:
;;  no-byte-compile: t
;;  End:
;;  ==========================================================================
;;  }}} File Local Variables
