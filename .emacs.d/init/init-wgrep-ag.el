(eval-when-compile
  (require 'init-package))
;;
(init-handler wgrep-ag init)
;;
(init-handler wgrep-ag config)
;;
(init-package wgrep-ag)
