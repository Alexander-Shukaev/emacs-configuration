(eval-when-compile
  (require 'init-package))
;;
(init-handler magit preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error))
  ;;
  (init-load color))
;;
(init-handler magit prepare
  (defcustom init-magit-after-save-refresh-status
    (not (memq system-type '(cygwin windows-nt)))
    "If non-nil, use `magit-after-save-refresh-status'."
    :group 'init
    :type 'boolean))
;;
(init-handler magit init
  (setq-default
    auto-revert-buffer-list-filter      'magit-auto-revert-repository-buffer-p
    magit-auto-revert-tracked-only      t
    magit-diff-highlight-hunk-body      nil
    magit-diff-refine-hunk              'all
    magit-diff-refine-ignore-whitespace nil
    magit-push-always-verify            nil))
;;
(init-handler magit config
  (let ((percent (if (< emacs-major-version 29) 2 10)))
    (let ((faces '(magit-diff-added
                   magit-diff-removed
                   magit-diff-our
                   magit-diff-base
                   magit-diff-their
                   magit-diff-context)))
      (dolist (face faces)
        (let ((background (face-background face))
              (foreground (face-foreground face)))
          (when background
            (set-face-background face
                                 (color-darken-name  background percent)))
          (when foreground
            (set-face-foreground face
                                 (color-lighten-name foreground percent))))))
    (let ((faces '(magit-diff-added-highlight
                   magit-diff-removed-highlight
                   magit-diff-our-highlight
                   magit-diff-base-highlight
                   magit-diff-their-highlight
                   magit-diff-context-highlight)))
      (dolist (face faces)
        (let ((background (face-background face))
              (foreground (face-foreground face)))
          (when background
            (set-face-background face
                                 (color-lighten-name background percent)))
          (when foreground
            (set-face-foreground face
                                 (color-darken-name  foreground percent)))))))
  ;;
  (dolist (hook '(server-switch-hook with-editor-filter-visit-hook))
    (remove-hook hook #'magit-commit-diff))
  ;;
  (init-ignore-progn
    ;; TODO: Broken?
    (dolist (hook '(magit-diff-mode-hook magit-log-mode-hook))
      (add-hook hook #'magit-toggle-buffer-lock)))
  ;;
  (init-defun magit-amend
      (&optional args)
    #$
    "Amend the last commit.
\n(git amend ARGS)"
    (interactive (list (magit-commit-arguments)))
    (magit-run-git-with-editor "amend" args))
  ;;
  (init-defad magit-key-mode-popup-committing
      (:after (&rest ...) init/:after)
    #$
    (magit-key-mode-toggle-option (quote committing) "--verbose"))
  ;;
  (after-load diff-hl
    (add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh))
  ;;
  (after-load devil
    (let ((magit-popup-mode-state (evil-initial-state 'magit-popup-mode
                                                      'emacs)))
      (devil-set-initial-state "^magit-\\(?:.*-\\)?mode$" 'motion)
      (devil-set-initial-state 'magit-popup-mode magit-popup-mode-state)))
  ;;
  (after-load (init-devil devil)
    (init-defun init-magit-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "lock-toggle" #'magit-toggle-buffer-lock
        ;;
        "lt[oggle]"   "lock-toggle"))
    (dolist (hook '(magit-diff-mode-hook magit-log-mode-hook))
      (add-hook hook #'init-magit-devil-setup)))
  ;;
  (after-load init-magit-filenotify
    (init-defun init-magit-filenotify-setup ()
      #$
      (unless (file-remote-p default-directory)
        (magit-filenotify-mode)))
    (add-hook 'magit-status-mode-hook #'init-magit-filenotify-setup))
  ;;
  (after-load init-magit-todos
    (add-hook 'magit-status-mode-hook #'magit-todos-mode)))
;;
(init-handler magit maps
  (let ((magit-popup-mode-map (make-sparse-keymap)))
    (init-clear-matching-keymaps "^magit-\\(?:.*-\\)?map$"))
  ;;
  (init-define-keys-kbd magit-mode-map
    "RET"       #'magit-visit-thing
    ;;
    "TAB"       #'magit-section-cycle
    "<backtab>" #'magit-section-cycle-global
    ;;
    "#"         #'magit-section-toggle
    ;;
    "+"         #'magit-diff-more-context
    "-"         #'magit-diff-less-context
    ;;
    "|"         #'magit-ediff-dwim
    ;;
    "d"         #'magit-delete-thing
    "r"         #'magit-revert-no-commit
    ;;
    "R"         #'magit-rebase
    ;;
    [remap revert-buffer] #'magit-refresh)
  ;;
  (after-load init-magit-annex
    (init-define-keys-kbd magit-mode-map
      "@" #'magit-annex-dispatch-or-init))
  ;;
  (after-load evil
    (evil-make-overriding-map magit-mode-map 'normal))
  ;;
  (init-define-keys-kbd magit-branch-section-map
    [remap magit-visit-thing]  #'magit-visit-ref
    [remap magit-delete-thing] #'magit-branch-delete
    ;;
    "R" #'magit-branch-rename)
  ;;
  (init-define-keys-kbd magit-commit-section-map
    [remap magit-visit-thing] #'magit-show-commit)
  ;;
  (init-define-keys-kbd magit-module-commit-section-map
    [remap magit-visit-thing] #'magit-show-commit)
  ;;
  (init-define-keys-kbd magit-file-section-map
    [remap magit-visit-thing]      #'magit-diff-visit-file
    [remap magit-delete-thing]     #'magit-discard
    [remap magit-revert-no-commit] #'magit-reverse
    ;;
    "a"   #'magit-apply
    "c"   #'magit-commit-add-log
    "s"   #'magit-stage
    "u s" #'magit-unstage
    "R"   #'magit-file-rename
    "u t" #'magit-file-untrack)
  ;;
  (init-define-keys-kbd magit-hunk-section-map
    [remap magit-visit-thing]      #'magit-diff-visit-file
    [remap magit-delete-thing]     #'magit-discard
    [remap magit-revert-no-commit] #'magit-reverse
    ;;
    "a"   #'magit-apply
    "c"   #'magit-commit-add-log
    "s"   #'magit-stage
    "u s" #'magit-unstage)
  ;;
  (init-define-keys-kbd magit-remote-section-map
    [remap magit-delete-thing] #'magit-remote-remove
    ;;
    "R" #'magit-remote-rename)
  ;;
  (init-define-keys-kbd magit-staged-section-map
    [remap magit-visit-thing]      #'magit-diff-staged
    [remap magit-delete-thing]     #'magit-discard
    [remap magit-revert-no-commit] #'magit-reverse
    ;;
    "s"   #'magit-stage
    "u s" #'magit-unstage)
  ;;
  (init-define-keys-kbd magit-unstaged-section-map
    [remap magit-visit-thing]  #'magit-diff-unstaged
    [remap magit-delete-thing] #'magit-discard
    ;;
    "s"   #'magit-stage
    "u s" #'magit-unstage)
  ;;
  (init-define-keys-kbd magit-untracked-section-map
    [remap magit-delete-thing] #'magit-discard
    ;;
    "s" #'magit-stage)
  ;;
  (init-define-keys-kbd magit-stashes-section-map
    [remap magit-delete-thing] #'magit-stash-clear)
  ;;
  (init-define-keys-kbd magit-stash-section-map
    [remap magit-visit-thing]  #'magit-stash-show
    [remap magit-delete-thing] #'magit-stash-drop
    ;;
    "a" #'magit-stash-apply
    "p" #'magit-stash-pop)
  ;;
  (init-define-keys-kbd magit-diff-mode-map
    "*" #'magit-toggle-buffer-lock)
  ;;
  (init-define-keys-kbd magit-log-mode-map
    "*" #'magit-toggle-buffer-lock))
;;
(init-package magit
  :commands
  (magit-amend))
