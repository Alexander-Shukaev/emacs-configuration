(eval-when-compile
  (require 'init-package))
;;
(init-handler cc-vars init
  (setq-default
    c-syntactic-indentation           nil
    c-syntactic-indentation-in-macros nil))
;;
(init-handler cc-vars config)
;;
(init-package cc-vars
  :built-in)
