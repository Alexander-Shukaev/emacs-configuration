(eval-when-compile
  (require 'init-package))
;;
(init-handler pine-script-mode init)
;;
(init-handler pine-script-mode config
  (defconst init-pine-script-mode-hooks
    '(pine-script-mode-hook))
  ;;
  (after-load (init-clang-format devil)
    (init-defun init-pine-script-mode-clang-format-devil-setup ()
      #$
      (setq-local devil-format-region-function #'clang-format-region))
    (dolist (hook init-pine-script-mode-hooks)
      (add-hook hook #'init-pine-script-mode-clang-format-devil-setup)))
  ;;
  (after-load init-electric-operator
    (dolist (hook init-pine-script-mode-hooks)
      (add-hook hook #'electric-operator-mode)))
  ;;
  (after-load init-highlight-operators
    (dolist (hook init-pine-script-mode-hooks)
      (add-hook hook #'highlight-operators-mode))))
;;
(init-package pine-script-mode
  :mode
  (("\\.pine\\'" . pine-script-mode)))
