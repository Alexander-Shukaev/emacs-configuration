(eval-when-compile
  (require 'init-package))
;;
(init-handler all-the-icons init)
;;
(init-handler all-the-icons config)
;;
(init-package all-the-icons
  :disabled)
