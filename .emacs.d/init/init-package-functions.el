(require 'init-functions)
(require 'init-macros)
;;
(require 'use-package nil :no-error)
;;
(require 'package)
;;
(eval-when-compile
  (require 'cl-lib))
;;
;; TODO: Why `eval-and-compile'?
(eval-and-compile
  (with-eval-after-load 'use-package
    (unless (bound-and-true-p init-package--use-package-patched)
      (let* ((symbol  'use-package-keywords)
             (current (default-value symbol))
             (default '(:pin
                        :ensure
                        :disabled
                        :load-path
                        :requires
                        :defines
                        :functions
                        :preface
                        :if
                        :when
                        :unless
                        :no-require
                        :catch
                        :after
                        :custom
                        :custom-face
                        :bind
                        :bind*
                        :bind-keymap
                        :bind-keymap*
                        :interpreter
                        :mode
                        :magic
                        :magic-fallback
                        :hook
                        :commands
                        :init
                        :defer
                        :demand
                        :load
                        :config
                        :diminish
                        :delight)))
        (unless (equal current default)
          (lwarn 'init-package
                 :emergency
                 "Changed the default value of variable `%s':
- current: %s
- default: %s\n%s"
                 symbol
                 current
                 default
                 (init-backtrace-to-string))))
      ;;
      (init-negate (memq :provide        use-package-keywords))
      (init-negate (memq :load-autoloads use-package-keywords))
      (init-negate (memq :prepare        use-package-keywords))
      (init-negate (memq :trap           use-package-keywords))
      (init-negate (memq :preload        use-package-keywords))
      ;;
      ;; ---------------------------------------------------------------------
      ;; CAUTION:
      ;;
      ;; Despite violating documentation string of `use-package' about one of
      ;; the purposes of the `:preface' keyword, it is of little use, while
      ;; conditional prevention of package installation and its byte
      ;; compilation (e.g. triggered by the `:ensure' keyword) is far more
      ;; important as per [1, 2, 3, 4]:
      ;;
      (if (and (fboundp 'native-comp-available-p)
               (native-comp-available-p))
          ;; -----------------------------------------------------------------
          ;; BUG:
          ;;
          ;; Check with Emacs developers as to why the below snippet with
          ;; `nreverse' does not produce expected results when
          ;; `native-comp-available-p' is non-nil.  WTF?
          ;;
          (dolist (keyword '(:provide :unless :when :if :disabled))
            (setq use-package-keywords
              (cons keyword (delq keyword use-package-keywords))))
        (dolist (keyword `,(nreverse
                            (list :disabled :if :when :unless :provide)))
          (setq use-package-keywords
            (cons keyword (delq keyword use-package-keywords)))))
      ;; ---------------------------------------------------------------------
      ;;
      (init-insq :load-autoloads :load-path use-package-keywords :unique)
      (init-insq :prepare        :preface   use-package-keywords :unique)
      (init-insq :trap           :catch     use-package-keywords :unique)
      (init-insq :preload        :init      use-package-keywords :unique)
      ;;
      (init-assert (memq :provide        use-package-keywords))
      (init-assert (memq :load-autoloads use-package-keywords))
      (init-assert (memq :prepare        use-package-keywords))
      (init-assert (memq :trap           use-package-keywords))
      (init-assert (memq :preload        use-package-keywords))
      ;;
      (init-assert (equal use-package-keywords
                          '(:disabled
                            :if
                            :when
                            :unless
                            ;; -----------------------------------------------
                            ;; CAUTION:
                            ;;
                            ;; The following order of the `:provide', `:pin',
                            ;; and `:ensure' keywords is paramount:
                            ;;
                            :provide
                            :pin
                            :ensure
                            ;;
                            ;; One of the subtle reasons (there are plenty of
                            ;; others) for this statement can be seen in the
                            ;; following function call chain excerpt resulting
                            ;; from the `:ensure' keyword (if the package is
                            ;; not yet installed):
                            ;;
                            ;; - `package-install'
                            ;;  - `package-unpack'
                            ;;   - `package-activate-1'
                            ;;   - `package--compile'
                            ;;   - `package--load-files-for-activation'
                            ;;
                            ;; That is a file with `init-' prefix is being
                            ;; loaded and the `:ensure' keyword handler is
                            ;; being run, then the subsequent
                            ;; `package-activate-1' (with the `:reload'
                            ;; argument) function call eventually will `load'
                            ;; the corresponding package, what in turn will
                            ;; `require' the same file with `init-' prefix yet
                            ;; again (thanks to the `init-load-advice/:before'
                            ;; advice to both `load' and `require').  As long
                            ;; as the `provide' function (thanks to the
                            ;; `:provide' keyword) is called for the
                            ;; corresponding feature with `init-' prefix
                            ;; already during the first loading of the
                            ;; corresponding file with `init-' prefix
                            ;; (i.e. before the `:ensure' keyword), then there
                            ;; will be no double loading upon the subsequent
                            ;; `require' function call for the same file with
                            ;; `init-' prefix.  Otherwise, apparently there is
                            ;; an issue of double loading of the file with
                            ;; `init-' prefix, what could potentially lead to
                            ;; surprising and unexpected issues, e.g. due to
                            ;; the `:preface' and/or `:prepare' and/or `:init'
                            ;; and/or `:preload' and/or other keyword handlers
                            ;; being run twice.  Hence, the importance of
                            ;; enforcing the initial statement.
                            ;; -----------------------------------------------
                            :load-path
                            :load-autoloads
                            :requires
                            :defines
                            :functions
                            :preface
                            :prepare
                            :no-require
                            :catch
                            :trap
                            :after
                            :custom
                            :custom-face
                            :bind
                            :bind*
                            :bind-keymap
                            :bind-keymap*
                            :interpreter
                            :mode
                            :magic
                            :magic-fallback
                            :hook
                            :commands
                            :init
                            :preload
                            :defer
                            :demand
                            :load
                            :config
                            :diminish
                            :delight)))
      ;;
      ;; ---------------------------------------------------------------------
      ;; TODO:
      ;;
      ;; Conditions (evaluated `arg') when file was byte-compiled may not
      ;; match conditions when file is loaded.  Thus, this may lead to awful
      ;; and unexpected quirks in configuration.  Hence, the evaluation result
      ;; of `arg' at compile time should be compared to the evaluation result
      ;; of `arg' at load time, where if different, two actions should take
      ;; place:
      ;;
      ;; 1.  disregarding the last byte-compiled version and using the current
      ;;     loaded version;
      ;; 2.  byte recompilation.
      ;;
      ;; This should also reveal usage of inappropriate (volatile) variables
      ;; and/or functions return values in conditions (e.g. `noninteractive',
      ;; `daemonp', `init-display-graphic-p', and etc.), which are especially
      ;; likely to lead to differences between compile-time and load-time
      ;; versions depending on the type/mode of Emacs instance that performs
      ;; byte compilation (e.g. batch mode, daemon, terminal, and etc.).
      ;; Furthermore, this should also draw exposing additional set of
      ;; keywords for load-time-only conditionals (in attempt to resolve the
      ;; above issue in another way) useless as a less comprehensive and
      ;; scalable solution that takes no advantage of byte compilation but
      ;; increases the number of keywords for special cases that user needs to
      ;; be aware of.
      ;;
      (init-assert (fboundp 'use-package-handler/:when))
      (defun use-package-handler/:when
          (name _keyword arg rest state)
        (if (or (bound-and-true-p byte-compile-current-file)
                (bound-and-true-p comp-native-compiling))
            (when (eval arg)
              (use-package-process-keywords name rest state))
          (let ((body (use-package-process-keywords name rest state)))
            `((when ,arg ,@body)))))
      ;;
      (init-assert (fboundp 'use-package-handler/:if))
      (defalias 'use-package-handler/:if #'use-package-handler/:when)
      ;;
      (init-assert (fboundp 'use-package-handler/:unless))
      (defun use-package-handler/:unless
          (name _keyword arg rest state)
        (if (or (bound-and-true-p byte-compile-current-file)
                (bound-and-true-p comp-native-compiling))
            (unless (eval arg)
              (use-package-process-keywords name rest state))
          (let ((body (use-package-process-keywords name rest state)))
            `((unless ,arg ,@body)))))
      ;; ---------------------------------------------------------------------
      ;;
      (init-negate (fboundp 'use-package-normalize/:provide))
      (defalias 'use-package-normalize/:provide
        #'use-package-normalize-predicate)
      ;;
      (init-negate (fboundp 'use-package-handler/:provide))
      (defun use-package-handler/:provide
          (name keyword arg rest state)
        (setq state (nconc state `(,keyword ,arg)))
        (let ((body   (use-package-process-keywords name rest state))
              (symbol (init-package-symbol          name)))
          (use-package-concat
           `((provide ',symbol)
             (init-eval-and-compile-when-compile
               (unless (or (bound-and-true-p byte-compile-current-file)
                           (bound-and-true-p comp-native-compiling))
                 (init-assert (featurep ',symbol)))))
           body)))
      ;;
      (init-assert (fboundp 'use-package-handler/:load-path))
      (defun use-package-handler/:load-path
          (name keyword arg rest state)
        (setq state (nconc state `(,keyword ,arg)))
        (let ((body (use-package-process-keywords name rest state)))
          (use-package-concat
           (mapcar
            #'(lambda
                  (path)
                `(eval-and-compile
                   (let ((path ,path))
                     (add-to-list 'load-path (directory-file-name path)))))
            arg)
           body)))
      ;;
      (init-negate (fboundp 'use-package-normalize/:load-autoloads))
      (defalias 'use-package-normalize/:load-autoloads
        #'use-package-normalize-predicate)
      ;;
      (init-negate (fboundp 'use-package-handler/:load-autoloads))
      (defun use-package-handler/:load-autoloads
          (name keyword arg rest state)
        (setq state (nconc state `(,keyword ,arg)))
        (let ((body (use-package-process-keywords name rest state)))
          (use-package-concat
           (when (and arg (not (plist-get state :demand)))
             (mapcar
              #'(lambda
                    (path)
                  `(eval-and-compile
                     (let ((path ,path))
                       (unless (init-package--autoload-path-member-p path)
                         (init-package--generate-and-load-autoloads path)))))
              (plist-get state :load-path)))
           body)))
      ;;
      (init-negate (fboundp 'use-package-normalize/:trap))
      (defalias 'use-package-normalize/:trap
        #'use-package-normalize-predicate)
      ;;
      (init-negate (fboundp 'use-package-handler/:trap))
      (defun use-package-handler/:trap
          (name keyword arg rest state)
        (setq state (nconc state `(,keyword ,arg)))
        (let ((body (use-package-process-keywords name rest state)))
          (if arg
              (macroexp-unprogn (init-package-trap-form name keyword body))
            body)))
      ;;
      (init-negate (fboundp 'use-package-normalize/:prepare))
      (defalias 'use-package-normalize/:prepare
        #'use-package-normalize-forms)
      ;;
      (init-negate (fboundp 'use-package-handler/:prepare))
      (defun use-package-handler/:prepare (name _keyword arg rest state)
        (let ((body (use-package-process-keywords name rest state)))
          (use-package-concat arg body)))
      ;;
      (init-negate (fboundp 'use-package-normalize/:preload))
      (defalias 'use-package-normalize/:preload
        #'use-package-normalize-forms)
      ;;
      (init-negate (fboundp 'use-package-handler/:preload))
      (defun use-package-handler/:preload (name _keyword arg rest state)
        (let ((body (use-package-process-keywords name rest state)))
          (use-package-concat arg body)))
      ;;
      (defconst init-package--use-package-patched t))))
;;
(defsubst init-package-as-string
    (string-or-symbol)
  "\
If STRING-OR-SYMBOL is already a string, return it.
Otherwise convert it to a string and return that."
  (if (stringp string-or-symbol)
      string-or-symbol
    (symbol-name string-or-symbol)))
;;
(defsubst init-package-as-symbol
    (string-or-symbol)
  "\
If STRING-OR-SYMBOL is already a symbol, return it.
Otherwise convert it to a symbol and return that."
  (if (symbolp string-or-symbol)
      string-or-symbol
    (intern string-or-symbol)))
;;
(defsubst init-package-as-keyword
    (string-or-symbol)
  "\
If STRING-OR-SYMBOL is already a keyword, return it.
Otherwise convert it to a keyword and return that."
  (if (keywordp string-or-symbol)
      string-or-symbol
    (intern (concat ":" (init-package-as-string string-or-symbol)))))
;;
(defsubst init-package-file
    (name)
  (concat "init-" (init-package-as-string name)))
;;
(defsubst init-package-feature
    (name)
  (intern (init-package-file name)))
;;
(defsubst init-package-string
    (directory &optional name)
  (or (and name (init-package-as-string name))
      (file-name-nondirectory (directory-file-name directory))))
;;
(defsubst init-package-symbol
    (name &optional suffix prefix)
  (init-assert name)
  (intern (concat (or  prefix "init-")
                  (and name   (init-package-as-string name))
                  (and suffix (init-package-as-string suffix)))))
;;
(defsubst init-package-handler-symbol
    (name phase-or-keyword)
  (init-assert phase-or-keyword)
  (setq phase-or-keyword (init-package-as-keyword phase-or-keyword))
  (init-package-symbol name
                       (concat "/"
                               (init-package-as-string phase-or-keyword))))
;;
(defsubst init-package-load-form
    (name &optional no-error)
  (cond ((stringp name)
         `(unless (init-load-history ,name)
            (load   ,name     ,no-error :no-message)))
        ((symbolp name)
         `(require ',name nil ,no-error))
        (t
         (signal 'wrong-type-argument
                 (list #'or
                       (list #'stringp name)
                       (list #'symbolp name))))))
;;
(defsubst init-package-load-with-demoted-errors-form
    (name &optional no-error)
  `(let (debug-on-error)
     (with-demoted-errors
         ,(format "Cannot load %s: %%S" name)
       ,(init-package-load-form name no-error))))
;;
(defsubst init-package-trap-form
    (name phase-or-keyword body)
  (let ((handler (init-package-handler-symbol name phase-or-keyword)))
    `(condition-case-unless-debug e
         ,(macroexp-progn body)
       (error (lwarn 'init-package
                     :error
                     "Failed to evaluate handler `%s': %s\n%s"
                     ',handler
                     (error-message-string e)
                     (init-backtrace-to-string))
              nil))))
;;
(defun init-package-using-form
    (name &optional no-error time-form-symbol)
  (unless (symbolp time-form-symbol)
    (signal 'wrong-type-argument (list 'symbolp time-form-symbol)))
  (when (init-compile-p)
    (setq time-form-symbol (or time-form-symbol
                               #'init-eval-and-compile-when-compile))
    `(,time-form-symbol
      (let ((byte-compile-current-file)
            (comp-native-compiling))
        ,(init-package-load-with-demoted-errors-form name no-error)))))
(defalias 'init-using-form #'init-package-using-form)
;;
(defun init-package-using-when-compile-form
    (name &optional no-error)
  (init-package-using-form name no-error #'eval-when-compile))
(defalias 'init-using-when-compile-form
  #'init-package-using-when-compile-form)
;;
(defun init-package-handler-body
    (name phase-or-keyword)
  (let* ((handler (init-package-handler-symbol name phase-or-keyword))
         (body    (or (get handler 'init-package-handler-body)
                      (nthcdr 2 (indirect-function handler)))))
    (when (eq (car-safe (car-safe body)) 'interactive)
      (setq body (cdr body)))
    body))
(defalias 'init-handler-body #'init-package-handler-body)
;;
(defun init-package-handler-p
    (name phase-or-keyword)
  (and (init-handler-body name phase-or-keyword) t))
(defalias 'init-handler-p #'init-package-handler-p)
;;
(defun init-package-handler-declare-form
    (name phase-or-keyword)
  (let ((handler (init-package-handler-symbol name phase-or-keyword)))
    `(declare-function ,handler ,(init-package-file name))))
(defalias 'init-handler-declare-form #'init-package-handler-declare-form)
;;
(defun init-package-handler-defun-form
    (name phase)
  (let* ((handler (init-package-handler-symbol name phase))
         (body    (get handler 'init-package-handler-body)))
    (when body
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      (init-ignore-progn
        (when (memq (init-package-as-symbol phase) '(preinit preconfig))
          (setq body `((init-eval-and-compile-when-compile
                         ,@body)))))
      ;; ---------------------------------------------------------------------
      (push `(eval-and-compile
               (put ',handler 'init-package-name  ',name )
               (put ',handler 'init-package-phase ',phase))
            body))
    `(init-defun ,handler ()
       ;; TODO: Should use the actual file, e.g. `#$':
       ,(init-package-file name)
       (interactive)
       (put ',handler 'init-package-inhibit-handler t)
       ,@body)))
(defalias 'init-handler-defun-form #'init-package-handler-defun-form)
;;
(defun init-package-handler-defun-forms
    (name phase-or-keyword)
  (macroexp-unprogn
   (macroexpand
    (init-handler-defun-form name phase-or-keyword))))
(defalias 'init-handler-defun-forms #'init-package-handler-defun-forms)
;;
(defun init-package-handler-call-form
    (name phase-or-keyword)
  `(,(init-package-handler-symbol name phase-or-keyword)))
(defalias 'init-handler-call-form #'init-package-handler-call-form)
;;
(defun init-package-handler-call-forms
    (name phase-or-keyword)
  (macroexp-unprogn
   (macroexpand
    (init-handler-call-form name phase-or-keyword))))
(defalias 'init-handler-call-forms #'init-package-handler-call-forms)
;;
(defun init-package-handler-call-maybe-form
    (name phase-or-keyword)
  (let ((handler (init-package-handler-symbol name phase-or-keyword)))
    `(unless (get ',handler 'init-package-inhibit-handler)
       (,handler))))
(defalias 'init-handler-call-maybe-form
  #'init-package-handler-call-maybe-form)
;;
(defun init-package-handler-call-maybe-forms
    (name phase-or-keyword)
  (macroexp-unprogn
   (macroexpand
    (init-handler-call-maybe-form name phase-or-keyword))))
(defalias 'init-handler-call-maybe-forms
  #'init-package-handler-call-maybe-forms)
;;
(defun init-package-handler-inhibit-form
    (name phase-or-keyword)
  (let ((handler (init-package-handler-symbol name phase-or-keyword)))
    `(put ',handler 'init-package-inhibit-handler t)))
(defalias 'init-handler-inhibit-form #'init-package-handler-inhibit-form)
;;
(defun init-package-handler-args
    (name phase &optional args)
  (let* ((symbol  (init-package-symbol     name))
         (keyword (init-package-as-keyword phase))
         (phases  (get symbol 'init-package-phases)))
    (init-assert keyword)
    (unless (memq keyword args)
      (if (memq keyword '(:preface :prepare :preload))
          (let ((body (init-handler-body name phase)))
            (when (eq keyword :prepare)
              (let (forms)
                ;; -----------------------------------------------------------
                ;; DEPRECATED:
                ;;
                (init-ignore-progn
                  (dolist (phase phases)
                    (unless (memq phase init-package-phases)
                      (push (init-handler-defun-form name phase) forms))))
                ;; -----------------------------------------------------------
                (when forms
                  ;; ---------------------------------------------------------
                  ;; CAUTION:
                  ;;
                  ;; Prefer `append' over `nconc' here not to modify the
                  ;; original `body' (which is coming from a symbol property,
                  ;; where such modification would become permanent and
                  ;; produce a surprising side effect):
                  ;;
                  (setq body (append body forms))
                  ;; ---------------------------------------------------------
                  )))
            (when body
              (when (eq keyword :preface)
                (setq body `((let ((byte-compile-current-file)
                                   (comp-native-compiling))
                               ,(unless (eq symbol 'init-after-init)
                                  '(init-load after-init :no-error))
                               ,(unless (eq symbol 'init-after-load)
                                  '(init-load after-load :no-error))
                               ,@body))))
              `(,keyword
                ;; -----------------------------------------------------------
                ;; CAUTION:
                ;;
                ;; In case of the `:preface' keyword (which expands to
                ;; `eval-and-compile') and if `init-package' does not expand
                ;; to top-level form(s), then the following snippet can be
                ;; evaluated at load time if eager macro expansion is enabled
                ;; (see `eval-and-compile') and will definitely fail in this
                ;; case:
                ;;
                ;;   (unless (or (bound-and-true-p byte-compile-current-file)
                ;;               (bound-and-true-p comp-native-compiling))
                ;;     (init-assert (featurep ',symbol)))
                ;;
                ;; The solution is to either ensure that `init-package' always
                ;; expands to top-level form(s), what is the case now, or to
                ;; redefine the handler for the `:preface' keyword to use
                ;; `init-eval-and-compile-when-compile' instead of
                ;; `eval-and-compile', what may not be worth it as expansion
                ;; of `init-package' to top-level form(s) is more important to
                ;; maintain for other reasons as well.
                ;; -----------------------------------------------------------
                ,@body)))
        `(,(cond ((eq keyword :preinit  ) :init  )
                 ((eq keyword :preconfig) :config)
                 (t                       keyword))
          (unless (or (bound-and-true-p byte-compile-current-file)
                      (bound-and-true-p comp-native-compiling))
            (init-assert (featurep ',symbol)))
          ,(init-package-trap-form
            name
            keyword
            `(,@(init-handler-defun-forms name phase)
              ,@(when (eq keyword :config)
                  (let (forms)
                    (dolist (phase phases)
                      (unless (memq phase init-package-phases)
                        (setq forms
                          (append (init-handler-defun-forms      name phase)
                                  forms))))
                    forms))
              ,@(init-handler-call-forms  name phase)
              ,@(when (eq keyword :config)
                  (let (forms)
                    (dolist (phase phases)
                      (unless (memq phase init-package-phases)
                        (setq forms
                          (append (init-handler-call-maybe-forms name phase)
                                  forms))))
                    forms)))))))))
(defalias 'init-handler-args #'init-package-handler-args)
;;
(defun init-package-form
    (name &optional args)
  (dolist (phase init-package-disabled-phases)
    (let ((keyword (init-package-as-keyword phase)))
      (when (memq keyword args)
        (error "Disabled keyword `%s'" keyword))))
  (init-setq :defines :variables args)
  `(use-package ,name
     ,@(when (prog1
                 (not (memq :no-provide args))
               (setq args (delq :no-provide args)))
         '(:provide))
     ;; ----------------------------------------------------------------------
     ;; TODO:
     ;;
     ;; Currently, `use-package' is just too dumb to realize that it should
     ;; not attempt to install packages from online archives which have the
     ;; `:load-path' keyword explicitly defined (i.e. they are most likely
     ;; local/personal and don't even exist in online archives):
     ;;
     ,@(when (or (prog1
                     (memq :built-in args)
                   (setq args (delq :built-in args)))
                 (memq :load-path args))
         '(:ensure nil))
     ;; ----------------------------------------------------------------------
     ;;
     ;; TODO:
     ;;
     ;; Make configurable with `init-package-always-load-autoloads':
     ;;
     :load-autoloads
     ;; ----------------------------------------------------------------------
     ;; CAUTION:
     ;;
     ;; As per [5]:
     ;;
     :no-require
     ;; ----------------------------------------------------------------------
     ,@(if (memq :trap args)
           '(:catch nil)
         `(:trap
           ,@(if (eq use-package-verbose 'errors)
                 '(:catch nil)
               '(nil))))
     ;; ----------------------------------------------------------------------
     ,@(when (and (init-compile-p)
                  (or (prog1
                          (not (memq :no-require args))
                        (setq args (delq :no-require args)))
                      (memq :load args)))
         `(;; ----------------------------------------------------------------
           ;; DEPRECATED:
           ;;
           ;; :preface
           ;; (eval-when-compile
           ;;   (let (byte-compile-current-file)
           ;;     ,(init-package-load-with-demoted-errors-form name)))
           ;; ----------------------------------------------------------------
           :preload
           (lambda ()
             (eval-and-compile
               (unless (or (bound-and-true-p byte-compile-current-file)
                           (bound-and-true-p comp-native-compiling))
                 (init-assert (featurep ',(init-package-symbol name))))
               ,@(when (eq use-package-verbose 'debug)
                   `((message ,(format "Preloading package %s" name))))
               (let ((byte-compile-current-file)
                     (comp-native-compiling))
                 ,(init-package-load-with-demoted-errors-form name))))))
     ;;
     ;; TODO:
     ;;
     ;; Make configurable with `init-package-always-no-require'.
     ;; ----------------------------------------------------------------------
     ,@(let (body)
         (dolist (phase init-package-phases)
           (setq body (nconc body
                             ;; ----------------------------------------------
                             ;; CAUTION:
                             ;;
                             ;; Application of `copy-sequence' is extremely
                             ;; important here as it ensures that there will
                             ;; be no (surprising and unwanted) duplication
                             ;; side effect(s) of returned forms:
                             ;;
                             (copy-sequence (init-handler-args name phase))
                             ;;
                             ;; NOTE:
                             ;;
                             ;; It was verified that omitting `copy-sequence'
                             ;; indeed produces a bug, i.e. results in
                             ;; duplication side effect(s) of returned forms.
                             ;; ----------------------------------------------
                             )))
         body)
     ;; ----------------------------------------------------------------------
     ,@args))
;;
(defun init-package--installed-p
    (package-list)
  "\
Check whether packages from PACKAGE-LIST are installed."
  (cl-loop for     package
           in      package-list
           unless  (package-installed-p package)
           do      (cl-return nil)
           finally (cl-return t)))
;;
(defun init-package--install
    (package-list)
  "\
Install packages from PACKAGE-LIST."
  (init-without-timers
    (let (url-http-attempt-keepalives)
      (unless noninteractive
        (message "Refreshing packages..."))
      (condition-case-unless-debug e
          (prog1
              (package-refresh-contents)
            (unless noninteractive
              (message "Refreshing packages...done")))
        (error (unless noninteractive
                 (message "Refreshing packages...failed"))
               (signal (car e) (cdr e))))
      (unless noninteractive
        (message "Installing packages..."))
      (condition-case-unless-debug e
          (prog1
              (dolist (package package-list)
                (unless (package-installed-p package)
                  (unless noninteractive
                    (message "Installing package %s..."
                             package))
                  (condition-case-unless-debug e
                      (prog1
                          (package-install package)
                        (unless noninteractive
                          (message "Installing package %s...done"
                                   package)))
                    (error (unless noninteractive
                             (message "Installing package %s...failed"
                                      package))
                           (signal (car e) (cdr e))))))
            (unless noninteractive
              (message "Installing packages...done")))
        (error (unless noninteractive
                 (message "Installing packages...failed"))
               (signal (car e) (cdr e)))))))
;;
(defun init-package--install-maybe
    (package-list)
  "\
Install the (not yet installed) packages from PACKAGE-LIST.

If all the packages from PACKAGE-LIST are already installed, do
nothing."
  ;; -------------------------------------------------------------------------
  (require 'init-coding-system)
  ;;
  ;; BUG:
  ;;
  ;; Requiring `init-coding-system' is vital here because somehow as soon as
  ;; `init-package--install-maybe' is called, on Windows, Emacs starts to
  ;; consume insane amounts of memory and shows some weird error regarding
  ;; encoding.  It seems to be related to some packages being installed.
  ;; -------------------------------------------------------------------------
  (unless (init-package--installed-p package-list)
    (init-package--install package-list)))
;; ---------------------------------------------------------------------------
(defun init-package--autoloads-directory
    (&optional directory-list)
  (if (not  directory-list)
      (setq directory-list (list user-lisp-directory))
    (unless (listp directory-list)
      (setq directory-list (list directory-list))))
  (catch 'break
    (dolist (directory directory-list)
      (unless (zerop (length directory))
        (when (file-directory-p directory)
          (throw 'break directory))))))
;;
(defun init-package--autoload-path-member-p
    (directory)
  (setq directory (init-user-directory-truename directory))
  (member (directory-file-name directory) init-package--autoload-path))
;;
(defun init-package--generate-autoloads
    (directory &optional name)
  (setq directory (init-user-directory-truename directory))
  (when (string-prefix-p (init-user-directory-truename package-user-dir)
                         directory)
    (error "Generating autoloads in subdirectory of `%s' (%s) not allowed: %s"
           'package-user-dir package-user-dir directory))
  (prog1
      (init-without-timers
        (package-generate-autoloads (init-package-string directory name)
                                    directory))
    (add-to-list 'init-package--autoload-path
                 (directory-file-name directory))))
;;
(defun init-package--load-autoloads
    (directory &optional name no-error)
  (setq directory (init-user-directory-truename directory))
  (when (string-prefix-p (init-user-directory-truename package-user-dir)
                         directory)
    (error "Loading autoloads from subdirectory of `%s' (%s) not allowed: %s"
           'package-user-dir package-user-dir directory))
  (let* ((name (if (string-suffix-p ".el" name)
                   name
                 (concat (init-package-string directory name)
                         "-autoloads.el")))
         (base (file-name-base name))
         (file (init-user-expand-file-name name directory)))
    (init-without-timers
      (when (and (not noninteractive) force-load-messages)
        (message "Loading autoloads %s..." base))
      (condition-case-unless-debug e
          (if (let (force-load-messages)
                (load file no-error :no-message :no-suffix :must-suffix))
              (when (and (not noninteractive) force-load-messages)
                (message "Loading autoloads %s...done" base))
            (when (and (not noninteractive) force-load-messages)
              (message "Loading autoloads %s...failed" base))
            (setq file nil))
        (error (when (and (not noninteractive) force-load-messages)
                 (message "Loading autoloads %s...failed" base))
               (signal (car e) (cdr e)))))
    file))
;;
(defun init-package--generate-and-load-autoloads
    (directory &optional name no-error)
  (init-package--load-autoloads directory
                                (init-package--generate-autoloads directory
                                                                  name)
                                no-error))
;;
(provide 'init-package-functions)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/jwiegley/use-package/issues/496'
;;  [2] URL `http://github.com/jwiegley/use-package/issues/523'
;;  [3] URL `http://github.com/jwiegley/use-package/issues/608'
;;  [4] URL `http://github.com/jwiegley/use-package/issues/637'
;;  [5] URL `http://github.com/jwiegley/use-package/commit/f6f690addec67735c7fdf25689742000c4fd96c2'
;;  ==========================================================================
;;  }}} References
