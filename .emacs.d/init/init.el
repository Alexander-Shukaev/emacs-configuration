(eval-when-compile
  (require 'init-macros))
;;
;; ---------------------------------------------------------------------------
;; BUG:
;;
;; At least with Emacs 25.1.1, `init-load-advice/:before' is not called for
;; `easymenu' as part of loading of `init-package' (when it is done after
;; `init-load-advice/:before' is defined of course).  So far, I have no
;; explanation for this bug.  Kill `easymenu' as early as possible, here:
;;
(with-eval-after-load 'easymenu
  (init-assert (fboundp 'easy-menu-add-item))
  (define-advice easy-menu-add-item
      (:override (&rest ...) init/:override))
  (init-assert (fboundp 'easy-menu-do-define))
  (define-advice easy-menu-do-define
      (:override (symbol &rest ...) init/:override)
    (when symbol
      (set symbol (make-sparse-keymap)))))
;;
;; Looks like there is explanation for this, see below.
;; ---------------------------------------------------------------------------
;;
(require 'init-package)
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Turns out these advices would only apply `load' and `require' function
;; calls performed during Emacs Lisp evaluation, but not if the corresponding
;; `Fload' and `Frequire' native functions are called directly in the C source
;; code!  This leaves one hole, namely `autoload' and more precisely
;; `autoload-do-load'.  That's the reason why when an autoloaded function is
;; called, the next first stack frame in a backtrace of the Emacs Lisp
;; debugger would be `do-after-load-evaluation' since although called from the
;; C source code (`Fautoload_do_load' -> `Fload' ->
;; `do-after-load-evaluation'), the actual call to `do-after-load-evaluation'
;; is done indirectly via `call1' referring to the corresponding symbol
;; `Qdo_after_load_evaluation'.
;;
;; TODO:
;;
;; Move to a better place:
(defvar init--features nil)
;;
(defun init-load-advice/:before
    (file)
  (let ((init--name (file-name-base file)))
    (unless (string-prefix-p "init-" init--name)
      (let ((init--feature (intern init--name)))
        (unless (memq init--feature init--features)
          (let ((init--features (cons init--feature init--features)))
            (if (fboundp 'init-load)
                (eval `(init-load ,init--feature :no-error))
              (setq init--feature (intern (concat "init-" init--name)))
              (let ((byte-compile-current-file)
                    (comp-native-compiling))
                (require init--feature nil :no-error)))))))))
;;
(define-advice load
    (:before (file &rest ...) init-load/:before -100)
  (init-load-advice/:before file))
;;
(define-advice require
    (:before (feature &optional filename &rest ...) init-require/:before -100)
  (unless (featurep feature)
    (init-load-advice/:before (or filename (symbol-name feature)))))
;; ---------------------------------------------------------------------------
(init-load gcmh)
;; ---------------------------------------------------------------------------
(init-load frame)
(init-load x-win)
(init-load xwidget)
;; ---------------------------------------------------------------------------
(init-load menu-bar)
(init-load scroll-bar)
(init-load tool-bar)
;; ---------------------------------------------------------------------------
(init-load let-alist)
;; ---------------------------------------------------------------------------
(init-load all-the-icons)
(init-load all-the-icons-dired)
(init-load all-the-icons-ivy)
;; ---------------------------------------------------------------------------
(init-load dracula-theme)
(init-load powerline)
(init-load solarized-theme)
(init-load theme)
;; ---------------------------------------------------------------------------
(init-load highlight-escape-sequences)
(init-load highlight-indentation)
(init-load highlight-numbers)
(init-load highlight-operators)
;; ---------------------------------------------------------------------------
(init-load simple)
(init-load button)
(init-load coding-system)
(init-load completion-list)
(init-load debug)
(init-load diff)
(init-load diff-hl)
(init-load diff-mode)
(init-load smerge-mode)
(init-load edebug)
(init-load ediff)
(init-load global)
(init-load bindings)
(init-load help-mode)
(init-load info)
(init-load mouse)
(init-load mwheel)
(init-load special)
(init-load tabulated-list)
(init-load wid-edit)
(init-load delsel)
(init-load indent)
(init-load apropos)
;; ---------------------------------------------------------------------------
(init-load font-lock)
(init-load hi-lock)
(init-load jit-lock)
;; ---------------------------------------------------------------------------
(init-load xclip)
(init-load fill)
(init-load auto-fill)
(init-load newcomment)
(init-load cmake-font-lock)
(init-load display-fill-column-indicator)
(init-load fill-column-indicator)
(init-load whitespace)
(init-load master-window)
(init-load bug-reference)
(init-load goto-addr)
(init-load golden-ratio)
(init-load ace-window)
(init-load windmove)
(init-load window)
(init-load buffer-manager)
(init-load scroll)
(init-load smooth-scrolling)
(init-load pixel-scroll)
(init-load nlinum)
(init-load rainbow-mode)
(init-load rainbow-delimiters)
(init-load minibuffer)
(init-load minibuffer-line)
(init-load paren)
;; ---------------------------------------------------------------------------
(init-load company)
(init-load company-flx)
(init-load company-lsp)
(init-load company-org-block)
(init-load company-php)
(init-load company-ycmd)
;; ---------------------------------------------------------------------------
(init-load flycheck)
(init-load flycheck-ycmd)
;; ---------------------------------------------------------------------------
(init-load ycmd)
;; ---------------------------------------------------------------------------
(init-load org)
(init-load org-indent)
(init-load org-num)
(init-load org-preview-html)
(init-load org-superstar)
;; ---------------------------------------------------------------------------
(init-load ob)
(init-load ob-async)
(init-load ob-core)
;; ---------------------------------------------------------------------------
(init-load undo)
(init-load undo-tree)
(init-load eldoc)
(init-load elint)
(init-load elisp-mode)
(init-load lisp-mode)
(init-load conf-mode)
(init-load prog-mode)
(init-load text-mode)
(init-load cmake-mode)
(init-load yaml-mode)
(init-load clang-format)
(init-load speck)
(init-load pdf-tools)
(init-load files)
(init-load ffap)
;; ---------------------------------------------------------------------------
(init-load bibtex)
(init-load context)
(init-load font-latex)
(init-load reftex)
(init-load tex)
(init-load tex-fold)
(init-load tex-font)
(init-load tex-mode)
;; ---------------------------------------------------------------------------
(init-load with-editor)
;; ---------------------------------------------------------------------------
(init-load git)
;; ---------------------------------------------------------------------------
(init-load git-annex)
;; ---------------------------------------------------------------------------
(init-load git-commit)
(init-load git-rebase)
;; ---------------------------------------------------------------------------
(init-load gitattributes-mode)
(init-load gitconfig-mode)
(init-load gitignore-mode)
;; ---------------------------------------------------------------------------
(init-load doc-view)
;; ---------------------------------------------------------------------------
(init-load mneme)
(init-load recentf)
(init-load recentf-ext)
(init-load sync-recentf)
;; ---------------------------------------------------------------------------

(init-load ansi-color)
(init-load bash-completion)
(init-load comint)
(init-load eshell)
(init-load esh-mode)
(init-load shell)

(init-load crosshairs)
(init-load hl-line)
(init-load hl-todo)

(init-load gnus)
(init-load rmail)
(init-load select)

(init-load find-dired)
(init-load dired)
(init-load dired-x)
(init-load dired-details)
(init-load dired-k)

(init-load ls-lisp)

(init-load header2)
(init-load autoinsert)
(init-load autorevert)
(init-load bat-mode)
(init-load python)
(init-load pydoc)
(init-load pydoc-info)
(init-load sh-script)

(init-load transient)

(init-load magit)
(init-load magit-annex)
(init-load magit-popup)
(init-load magit-filenotify)
(init-load magit-todos)

(init-load wgrep)
(init-load wgrep-ag)
(init-load wgrep-rg)
(init-load ag)
(init-load rg)
(init-load compile)
(init-load grep)
(init-load deadgrep)
(init-load isearch)

(init-load auth-source)
(init-load auth-source-pass)
(init-load ssh)
(init-load authinfo)
(init-load epa)
(init-load epa-file)
(init-load epa-hook)
(init-load epg)

(init-load easy-kill)

(init-load make-mode)

(init-load avy)

(init-load perl-mode)
(init-load cperl-mode)

(init-load elec-pair)
(init-load electric-operator)

(init-load mb-depth)

(init-load abbrev)

(init-load hungry-delete)

;; ---------------------------------------------------------------------------
(init-load cc-mode)
(init-load cc-vars)
;; ---------------------------------------------------------------------------
(init-load php-extras)
(init-load php-mode)
;; ---------------------------------------------------------------------------
(init-load js)
(init-load js2-mode)
(init-load json-mode)
;; ---------------------------------------------------------------------------
(init-load protobuf-mode)
;; ---------------------------------------------------------------------------
(init-load pine-script-mode)

(init-load web-mode)

(init-load completion-ignored-extensions)

(init-load user)

(init-load server)

(init-load savehist)
(init-load easymenu)

(init-load tramp)
(init-load tramp-sh)

(init-load password-cache)

(init-load vc)

(init-load expand-region)

(init-load swiper)
;; ---------------------------------------------------------------------------
(init-load counsel)
(init-load counsel-filter)
(init-load counsel-pydoc)
;; ---------------------------------------------------------------------------
(init-load ido)
;; ---------------------------------------------------------------------------
(init-load ivy)
(init-load ivy-historian)
(init-load ivy-hydra)
;; ---------------------------------------------------------------------------
(init-load flx)
(init-load flx-isearch)
;; ---------------------------------------------------------------------------
(init-load lsp-mode)
(init-load lsp-ui)
(init-load lsp-java)
;; ---------------------------------------------------------------------------
(init-load url)
(init-load url-methods)
;; ---------------------------------------------------------------------------


(init-load   man)
(init-load woman)

(init-load itail)

(init-load esup)
(init-load memory-usage)

(init-load gif-screencast)

(init-load global-longlines)
(init-load longlines)
(init-load line-number-display-limit)
(init-load inhibit-set-auto-mode)

(init-load term)
(init-load term-cursor)

(init-load find-func)

(init-load annalist)
(init-load helpful)

(init-load memoize)

(init-load treemacs)

(init-load devil)
(init-load evil)
(init-load evil-collection)
(init-load evil-collection-vterm)
(init-load evil-surround)
(init-load evil-terminal-cursor-changer)

(init-load vterm)
(init-load zmq)
;;
(provide 'init)
