(eval-when-compile
  (require 'init-package))
;;
(init-handler master-window init
  (setq-default
    master-window-side                   'right
    master-window-min-height             (+ 40 window-safe-min-height)
    master-window-split-height-threshold 80))
;;
(init-handler master-window config)
;;
(init-package master-window
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Compatibility with `ediff'.
  ;; -------------------------------------------------------------------------
  :load-path "lisp/master-window")
