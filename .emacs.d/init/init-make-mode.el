(eval-when-compile
  (require 'init-package))
;;
(init-handler make-mode init)
;;
(init-handler make-mode config
  (init-defun makefile-mode-setup ()
    #$
    (setq-local indent-tabs-mode nil))
  (add-hook 'makefile-mode-hook #'makefile-mode-setup))
;;
(init-package make-mode
  :built-in)
