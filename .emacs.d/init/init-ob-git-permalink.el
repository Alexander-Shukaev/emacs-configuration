(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-git-permalink prepare
  (init-load ob :no-error))
;;
(init-handler ob-git-permalink init
  (after-load org
    (dolist (language '((git-permalink . t)))
      (add-to-list 'org-babel-load-languages language))))
;;
(init-handler ob-git-permalink config)
;;
(init-package ob-git-permalink)
