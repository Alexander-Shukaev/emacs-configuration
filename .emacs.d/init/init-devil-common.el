(eval-when-compile
  (require 'init-package))
;;
(eval-and-compile
  (init-load devil-load-path))
;;
(init-handler devil-common init)
;;
(init-handler devil-common config)
;;
(init-package devil-common
  :load-path init-devil-load-path
  :ensure evil)
