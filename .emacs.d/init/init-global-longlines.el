(eval-when-compile
  (require 'init-package))
;;
(init-handler global-longlines prepare
  (init-load longlines :no-error))
;;
(init-handler global-longlines init
  (init-load line-number-display-limit :no-error)
  ;;
  (setq-default global-longlines-threshold line-number-display-limit-width)
  ;;
  (after-init (global-longlines-mode)))
;;
(init-handler global-longlines config
  (after-load simple
    (init-defun init-global-longlines-messages-buffer-mode-function ()
      #$
      ;; ---------------------------------------------------------------------
      ;; NOTE:
      ;;
      ;; Good sources for inspiration on various interesting criteria to
      ;; implement or customize the `turn-on' function for a global minor mode
      ;; (see `define-globalized-minor-mode') are functions `company-mode-on',
      ;; `flycheck-may-enable-mode', and `ycmd--maybe-enable-mode'.
      ;; ---------------------------------------------------------------------
      (and (not noninteractive)
           (init-interesting-buffer-p)
           (not (eq (get major-mode 'mode-class) 'special))))
    (add-hook 'global-longlines-functions
              #'init-global-longlines-messages-buffer-mode-function)))
;;
(init-package global-longlines
  :load-path "lisp/global-longlines"
  :diminish
  (global-longlines-mode))
