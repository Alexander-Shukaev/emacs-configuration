(eval-when-compile
  (require 'init-package))
;;
(init-handler menu-bar init
  ;; -------------------------------------------------------------------------
  ;; NOTE:
  ;;
  ;; `<C-mouse-3>' can still be used to open menu in contextual (pop-up) form.
  ;; -------------------------------------------------------------------------
  (menu-bar-mode -1))
;;
(init-handler menu-bar config)
;;
(init-package menu-bar
  :built-in
  :commands
  (menu-bar-mode))
