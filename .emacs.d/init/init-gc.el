(eval-when-compile
  (require 'init-macros))
;;
(defcustom init-gc-high-cons-threshold
  (max (* 100 (or (bound-and-true-p default-gc-cons-threshold) 0))
       ;; Hexadecimal: 10000000
       (eval-when-compile (* 64 1024 1024)))
  "High value of variable `gc-cons-threshold'."
  :group 'init
  :type 'integer)
(init-assert (>= init-gc-high-cons-threshold
                 (or (bound-and-true-p default-gc-cons-threshold) 0)))
;;
(defcustom init-gc-low-cons-threshold
  (max (* 10 (or (bound-and-true-p default-gc-cons-threshold) 0))
       ;; Hexadecimal: 1000000
       (eval-when-compile (* 16 1024 1024)))
  "Low value of variable `gc-cons-threshold'."
  :group 'init
  :type 'integer)
(init-assert (>= init-gc-low-cons-threshold
                 (or (bound-and-true-p default-gc-cons-threshold) 0)))
;;
(init-assert (>= init-gc-high-cons-threshold init-gc-low-cons-threshold))
;;
(defsubst init-gc-max-cons-threshold
    (&optional value)
  "\
Return maximum value of variable `gc-cons-threshold'."
  (max (or value gc-cons-threshold)
       (max init-gc-high-cons-threshold
            (* 100 (or (bound-and-true-p default-gc-cons-threshold) 0))
            ;; Hexadecimal: 10000000
            (eval-when-compile (* 64 1024 1024)))))
(init-assert (>= (init-gc-max-cons-threshold) gc-cons-threshold))
(init-assert (>= (init-gc-max-cons-threshold) init-gc-high-cons-threshold))
(init-assert (>= (init-gc-max-cons-threshold)
                 (or (bound-and-true-p default-gc-cons-threshold) 0)))
;;
(defsubst init-gc-min-cons-threshold
    (&optional value)
  "\
Return minimum value of variable `gc-cons-threshold'."
  (min (or value gc-cons-threshold)
       (max init-gc-low-cons-threshold
            (* 10 (or (bound-and-true-p default-gc-cons-threshold) 0))
            ;; Hexadecimal: 1000000
            (eval-when-compile (* 16 1024 1024)))))
(init-assert (<= (init-gc-min-cons-threshold) gc-cons-threshold))
;;
(init-assert (>= (init-gc-max-cons-threshold) (init-gc-min-cons-threshold)))
;;
(provide 'init-gc)
