(eval-when-compile
  (require 'init-package))
;;
(init-handler comint preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler comint init
  (init-load files :no-error)
  ;;
  (setq-default
    comint-buffer-maximum-size        large-file-warning-threshold
    comint-eol-on-send                t
    comint-history-isearch            nil
    comint-input-ignoredups           t
    comint-move-point-for-output      nil
    comint-pager                      (and (executable-find "cat") "cat")
    comint-prompt-read-only           t
    comint-scroll-to-bottom-on-input  t
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; When `comint-scroll-show-maximum-output' is non-nil, it significantly
    ;; slows down output consumption because of repeatedly calling `recenter'.
    ;;
    comint-scroll-show-maximum-output nil
    ;; -----------------------------------------------------------------------
    comint-use-prompt-regexp          nil))
;;
(init-handler comint config
  ;; TODO: [2]
  (setq-default
    comint-password-prompt-regexp
    (concat comint-password-prompt-regexp
            (eval-when-compile
              (concat "\\|"
                      "^.*"
                      "\\("
                      "[Pp]assword"
                      "\\|"
                      "[Ee]nter any "
                      "\\(?:existing \\|remaining \\)?"
                      "passphrase"
                      "\\|"
                      "[Vv]erify [Pp]assphrase"
                      "\\|"
                      "[Pp]assword for .+"
                      "\\(?: (never stored)\\)?"
                      "\\|"
                      "[Bb]-?[Uu]nit"
                      "\\(?: .+\\)?"
                      "\\|"
                      "[Yy]ubikey"
                      "\\(?: .+\\)?"
                      "\\)"
                      "[[:blank:]]*[:：៖]"
                      "\\(?:[[:blank:]]*\\[.+\\]\\)?[[:space:]]*\\'"))))
  ;;
  (remove-hook 'comint-output-filter-functions #'ansi-color-process-output)
  ;;
  (add-hook    'comint-output-filter-functions #'comint-truncate-buffer)
  (add-hook    'comint-output-filter-functions #'comint-strip-ctrl-m)
  ;;
  (init-defun init-comint-buffer-substring--filter
      (beg end &optional delete)
    #$
    "Function to use for `filter-buffer-substring-function' in `comint-mode'."
    (if comint-prompt-read-only
        (if delete
            (let ((inhibit-read-only t))
              (buffer-substring--filter beg end delete))
          (buffer-substring-no-properties beg end))
      (buffer-substring--filter beg end delete)))
  ;;
  (init-defun init-comint-strip-ctrl-g
      (&optional _string)
    #$
    "Strip leading `^G' characters from the current output group.

This function could be on `comint-output-filter-functions' or bound to a key."
    (interactive)
    (let ((pmark (process-mark (get-buffer-process (current-buffer)))))
      (save-excursion
        (ignore-errors
          (goto-char (if (called-interactively-p 'interactive)
                         comint-last-input-end
                       comint-last-output-start)))
        (while (re-search-forward "^\a+" pmark t)
          (ding)
          (replace-match "" t t)))))
  (add-hook 'comint-output-filter-functions #'init-comint-strip-ctrl-g)
  ;;
  (add-hook 'comint-output-filter-functions #'ansi-color-process-output)
  ;;
  (init-defun comint-bolp ()
    #$
    "Return t if point is at the beginning of a line or a prompt."
    (= (comint-line-beginning-position) (point)))
  ;;
  (init-defun comint-send-input-or-insert-previous-input ()
    #$
    "Call `comint-send-input' if point is after the process output marker.
Otherwise, move point to the process mark and try to insert a previous input
from `comint-input-ring' (if any) returned by `comint-previous-input-string'
and affected by the current value of `comint-input-ring-index'.

Implementation is synthesized from and inspired by the `comint-after-pmark-p',
`comint-goto-process-mark', and `comint-copy-old-input' functions."
    (interactive)
    (let ((process (get-buffer-process (current-buffer))))
      (if (not process)
          (user-error "Current buffer has no process")
        (let ((pmark (process-mark process)))
          (if (<= (marker-position pmark) (point))
              (comint-send-input)
            (goto-char pmark)
            (when (and (eolp) comint-input-ring)
              (let ((input (comint-previous-input-string 0)))
                (when (char-or-string-p input)
                  (insert input)))))))))
  ;;
  (init-defun init-comint-setup ()
    #$
    (setq-local fill-column 128)
    (setq-local filter-buffer-substring-function
      #'init-comint-buffer-substring--filter))
  (add-hook 'comint-mode-hook #'init-comint-setup)
  ;;
  (after-load init-display-fill-column-indicator
    (add-hook 'comint-mode-hook #'display-fill-column-indicator-mode))
  ;;
  (after-load init-fill-column-indicator
    (add-hook 'comint-mode-hook #'fci-mode))
  ;;
  (after-load init-compile
    (add-hook 'comint-mode-hook #'compilation-shell-minor-mode))
  ;;
  (after-load init-bug-reference
    (add-hook 'comint-mode-hook #'bug-reference-mode))
  ;;
  (after-load init-goto-addr
    (add-hook 'comint-mode-hook #'goto-address-mode))
  ;;
  (after-load (init-font-lock font-lock init-simple simple)
    (font-lock-add-keywords 'comint-mode
                            init-fundamental-mode-font-lock-keywords))
  ;;
  (after-load longlines
    (init-defun init-comint-longlines-setup ()
      #$
      (setq-local longlines-auto-wrap nil))
    (add-hook 'comint-mode-hook #'init-comint-longlines-setup)
    ;;
    (init-defun init-comint-output-filter-longlines-function
        (string)
      #$
      (when (bound-and-true-p longlines-mode)
        (let ((buffer-undo-list           t)
              (inhibit-field-text-motion  t)
              (inhibit-modification-hooks t)
              (mod                        (buffer-modified-p)))
          (with-silent-modifications
            (longlines-decode-region comint-last-output-start (point-max))
            (when (bound-and-true-p longlines-showing)
              (longlines-show-region comint-last-output-start (point-max))))
          (longlines-wrap-region comint-last-output-start (point-max))
          (set-buffer-modified-p mod))))
    (add-hook 'comint-output-filter-functions
              #'init-comint-output-filter-longlines-function))
  ;;
  (after-load evil
    (evil-set-initial-state 'comint-mode 'normal)
    ;;
    (evil-declare-motion #'comint-next-prompt)
    (evil-declare-motion #'comint-previous-prompt)
    ;;
    (evil-set-type #'comint-next-prompt     'line)
    (evil-set-type #'comint-previous-prompt 'line))
  ;;
  (after-load (init-devil devil)
    (init-defun init-comint-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "process-eof"   #'comint-send-eof
        ;;
        "pe[of]"        "process-eof"
        ;;
        "job-kill"      #'comint-kill-subjob
        "job-quit"      #'comint-quit-subjob
        "job-continue"  #'comint-continue-subjob
        "job-stop"      #'comint-stop-subjob
        "job-interrupt" #'comint-interrupt-subjob
        ;;
        "jk[ill]"       "job-kill"
        "jq[uit]"       "job-quit"
        "jc[ontinue]"   "job-continue"
        "js[top]"       "job-stop"
        "ji[nterrupt]"  "job-interrupt"
        ;;
        "output-write"  #'comint-write-output
        ;;
        "ow[rite]"      "output-write"
        ;;
        "buffer-clear"  #'comint-clear-buffer
        "screen-clear"  #'comint-clear-buffer
        ;;
        "bc[lear]"      "buffer-clear"
        "sc[lear]"      "screen-clear"
        "cls[creen]"    "screen-clear"))
    (add-hook 'comint-mode-hook #'init-comint-devil-setup)))
;;
(init-handler comint mode-map
  (let ((keymap comint-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      "<mouse-3>"               #'comint-insert-input
      ;;
      "<down>"                  #'comint-next-matching-input-from-input
      "<up>  "                  #'comint-previous-matching-input-from-input
      ;;
      "<S-down>"                #'comint-next-input
      "<S-up>  "                #'comint-previous-input
      ;;
      "<C-down>"                #'comint-next-input
      "<C-up>  "                #'comint-previous-input
      ;;
      "C-a"                     #'comint-accumulate
      ;;
      ;; As per [3]:
      "C-d"                     #'comint-delchar-or-maybe-eof
      ;;
      "C-r"                     #'comint-history-isearch-backward-regexp
      ;;
      [remap comint-send-input] #'comint-send-input-or-insert-previous-input
      "RET"                     #'comint-send-input-or-insert-previous-input
      ;;
      "C->"                     #'comint-next-prompt
      "M->"                     #'comint-next-prompt
      "C-<"                     #'comint-previous-prompt
      "M-<"                     #'comint-previous-prompt))
  ;;
  (after-load init-counsel
    (init-define-keys-kbd comint-mode-map
      "C-s" #'counsel-shell-history))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd comint-mode-map
      :motion
      "> >" #'comint-next-prompt
      "< <" #'comint-previous-prompt)
    ;;
    (devil-repeat-motions-kbd
      :keymap comint-mode-map
      "> >" "> >" "< <"
      "< <" "> >" "< <")))
;;
(init-package comint
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://www.masteringemacs.org/article/shell-comint-secrets-history-commands'
;;  [2] URL `http://oleksandrmanzyuk.wordpress.com/2011/10/23/a-persistent-command-history-in-emacs'
;;  [3] URL `http://en.wikipedia.org/wiki/End-of-Transmission_character'
;;  ==========================================================================
;;  }}} References
