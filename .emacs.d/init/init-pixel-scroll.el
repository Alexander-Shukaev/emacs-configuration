(eval-when-compile
  (require 'init-package))
;;
(init-handler pixel-scroll init
  (after-init (pixel-scroll-precision-mode)))
;;
(init-handler pixel-scroll config)
;;
(init-package pixel-scroll
  :built-in)
