(eval-when-compile
  (require 'init-package))
;;
(init-handler bash-completion init)
;;
(init-handler bash-completion config
  ;; -------------------------------------------------------------------------
  ;; DEPRECATED:
  ;;
  (init-ignore-progn
    (init-defad bash-completion-dynamic-complete
        (:around (function &rest ...) init/:around)
      #$
      (unless (file-remote-p default-directory)
        (apply function ...))))
  ;; -------------------------------------------------------------------------
  )
;;
(init-package bash-completion)
