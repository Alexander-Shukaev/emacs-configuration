(eval-when-compile
  (require 'init-package))
;;
(init-handler bug-reference preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler bug-reference init)
;;
(init-handler bug-reference config)
;;
(init-handler bug-reference map
  (let ((keymap bug-reference-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      [remap find-file-at-point] #'bug-reference-push-button
      ;;
      "<mouse-2>"                #'bug-reference-push-button))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd bug-reference-map
      :motion
      [remap evil-find-file-at-point-with-line] #'bug-reference-push-button
      ;;
      [remap find-file-at-point]                #'bug-reference-push-button)))
;;
(init-package bug-reference
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://emacs.stackexchange.com/questions/35878/multiple-url-formats-for-bug-reference-mode'
;;  [2] URL `http://tromey.com/blog/?p=895'
;;  ==========================================================================
;;  }}} References
