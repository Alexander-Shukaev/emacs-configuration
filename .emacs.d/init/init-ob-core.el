(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-core init
  (setq-default
    org-babel-no-eval-on-ctrl-c-ctrl-c nil
    org-confirm-babel-evaluate         nil))
;;
(init-handler ob-core config
  (after-load org
    (add-hook 'org-babel-after-execute-hook #'org-redisplay-inline-images
              :append))
  ;;
  (after-load (init-jupyter-mime jupyter-mime)
    (add-hook 'org-babel-after-execute-hook #'init-jupyter-ansi-color-apply
              :append)))
;;
(init-package ob-core
  :built-in)
