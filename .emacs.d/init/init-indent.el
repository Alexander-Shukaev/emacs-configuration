(eval-when-compile
  (require 'init-package))
;;
(init-handler "indent" prepare
  (defcustom init-indent-width
    2
    "Width of indentation."
    :group 'init
    :type 'integer)
  (put 'init-indent-width 'safe-local-variable #'integerp))
;;
(init-handler "indent" init
  (setq-default
    indent-tabs-mode  nil
    tab-always-indent t
    tab-stop-list     nil
    tab-width         8))
;;
(init-handler "indent" config)
;;
(init-package "indent"
  :built-in)
