(eval-when-compile
  (require 'init-package))
;;
(init-handler esh-mode init)
;;
(init-handler esh-mode config
  (after-load init-company
    (add-hook 'eshell-mode-hook #'init-company-idle-delay-setup))
  ;;
  (after-load init-counsel
    (init-defun init-eshell-mode-counsel-setup ()
      #$
      (init-assert (local-variable-p 'eshell-mode-map))
      (init-define-keys-kbd eshell-mode-map
        "C-s" #'counsel-esh-history))
    (add-hook 'eshell-mode-hook #'init-eshell-mode-counsel-setup)))
;;
(init-package esh-mode
  :built-in)
