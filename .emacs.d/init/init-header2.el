(eval-when-compile
  (require 'init-package))
;;
(init-handler header2 preface
  (eval-when-compile
    (init-load macros)))
;;
(init-handler header2 init
  (setq-default make-header-hook '(init-header2--insert-preamble-start
                                   init-header2--insert-thick-separator-line
                                   init-header2--insert-file
                                   init-header2--insert-thin-separator-line
                                   init-header2--insert-author
                                   init-header2--insert-thin-separator-line
                                   init-header2--insert-maintainer
                                   init-header2--insert-thin-separator-line
                                   init-header2--insert-thick-separator-line
                                   init-header2--insert-preamble-end)))
;;
(init-handler header2 config
  (defsubst init-header2--user-name
      (&optional value)
    (or (bound-and-true-p init-user-name) value "John Doe"))
  ;;
  (defsubst init-header2--user-email
      (&optional value)
    (or (bound-and-true-p init-user-email) value "john.doe@example.com"))
  ;;
  (defsubst init-header2--user
      (&optional value)
    (concat (init-header2--user-name)
            " "
            "<"
            (init-header2--user-email)
            ">"))
  ;;
  (defsubst init-header2--insert-separator-line
      (char)
    "Insert separator line."
    (insert header-prefix-string (substring header-prefix-string -1))
    (insert-char char
                 (- (or (bound-and-true-p fci-rule-column) fill-column)
                    (current-column)))
    (insert "\n"))
  ;;
  (init-defun init-header2--insert-preamble-start ()
    #$
    "Insert preamble start."
    (insert (concat comment-start
                    (or (and (= 1 (length comment-start)) header-prefix-string)
                        (substring header-prefix-string -1)))
            "Preamble {{{"
            "\n"))
  ;;
  (init-defun init-header2--insert-preamble-end ()
    #$
    "Insert preamble end."
    (insert header-prefix-string (substring header-prefix-string -1)
            "}}} Preamble"
            "\n"
            (or (nonempty-comment-end) (substring header-prefix-string 0 -1))
            "\n"))
  ;;
  (init-defun init-header2--insert-thick-separator-line ()
    #$
    "Insert thick separator line."
    (init-header2--insert-separator-line ?=))
  ;;
  (init-defun init-header2--insert-thin-separator-line ()
    #$
    "Insert thin separator line."
    (init-header2--insert-separator-line ?-))
  ;;
  (init-defun init-header2--insert-file ()
    #$
    "Insert @file."
    (insert header-prefix-string (substring header-prefix-string -1)
            "\
      @file "
            (if (buffer-file-name)
                (file-name-nondirectory (buffer-file-name))
              (buffer-name))
            "\n"))
  ;;
  (init-defun init-header2--insert-author ()
    #$
    "Insert @author."
    (insert header-prefix-string (substring header-prefix-string -1)
            "\
    @author Author <>"
            "\n"))
  ;;
  (init-defun init-header2--insert-maintainer ()
    #$
    "Insert @maintainer."
    (insert header-prefix-string (substring header-prefix-string -1)
            "\
@maintainer Maintainer <>"
            "\n"))
  ;;
  (init-defun init-header2--update-file ()
    #$
    "Update @file."
    (init-without-undo
      (beginning-of-line)
      (save-match-data
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@file$"))
          (end-of-line)
          (insert " "
                  (file-name-nondirectory (buffer-file-name))))
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@file +\\(.*\\)"))
          (goto-char (match-beginning 1))
          (delete-region (match-beginning 1) (match-end 1))
          (insert (file-name-nondirectory (buffer-file-name)))))))
  (register-file-header-action "^.* *@file\\(?:$\\| +.*\\)"
                               #'init-header2--update-file)
  ;;
  (init-defun init-header2--update-updated ()
    #$
    "Update @updated."
    (init-without-undo
      (beginning-of-line)
      (save-match-data
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@updated$"))
          (end-of-line)
          (insert " "
                  (format-time-string "%F %A %T (%z)")))
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@updated +\\(.*\\)"))
          (goto-char (match-beginning 1))
          (delete-region (match-beginning 1) (match-end 1))
          (insert (format-time-string "%F %A %T (%z)"))))))
  (register-file-header-action "^.* *@updated\\(?:$\\| +.*\\)"
                               #'init-header2--update-updated)
  ;;
  (init-defun init-header2--update-author ()
    #$
    "Update @author."
    (init-without-undo
      (beginning-of-line)
      (save-match-data
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@author$"))
          (end-of-line)
          (insert " "
                  (init-header2--user)))
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@author +\\(.*\\)"))
          (goto-char (match-beginning 1))
          (delete-region (match-beginning 1) (match-end 1))
          (insert (init-header2--user))))))
  (register-file-header-action "^.* *@author\\(?:$\\| +.*\\)"
                               #'init-header2--update-author)
  ;;
  (init-defun init-header2--update-maintainer ()
    #$
    "Update @maintainer."
    (init-without-undo
      (beginning-of-line)
      (save-match-data
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@maintainer$"))
          (end-of-line)
          (insert " "
                  (init-header2--user)))
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  " *@maintainer +\\(.*\\)"))
          (goto-char (match-beginning 1))
          (delete-region (match-beginning 1) (match-end 1))
          (insert (init-header2--user))))))
  (register-file-header-action "^.* *@maintainer\\(?:$\\| +.*\\)"
                               #'init-header2--update-maintainer)
  ;;
  (init-defun init-header2--update-copyright ()
    #$
    "Update @copyright."
    (init-without-undo
      (beginning-of-line)
      (save-match-data
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  "\\( *\\)@copyright$"))
          (end-of-line)
          (insert " "
                  "Copyright (C)"
                  " "
                  (format-time-string "%Y")
                  ",")
          (forward-line)
          (delete-region (line-beginning-position) (line-end-position))
          (insert (header-prefix-string)
                  (match-string 1)
                  "          "
                  " "
                  (init-header2--user)
                  "."))
        (when (looking-at (concat (regexp-quote (header-prefix-string))
                                  "\\( *\\)@copyright\\( +\\)\\(.*\\)"))
          (goto-char (match-beginning 3))
          (delete-region (match-beginning 3) (match-end 3))
          (insert "Copyright (C)"
                  " "
                  (format-time-string "%Y")
                  ",")
          (forward-line)
          (delete-region (line-beginning-position) (line-end-position))
          (insert (header-prefix-string)
                  (match-string 1)
                  "          "
                  (match-string 2)
                  (init-header2--user)
                  ".")))))
  (register-file-header-action "^.* *@copyright\\(?:$\\| +.*\\)"
                               #'init-header2--update-copyright))
;;
(init-package header2
  :commands
  (auto-make-header
   auto-update-file-header))
