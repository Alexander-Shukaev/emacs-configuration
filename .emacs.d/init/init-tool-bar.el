(eval-when-compile
  (require 'init-package))
;;
(init-handler tool-bar init
  (tool-bar-mode -1))
;;
(init-handler tool-bar config
  (init-assert (fboundp 'tool-bar-add-item-from-menu))
  (init-defad tool-bar-add-item-from-menu
      (:override (&rest ...) init/:override)
    #$)
  (init-assert (fboundp 'tool-bar-local-item-from-menu))
  (init-defad tool-bar-local-item-from-menu
      (:override (&rest ...) init/:override)
    #$))
;;
(init-package tool-bar
  :built-in
  :commands
  (tool-bar-mode))
