(eval-when-compile
  (require 'init-package))
;;
(init-handler url prepare
  (init-load url-methods :no-error))
;;
(init-handler url init)
;;
(init-handler url config)
;;
(init-package url
  :built-in)
