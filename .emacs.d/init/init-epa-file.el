(eval-when-compile
  (require 'init-package))
;;
(init-handler epa-file prepare
  (init-load epa      :no-error)
  (init-load epa-hook :no-error)
  ;;
  (defvar-local init-epa-file-error
    nil
    "File error signaled by `epa-file-insert-file-contents'.")
  (put 'init-epa-file-error 'permanent-local t)
  ;;
  (defun epa-file-buffer-armor-type
      (&optional buffer)
    "\
Return type of ASCII armored data in buffer BUFFER, if any.

Return nil if there is no ASCII armored data in buffer BUFFER."
    (with-current-buffer (or buffer (current-buffer))
      (save-restriction
        (widen)
        (save-excursion
          (goto-char (point-min))
          (narrow-to-region (point-min) (min (point-max) (+ (point-min) 160)))
          (save-match-data
            (when (looking-at "^-----BEGIN PGP \\(.+\\)-----$")
              (buffer-substring-no-properties (match-beginning 1)
                                              (match-end       1))))))))
  ;;
  (defun epa-file-buffer-armor-type-p
      (&optional buffer)
    "\
Return non-nil if there is ASCII armored data in buffer BUFFER."
    (stringp (epa-file-buffer-armor-type)))
  ;;
  (defun epa-file-buffer-armor-p
      (&optional buffer)
    "\
Return non-nil if buffer BUFFER is visiting ASCII armored file."
    (with-current-buffer (or buffer (current-buffer))
      (when buffer-file-name
        (save-match-data
          (and (string-match epa-file-name-regexp buffer-file-name)
               (equal (substring-no-properties buffer-file-name
                                               (match-beginning 1)
                                               (match-end       1))
                      "asc"))))))
  ;;
  (define-minor-mode epa-file-inhibit-mode
    "\
Toggle automatic file encryption/decryption in the current buffer.

With a prefix argument ARG, enable the mode if ARG is positive,
and disable it otherwise.  If called from Lisp, enable the mode
if ARG is omitted or nil.

See `epa-inhibit'."
    :group 'epa-file
    (require 'epa-file)
    (if (bound-and-true-p epa-file-inhibit-mode)
        (setq-local epa-inhibit t)
      (kill-local-variable 'epa-inhibit))))
;;
(init-handler epa-file init
  (setq-default epa-file-cache-passphrase-for-symmetric-encryption t))
;;
(init-handler epa-file config
  (put 'epa-inhibit 'permanent-local t)
  ;;
  (init-defun init-epa-file-setup ()
    #$
    (remove-hook 'find-file-hook #'init-epa-file-setup)
    (if init-epa-file-error
        (progn
          (add-hook 'after-change-major-mode-hook #'init-epa-file-error-setup
                    :append)
          (unless (epa-file-buffer-armor-type-p)
            (hexl-mode)))
      (when (epa-file-buffer-armor-p)
        (setq-local epa-armor t))))
  ;;
  (init-defun init-epa-file-error-setup ()
    #$
    (remove-hook 'after-change-major-mode-hook #'init-epa-file-error-setup)
    (when init-epa-file-error
      (setq-local buffer-read-only t)
      (setq-local epa-inhibit      t)
      (add-hook 'write-contents-functions
                #'init-epa-file-error-write-contents-function
                nil
                :local)))
  ;;
  (init-defun init-epa-file-error-write-contents-function ()
    #$
    (setq-local buffer-read-only t)
    (setq-local epa-inhibit      t)
    (barf-if-buffer-read-only))
  ;;
  (init-defad epa-file-insert-file-contents
      (:around (function &rest ...) init/:around)
    #$
    (add-hook 'find-file-hook #'init-epa-file-setup
              :append)
    (condition-case e
        (save-current-buffer
          (apply function ...))
      (file-missing
       (signal (car e) (cdr e)))
      (file-error
       (ding)
       (message "%s" (error-message-string e))
       (setq-local init-epa-file-error e)
       (add-hook 'after-change-major-mode-hook #'init-epa-file-error-setup
                 :append)
       (epa-file-run-real-handler #'insert-file-contents ...))))
  ;;
  (setenv "GPG_AGENT_INFO" nil))
;;
(init-package epa-file
  :built-in
  :variables
  (epa-inhibit))
