(eval-when-compile
  (require 'init-package))
;;
(init-handler info preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler info init)
;;
(init-handler info config
  (after-load evil
    (evil-set-initial-state 'Info-mode 'motion)
    ;;
    (evil-declare-motion #'Info-next-reference)
    (evil-declare-motion #'Info-prev-reference))
  ;;
  (after-load (init-devil devil)
    (init-defun init-info-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "index"         #'Info-index
        "virtual-index" #'Info-virtual-index
        ;;
        "ii[ndex]"      "index"
        "ivi[ndex]"     "virtual-index"))
    (add-hook 'Info-mode-hook #'init-info-devil-setup)))
;;
(init-handler info mode-map
  (let ((keymap Info-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (set-keymap-parent    keymap special-mode-map)
    (init-define-keys-kbd keymap
      ;; ---------------------------------------------------------------------
      ;; TODO:
      ;;
      ;; `Info-nth-menu-item', `Info-help', `Info-menu'
      ;; ---------------------------------------------------------------------
      ;;
      "<follow-link>"        'mouse-face
      ;;
      "<mouse-2>"           #'Info-mouse-follow-nearest-node
      ;;
      "<XF86Forward>"       #'Info-history-forward
      "<XF86Back>   "       #'Info-history-back
      ;;
      "<C-right>"           #'Info-history-forward
      "<C-left> "           #'Info-history-back
      ;;
      "<S-backspace>"       #'Info-history-forward
      "  <backspace>"       #'Info-history-back
      ;;
      "TAB"                 #'Info-next-reference
      "<backtab>"           #'Info-prev-reference
      ;;
      "RET"                 #'Info-follow-nearest-node
      ;;
      [remap describe-mode] #'Info-summary
      [remap quit-window]   #'Info-exit
      ;;
      "C-n"                 #'Info-next
      "C-p"                 #'Info-prev
      ;;
      "}"                   #'Info-forward-node
      "{"                   #'Info-backward-node
      ;;
      "^"                   #'Info-top-node
      "$"                   #'Info-final-node
      ;;
      "c"                   #'Info-copy-current-node-name
      ;;
      "d"                   #'Info-directory
      ;;
      "i"                   #'Info-index
      "I"                   #'Info-virtual-index
      ;;
      "s"                   #'Info-search
      "S"                   #'Info-search-case-sensitively
      ;;
      "t"                   #'Info-top-node
      "T"                   #'Info-toc
      ;;
      "u"                   #'Info-up
      ".."                  #'Info-up
      ;;
      ","                   #'Info-index-next))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd Info-mode-map
      :motion
      "TAB"       #'Info-next-reference
      "<backtab>" #'Info-prev-reference)
    ;;
    (devil-repeat-motions-kbd
      :keymap Info-mode-map
      "TAB"       "TAB" "<backtab>"
      "<backtab>" "TAB" "<backtab>")))
;;
(init-package info
  :built-in)
