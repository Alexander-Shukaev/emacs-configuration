(eval-when-compile
  (require 'init-package))
;;
(init-handler jupyter-org-client preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler jupyter-org-client prepare
  (init-load jupyter-mime :no-error)
  ;;
  (init-load ob :no-error))
;;
(init-handler jupyter-org-client init
  (setq-default jupyter-org-resource-directory
    (abbreviate-file-name (expand-directory "ob-jupyter/"
                                            user-data-directory))))
;;
(init-handler jupyter-org-client config
  (require 'jupyter-org-extensions nil :no-error)
  ;;
  (after-load jupyter-org-extensions
    (init-call jupyter-org-client interaction-mode-map))
  ;;
  (init-assert (memq #'jupyter-org-interaction-mode org-mode-hook))
  ;;
  ;; -------------------------------------------------------------------------
  (init-defad jupyter-org-interaction-mode
      (:before (&rest ...) init/:before)
    #$
    (require 'jupyter))
  ;;
  ;; CAUTION:
  ;;
  ;; The `jupyter' feature might never be loaded unless loaded explicitly.  As
  ;; a result, in order to avoid this misbehavior, it is a good idea to advise
  ;; the beginning of `jupyter-org-interaction-mode' to explicitly try to load
  ;; the `jupyter' feature.
  ;; -------------------------------------------------------------------------
  ;;
  (init-ignore-progn
    (init-defad jupyter-org-sync-results
        (:override (req) init/:override)
      #$
      (when-let* ((results (jupyter-org--coalesce-stream-results
                            (jupyter-org--process-pandoc-results
                             (nreverse (jupyter-org-request-results req)))))
                  (params (jupyter-org-request-block-params req))
                  (result-params (alist-get :result-params params)))
        (when (member "value" result-params)
          (let ((result (car (last results))))
            (setq results
              (list (cond ((eq 'fixed-width (org-element-type result))
                           (org-element-property :value result))
                          (t
                           result))))))
        (when (member "both" result-params)
          (let ((result (car (last results))))
            (setf (car (last results))
                  (cond ((eq 'fixed-width (org-element-type result))
                         (org-element-property :value result))
                        (t
                         result)))))
        (org-element-interpret-data
         (if (or (and (= (length results) 1)
                      (jupyter-org-babel-result-p (car results)))
                 (member "raw" result-params))
             (car results)
           (if (jupyter-org-request-silent-p req)
               ;; This happens when a named block is a variable in another
               ;; block.  It is different than a `:results' silent header.
               results
             (apply #'jupyter-org-results-drawer results))))))))
;;
(init-handler jupyter-org-client interaction-mode-map
  (let ((keymap jupyter-org-interaction-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap :recursively-p))
  ;;
  (jupyter-org-define-key (kbd "C-e C-e") #'jupyter-eval)
  (jupyter-org-define-key (kbd "C-e   e") #'jupyter-eval)
  ;;
  (jupyter-org-define-key [remap eval-buffer] #'jupyter-eval-buffer)
  (jupyter-org-define-key (kbd "C-e C-b")     #'jupyter-eval-buffer)
  (jupyter-org-define-key (kbd "C-e   b")     #'jupyter-eval-buffer)
  ;;
  (jupyter-org-define-key (kbd "C-e C-f") #'jupyter-eval-defun)
  (jupyter-org-define-key (kbd "C-e   f") #'jupyter-eval-defun)
  ;;
  (jupyter-org-define-key (kbd "C-e C-r") #'jupyter-eval-line-or-region)
  (jupyter-org-define-key (kbd "C-e   r") #'jupyter-eval-line-or-region)
  ;;
  (jupyter-org-define-key (kbd "C-e C-s") #'jupyter-eval-string-command)
  (jupyter-org-define-key (kbd "C-e   s") #'jupyter-eval-string-command)
  ;;
  (jupyter-org-define-key (kbd "C-/    ") #'jupyter-inspect-at-point)
  (jupyter-org-define-key (kbd "C-o    ") #'jupyter-inspect-at-point)
  ;;
  (jupyter-org-define-key (kbd "C-k C-i") #'jupyter-repl-interrupt-kernel)
  (jupyter-org-define-key (kbd "C-k   i") #'jupyter-repl-interrupt-kernel)
  ;;
  (jupyter-org-define-key (kbd "C-k C-r") #'jupyter-repl-restart-kernel)
  (jupyter-org-define-key (kbd "C-k   r") #'jupyter-repl-restart-kernel)
  ;;
  (after-load (init-devil devil init-jupyter-org-extensions)
    (devil-define-keys-kbd jupyter-org-interaction-mode-map
      :normal
      "SPC j" #'jupyter-org-hydra/body)))
;;
(init-package jupyter-org-client
  :unless
  (< emacs-major-version 26)
  :ensure jupyter)
