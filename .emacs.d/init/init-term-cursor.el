(eval-when-compile
  (require 'init-package))
;;
(init-handler term-cursor prepare
  (defun init-term-cursor-setup
      (&optional frame)
    (unless frame (setq frame (selected-frame)))
    (with-selected-frame frame
      (unless (or noninteractive (init-display-graphic-p))
        (cond ((equal (tty-type) "linux")
               (setf (terminal-parameter nil 'term-cursor-block-blinking)
                     "\e[?6c")
               (setf (terminal-parameter nil 'term-cursor-block-steady)
                     "\e[?112c")
               (setf (terminal-parameter nil 'term-cursor-underline-blinking)
                     "\e[?2c")
               (setf (terminal-parameter nil 'term-cursor-underline-steady)
                     "\e[?112c")
               (setf (terminal-parameter nil 'term-cursor-bar-blinking)
                     "\e[?6c")
               (setf (terminal-parameter nil 'term-cursor-bar-steady)
                     "\e[?112c"))
              ((stringp (tty-type))
               (setf (terminal-parameter nil 'term-cursor-block-blinking)
                     "\e[1 q")
               (setf (terminal-parameter nil 'term-cursor-block-steady)
                     "\e[2 q")
               (setf (terminal-parameter nil 'term-cursor-underline-blinking)
                     "\e[3 q")
               (setf (terminal-parameter nil 'term-cursor-underline-steady)
                     "\e[4 q")
               (setf (terminal-parameter nil 'term-cursor-bar-blinking)
                     "\e[5 q")
               (setf (terminal-parameter nil 'term-cursor-bar-steady)
                     "\e[6 q")))))))
;;
(init-handler term-cursor init
  (when (daemonp)
    (init-negate (init-display-graphic-p))
    (init-negate (tty-type)))
  (unless (or noninteractive (daemonp))
    (init-term-cursor-setup))
  ;;
  (after-init (global-term-cursor-mode)))
;;
(init-handler term-cursor config
  (init-defad term-cursor--determine-esc
      (:around (function &rest ...) init/:around)
    #$
    (let ((term-cursor-block-blinking     (terminal-parameter
                                           nil
                                           'term-cursor-block-blinking))
          (term-cursor-block-steady       (terminal-parameter
                                           nil
                                           'term-cursor-block-steady))
          (term-cursor-underline-blinking (terminal-parameter
                                           nil
                                           'term-cursor-underline-blinking))
          (term-cursor-underline-steady   (terminal-parameter
                                           nil
                                           'term-cursor-underline-steady))
          (term-cursor-bar-blinking       (terminal-parameter
                                           nil
                                           'term-cursor-bar-blinking))
          (term-cursor-bar-steady         (terminal-parameter
                                           nil
                                           'term-cursor-bar-steady)))
      (apply function ...)))
  ;;
  (init-defad term-cursor--eval
      (:around (function &rest ...) init/:around)
    #$
    (when (stringp (tty-type))
      (apply function ...)))
  ;;
  (after-load frame
    (dolist (hook '(after-make-frame-functions))
      (add-hook hook #'init-term-cursor-setup))))
;;
(init-package term-cursor
  :unless
  (version< emacs-version "26.1")
  :load-path "lisp/term-cursor")
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://linuxgazette.net/137/anonymous.html'
;;  ==========================================================================
;;  }}} References
