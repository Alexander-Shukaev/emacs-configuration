(eval-when-compile
  (require 'init-package))
;;
(init-handler mwheel init
  (setq-default
    mouse-wheel-progressive-speed t
    mouse-wheel-follow-mouse      t))
;;
(init-handler mwheel config)
;;
(init-package mwheel
  :built-in)
