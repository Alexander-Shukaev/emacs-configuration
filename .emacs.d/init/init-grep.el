(eval-when-compile
  (require 'init-package))
;;
(init-handler grep preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler grep init
  ;; If `init-completion-ignored-extensions' exists, then `require' it in hope
  ;; that it sets `completion-ignored-extensions' properly:
  (init-load completion-ignored-extensions :no-error))
;;
(init-handler grep config
  (dolist (directory '(".build"
                       ".dockervolumes"
                       ".eggs"
                       ".gitmodules.d"
                       ".install"
                       ".make"
                       ".mypy_cache"
                       ".pre-commit"
                       ".pytest_cache"
                       ".thunderbird/*/ImapMail"
                       ".venv"
                       "_berg"
                       "run/udev/watch"
                       "share/cmake/Helpers"
                       "share/git/hooks"
                       "share/make/scripts"
                       "var/lib/docker"
                       "var/run/udev/watch"
                       ;;
                       "/dev"
                       "/proc"
                       "/sys"))
    (add-to-list 'grep-find-ignored-directories directory))
  ;;
  (dolist (file '("history"
                  ".emacs-history"
                  "*.min.js"))
    (add-to-list 'grep-find-ignored-files file))
  ;;
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; Report that `grep' has to be extended in order to support the following:
  ;;
  ;; (dolist (file '("*/.emacs.d/history"
  ;;                 "*/.emacs-history"))
  ;;   (add-to-list 'grep-find-ignored-files file))
  ;;
  ;; (after-load savehist
  ;;   (add-to-list 'grep-find-ignored-files
  ;;                (expand-file-name savehist-file)))
  ;; -------------------------------------------------------------------------
  ;;
  (after-load "isearch"
    (init-assert (boundp 'grep-regexp-history))
    (set         'grep-regexp-history  regexp-search-ring)
    (defvaralias 'grep-regexp-history 'regexp-search-ring))
  ;;
  (after-load evil
    (evil-set-initial-state 'grep-mode 'motion))
  ;;
  (after-load (init-devil devil)
    (init-defun init-grep-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "k[ill]" #'kill-grep))
    (add-hook 'grep-mode-hook #'init-grep-devil-setup))
  ;;
  (after-load init-wgrep
    (add-hook 'grep-setup-hook #'wgrep-setup)))
;;
(init-handler grep mode-map
  (init-clear-keymap grep-mode-map))
;;
(init-handler grep mode-tool-bar-map
  (init-clear-keymap grep-mode-tool-bar-map))
;;
(init-package grep
  :built-in)
