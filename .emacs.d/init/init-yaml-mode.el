(eval-when-compile
  (require 'init-package))
;;
(init-handler yaml-mode init)
;;
(init-handler yaml-mode config)
;;
(init-package yaml-mode
  :mode
  (("\\.ya?ml\\'"        . yaml-mode)
   ("\\.clang-format\\'" . yaml-mode)
   ("\\.clang-tidy\\'"   . yaml-mode)))
