(eval-when-compile
  (require 'init-package))
;;
(init-handler "window" prepare
  (define-advice set-window-buffer
      (:around (function window &rest ...) init/:around)
    (let ((dedicated (window-dedicated-p window)))
      (prog1
          (apply function window ...)
        (when (and dedicated (window-parameter window 'window-side))
          (set-window-dedicated-p window dedicated)))))
  ;;
  (define-advice select-window
      (:after (window &optional norecord) init/:after)
    (unless norecord
      (when (memq (window-parameter window 'window-side) '(top bottom))
        (ignore-errors (maximize-window window)))
      (let ((old-selected-window (old-selected-window)))
        (when (and old-selected-window
                   (not (eq old-selected-window window))
                   (memq (window-parameter old-selected-window 'window-side)
                         '(top bottom)))
          (ignore-errors (minimize-window old-selected-window)))))))
;;
(init-handler "window" init
  (init-load ivy    :no-error)
  (init-load scroll :no-error)
  ;;
  (let ((parameters '(window-parameters . ((no-other-window         . t)
                                           (no-delete-other-windows . t)))))
    (setq-default
      display-buffer-alist `((,(concat "\\*"
                                       "\\(?:"
                                       "Buffer List"
                                       "\\|"
                                       "Buffer-Manager"
                                       "\\|"
                                       "Find"
                                       "\\|"
                                       "ag"
                                       "\\|"
                                       "grep"
                                       "\\|"
                                       "ivy-occur"
                                       "\\(?:[\s\t:].*\\)?"
                                       "\\|"
                                       "rg"
                                       "\\)"
                                       "\\*")
                              display-buffer-in-side-window
                              (side          . top)
                              (slot          . 0)
                              ,parameters)
                             (,(concat "\\*"
                                       "\\(?:"
                                       "Async Shell Command"
                                       "\\|"
                                       "Compile-Log"
                                       "\\|"
                                       "Messages"
                                       "\\|"
                                       "Shell Command Output"
                                       "\\|"
                                       "compilation"
                                       "\\|"
                                       "itail"
                                       "\\|"
                                       "shell"
                                       "\\|"
                                       "ycmd-content-log"
                                       "\\|"
                                       "ycmd-server"
                                       "\\)"
                                       "\\(?::.*\\)?"
                                       "\\*"
                                       "\\|"
                                       "magit-process:.*")
                              display-buffer-in-side-window
                              (side          . bottom)
                              (slot          . -1)
                              ,parameters)
                             (,(concat "\\*"
                                       "\\(?:"
                                       "Warnings"
                                       "\\)"
                                       "\\(?::.*\\)?"
                                       "\\*")
                              display-buffer-in-side-window
                              (side          . bottom)
                              (slot          . +1)
                              ,parameters))))
  ;;
  ;; -------------------------------------------------------------------------
  ;; NOTE:
  ;;
  ;; The following custom values of variables `display-buffer-alist' and
  ;; `display-buffer-base-action' have been assigned considering related
  ;; behavior of packages `compile.el', `man.el' (see also `init-man.el' for
  ;; custom values of variables `Man-notify-method' and `Man-notify'), and
  ;; `transient.el':
  ;;
  (setq-default
    display-buffer-alist       (nconc display-buffer-alist
                                      '(("" . ((display-buffer-reuse-window)
                                               . ((reusable-frames . t))))))
    display-buffer-base-action '((display-buffer-same-window
                                  display-buffer-in-previous-window
                                  display-buffer-use-some-window
                                  display-buffer-pop-up-window
                                  display-buffer-pop-up-frame)))
  ;; -------------------------------------------------------------------------
  ;;
  (setq-default
    display-buffer-mark-dedicated         nil
    ;;
    fit-window-to-buffer-horizontally     t
    ;;
    even-window-heights                   nil
    even-window-sizes                     'width-only
    pop-up-frames                         nil
    pop-up-windows                        nil
    ;;
    window-min-height                     (1+ (max ivy-height
                                                   (* 2 scroll-margin)))
    window-min-width                      (1+ fill-column)
    ;;
    window-resize-pixelwise               t
    ;;
    same-window-buffer-names              nil
    same-window-regexps                   nil
    ;;
    split-height-threshold                35
    split-width-threshold                 160
    ;;
    split-window-keep-point               t
    ;;
    split-window-below                    nil
    split-window-right                    t
    ;;
    switch-to-buffer-obey-display-actions t))
;;
(init-handler "window" config
  (when (bound-and-true-p display-buffer--same-window-action)
    (init-negate (equal display-buffer-base-action
                        display-buffer--same-window-action)))
  ;;
  (defcustom split-window-below
    nil
    "If non-nil, vertical splits produce new windows below."
    :group 'windows
    :type 'boolean)
  ;;
  (defcustom split-window-right
    nil
    "If non-nil, horizontal splits produce new windows to the right."
    :group 'windows
    :type 'boolean)
  ;;
  (init-assert (fboundp 'split-window-sensibly))
  (fmakunbound 'split-window-sensibly)
  (defun split-window-sensibly
      (&optional window)
    "\
Split WINDOW in a way suitable for `display-buffer'.

WINDOW defaults to the currently selected window.  If
`split-width-threshold' specifies an integer, WINDOW is at least
`split-width-threshold' columns wide and can be split horizontally,
split WINDOW into two windows side by side and return either the right
window if `split-window-right' is non-nil or the left window if
`split-window-right' is nil.  Otherwise, if `split-height-threshold'
specifies an integer, WINDOW is at least `split-height-threshold' lines
tall and can be split vertically, split WINDOW into two windows one
above the other and return either the lower window if
`split-window-below' is non-nil or the upper window if
`split-window-below' is nil.  If this can't be done either and WINDOW
is the only window on its frame, try to split WINDOW horizontally
disregarding any value specified by `split-width-threshold'.  If that
succeeds, return either the right window if `split-window-right' is
non-nil or the left window if `split-window-right' is nil.  Return nil
otherwise.

By default `display-buffer' routines call this function to split the
largest or least recently used window.  To change the default customize
the option `split-window-preferred-function'.

You can enforce this function to not split WINDOW horizontally, by
setting (or let-binding) the variable `split-width-threshold' to nil.
If, in addition, you set `split-height-threshold' to zero, chances
increase that this function does split WINDOW vertically.

In order to not split WINDOW vertically, set (or bind) the variable
`split-height-threshold' to nil.  Additionally, you can set
`split-width-threshold' to zero to make a horizontal split more likely
to occur.

Have a look at the function `window-splittable-p' if you want to know
how `split-window-sensibly' determines whether WINDOW can be split."
    (setq window (or window (selected-window)))
    (or (and (window-splittable-p window 'horizontal)
             ;; Split window horizontally.
             (split-window window nil (if split-window-right 'left  'right)))
        (and (window-splittable-p window)
             ;; Split window vertically.
             (split-window window nil (if split-window-below 'above 'below)))
        (and (eq window (frame-root-window (window-frame window)))
             (not (window-minibuffer-p window))
             ;; If WINDOW is the only window on its frame and is not the
             ;; minibuffer window, try to split it horizontally disregarding
             ;; the value of `split-width-threshold'.
             (let ((split-width-threshold 0))
               (when (window-splittable-p window t)
                 (split-window window nil (if split-window-right
                                              'left
                                            'right))))))))
;;
(init-package "window"
  :built-in
  :variables
  (split-window-below
   split-window-right))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(elisp) Frame Layouts with Side Windows'
;;  ==========================================================================
;;  }}} References
