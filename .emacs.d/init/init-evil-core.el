(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-core init)
;;
(init-handler evil-core config
  ;; -------------------------------------------------------------------------
  (init-defad evil-mode
      (:before (&rest ...) init/:before)
    #$
    (require 'evil))
  ;;
  ;; CAUTION:
  ;;
  ;; Unfortunately, `evil-mode' is annotated with the weird `autoload' cookie.
  ;; That is instead of loading the `evil-core' feature it performs
  ;; indirection and loads the `evil' feature.  On the one hand, this is nice
  ;; and we indeed rely on this fact (as a usual practice) in the `init-evil'
  ;; feature.  On the other hand, if the `evil-core' feature had already been
  ;; loaded (for whatever reason) *before* `evil-mode' has been called for the
  ;; first time, then when `evil-mode' is actually called, the corresponding
  ;; `autoload' is ultimately ignored and the `evil' feature is never loaded.
  ;; As a result, in order to avoid this corner case, it is a good idea to
  ;; advise the beginning of `evil-mode' to explicitly try to load the `evil'
  ;; feature.
  ;; -------------------------------------------------------------------------
  )
;;
(init-package evil-core
  :ensure evil)
