(eval-when-compile
  (require 'init-package))
;;
(init-handler lsp-java prepare
  (init-load lsp-mode :no-error))
;;
(init-handler lsp-java init
  (setq-default
    lsp-java-server-install-dir
    (locate-user-emacs-file "lsp/server/eclipse.jdt.ls/")
    ;;
    lsp-java-workspace-dir
    (expand-file-name (locate-user-emacs-file "lsp/workspace/"))))
;;
(init-handler lsp-java config
  (init-assert (file-name-absolute-p lsp-java-workspace-dir)))
;;
(init-package lsp-java)
