(eval-when-compile
  (require 'init-package))
;;
(init-handler lsp-mode init)
;;
(init-handler lsp-mode config
  (after-load (init-company-lsp company)
    (init-defun init-lsp-mode-company-setup ()
      #$
      (make-local-variable 'company-backends)
      (if (bound-and-true-p lsp-mode)
          (add-to-list 'company-backends #'company-lsp)
        (setq-local company-backends (delq #'company-lsp company-backends))))
    (add-hook 'lsp-mode-hook #'init-lsp-mode-company-setup)))
;;
(init-package lsp-mode)
