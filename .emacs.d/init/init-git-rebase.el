(eval-when-compile
  (require 'init-package))
;;
(init-handler git-rebase preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler git-rebase init)
;;
(init-handler git-rebase config
  (after-load init-undo-tree
    (init-defun init-git-rebase-mode-undo-tree-setup ()
      #$
      (setq-local undo-tree-auto-save-history nil))
    (add-hook 'git-rebase-mode-hook #'init-git-rebase-mode-undo-tree-setup
              :append))
  ;;
  (after-load (init-devil devil)
    (init-defun init-git-rebase-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "fu[p]"    #'git-rebase-fixup
        "k[ill]"   #'git-rebase-kill-line
        "mn[ext]"  #'git-rebase-move-line-down
        "mp[rev]"  #'git-rebase-move-line-up
        ;;
        "c[ommit]" #'with-editor-finish))
    (add-hook 'git-rebase-mode-hook #'init-git-rebase-devil-setup))
  ;;
  (after-load evil
    (evil-set-initial-state 'git-rebase-mode 'motion)
    ;;
    (evil-declare-motion #'git-rebase-backward-line)
    ;;
    (evil-set-type #'git-rebase-backward-line 'line)))
;;
(init-handler git-rebase mode-map
  (let ((keymap git-rebase-mode-map))
    (init-assert (equal (keymap-parent keymap) special-mode-map))
    (init-clear-keymap    keymap)
    (init-assert (equal (keymap-parent keymap) special-mode-map))
    (init-define-keys-kbd keymap
      [remap next-line    ] #'forward-line
      [remap previous-line] #'git-rebase-backward-line
      ;;
      [remap redo]          #'git-rebase-undo
      [remap undo]          #'git-rebase-undo
      [remap undo-only]     #'git-rebase-undo
      ;;
      "DEL"                 #'git-rebase-noop
      "RET"                 #'git-rebase-show-commit
      ;;
      "a"                   #'git-rebase-edit
      "A"                   #'git-rebase-edit
      ;;
      "d"                   #'git-rebase-kill-line
      "D"                   #'git-rebase-kill-line
      ;;
      "p"                   #'git-rebase-pick
      "P"                   #'git-rebase-pick
      ;;
      "r"                   #'git-rebase-reword
      "R"                   #'git-rebase-reword
      ;;
      "s"                   #'git-rebase-squash
      "S"                   #'git-rebase-squash
      ;;
      "x"                   #'git-rebase-exec
      "X"                   #'git-rebase-exec
      ;;
      "i"                   #'git-rebase-insert
      "I"                   #'git-rebase-insert))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd git-rebase-mode-map
      :motion
      [remap devil-line-forward ] #'forward-line
      [remap devil-line-backward] #'git-rebase-backward-line
      ;;
      "TAB"                       #'forward-line
      "<backtab>"                 #'git-rebase-backward-line)
    ;;
    (devil-repeat-motions-kbd
      :keymap git-rebase-mode-map
      "TAB"       "TAB" "<backtab>"
      "<backtab>" "TAB" "<backtab>")))
;;
(init-package git-rebase
  :ensure magit)
