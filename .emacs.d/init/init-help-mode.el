(eval-when-compile
  (require 'init-package))
;;
(init-handler help-mode init
  (setq-default
    help-back-label    "<backward>"
    help-forward-label "<forward>"))
;;
(init-handler help-mode config
  (after-load evil
    (evil-set-initial-state 'help-mode 'motion)))
;;
(init-handler help-mode map
  (let ((keymap help-mode-map))
    (init-assert (equal (keymap-parent keymap)
                        (make-composed-keymap button-buffer-map
                                              special-mode-map)))
    (init-clear-keymap    keymap)
    (init-assert (equal (keymap-parent keymap)
                        (make-composed-keymap button-buffer-map
                                              special-mode-map)))
    (init-define-keys-kbd keymap
      ;; ---------------------------------------------------------------------
      ;; TODO:
      ;;
      ;; I don't understand why the following is not necessary in `help-mode':
      ;;
      ;; "<follow-link>" 'mouse-face
      ;; ---------------------------------------------------------------------
      ;;
      "<mouse-2>"     #'help-follow-mouse
      ;;
      "<XF86Forward>" #'help-go-forward
      "<XF86Back>   " #'help-go-back
      ;;
      "<C-right>"     #'help-go-forward
      "<C-left> "     #'help-go-back
      ;;
      "C-n"           #'help-go-forward
      "C-p"           #'help-go-back
      ;;
      "<S-backspace>" #'help-go-forward
      "  <backspace>" #'help-go-back
      ;;
      "RET"           #'help-follow
      ;;
      "?"             #'help-follow-symbol)))
;;
(init-package help-mode
  :built-in)
