(eval-when-compile
  (require 'init-package))
;;
(init-handler eldoc init)
;;
(init-handler eldoc config)
;;
(init-package eldoc
  :built-in
  :diminish
  (eldoc-mode
   global-eldoc-mode))
