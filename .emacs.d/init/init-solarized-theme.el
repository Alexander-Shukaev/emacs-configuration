(eval-when-compile
  (require 'init-package))
;;
(init-handler solarized-theme preface
  (eval-when-compile
    (require 'solarized-palettes)))
;;
(init-handler solarized-theme prepare
  (init-load ansi-color)
  (init-load theme)
  ;;
  (defvar init-solarized-black
    init-ansi-color-black
    "Solarized color of black.")
  ;;
  (defvar init-solarized-red
    init-ansi-color-red
    "Solarized color of red.")
  ;;
  (defvar init-solarized-green
    init-ansi-color-green
    "Solarized color of green.")
  ;;
  (defvar init-solarized-yellow
    init-ansi-color-yellow
    "Solarized color of yellow.")
  ;;
  (defvar init-solarized-blue
    init-ansi-color-blue
    "Solarized color of blue.")
  ;;
  (defvar init-solarized-magenta
    init-ansi-color-magenta
    "Solarized color of magenta.")
  ;;
  (defvar init-solarized-cyan
    init-ansi-color-cyan
    "Solarized color of cyan.")
  ;;
  (defvar init-solarized-white
    init-ansi-color-white
    "Solarized color of white.")
  ;;
  (define-advice enable-theme
      (:after (theme &rest ...) init-solarized-theme/:after +100)
    (when (string-prefix-p "solarized-" (symbol-name theme))
      (let* ((var 'ansi-color-names-vector)
             (vec (car-safe (custom-variable-theme-value var))))
        (when vec
          (message "`%s': %s" 'ansi-color-names-vector vec)
          (setq-default
            init-solarized-black   (aref vec 0)
            init-solarized-red     (aref vec 1)
            init-solarized-green   (aref vec 2)
            init-solarized-yellow  (aref vec 3)
            init-solarized-blue    (aref vec 4)
            init-solarized-magenta (aref vec 5)
            init-solarized-cyan    (aref vec 6)
            init-solarized-white   (aref vec 7))))
      ;; TODO: These would better belong to `init-mode-line.el':
      (set-face-attribute 'mode-line nil
                          :overline  'unspecified
                          :underline 'unspecified)
      (set-face-attribute 'mode-line-inactive nil
                          :overline  'unspecified
                          :underline 'unspecified))))
;;
(init-handler solarized-theme init
  (setq-default
    solarized-distinct-doc-face          t
    solarized-distinct-fringe-background nil
    solarized-high-contrast-mode-line    nil
    solarized-scale-org-headlines        nil
    solarized-scale-outline-headlines    nil
    solarized-use-less-bold              t
    solarized-use-more-italic            t
    solarized-use-variable-pitch         nil)
  ;;
  (load-theme (if (bound-and-true-p init-bb)
                  'solarized-dark
                'solarized-dark)
              :no-confirm)
  ;;
  (after-load solarized
    (require 'solarized-palettes)
    (require 'solarized-theme)))
;;
(init-handler solarized-theme config
  (init-defun init-solarized-with-color-variables-form
      (theme &optional body)
    #$
    (let* ((string  (init-package-as-string theme))
           (variant (cond ((or (and (string-prefix-p "solarized-" string)
                                    (string-suffix-p "-dark"      string))
                               (string-prefix-p "solarized-dark"  string))
                           'dark)
                          ((or (and (string-prefix-p "solarized-" string)
                                    (string-suffix-p "-light"     string))
                               (string-prefix-p "solarized-light" string))
                           'light)))
           (symbol)
           (alist))
      (when variant
        (setq
          symbol (intern (format "%s-color-palette-alist" string))
          alist  (symbol-value symbol))
        (if alist
            `(let* ((theme   ',(init-package-as-symbol theme))
                    (variant ',variant)
                    ,@(mapcar (lambda (c) `(,(car c) ,(cdr c))) alist)
                    (s-base03 base03)
                    (s-base02 base02)
                    (s-base01 base01)
                    (s-base00 base00)
                    (s-base3  base3)
                    (s-base2  base2)
                    (s-base1  base1)
                    (s-base0  base0)
                    (base03 ,(if (eq variant 'light) 's-base3  's-base03))
                    (base02 ,(if (eq variant 'light) 's-base2  's-base02))
                    (base01 ,(if (eq variant 'light) 's-base1  's-base01))
                    (base00 ,(if (eq variant 'light) 's-base0  's-base00))
                    (base0  ,(if (eq variant 'light) 's-base00 's-base0))
                    (base1  ,(if (eq variant 'light) 's-base01 's-base1))
                    (base2  ,(if (eq variant 'light) 's-base02 's-base2))
                    (base3  ,(if (eq variant 'light) 's-base03 's-base3)))
               (dolist (symbol '(base03
                                 base02
                                 base01
                                 base00
                                 base0
                                 base1
                                 base2
                                 base3))
                 (let ((value (symbol-value symbol)))
                   (message "- `%s': %s"
                            symbol
                            (propertize value 'face `(:foreground ,value)))))
               ,@body)
          (lwarn 'init
                 :warning
                 "Expected the non-nil value of variable `%s'\n%s"
                 symbol
                 (init-backtrace-to-string))))))
  ;;
  (defmacro init-solarized-with-color-variables
      (theme &rest body)
    (declare (debug  t)
             (indent defun))
    (when (and (listp theme)
               (eq (car-safe theme) 'quote))
      (setq theme (cadr theme)))
    `(progn
       (message "`init-solarized-with-color-variables' [%s]"
                (init-current-function))
       (if (or (and (symbolp ',theme)
                    (boundp  ',theme))
               (stringp ,theme))
           (if ,theme
               (eval (init-solarized-with-color-variables-form ,theme ',body))
             (lwarn 'init
                    :warning
                    "Expected the non-nil value of variable `%s'\n%s"
                    ',theme
                    (init-backtrace-to-string)))
         ,(init-solarized-with-color-variables-form theme body)))))
;;
(init-package solarized-theme)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/bbatsov/solarized-emacs/blob/master/UPGRADE-GUIDE.md'
;;  ==========================================================================
;;  }}} References
