(eval-when-compile
  (require 'init-package))
;;
(init-handler magit-annex init)
;;
(init-handler magit-annex config)
;;
(init-package magit-annex)
