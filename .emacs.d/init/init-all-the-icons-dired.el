(eval-when-compile
  (require 'init-package))
;;
(init-handler all-the-icons-dired init)
;;
(init-handler all-the-icons-dired config)
;;
(init-package all-the-icons-dired
  :disabled)
