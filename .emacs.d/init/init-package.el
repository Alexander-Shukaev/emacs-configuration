(defgroup init-package
  nil
  "Customization group for Init Package (IP)."
  :tag "Init Package (IP)"
  :group 'startup
  :group 'init
  :prefix 'init-)
;; ---------------------------------------------------------------------------
(require 'init-functions)
;; ---------------------------------------------------------------------------
(eval-and-compile
  (defconst init-package-archive-location-gnu
    "http://elpa.gnu.org/packages/"
    "Package archive location for GNU.")
  ;;
  (defconst init-package-archive-location-marmalade-repo
    "http://marmalade-repo.org/packages/"
    "Package archive location for Marmalade Repo.")
  ;;
  (defconst init-package-archive-location-melpa
    "http://melpa.org/packages/"
    "Package archive location for MELPA.")
  ;;
  (defconst init-package-archive-location-melpa-stable
    "http://stable.melpa.org/packages/"
    "Package archive location for Stable MELPA.")
  ;;
  (defconst init-package-archive-location-emacswiki
    "http://mirrors.tuna.tsinghua.edu.cn/elpa/emacswiki/"
    "Package archive location for EmacsWiki.")
  ;;
  (setq-default
    package-user-dir
    (abbreviate-file-name (init-user-expand-directory "elpa/"))
    ;; -----------------------------------------------------------------------
    package-archives
    `(("gnu"            . ,init-package-archive-location-gnu)
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ("marmalade-repo" . ,init-package-archive-location-marmalade-repo)
      ;; ---------------------------------------------------------------------
      ("melpa"          . ,init-package-archive-location-melpa)
      ("melpa.stable"   . ,init-package-archive-location-melpa-stable)
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ("emacswiki"      . ,init-package-archive-location-emacswiki)
      ;; ---------------------------------------------------------------------
      )
    package-archives
    (delq (assoc "marmalade-repo" package-archives)
          package-archives)
    package-archives
    (delq (assoc "emacswiki"      package-archives)
          package-archives)
    ;; -----------------------------------------------------------------------
    package-archive-exclude-alist
    '(("gnu")
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ("marmalade-repo")
      ;; ---------------------------------------------------------------------
      ("melpa")
      ("melpa.stable")
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ("emacswiki")
      ;; ---------------------------------------------------------------------
      )
    package-archive-exclude-alist
    (delq (assoc "marmalade-repo" package-archive-exclude-alist)
          package-archive-exclude-alist)
    package-archive-exclude-alist
    (delq (assoc "emacswiki"      package-archive-exclude-alist)
          package-archive-exclude-alist)
    ;; -----------------------------------------------------------------------
    package-archive-priorities
    '(("gnu"            . 1)
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ("marmalade-repo" . 1)
      ;; ---------------------------------------------------------------------
      ("melpa"          . 2)
      ("melpa.stable"   . 1)
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ("emacswiki"      . 2)
      ;; ---------------------------------------------------------------------
      )
    package-archive-priorities
    (delq (assoc "marmalade-repo" package-archive-priorities)
          package-archive-priorities)
    package-archive-priorities
    (delq (assoc "emacswiki"      package-archive-priorities)
          package-archive-priorities)
    ;; -----------------------------------------------------------------------
    package-enable-at-startup
    nil
    ;; -----------------------------------------------------------------------
    package-menu-hide-low-priority
    t
    ;; -----------------------------------------------------------------------
    ;; TODO:
    ;;
    ;; A combination of the `:ensure' and `:pin' keywords in `use-package' is
    ;; unfortunately broken at the moment:
    ;;
    package-pinned-packages
    nil
    ;; -----------------------------------------------------------------------
    package-native-compile
    nil))
;; ---------------------------------------------------------------------------
(eval-and-compile
  (with-eval-after-load 'package
    (when (bound-and-true-p init-bb)
      (setq-default package-check-signature nil))
    (fset 'package--save-selected-packages #'ignore))
  (package-initialize)
  (init-assert (featurep 'package)))
;; ---------------------------------------------------------------------------
(eval-and-compile
  (setq-default
    use-package-always-defer      t
    use-package-always-demand     nil
    use-package-always-ensure     t
    use-package-check-before-init t
    use-package-expand-minimally  nil
    ;; -----------------------------------------------------------------------
    ;; DEPRECATED:
    ;;
    use-package-verbose           'debug
    ;; -----------------------------------------------------------------------
    use-package-verbose           'errors))
;; ---------------------------------------------------------------------------
(require 'init-package-functions)
;; ---------------------------------------------------------------------------
(eval-and-compile
  (init-package--install-maybe '(delight
                                 diminish
                                 ;; TODO: `req-package'?
                                 use-package)))
;; ---------------------------------------------------------------------------
(require 'init-package-variables)
(require 'init-package-commands)
(require 'init-package-macros)
;; ---------------------------------------------------------------------------
(eval-and-compile
  (init-assert (featurep 'use-package)))
;; ---------------------------------------------------------------------------
(define-advice byte-compile-file
    (:around (function filename &rest ...) init-package/:around -100)
  (if (memq this-command init-package-byte-compile-commands)
      (condition-case-unless-debug e
          (apply function filename ...)
        (error (lwarn 'init-package
                      :error
                      "Failed to compile file (%s): %s\n%s"
                      filename
                      (error-message-string e)
                      (init-backtrace-to-string))
               nil))
    (apply function filename ...)))
;; ---------------------------------------------------------------------------
(require 'delight)
(require 'diminish)
;; ---------------------------------------------------------------------------
(eval-and-compile
  (with-eval-after-load 'init-package
    (let ((byte-compile-current-file)
          (comp-native-compiling))
      (dolist (feature '(init-auto-compile
                         init-after-init
                         init-after-load))
        (require feature)))))
;; ---------------------------------------------------------------------------
(provide 'init-package)
