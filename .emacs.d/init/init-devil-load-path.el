(defcustom init-devil-load-path
  '("lisp/devil")
  "List of directories to search for `devil' files to load."
  :group 'init
  :type '(repeat string))
(put 'init-devil-load-path 'safe-local-variable #'listp)
;;
(provide 'init-devil-load-path)
