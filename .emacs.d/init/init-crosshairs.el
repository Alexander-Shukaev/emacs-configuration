(eval-when-compile
  (require 'init-package))
;;
(init-handler crosshairs prepare
  (init-load col-highlight)
  (init-load hl-line))
;;
(init-handler crosshairs init
  (after-init (crosshairs-mode)))
;;
(init-handler crosshairs config)
;;
(init-package crosshairs
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Sometimes slow and sluggish.
  ;; -------------------------------------------------------------------------
  :unless
  (< emacs-major-version 25))
