(eval-when-compile
  (require 'init-package))
;;
(init-handler find-dired prepare
  (init-negate (fboundp 'dired-find))
  (defun dired-find
      (args)
    "Run `find-dired' with `default-directory' as the first argument."
    (interactive "sRun find (with args): ")
    (find-dired default-directory args))
  ;;
  (init-negate (fboundp 'dired-find-name))
  (defun dired-find-name
      (pattern)
    "Run `find-name-dired' with `default-directory' as the first argument."
    (interactive "sFind-name (filename wildcard): ")
    (find-name-dired default-directory pattern))
  ;;
  (init-negate (fboundp 'dired-find-grep))
  (defun dired-find-grep
      (regexp &optional options)
    "Run `find-grep-dired' with `default-directory' as the first argument."
    (interactive
     (nreverse (list (when current-prefix-arg
                       (read-string "Find-grep (grep options): "))
                     (read-string "Find-grep (grep regexp): "))))
    (require 'find-dired)
    (let ((find-grep-options (if options
                                 (concat find-grep-options " " options)
                               find-grep-options)))
      (find-grep-dired default-directory regexp))))
;;
(init-handler find-dired init)
;;
(init-handler find-dired config)
;;
(init-package find-dired
  :built-in)
