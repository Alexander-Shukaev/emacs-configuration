(eval-when-compile
  (require 'init-package))
;;
(init-handler inhibit-set-auto-mode prepare
  (init-load files :no-error))
;;
(init-handler inhibit-set-auto-mode init
  (after-init (inhibit-set-auto-mode)))
;;
(init-handler inhibit-set-auto-mode config
  (init-defun init-inhibit-set-auto-mode-large-file-function ()
    #$
    (and (integerp large-file-warning-threshold)
         (> (buffer-size) large-file-warning-threshold)))
  (add-hook 'inhibit-set-auto-mode-functions
            #'init-inhibit-set-auto-mode-large-file-function)
  ;;
  (after-load (init-epa-file epa-file)
    (init-defun init-inhibit-set-auto-mode-epa-file-function ()
      #$
      (and init-epa-file-error (epa-file-buffer-armor-type-p)))
    (add-hook 'inhibit-set-auto-mode-functions
              #'init-inhibit-set-auto-mode-epa-file-function)))
;;
(init-package inhibit-set-auto-mode
  :load-path "lisp/inhibit-set-auto-mode")
