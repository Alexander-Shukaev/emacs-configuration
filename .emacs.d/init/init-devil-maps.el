(eval-when-compile
  (require 'init-package))
;;
(eval-and-compile
  (init-load devil-load-path))
;;
(init-handler devil-maps preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler devil-maps init)
;;
(init-handler devil-maps config
  (after-load (init-devil-ex devil-ex evil-maps)
    (after-load init-find-dired
      (devil-ex-define-cmds
        "df[ind]"   #'dired-find
        "dfn[ame]"  #'dired-find-name
        "dfg[rep]"  #'dired-find-grep
        ;;
        "f[ind]"    #'dired-find
        ;;
        "find-name" #'dired-find-name
        "find-grep" #'dired-find-grep
        ;;
        "fn[ame]"   "find-name"
        "fg[rep]"   "find-grep"))
    ;;
    (after-load init-expand-region
      (devil-define-keys-kbd evil-motion-state-map
        "e" #'er/expand-region)
      ;;
      (after-load expand-region
        (devil-define-keys-kbd evil-motion-state-map
          "E" #'er/contract-region)))
    ;;
    (after-load init-buffer-manager
      (devil-define-keys-kbd evil-motion-state-map
        "b C-m" #'bm-switch-to-bm-buffer-other-frame
        "b M"   #'bm-switch-to-bm-buffer-other-window
        "b m"   #'bm-switch-to-bm-buffer))
    ;;
    (after-load init-counsel
      (devil-ex-define-cmds
        ":ag"           #'counsel-ag
        ":rg"           #'counsel-rg
        ":a[propos]"    #'counsel-apropos
        ":b[ookmark]"   #'counsel-bookmark
        ":c[ompany]"    #'counsel-company
        ;; TODO: Delete them
        ;; TODO: Deprecate them:
        ;; ":d[ired]"      #'counsel-dired-jump
        ;; ":f[ile]"       #'counsel-file-jump
        ":fd[ired]"     #'counsel-dired
        ":fz[f]"        #'counsel-fzf
        ":fs[ymbol]"    #'counsel-find-symbol
        ":ff[ile]"      #'counsel-find-file
        ":fl[ibrary]"   #'counsel-find-library
        ":ll[ibrary]"   #'counsel-load-library
        ":lt[heme]"     #'counsel-load-theme
        ":is[ymbol]"    #'counsel-info-lookup-symbol
        ":lp[rocesses]" #'counsel-list-processes
        ":im[enu]"      #'counsel-imenu
        ":gf[ile]"      #'counsel-git
        ":gcw[orktree]" #'counsel-git-change-worktree
        ":gg[rep]"      #'counsel-git-grep
        ":gl[og]"       #'counsel-git-log
        ":gs[tash]"     #'counsel-git-stash
        ":g[rep]"       #'counsel-grep
        ":s[wiper]"     #'counsel-grep-or-swiper
        ":r[ecentf]"    #'counsel-recentf
        ":rf[ile]"      #'counsel-recentf
        ":rd[ired]"     #'counsel-recentd
        ":ch[istory]"   #'counsel-command-history
        ":mh[istory]"   #'counsel-minibuffer-history
        ":shb[uffer]"   #'counsel-switch-to-shell-buffer
        ":la[pp]"       #'counsel-linux-app
        ":dpkg"         #'counsel-dpkg
        ":rpm"          #'counsel-rpm
        ;;
        "bs[hell]"      #'counsel-switch-to-shell-buffer
        ;;
        "r[ecentf]"     #'counsel-recentf
        "rf[ile]"       #'counsel-recentf
        "rd[ired]"      #'counsel-recentd)
      ;;
      (devil-define-keys-kbd evil-motion-state-map
        [remap recentf-open-files]       #'counsel-recentf
        [remap recentf-open-directories] #'counsel-recentd
        ;;
        ;; TODO: `devil-leader':
        "SPC s"     #'counsel-grep-or-swiper
        ;;
        "SPC g"     nil
        ;;
        "SPC g c"   nil
        ;;
        "SPC g c w" #'counsel-git-change-worktree
        ;;
        "SPC g f"   #'counsel-git
        ;;
        "SPC g g"   #'counsel-git-grep
        ;;
        "SPC g l"   #'counsel-git-log
        ;;
        "SPC g s"   #'counsel-git-stash
        ;;
        "b s"       #'counsel-switch-to-shell-buffer))
    ;;
    (after-load init-counsel-filter
      (devil-ex-define-cmds
        ":d[irectory]" #'counsel-filter-directory
        ":f[ile]"      #'counsel-filter-file))
    ;;
    (after-load init-counsel-pydoc
      (devil-ex-define-cmds
        ":pyd[oc]" #'counsel-pydoc
        ":pd[oc]"  #'counsel-pydoc))
    ;;
    (after-load init-easy-kill
      (devil-define-keys-kbd evil-motion-state-map
        "C-y" #'easy-kill))
    ;;
    (after-load init-ivy
      (devil-ex-define-cmds
        ":" #'ivy-resume))
    ;;
    (after-load init-info
      (devil-ex-define-cmds
        ;; -------------------------------------------------------------------
        ;; TODO: `ivy' integration and prefix argument:
        ;;
        "i[nfo]"     #'info
        ;; -------------------------------------------------------------------
        "ia[propos]" #'info-apropos))
    ;;
    (after-load init-recentf
      (unless (featurep 'init-counsel)
        (devil-ex-define-cmds
          "r[ecentf]" #'recentf-open-files
          "rf[ile]"   #'recentf-open-files)))
    ;;
    (after-load init-magit
      (devil-ex-define-cmds
        "ma[mend]"       #'magit-amend
        "mc[ommit]"      #'magit-commit-create
        "mch[eckout]"    #'magit-checkout
        "mco[ut]"        #'magit-checkout
        "md[iff]"        #'magit-diff
        "me[diff]"       #'magit-ediff
        "mff[ile]"       #'magit-find-file
        "ml[og]"         #'magit-log
        "mla[ll]"        #'magit-log-all
        "mlab[ranches]"  #'magit-log-all-branches
        "mlb[ranches]"   #'magit-log-branches
        "mlf[ile]"       #'magit-log-buffer-file
        "mlh[ead]"       #'magit-log-head
        "mlo[ther]"      #'magit-log-other
        "mp[ush]"        #'magit-push
        "mpc[urrent]"    #'magit-push-current
        "mpo[other]"     #'magit-push-other
        "mpul[l]"        #'magit-pull
        "mpb[ranch]"     #'magit-pull-branch
        "mpp[ushremote]" #'magit-pull-from-pushremote
        "mpu[pstream]"   #'magit-pull-from-upstream
        "mpr[ebase]"     #'magit-pull.rebase
        "mr[ebase]"      #'magit-rebase
        "ms[tatus]"      #'magit-status))
    ;;
    (after-load init-pydoc
      (devil-ex-define-cmds
        "pyd[oc]" #'pydoc
        "pd[oc]"  #'pydoc))
    ;;
    (after-load init-swiper
      (devil-define-keys-kbd evil-motion-state-map
        ;; TODO: `devil-leader':
        "SPC S" #'swiper))))
;;
(init-package devil-maps
  :load-path init-devil-load-path
  :ensure evil
  :ensure memoize)
