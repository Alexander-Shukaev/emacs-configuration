(eval-when-compile
  (require 'init-package))
;;
(init-handler man init
  (setq-default
    Man-name-regexp   (let ((regexp "[!-'*-~]+"))
                        (concat regexp "\\(?:(" regexp ")\\)?"))
    Man-notify        'pushy
    Man-notify-method 'pushy))
;;
(init-handler man config
  (init-defad Man-getpage-in-background
      (:around (function topic &rest ...) init/:around)
    #$
    (setq topic (mapconcat #'shell-quote-argument (split-string topic) " "))
    (apply function topic ...)))
;;
(init-package man
  :built-in)
