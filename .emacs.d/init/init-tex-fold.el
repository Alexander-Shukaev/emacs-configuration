(eval-when-compile
  (require 'init-package))
;;
(init-handler tex-fold preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler tex-fold init
  (setq-default
    TeX-fold-command-prefix    nil
    TeX-fold-force-fontify     nil
    TeX-fold-keymap            nil
    TeX-fold-preserve-comments t
    TeX-fold-type-list         '(env math)))
;;
(init-handler tex-fold config)
;;
(init-handler tex-fold mode-map
  (let ((keymap TeX-fold-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap :recursively-p))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd TeX-fold-mode-map
      :motion
      "#" #'TeX-fold-dwim)))
;;
(init-package tex-fold
  :ensure auctex)
