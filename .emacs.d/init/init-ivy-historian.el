(eval-when-compile
  (require 'init-package))
;;
(init-handler ivy-historian init)
;;
(init-handler ivy-historian config)
;;
(init-package ivy-historian)
