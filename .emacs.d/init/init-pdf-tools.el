(eval-when-compile
  (require 'init-package))
;;
(init-handler pdf-tools prepare
  (define-advice pdf-tools-install
      (:around (function &rest ...) init/:around)
    (let ((compilation-buffer-name-function #'(lambda
                                                  (name)
                                                (setq name (downcase name))
                                                (init-assert
                                                  (equal name "compilation"))
                                                (concat "*"
                                                        name
                                                        ": pdf-tools"
                                                        "*"))))
      (apply function ...)))
  ;;
  (defun init-pdf-tools-setup ()
    (interactive)
    (pdf-loader-install :no-query-p :skip-dependencies-p :no-error-p)))
;;
(init-handler pdf-tools init
  (after-init (init-pdf-tools-setup)))
;;
(init-handler pdf-tools config)
;;
(init-package pdf-tools
  :unless
  (bound-and-true-p init-bb))
