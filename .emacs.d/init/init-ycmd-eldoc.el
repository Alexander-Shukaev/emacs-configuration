(eval-when-compile
  (require 'init-package))
;;
(init-handler ycmd-eldoc init
  (setq-default ycmd-eldoc-always-semantic-server-query-modes nil))
;;
(init-handler ycmd-eldoc config
  (init-defad ycmd-eldoc--documentation-function
      (:around (function &rest ...) init/:around)
    #$
    (when (and ycmd-mode ycmd-eldoc-mode)
      (apply function ...)))
  ;;
  (init-defad ycmd-eldoc--check-if-semantic-completer-exists-for-mode
      (:around (function &rest ...) init/:around)
    #$
    (when (and ycmd-mode ycmd-eldoc-mode)
      (apply function ...)))
  ;;
  (init-defad ycmd-eldoc--info-at-point
      (:around (function &rest ...) init/:around)
    #$
    (when (and ycmd-mode ycmd-eldoc-mode)
      (apply function ...)))
  ;;
  (init-defad ycmd-eldoc--get-type
      (:around (function &rest ...) init/:around)
    #$
    (when (and ycmd-mode ycmd-eldoc-mode)
      (apply function ...)))
  ;;
  (init-defad ycmd-eldoc--get-type-command-deferred
      (:around (function &rest ...) init/:around)
    #$
    (when (and ycmd-mode ycmd-eldoc-mode)
      (apply function ...))))
;;
(init-package ycmd-eldoc
  :ensure ycmd)
