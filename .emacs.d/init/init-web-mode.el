(eval-when-compile
  (require 'init-package))
;;
(init-handler web-mode init)
;;
(init-handler web-mode config)
;;
(init-package web-mode
  :mode
  (("\\.html?\\'"       . web-mode)
   ("\\.html\\.twig\\'" . web-mode)
   ("\\.phtml\\'"       . web-mode)
   ("\\.tpl\\.php\\'"   . web-mode)))
