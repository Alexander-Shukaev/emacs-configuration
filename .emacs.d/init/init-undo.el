(setq-default
  undo-limit        (* 128 1024 1024)
  undo-strong-limit (* 2 undo-limit)
  undo-outer-limit  (* 2 undo-strong-limit))
;;
(provide 'init-undo)
