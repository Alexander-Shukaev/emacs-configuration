(eval-when-compile
  (require 'init-package))
;;
(init-handler epg init
  (setq-default epg-pinentry-mode 'loopback))
;;
(init-handler epg config)
;;
(init-package epg
  :built-in)
