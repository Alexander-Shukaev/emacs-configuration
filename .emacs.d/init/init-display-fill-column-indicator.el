(eval-when-compile
  (require 'init-package))
;;
(init-handler display-fill-column-indicator init)
;;
(init-handler display-fill-column-indicator config)
;;
(init-package display-fill-column-indicator
  :built-in
  :unless
  (version< emacs-version "27.1"))
