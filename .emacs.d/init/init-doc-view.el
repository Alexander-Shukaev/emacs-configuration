(eval-when-compile
  (require 'init-package))
;;
(init-handler doc-view init
  (when (and (memq system-type '(cygwin windows-nt))
             (executable-find "gswin32c"))
    (setq-default doc-view-ghostscript-program "gswin32c")))
;;
(init-handler doc-view config)
;;
(init-package doc-view)
