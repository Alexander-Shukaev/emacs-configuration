(eval-when-compile
  (require 'init-package))
;;
(init-handler magit-filenotify init)
;;
(init-handler magit-filenotify config
  (add-to-list 'magit-filenotify-ignored "\\`index\\.lock\\'")
  ;;
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; As per [1], using the following workaround:
  ;;
  (init-defad magit-filenotify--callback
      (:around (function ev &rest ...) init/:around)
    #$
    (let* ((descriptor (car  ev))
           (action     (cadr ev))
           (buffer     (cadr (gethash descriptor magit-filenotify-data))))
      (if (eq action 'stopped)
          (progn
            (when (bufferp buffer)
              (remhash buffer magit-filenotify--last-event-times))
            (remhash descriptor magit-filenotify-data))
        (save-match-data
          (apply function ev ...)))))
  ;; -------------------------------------------------------------------------
  )
;;
(init-package magit-filenotify
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Slow and sluggish.
  ;; -------------------------------------------------------------------------
  :unless
  (featurep 'w32notify)
  :diminish
  (magit-filenotify-mode))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/ruediger/magit-filenotify/issues/22'
;;  ==========================================================================
;;  }}} References
