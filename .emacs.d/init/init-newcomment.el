(eval-when-compile
  (require 'init-package))
;;
(init-handler newcomment init
  (setq-default comment-auto-fill-only-comments nil))
;;
(init-handler newcomment config)
;;
(init-package newcomment
  :built-in)
