(eval-when-compile
  (require 'init-package))
;;
(init-handler electric-operator init
  (setq-default
    c-pointer-type-style 'type
    double-space-docs    t
    enable-in-docs       t))
;;
(init-handler electric-operator config)
;;
(init-package electric-operator
  :diminish)
