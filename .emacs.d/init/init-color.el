(require 'color)
;;
(eval-when-compile
  (require 'cl-lib))
;;
(defun init-color-name
    (r g b)
  "\
Return a color built from R G B.

Inverse of `color-values'."
  (format "#%02x%02x%02x" (ash r -8) (ash g -8) (ash b -8)))
;;
(defun init-color-add (c1 c2)
  "\
Add the two colors C1 and C2."
  (apply #'init-color-name (cl-mapcar #'+
                                      (color-values c1)
                                      (color-values c2))))
;;
(defun init-color-sub (c1 c2)
  "\
Add the two colors C1 and C2."
  (apply #'init-color-name (cl-mapcar #'-
                                      (color-values c1)
                                      (color-values c2))))
;;
(defun init-color-blend (c1 c2 &optional a)
  "\
Blend the two colors C1 and C2 with A.

A is a number between 0.0 and 1.0."
  (setq a (or a 0.5))
  (apply #'init-color-name (cl-mapcar #'(lambda
                                            (c1 c2)
                                          (round (+ (* c1 a) (* c2 (- 1 a)))))
                                      (color-values c1)
                                      (color-values c2))))
;;
(provide 'init-color)
