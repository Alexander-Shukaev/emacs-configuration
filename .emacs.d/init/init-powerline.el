(eval-when-compile
  (require 'init-package))
;;
(init-handler powerline preface
  (eval-when-compile
    (require 'subr-x)))
;;
(init-handler powerline prepare
  (defvar-local init-powerline-theme-string
    nil
    "\
Cached latest buffer-local value returned by `init-powerline-theme'.")
  (put 'init-powerline-theme-string 'risky-local-variable t)
  ;;
  (defcustom init-powerline-misc-info
    'mode-line-misc-info
    "\
Mode line construct for miscellaneous information.

By default, this shows the information specified by `mode-line-misc-info'."
    :group 'init
    :type `(choice (const  :tag "Nothing" nil)
                   (string :tag "String")
                   (symbol :tag "Symbol")
                   (list   :tag "List")))
  (put 'init-powerline-misc-info 'risky-local-variable t)
  ;;
  (defun init-powerline-theme-setup ()
    (interactive)
    (setq-default mode-line-format
      '("%e" (:eval (condition-case e
                        (let (mode-line-format)
                          ;; -------------------------------------------------
                          ;; CAUTION:
                          ;;
                          ;; Since the function `init-powerline-theme' is
                          ;; autoloading the `powerline' feature (as per the
                          ;; `:commands' keyword below), in an unfortunate
                          ;; intersection of events (e.g. when running Emacs
                          ;; instance in daemon mode and some message is being
                          ;; printed, while the initial frame is being created
                          ;; upon client connection), it might signal error
                          ;; with the message: "Recursive load".  Avoid this
                          ;; issue by locally let-binding the variable
                          ;; `mode-line-format' to nil, what effectively
                          ;; temporarily disables mode line display.
                          ;; -------------------------------------------------
                          (unless (bound-and-true-p comp-native-compiling)
                            (init-powerline-theme "%Z" "%l" "%c" "%p")))
                      (error (lwarn 'init
                                    :error
                                    "Error: %s\n%s"
                                    (error-message-string e)
                                    (init-backtrace-to-string))
                             nil)))))
    (save-current-buffer
      (dolist (buffer (buffer-list))
        (when (init-interesting-buffer-p buffer)
          (set-buffer buffer)
          (setq-local mode-line-format (default-value 'mode-line-format)))))
    (force-mode-line-update :all)))
;;
(init-handler powerline init
  (setq-default init-powerline-misc-info nil)
  ;;
  (setq-default powerline-default-separator 'arrow)
  ;;
  (if (or (eq system-type 'windows-nt)
          (featurep 'w32))
      (init-powerline-theme-setup)
    ;; -----------------------------------------------------------------------
    (after-init (init-powerline-theme-setup))
    ;;
    ;; BUG:
    ;;
    ;; Emacs crashes on Windows.
    ;; -----------------------------------------------------------------------
    ))
;;
(init-handler powerline config
  (defface powerline-modified-flag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color)
        (min-colors 88)
        (background light))
       :foreground "yellow"
       :weight bold)
      (((class color)
        (min-colors 88)
        (background dark))
       :foreground "yellow"
       :weight bold)
      (((class color)
        (min-colors 16)
        (background light))
       :foreground "yellow"
       :weight bold)
      (((class color)
        (min-colors 16)
        (background dark))
       :foreground "yellow"
       :weight bold)
      (((class color)
        (min-colors 8)
        (background light))
       :foreground "yellow"
       :weight bold)
      (((class color)
        (min-colors 8)
        (background dark))
       :foreground "yellow"
       :weight bold)
      (t
       :weight bold))
    "Powerline face used to highlight modified flag."
    :group 'powerline)
  ;;
  (defface powerline-read-only-flag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color)
        (min-colors 88)
        (background light))
       :foreground "red"
       :weight bold)
      (((class color)
        (min-colors 88)
        (background dark))
       :foreground "red"
       :weight bold)
      (((class color)
        (min-colors 16)
        (background light))
       :foreground "red"
       :weight bold)
      (((class color)
        (min-colors 16)
        (background dark))
       :foreground "red"
       :weight bold)
      (((class color)
        (min-colors 8)
        (background light))
       :foreground "red"
       :weight bold)
      (((class color)
        (min-colors 8)
        (background dark))
       :foreground "red"
       :weight bold)
      (t
       :weight bold))
    "Powerline face used to highlight read-only flag."
    :group 'powerline)
  ;;
  (defsubst init-powerline-property-at
      (pos property string)
    (unless (zerop (length string))
      (setq pos (cond ((eq pos 'l) 0)
                      ((eq pos 'r) (1- (length string)))
                      (t           pos)))
      (get-text-property pos property string)))
  ;;
  (defsubst init-powerline-face-at
      (pos string)
    (init-powerline-property-at pos 'face string))
  ;;
  (defsubst init-powerline-current-separator ()
    (let ((separator (powerline-current-separator)))
      (and (not (eq separator 'utf-8))
           (init-display-graphic-p)
           separator)))
  ;;
  (defsubst init-powerline-separator-l ()
    (intern-soft (format "powerline-%s-%s"
                         (init-powerline-current-separator)
                         (car powerline-default-separator-dir))))
  ;;
  (defsubst init-powerline-separator-r ()
    (intern-soft (format "powerline-%s-%s"
                         (init-powerline-current-separator)
                         (cdr powerline-default-separator-dir))))
  ;;
  (init-defun init-powerline-theme-l
      (%Z mode-line-face face-1 face-2 separator-l)
    #$
    `(,(powerline-raw `(,%Z " ") face-2 'l)
      ,(powerline-raw (if (buffer-modified-p)
                          (propertize "*" 'face 'powerline-modified-flag)
                        " ")
                      face-2)
      ,(powerline-raw (if buffer-read-only
                          (propertize "#" 'face 'powerline-read-only-flag)
                        " ")
                      face-2)
      ;; ---------------------------------------------------------------------
      ;; BUG:
      ;;
      ;; Function `powerline-buffer-id' is a buggy piece of garbage because it
      ;; reinterprets `mode-line-buffer-identification' with
      ;; `format-mode-line' multiple times, what can cause incorrect
      ;; computation of the final resulting string value, e.g. when % is
      ;; present in either the name of buffer (`buffer-name') or the name of
      ;; file-visiting buffer (`buffer-file-name'):
      ;;
      ;;   ,(powerline-buffer-id face-2 'r)
      ;;
      ;; Prefer simple implementation instead:
      ;;
      ,(powerline-raw '(" " mode-line-buffer-identification) face-2 'r)
      ;; ---------------------------------------------------------------------
      ,(funcall separator-l mode-line-face face-1)
      ,(powerline-narrow face-1 'l)
      ,(powerline-vc face-1 'l)
      ,(powerline-raw " " face-1)))
  ;;
  (init-defun init-powerline-theme-r
      (%l %c %p mode-line-face face-1 face-2 separator-r)
    #$
    `(,(powerline-raw '(" " init-powerline-misc-info) face-1)
      ,(powerline-raw `(,%l ":" ,%c) face-1 'r)
      ,(funcall separator-r face-1 mode-line-face)
      ,(powerline-raw `(" " ,%p) face-2 'r)))
  ;;
  (init-defun init-powerline-theme-c
      (active mode-line-face face-1 face-2 separator-l separator-r)
    #$
    (append
     (list (powerline-raw " " face-1)
           (funcall separator-l face-1 face-2)
           (and (bound-and-true-p erc-modified-channels-object)
                (stringp          erc-modified-channels-object)
                (powerline-raw    erc-modified-channels-object face-2 'l))
           (powerline-major-mode face-2 'l)
           (powerline-process face-2)
           (powerline-raw " " face-2))
     (when (and (bound-and-true-p evil-mode)
                (bound-and-true-p evil-mode-line-format)
                (bound-and-true-p evil-mode-line-tag))
       (if (stringp evil-mode-line-tag)
           (let ((face-l (or (when active
                               (init-powerline-face-at 'l evil-mode-line-tag))
                             face-1))
                 (face-r (or (when active
                               (init-powerline-face-at 'r evil-mode-line-tag))
                             face-1))
                 (tag    (if active
                             evil-mode-line-tag
                           (substring-no-properties evil-mode-line-tag))))
             (list (funcall separator-r face-2 face-l)
                   (powerline-raw " " face-l)
                   (powerline-raw (string-trim tag) face-1)
                   (powerline-raw " " face-r)
                   (funcall separator-l face-r face-2)))
         (list (funcall separator-r face-2 face-1)
               (powerline-raw '("" evil-mode-line-tag) face-1)
               (funcall separator-l face-1 face-2))))
     (list (powerline-minor-modes face-2 'l)
           (powerline-raw " " face-2)
           (funcall separator-r face-2 face-1))))
  ;;
  (init-defun init-powerline-theme
      (%Z %l %c %p)
    #$
    (let* ((active         (powerline-selected-window-active))
           (mode-line-face (if active
                               'mode-line
                             'mode-line-inactive))
           (face-1         (if active
                               'powerline-active1
                             'powerline-inactive1))
           (face-2         (if active
                               'powerline-active2
                             'powerline-inactive2))
           (separator-l    (init-powerline-separator-l))
           (separator-r    (init-powerline-separator-r))
           (theme-l        (init-powerline-theme-l %Z
                                                   mode-line-face
                                                   face-1
                                                   face-2
                                                   separator-l))
           (theme-r        (init-powerline-theme-r %l
                                                   %c
                                                   %p
                                                   mode-line-face
                                                   face-1
                                                   face-2
                                                   separator-r))
           (theme-c        (init-powerline-theme-c active
                                                   mode-line-face
                                                   face-1
                                                   face-2
                                                   separator-l
                                                   separator-r)))
      (setq theme-l (powerline-render theme-l))
      (setq theme-r (powerline-render theme-r))
      (setq theme-c (powerline-render theme-c))
      (setq-local init-powerline-theme-string
        (concat theme-l
                ;; -----------------------------------------------------------
                ;; CAUTION:
                ;;
                ;; It is tempting to use `powerline-width' instead of plain
                ;; `string-width', but the former recursively applies
                ;; `format-mode-line' to the argument before computing the
                ;; total resulting string width.  As a result, if the argument
                ;; has already been interpreted with `format-mode-line',
                ;; e.g. by `powerline-raw' as done above, then the second
                ;; reinterpretation by `powerline-width' can cause incorrect
                ;; computation of the total resulting string width, e.g. when
                ;; % is present (especially true for `mode-line-misc-info',
                ;; `global-mode-string', `display-battery-mode', and
                ;; `battery-mode-line-string').  Hence, use `string-width':
                ;;
                (powerline-fill-center face-1 (/ (string-width theme-c) 2.0))
                theme-c
                (powerline-fill        face-1 (string-width theme-r))
                ;; -----------------------------------------------------------
                theme-r)))
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; Take advantage of the implementation detail in `format-mode-line' that
    ;; is described in `mode-line-format':
    ;;
    ;; For any symbol other than t or nil, the symbol's value is processed as
    ;;  a mode line construct.  As a special exception, if that value is a
    ;;  string, the string is processed verbatim, without handling any
    ;;  %-constructs (see below).  Also, unless the symbol has a non-nil
    ;;  `risky-local-variable' property, all properties in any strings, as
    ;;  well as all `:eval' and `:propertize' forms in the value, are ignored.
    ;;
    ;; Therefore, in order to avoid the second reinterpretation of the string
    ;; value, instead of returning the string value itself, return the symbol
    ;; `init-powerline-theme-string' having that final resulting string value:
    ;;
    'init-powerline-theme-string
    ;; -----------------------------------------------------------------------
    ))
;;
(init-package powerline
  :commands
  (init-powerline-theme))
