(eval-when-compile
  (require 'init-package))
;;
(init-handler tabulated-list preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler tabulated-list init)
;;
(init-handler tabulated-list config)
;;
(init-handler tabulated-list mode-map
  (let ((keymap tabulated-list-mode-map))
    (init-assert (if (< emacs-major-version 27)
                     (eq (keymap-parent keymap) button-buffer-map)
                   (equal (keymap-parent keymap) (make-composed-keymap
                                                  button-buffer-map
                                                  special-mode-map))))
    (init-clear-keymap    keymap :recursively-p)
    (set-keymap-parent    keymap
                          (make-composed-keymap (list special-mode-map
                                                      button-buffer-map)))
    (init-define-keys-kbd keymap
      "<follow-link>" 'mouse-face
      ;;
      "<mouse-2>" #'mouse-select-window
      ;;
      "$" #'tabulated-list-sort))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd tabulated-list-mode-map
      :motion
      [remap next-line    ] #'devil-line-forward
      [remap previous-line] #'devil-line-backward)))
;;
(init-handler tabulated-list sort-button-map
  (let ((keymap tabulated-list-sort-button-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      "<follow-link>" 'mouse-face
      ;;
      "<header-line> <mouse-1>" #'tabulated-list-col-sort
      "              <mouse-1>" #'tabulated-list-col-sort
      "<header-line> <mouse-2>" #'tabulated-list-col-sort
      "              <mouse-2>" #'tabulated-list-col-sort
      ;;
      "RET" #'tabulated-list-sort)))
;;
(init-package tabulated-list
  :built-in)
