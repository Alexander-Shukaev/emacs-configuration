(eval-when-compile
  (require 'init-package))
;;
(init-handler lisp-mode init
  (when (< emacs-major-version 25)
    (init-call elisp-mode)))
;;
(init-handler lisp-mode config
  (put 'lambda       'lisp-indent-function 1)
  (put 'defsubst     'lisp-indent-function 2)
  (put 'define-key   'lisp-indent-function 'defun)
  (put 'setq         'lisp-indent-function 'defun)
  (put 'setq-default 'lisp-indent-function 'defun)
  (put 'setq-local   'lisp-indent-function 'defun)
  ;;
  (after-load init-font-lock
    ;; TODO: Consider `highlight-quoted' instead?
    (defconst init-lisp-mode-font-lock-keywords
      (eval-when-compile
        ;; TODO: Why `regexp-quote' before concatenating with "[" and "]"?
        (let* ((symbol-pattern           (regexp-quote (concat "-+=*/"
                                                               "|_~!@"
                                                               "$%^&:"
                                                               "<>{}?"
                                                               "a-z"
                                                               "A-Z")))
               (symbol-pattern           (concat "[" symbol-pattern "]"
                                                 "[" symbol-pattern "0-9" "]"
                                                 "*"))
               (font-lock-symbol-pattern (concat "\\(" symbol-pattern "\\)"
                                                 "\\_>")))
          `(("#?[',:@`]"
             (0 font-lock-operator-face))
            (,(regexp-quote "#$")
             (0 font-lock-operator-face))
            (,(concat "[']" font-lock-symbol-pattern)
             (1 font-lock-variable-name-face))
            (,(concat "[,]" font-lock-symbol-pattern)
             (1 font-lock-variable-name-face))
            (,(concat "[:]" font-lock-symbol-pattern)
             (1 font-lock-constant-face))
            (,(concat "[@]" font-lock-symbol-pattern)
             (1 font-lock-variable-name-face))
            (,(concat "[`]" font-lock-symbol-pattern)
             (1 font-lock-variable-name-face))
            (,(regexp-opt '("t" "nil") 'symbols)
             (0 font-lock-constant-face))))))
    (after-load font-lock
      (font-lock-add-keywords 'lisp-mode init-lisp-mode-font-lock-keywords)))
  ;;
  (after-load speck
    (init-defun init-lisp-speck-setup ()
      #$
      ;; ---------------------------------------------------------------------
      ;; NOTE:
      ;;
      ;; As per `init-prog-mode-speck-setup':
      ;;
      (init-assert (and (local-variable-p 'speck-syntactic)
                        (eq speck-syntactic t)))
      ;; ---------------------------------------------------------------------
      (setq-local speck-face-inhibit-list '(font-lock-constant-face)))
    (add-hook 'lisp-mode-hook #'init-lisp-speck-setup))
  ;;
  (when (< emacs-major-version 25)
    (init-call elisp-mode)))
;;
(init-handler lisp-mode shared-map
  (let ((keymap lisp-mode-shared-map))
    (init-assert (eq (keymap-parent keymap) prog-mode-map))
    (init-clear-keymap keymap)
    (init-assert (eq (keymap-parent keymap) prog-mode-map))))
;;
(init-handler lisp-mode map
  (let ((keymap lisp-mode-map))
    (init-assert (eq (keymap-parent keymap) lisp-mode-shared-map))
    (init-clear-keymap keymap)
    (init-assert (eq (keymap-parent keymap) lisp-mode-shared-map))))
;;
(init-package lisp-mode
  :built-in)
