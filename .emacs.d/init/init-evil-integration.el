(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-integration init)
;;
(init-handler evil-integration config
  (init-assert (bound-and-true-p evil-want-integration))
  (init-negate (bound-and-true-p evil-emacs-state-modes))
  (init-negate (bound-and-true-p evil-insert-state-modes))
  (init-negate (bound-and-true-p evil-motion-state-modes)))
;;
(init-package evil-integration
  :ensure evil)
