(eval-when-compile
  (require 'init-package))
;;
(init-handler vc prepare
  (init-load vc-hooks))
;;
(init-handler vc init)
;;
(init-handler vc config)
;;
(init-package vc
  :built-in)
