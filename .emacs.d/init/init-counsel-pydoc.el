(eval-when-compile
  (require 'init-package))
;;
(init-handler counsel-pydoc prepare
  (init-load counsel :no-error))
;;
(init-handler counsel-pydoc init)
;;
(init-handler counsel-pydoc config)
;;
(init-package counsel-pydoc)
