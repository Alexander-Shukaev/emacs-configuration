(eval-when-compile
  (require 'init-package))
;;
(init-handler avy init
  (setq-default avy-keys (number-sequence ?a ?z)))
;;
(init-handler avy config)
;;
(init-package avy)
