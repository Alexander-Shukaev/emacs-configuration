(eval-when-compile
  (require 'init-package))
;;
(init-handler "fill" init
  (setq-default fill-column 78))
;;
(init-handler "fill" config)
;;
(init-package "fill"
  :built-in)
