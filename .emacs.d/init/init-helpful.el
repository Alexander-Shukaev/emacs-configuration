(eval-when-compile
  (require 'init-package))
;;
(init-handler helpful init)
;;
(init-handler helpful config)
;;
(init-handler helpful mode-map
  (let ((keymap helpful-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (set-keymap-parent    keymap
                          (make-composed-keymap (list button-buffer-map
                                                      special-mode-map)))
    (init-define-keys-kbd keymap
      [remap revert-buffer] #'helpful-update
      ;;
      "RET" #'helpful-visit-reference)))
;;
(init-package helpful)
