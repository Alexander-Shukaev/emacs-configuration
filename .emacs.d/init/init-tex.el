(eval-when-compile
  (require 'init-package))
;;
(init-handler tex init
  (setq-default
    TeX-PDF-mode          t
    TeX-auto-save         t
    ;; TODO:
    ;; TeX-install-font-lock #'ignore
    TeX-master            nil
    TeX-parse-self        t))
;;
(init-handler tex config
  ;; TODO:
  ;; (after-load init-header2
  ;;   (add-hook 'TeX-mode-hook #'auto-make-header))
  ;;
  (after-load init-reftex
    (add-hook 'TeX-mode-hook #'reftex-mode))
  ;;
  (after-load init-tex-fold
    (add-hook 'TeX-mode-hook #'TeX-fold-mode))
  ;;
  (after-load speck
    (init-defun init-tex-speck-setup ()
      #$
      (setq-local speck-face-inhibit-list '(font-latex-sedate-face
                                            font-latex-warning-face
                                            font-lock-constant-face
                                            font-lock-variable-name-face)))
    (add-hook 'TeX-mode-hook #'init-tex-speck-setup)))
;;
(init-package tex
  :ensure auctex)
