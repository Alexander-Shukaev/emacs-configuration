(eval-when-compile
  (require 'init-package))
;;
(init-handler rg init)
;;
(init-handler rg config
  (after-load init-wgrep-rg
    (add-hook 'rg-mode-hook #'wgrep-rg-setup)))
;;
(init-package rg)
