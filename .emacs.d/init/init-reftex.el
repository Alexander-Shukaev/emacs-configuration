(eval-when-compile
  (require 'init-package))
;;
(init-handler reftex init
  (setq-default reftex-plug-into-AUCTeX t))
;;
(init-handler reftex config)
;;
(init-package reftex
  :built-in)
