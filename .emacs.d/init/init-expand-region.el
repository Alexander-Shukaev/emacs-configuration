(eval-when-compile
  (require 'init-package))
;;
(init-handler expand-region init
  (setq-default expand-region-fast-keys-enabled nil))
;;
(init-handler expand-region config)
;;
(init-package expand-region)
