(setq-default
  line-number-display-limit       (* 8 1024 1024)
  line-number-display-limit-width 512)
;;
(provide 'init-line-number-display-limit)
