(eval-when-compile
  (require 'init-package))
;;
(init-handler hl-line init
  (after-init (global-hl-line-mode)))
;;
(init-handler hl-line config)
;;
(init-package hl-line
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Sometimes slow and sluggish.
  ;; -------------------------------------------------------------------------
  :built-in)
