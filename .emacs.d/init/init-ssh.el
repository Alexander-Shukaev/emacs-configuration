(eval-when-compile
  (require 'init-package))
;;
(init-handler ssh init)
;;
(init-handler ssh config)
;;
(init-package ssh
  :load-path "lisp/ssh")
