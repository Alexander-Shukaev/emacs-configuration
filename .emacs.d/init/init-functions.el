(require 'init-macros)
;;
(defsubst init-user-expand-file-name
    (name &optional default-directory)
  "\
Convert filename NAME to absolute, and canonicalize it.

DEFAULT-DIRECTORY is directory to start with if NAME is
relative (does not start with slash or tilde); both the directory
name and a directory's file name are accepted.
If DEFAULT-DIRECTORY is nil or missing, the value of
`user-emacs-directory' is used.  NAME should be a string that is
a valid file name for the underlying filesystem.

See `expand-file-name'."
  (expand-file-name name (unless (file-name-absolute-p name)
                           (or default-directory user-emacs-directory))))
;;
(defsubst init-user-expand-directory
    (name &optional default-directory)
  "\
Convert filename NAME to absolute, and canonicalize it.

DEFAULT-DIRECTORY is directory to start with if NAME is
relative (does not start with slash or tilde); both the directory
name and a directory's file name are accepted.
If DEFAULT-DIRECTORY is nil or missing, the value of
`user-emacs-directory' is used.  NAME should be a string that is
a valid file name for the underlying filesystem.

Return a string representing filename NAME interpreted as a
directory.

See `init-user-expand-file-name' and `file-name-as-directory'."
  (file-name-as-directory (init-user-expand-file-name name
                                                      default-directory)))
;;
(defsubst init-user-file-truename
    (filename)
  "\
Return the truename of FILENAME.

If FILENAME is not absolute, first expands it against
‘user-emacs-directory’.

See `init-user-expand-file-name' and `file-truename'."
  (file-truename (if (file-name-absolute-p filename)
                     filename
                   (init-user-expand-file-name filename))))
;;
(defsubst init-user-directory-truename
    (filename)
  "\
Return the truename of FILENAME.

If FILENAME is not absolute, first expands it against
‘user-emacs-directory’.

Return a string representing filename FILENAME interpreted as a
directory.

See `init-user-file-truename' and `file-name-as-directory'."
  (file-name-as-directory (init-user-file-truename filename)))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Redefine own function?
;;
(require 'server)
(defalias 'init-make-safe-directory #'server-ensure-safe-dir)
;; ---------------------------------------------------------------------------
;;
(defun init-current-file-name ()
  "\
Return current file name."
  (or (and load-in-progress load-file-name)
      (bound-and-true-p byte-compile-current-file)
      (buffer-file-name)))
;;
(defun init-interesting-buffer-p
    (&optional buffer)
  "\
Return t if BUFFER is a live buffer whose name does not start with a space."
  (and (buffer-live-p (or buffer (current-buffer)))
       (or (zerop (length (buffer-name buffer)))
           (/=    (aref   (buffer-name buffer) 0) ?\s))
       (not (minibufferp buffer))))
;;
(defun init-insq
    (newelt oldelt list &optional unique)
  (unless (and unique (memq newelt list))
    (while (let ((car (car-safe list))
                 (cdr (cdr-safe list)))
             (when (eq car oldelt)
               (setcdr list (cons newelt cdr)))
             (setq list cdr))))
  list)
;;
(defun init-setq
    (newelt oldelt list &optional unique)
  (unless (and unique (memq newelt list))
    (while (let ((car (car-safe list))
                 (cdr (cdr-safe list)))
             (when (eq car oldelt)
               (setcar list newelt))
             (setq list cdr))))
  list)
;;
(defun init-subdirectories
    (directory)
  (delq nil (mapcar #'(lambda
                          (file)
                        (when (and (not (string-prefix-p
                                         "." (file-name-nondirectory
                                              (directory-file-name file))))
                                   (file-directory-p file))
                          (file-name-as-directory file)))
                    (directory-files directory :full))))
;;
(defun init-symbol-matches
    (regexp &optional predicate)
  (let (matches)
    (mapatoms #'(lambda
                    (symbol)
                  (when (and (or (null predicate) (funcall predicate symbol))
                             (string-match-p regexp (symbol-name symbol)))
                    (push symbol matches))))
    matches))
;;
(defun init-variable-symbol-matches
    (regexp)
  (init-symbol-matches regexp #'boundp))
;;
(defun init-keymap-symbol-matches
    (regexp)
  (init-symbol-matches regexp #'(lambda
                                    (symbol)
                                  (and (boundp symbol)
                                       (keymapp (symbol-value symbol))))))
;;
(defun init-function-symbol-matches
    (regexp)
  (init-symbol-matches regexp #'fboundp))
;;
(defun init-load-path-prepend
    (path)
  (last (mapcar #'(lambda
                      (directory)
                    (setq directory (init-user-expand-directory directory))
                    (add-to-list 'load-path (directory-file-name directory)))
                (if (stringp path)
                    (list path)
                  (reverse path)))))
;;
(defun init-clear-keymap
    (keymap &optional recursively-p)
  "\
Clear keymap KEYMAP.

If RECURSIVELY-P is non-nil, remove the parent keymap of keymap KEYMAP too.

Return KEYMAP."
  (unless (keymapp keymap)
    (signal 'wrong-type-argument (list 'keymapp keymap)))
  (if recursively-p
      (progn
        (setcdr keymap nil)
        (init-assert (null (keymap-parent keymap))))
    (init-without-keymap-parent keymap
      (setcdr keymap nil)))
  keymap)
;;
(defun init-clear-matching-keymaps
    (regexp &optional recursively-p)
  (let ((keymap-symbols (init-keymap-symbol-matches regexp)))
    (dolist (keymap-symbol keymap-symbols keymap-symbols)
      (init-clear-keymap (symbol-value keymap-symbol) recursively-p))))
;;
(defun init-make-keymap
    (keymap &optional recursively-p)
  "\
Make keymap KEYMAP.

If RECURSIVELY-P is non-nil, remove the parent keymap of keymap KEYMAP too.

Return KEYMAP."
  (if recursively-p
      (progn
        (setcdr keymap (cdr (make-keymap)))
        (init-assert (null (keymap-parent keymap))))
    (init-without-keymap-parent keymap
      (setcdr keymap (cdr (make-keymap)))))
  keymap)
;;
(defun init-key-command
    (key-or-command &optional keymap)
  "\
Return command for KEY-OR-COMMAND."
  (cond ((and (listp key-or-command)
              (eq (car-safe key-or-command) 'quote))
         (cadr key-or-command))
        ((or (stringp key-or-command)
             (vectorp key-or-command))
         (let ((key (lookup-key keymap key-or-command)))
           (unless (or (integerp key)
                       (keymapp  key))
             (init-key-command key keymap))))
        ((or (symbolp  key-or-command)
             (commandp key-or-command))
         key-or-command)
        (t
         (signal 'wrong-type-argument (list 'commandp key-or-command)))))
;;
(defun init-command-keys
    (command &optional keymap)
  "\
Return keys for COMMAND."
  (and (fboundp command)
       (or (where-is-internal command keymap))))
;;
(defun init-replace-in-string
    (exp rep string)
  "\
Replace all matches for EXP with REP in STRING and return it."
  (replace-regexp-in-string (regexp-quote exp) rep string nil :literal))
;;
(defun init-format-quote
    (string)
  "\
Substitute all % with %% in STRING and return it."
  (init-replace-in-string "%" "%%" string))
;;
(provide 'init-functions)
