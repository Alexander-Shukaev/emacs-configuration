(eval-when-compile
  (require 'init-package))
;;
(init-handler easymenu init)
;;
(init-handler easymenu config)
;;
(init-package easymenu
  :built-in)
