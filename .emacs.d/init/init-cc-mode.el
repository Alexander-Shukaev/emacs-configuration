(eval-when-compile
  (require 'init-package))
;;
(init-handler cc-mode init)
;;
(init-handler cc-mode config
  (defconst init-cc-mode-hooks
    '(c-mode-hook
      c++-mode-hook
      java-mode-hook
      objc-mode-hook))
  ;;
  (after-load (init-clang-format devil)
    (init-defun init-cc-mode-clang-format-devil-setup ()
      #$
      (setq-local devil-format-region-function #'clang-format-region))
    (dolist (hook init-cc-mode-hooks)
      (add-hook hook #'init-cc-mode-clang-format-devil-setup)))
  ;;
  (after-load init-electric-operator
    (dolist (hook init-cc-mode-hooks)
      (add-hook hook #'electric-operator-mode)))
  ;;
  (after-load init-highlight-operators
    (dolist (hook init-cc-mode-hooks)
      (add-hook hook #'highlight-operators-mode)))
  ;;
  (after-load init-lsp-mode
    (init-defun init-cc-mode-lsp-java-setup ()
      #$
      (when (require 'lsp-java nil :no-error)
        (lsp)))
    (add-hook 'java-mode-hook #'init-cc-mode-lsp-java-setup)))
;;
(init-package cc-mode
  :built-in
  :mode
  (("\\.\\(CC?\\|HH?\\|II?\\|TT?\\)\\'"      . c++-mode)
   ("\\.\\(cc\\|hh\\|ii\\|tt\\|tcc\\)\\'"    . c++-mode)
   ("\\.[CHIT]PP\\'"                         . c++-mode)
   ("\\.[chit]\\(\\+\\+\\|p\\|pp\\|xx\\)\\'" . c++-mode)))
