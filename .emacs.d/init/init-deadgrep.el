(eval-when-compile
  (require 'init-package))
;;
(init-handler deadgrep init)
;;
(init-handler deadgrep config)
;;
(init-package deadgrep
  :disabled)
