(eval-when-compile
  (require 'init-package))
;;
(init-handler vterm preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler vterm prepare
  (init-load color   :no-error)
  (init-load compile :no-error)
  (init-load term    :no-error)
  (init-load tramp   :no-error))
;;
(init-handler vterm init
  (setq-default vterm-always-compile-module t))
;;
(init-handler vterm config
  (init-assert (equal vterm-keymap-exceptions
                      '("C-c"
                        "C-x"
                        "C-u"
                        "C-g"
                        "C-h"
                        "C-l"
                        "M-x"
                        "M-o"
                        "C-y"
                        "M-y")))
  ;;
  (init-assert (fboundp 'vterm--redraw))
  (init-defad vterm--redraw
      (:around (function &rest ...) init/:around)
    #$
    (let ((cursor-type cursor-type))
      (apply function ...)))
  ;;
  (after-load (init-evil-collection evil)
    (when (evil-collection-require 'vterm :no-error)
      (add-hook 'vterm-mode-hook #'evil-collection-vterm-escape-stay)
      ;;
      (init-assert (fboundp 'evil-collection-vterm-append))
      (init-defad evil-collection-vterm-append
          (:before (&rest ...) init/:before)
        #$
        (vterm-goto-char (+ 1 (point))))
      ;;
      (evil-set-command-property #'evil-collection-vterm-delete-char
                                 :motion
                                 'evil-forward-char))))
;;
(init-handler vterm mode-map
  (let ((keymap vterm-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-define-keys-kbd keymap
      "<backspace>" #'undefined
      ;;
      "<return>"    #'undefined
      ;;
      "<tab>"       #'undefined
      ;;
      "C-c"         nil
      ;;
      "C-h"         #'vterm--self-insert
      "C-x"         #'vterm--self-insert
      "C-y"         #'vterm--self-insert))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd vterm-mode-map
      :motion
      "> >" #'vterm-next-prompt
      "< <" #'vterm-previous-prompt
      ;;
      :normal
      "SPC SPC" #'vterm-send-C-c
      "SPC c"   #'vterm-send-C-c
      "SPC i"   #'vterm-send-C-c
      ;;
      "SPC g"   #'vterm-send-C-g
      ;;
      "SPC u"   #'vterm-send-C-u
      ;;
      "SPC p"   #'vterm-reset-cursor-point
      ;;
      "SPC y"   #'vterm-copy-mode)
    ;;
    (devil-repeat-motions-kbd
      :keymap vterm-mode-map
      "> >" "> >" "< <"
      "< <" "> >" "< <")
    ;;
    (after-load evil-collection-vterm
      (devil-define-keys-kbd vterm-mode-map
        :motion
        [remap devil-first-non-blank-char]
        #'evil-collection-vterm-first-non-blank
        ;;
        :normal
        [remap devil-insert-at-eol]  #'evil-collection-vterm-append-line
        [remap devil-insert-at-bol]  #'evil-collection-vterm-insert-line
        ;;
        [remap devil-insert-at-eop]  #'evil-collection-vterm-append
        [remap devil-insert-at-bop]  #'evil-collection-vterm-insert
        ;;
        [remap devil-paste-forward]  #'evil-collection-vterm-paste-after
        [remap devil-paste-backward] #'vterm-yank
        ;;
        [remap devil-amend]          #'evil-collection-vterm-change
        [remap devil-amend-to-eol]   #'evil-collection-vterm-change-line
        ;;
        [remap devil-delete]         #'evil-collection-vterm-delete
        [remap devil-delete-to-eol]  #'evil-collection-vterm-delete-line
        ;;
        [remap devil-delete-char-forward]
        #'evil-collection-vterm-delete-char
        [remap devil-delete-char-backward]
        #'evil-collection-vterm-delete-backward-char
        ;;
        [remap undo]                 #'vterm-undo
        [remap undo-only]            #'vterm-undo))))
;;
(init-package vterm
  :unless
  (or (version< emacs-version "25.1")
      (eq system-type 'windows-nt)))
