(require 'init-package-variables)
(require 'init-package-functions)
;;
(require 'init-gc)
;;
(require 'init-functions)
(require 'init-macros)
;;
(defun init-package-byte-compile ()
  (interactive)
  (init-without-timers
    (unless noninteractive
      (message "Compiling packages..."))
    (let ((time (current-time)))
      (condition-case-unless-debug e
          ;; Optimize the byte compilation process with:
          (let (;; 1.  Preventing calls of file name-specific handlers:
                (file-name-handler-alist nil)
                ;; 2.  Preventing garbage collection:
                (gc-cons-threshold       (init-gc-max-cons-threshold))
                ;; 3.  Pretending that nothing is being loaded:
                (load-file-name          nil)
                (load-in-progress        nil)
                ;; 4.  Localizing modification of `load-path':
                (load-path               (copy-sequence load-path))
                ;; 5.  Preferring loading of files with `.elc' suffixes:
                (load-suffixes           (or (bound-and-true-p
                                              byte-compile-load-suffixes)
                                             load-suffixes))
                ;; 6.  Preventing messages when saving file(s):
                (save-silently           (bound-and-true-p
                                          inhibit-message)))
            (dolist (directory (list package-user-dir))
              (setq directory (init-user-expand-directory directory))
              (let ((default-directory directory))
                (setq load-path
                  (append (let ((load-path (copy-sequence load-path)))
                            (normal-top-level-add-subdirs-to-load-path))
                          load-path)))
              (when byte-compile-verbose
                (message "Compiling %s..."
                         (expand-directory directory)))
              (condition-case-unless-debug e
                  (prog1
                      (byte-recompile-directory directory 0)
                    (when byte-compile-verbose
                      (message "Compiling %s...done"
                               (expand-directory directory))))
                (error (when byte-compile-verbose
                         (message "Compiling %s...failed"
                                  (expand-directory directory)))
                       (signal (car e) (cdr e)))))
            (unless noninteractive
              (message "Compiling packages...done (%.3fs)"
                       (float-time (time-since time)))))
        (error (unless noninteractive
                 (message "Compiling packages...failed (%.3fs)"
                          (float-time (time-since time))))
               (signal (car e) (cdr e)))))))
(add-to-list 'init-package-byte-compile-commands #'init-package-byte-compile)
;;
(defun init-package-generate-autoloads
    (&optional directory)
  (interactive (list (when current-prefix-arg
                       (let ((directory (init-package--autoloads-directory)))
                         (read-directory-name (if directory
                                                  (format "Directory (%s): "
                                                          directory)
                                                "Directory: ")
                                              user-emacs-directory
                                              directory)))))
  (delq nil (mapcar
             #'(lambda
                   (directory)
                 (let (debug-on-error)
                   (init-with-demoted-errors-when-called-interactively "%s"
                     (init-package--generate-autoloads directory))))
             (init-subdirectories (or directory user-lisp-directory)))))
;;
(defun init-package-load-autoloads
    (&optional directory)
  (interactive (list (when current-prefix-arg
                       (let ((directory (init-package--autoloads-directory)))
                         (read-directory-name (if directory
                                                  (format "Directory (%s): "
                                                          directory)
                                                "Directory: ")
                                              user-emacs-directory
                                              directory)))))
  (delq nil (mapcar
             #'(lambda
                   (directory)
                 (let (debug-on-error)
                   (init-with-demoted-errors-when-called-interactively "%s"
                     (init-package--load-autoloads directory))))
             (init-subdirectories (or directory user-lisp-directory)))))
;;
(defun init-package-generate-and-load-autoloads
    (&optional directory)
  (interactive (list (when current-prefix-arg
                       (let ((directory (init-package--autoloads-directory)))
                         (read-directory-name (if directory
                                                  (format "Directory (%s): "
                                                          directory)
                                                "Directory: ")
                                              user-emacs-directory
                                              directory)))))
  (delq nil (mapcar
             #'(lambda
                   (directory)
                 (let (debug-on-error)
                   (init-with-demoted-errors-when-called-interactively "%s"
                     (init-package--generate-and-load-autoloads directory))))
             (init-subdirectories (or directory user-lisp-directory)))))
;;
(provide 'init-package-commands)
