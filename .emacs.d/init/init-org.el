(eval-when-compile
  (require 'init-package))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Try [1] and [2].
;; ---------------------------------------------------------------------------
;;
(init-handler org preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler org prepare
  (init-load org-indent       :no-error)
  (init-load org-num          :no-error)
  (init-load org-preview-html :no-error)
  (init-load org-superstar    :no-error)
  ;;
  (init-load ob-C             :no-error)
  (init-load ob-compile       :no-error)
  (init-load ob-emacs-lisp    :no-error)
  (init-load ob-git-permalink :no-error)
  (init-load ob-jupyter       :no-error)
  (init-load ob-makefile      :no-error)
  (init-load ob-python        :no-error)
  (init-load ob-shell         :no-error))
;;
(init-handler org init
  (setq-default
    ;; -----------------------------------------------------------------------
    org-adapt-indentation            nil
    ;;
    ;; NOTE:
    ;;
    ;; Although `org-adapt-indentation' is already set to nil by
    ;; `org-indent-mode' (which is always turned on since
    ;; `org-startup-indented' is set to t), keep an explicit setting for it
    ;; here due to robustness reasons (in order to enforce it even when
    ;; `org-indent-mode' is unavailable).
    ;; -----------------------------------------------------------------------
    org-edit-src-content-indentation 0
    org-image-actual-width           '(1024)
    org-log-done                     'time
    org-src-fontify-natively         t
    org-startup-folded               nil
    org-startup-indented             t
    org-startup-numerated            t))
;;
(init-handler org config
  (init-inhibit org mode-map)
  ;;
  (add-hook 'org-mode-hook #'init-org/:mode-map)
  ;;
  (dolist (module '(org-tempo))
    (add-to-list 'org-modules module))
  ;;
  (dolist (template '(("el" . "src emacs-lisp")
                      ("py" . "src python")
                      ("sh" . "src shell")))
    (add-to-list 'org-structure-template-alist template))
  ;;
  ;; -------------------------------------------------------------------------
  (init-defad org-mode
      (:before (&rest ...) init/:before -100)
    #$
    (org-babel-do-load-languages 'org-babel-load-languages
                                 org-babel-load-languages))
  ;;
  ;; CAUTION:
  ;;
  ;; Adding `org-babel-do-load-languages' to `org-mode-hook' is almost
  ;; certainly a really bad idea.  For instance, consider how packages
  ;; (features) loaded for languages, that are listed in
  ;; `org-babel-load-languages', may potentially also add some functions to
  ;; the beginning of `org-mode-hook' (by default), what would unexpectedly
  ;; lead to not running them (unless added to the end using `:append' keyword
  ;; perhaps) as looping over `org-mode-hook' would already be in progress.
  ;; One such example is `jupyter-org-interaction-mode'.
  ;; -------------------------------------------------------------------------
  ;;
  (after-load elec-pair
    (init-defun init-org-elec-pair-inhibit
        (char)
      #$
      (or (memq char '(?\<))
          (funcall (default-value 'electric-pair-inhibit-predicate) char)))
    ;;
    (init-defun init-org-elec-pair-setup ()
      #$
      (setq-local electric-pair-inhibit-predicate
        #'init-org-elec-pair-inhibit))
    (add-hook 'org-mode-hook #'init-org-elec-pair-setup))
  ;;
  (after-load tempo
    (init-defun init-org-tempo-insert-string
        (string)
      #$
      (if (string-prefix-p "#+" string)
          (let ((pos (seq-position string ?\ )))
            (if pos
                (concat (upcase (substring string nil pos))
                        (substring string pos))
              (upcase string)))
        string))
    ;;
    (init-defun init-org-tempo-setup ()
      #$
      (setq-local tempo-insert-string-functions
        #'init-org-tempo-insert-string))
    (add-hook 'org-mode-hook #'init-org-tempo-setup))
  ;;
  (after-load (init-company-org-block company)
    (init-defun init-org-block-company-setup ()
      #$
      (make-local-variable 'company-backends)
      (if (bound-and-true-p org-mode)
          (add-to-list 'company-backends #'company-org-block)
        (setq-local company-backends (delq #'company-org-block company-backends))))
    (add-hook 'org-mode-hook #'init-org-block-company-setup))
  ;;
  (after-load (init-devil devil)
    (init-defun init-org-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "link-toggle" #'org-toggle-link-display
        ;;
        "lt[oggle]"   "link-toggle"))
    (add-hook 'org-mode-hook #'init-org-devil-setup))
  ;;
  (after-load init-org-superstar
    (add-hook 'org-mode-hook #'org-superstar-mode)))
;;
(init-handler org babel-map
  (let ((keymap org-babel-map))
    (init-assert (null (keymap-parent keymap)))))
;;
(init-handler org mode-map
  (let ((keymap org-mode-map))
    (init-assert (equal (keymap-parent keymap) outline-mode-map))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      "TAB"       #'org-cycle
      "<backtab>" #'org-global-cycle
      ;;
      "#"         #'org-toggle-link-display))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd org-mode-map
      :normal
      "SPC RET" #'org-ctrl-c-ctrl-c
      ;;
      "SPC c"   #'org-ctrl-c-ctrl-c
      ;;
      "SPC b"   org-babel-map)))
;;
(init-package org)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://graphviz.org'
;;  [2] URL `http://plantuml.com'
;;  ==========================================================================
;;  }}} References
