(eval-when-compile
  (require 'init-package))
;;
(init-handler gitattributes-mode init)
;;
(init-handler gitattributes-mode config
  (init-defun gitattributes-mode-setup ()
    #$
    (setq-local comment-start "#"))
  (add-hook 'gitattributes-mode-hook #'gitattributes-mode-setup))
;;
(init-package gitattributes-mode
  :ensure git-modes)
