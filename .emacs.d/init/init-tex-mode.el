(eval-when-compile
  (require 'init-package))
;;
(init-handler tex-mode init)
;;
(init-handler tex-mode config
  (after-load init-font-lock
    (defconst init-tex-mode-font-lock-keywords
      '(("\\\\c!\\(\\w+\\)\\b"
         1 font-lock-variable-name-face)
        ("\\\\s!\\(\\w+\\)\\b"
         1 font-lock-constant-face)
        ("\\(\\[\\|,\\)[^[:graph:]]*\\(\\w+\\)="
         2 font-lock-variable-name-face)
        ("\\w+\\(=\\)"
         1 font-lock-operator-face)
        ("\\\\[csv]\\(!\\)\\w+\\b"
         1 font-lock-operator-face)
        (,(regexp-quote "---")
         0 font-lock-operator-face)
        (,(regexp-quote "--")
         0 font-lock-operator-face)
        (,(regexp-quote "~")
         0 font-lock-operator-face)))
    (after-load font-lock
      ;; ---------------------------------------------------------------------
      ;; CAUTION:
      ;;
      ;; Somehow it must be strictly `plain-tex-mode' here, while
      ;; `plain-TeX-mode' does not apply:
      ;;
      (font-lock-add-keywords 'plain-tex-mode
                              init-tex-mode-font-lock-keywords)
      ;; ---------------------------------------------------------------------
      )))
;;
(init-package tex-mode
  :built-in)
