(eval-when-compile
  (require 'init-package))
;;
(init-handler delsel init
  (after-init (delete-selection-mode)))
;;
(init-handler delsel config
  ;; -------------------------------------------------------------------------
  ;; CAUTION:
  ;;
  ;; Function `init-delsel/:config' should try to (stickily) enable
  ;; `delete-selection-mode' again (maybe redundantly) because function
  ;; `init-minibuffer/:config' reloads the `delsel' feature by calling
  ;; `unload-feature' and `require':
  ;;
  (after-init (delete-selection-mode))
  ;; -------------------------------------------------------------------------
  )
;;
(init-package delsel
  :built-in)
