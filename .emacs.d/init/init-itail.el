(eval-when-compile
  (require 'init-package))
;;
(init-handler itail init
  (setq-default
    itail-lines
    #'(lambda ()
        (- (count-screen-lines (window-start)
                               (window-end))))
    ;; -----------------------------------------------------------------------
    itail-highlight-list
    '(("\\<[Ff][Aa][Tt][Aa][Ll].*"         . hi-red-b)
      ("\\<[Ee][Rr][Oo][Rr][Rr].*"         . hi-red-b)
      ("\\<[Ww][Aa][Rr][Nn][Ii][Nn][Gg].*" . hi-yellow-b))))
;;
(init-handler itail config
  (when (bound-and-true-p init-bb)
    (add-to-list 'itail-highlight-list '("\\<F :.*" . hi-red-b))
    (add-to-list 'itail-highlight-list '("\\<E :.*" . hi-red-b))
    (add-to-list 'itail-highlight-list '("\\<W :.*" . hi-yellow-b)))
  ;;
  (add-hook 'itail-mode-hook #'itail-highlight)
  ;;
  (init-defun init-itail-mode-setup ()
    #$
    (goto-char (point-max))
    (setq-local buffer-read-only t))
  (add-hook 'itail-mode-hook #'init-itail-mode-setup)
  ;;
  (after-load (init-evil evil)
    (add-hook 'itail-mode-hook #'evil-motion-state)))
;;
(init-handler itail keymap
  (let ((keymap itail-keymap))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap)))
;;
(init-package itail
  :diminish
  (itail-mode))
