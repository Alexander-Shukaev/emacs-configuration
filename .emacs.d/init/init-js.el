(eval-when-compile
  (require 'init-package))
;;
(init-handler js init
  (init-load indent)
  ;;
  (setq-default js-indent-level init-indent-width))
;;
(init-handler js config)
;;
(init-handler js mode-map
  (let ((keymap js-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap)))
;;
(init-package js
  :built-in)
