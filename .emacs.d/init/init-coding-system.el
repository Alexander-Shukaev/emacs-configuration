;; `set-language-environment' sets coding system priority and default input
;; method.
(set-language-environment 'utf-8)
;; ---------------------------------------------------------------------------
;; `prefer-coding-system' sets the following coding systems:
;; - default coding system for newly created buffers;
;; - default coding system for subprocess I/O;
;; - default coding system for `file-name-coding-system';
;; - default coding system for `set-terminal-coding-system';
;; - default coding system for `set-keyboard-coding-system'.
(prefer-coding-system 'utf-8-unix)
;; ---------------------------------------------------------------------------
;; `set-default-coding-systems' sets the following coding systems:
;; - default coding system for newly created buffers;
;; - default coding system for subprocess I/O;
;; - default coding system for `file-name-coding-system';
;; - default coding system for `set-terminal-coding-system';
;; - default coding system for `set-keyboard-coding-system'.
(set-default-coding-systems 'utf-8-unix)
;;(set-keyboard-coding-system 'utf-8-unix)
;;(set-terminal-coding-system 'utf-8-unix)
;; ---------------------------------------------------------------------------
(unless (eq system-type 'windows-nt)
  ;; `set-clipboard-coding-system' is an alias for
  ;; `set-selection-coding-system'.
  (set-selection-coding-system 'utf-8-unix)
  (set-clipboard-coding-system 'utf-8-unix))
;; ---------------------------------------------------------------------------
(setq-default
  ;; Coding system to use with system messages.
  locale-coding-system  'utf-8-unix
  ;; Data type request for X selection.
  x-select-request-type (when (< emacs-major-version 28)
                          '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))
;; ---------------------------------------------------------------------------
(provide 'init-coding-system)
