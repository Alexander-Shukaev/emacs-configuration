(require 'use-package)
;;
(dolist (symbol '(always-defer
                  always-demand
                  always-ensure
                  always-pin
                  check-before-init
                  compute-statistics
                  defaults
                  deferring-keywords
                  enable-imenu-support
                  ensure-function
                  expand-minimally
                  form-regexp-eval
                  hook-name-suffix
                  ignore-unknown-keywords
                  inject-hooks
                  keywords
                  merge-key-alist
                  minimum-reported-time
                  verbose))
  (defvaralias
    (intern (format "init-package-%s" symbol))
    (intern (format  "use-package-%s" symbol))))
;;
(defcustom init-package-phases
  '(preface prepare preinit init preload preconfig config)
  "Set of valid phases, in the order they are processed in."
  :group 'init-package
  :type '(repeat symbol))
;;
(defcustom init-package-disabled-phases
  (nconc '(catch defer defines load) init-package-phases)
  "Set of disabled phases."
  :group 'init-package
  :type '(repeat symbol))
;;
(defcustom init-package-byte-compile-commands
  '(init-package-byte-compile)
  "List of byte compile commands."
  :type '(repeat symbol))
;;
;;;###autoload
(defconst init-package-font-lock-keywords
  `(,@(let ((anchor-pattern "\\(?:^\\|[^`']\\)(%s\\_>")
            (symbol-pattern "\\(?:\\sw\\|\\s_\\)+")
            (keywords       nil))
        (dolist (symbol '(;
                          init-package-declare-handler
                          init-declare-handler
                          init-handler
                          init-package-define-handler
                          init-define-handler
                          init-package-inline-handler
                          init-inline-handler
                          init-package-call-handler
                          init-call-handler
                          init-call
                          init-package-inhibit-handler
                          init-inhibit-handler
                          init-inhibit))
          (push `(,(format anchor-pattern symbol)
                  (,symbol-pattern
                   (save-excursion
                     (search-forward-regexp ,(concat symbol-pattern "\\|)")))
                   nil
                   (0 font-lock-constant-face nil t))
                  (,symbol-pattern
                   (save-excursion
                     (search-forward-regexp ,(concat symbol-pattern "\\|)")))
                   nil
                   (0 font-lock-type-face     nil t)))
                keywords))
        (dolist (symbol '(;
                          init-package
                          init-package-load
                          init-load
                          init-package-using
                          init-using
                          init-package-using-when-compile
                          init-using-when-compile
                          init-package-with
                          init-with
                          init-require))
          (push `(,(format anchor-pattern symbol)
                  (,symbol-pattern
                   (save-excursion
                     (search-forward-regexp ,(concat symbol-pattern "\\|)")))
                   nil
                   (0 font-lock-constant-face nil t)))
                keywords))
        (dolist (symbol '(;
                          init-defad
                          init-defun))
          (push `(,(format anchor-pattern symbol)
                  (,symbol-pattern
                   (save-excursion
                     (search-forward-regexp ,(concat symbol-pattern "\\|)")))
                   nil
                   (0 font-lock-function-name-face nil t)))
                keywords))
        keywords)))
;;
;;;###autoload
(font-lock-add-keywords 'emacs-lisp-mode init-package-font-lock-keywords)
;;
(defvar init-package--autoload-path
  nil
  "List of directories containing `generated-autoload-file'.")
;;
(provide 'init-package-variables)
