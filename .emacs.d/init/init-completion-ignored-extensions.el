(setq-default completion-ignored-extensions
  (nconc completion-ignored-extensions '(".build/" ".install/")))
;;
(provide 'init-completion-ignored-extensions)
