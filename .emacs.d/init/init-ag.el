(eval-when-compile
  (require 'init-package))
;;
(init-handler ag init)
;;
(init-handler ag config
  (after-load init-wgrep-ag
    (add-hook 'ag-mode-hook #'wgrep-ag-setup)))
;;
(init-package ag)
