(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-collection-vterm prepare
  (init-load evil-collection :no-error))
;;
(init-handler evil-collection-vterm init)
;;
(init-handler evil-collection-vterm config
  (init-assert (fboundp 'evil-collection-vterm-setup))
  (fmakunbound 'evil-collection-vterm-setup))
;;
(init-package evil-collection-vterm
  :ensure evil-collection)
