(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-shell prepare
  (init-load ob :no-error))
;;
(init-handler ob-shell init
  (setq-default
    org-babel-default-header-args:shell '((:results . "output replace"))
    ;;
    org-babel-default-header-args:sh    org-babel-default-header-args:shell
    ;;
    org-babel-default-header-args:bash  org-babel-default-header-args:shell)
  ;;
  (after-load org
    (dolist (language '((shell . t)))
      (add-to-list 'org-babel-load-languages language))))
;;
(init-handler ob-shell config)
;;
(init-package ob-shell
  :built-in)
