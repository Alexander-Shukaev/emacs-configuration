;; TODO: `after-load', `init-assert'
(when (or (eq system-type 'windows-nt)
          (featurep 'w32))
  (setq-default
    w32-get-true-file-attributes nil
    w32-pass-lwindow-to-system   nil
    w32-pass-rwindow-to-system   nil
    w32-lwindow-modifier         'super
    w32-rwindow-modifier         'super))
;;
(provide 'init-w32)
