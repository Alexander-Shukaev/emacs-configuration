(eval-when-compile
  (require 'init-package))
;;
(init-handler git init
  (setenv "GIT_DISCOVERY_ACROSS_FILESYSTEM" "1")
  (when (and (eq system-type 'windows-nt)
             (zerop (length (getenv "GIT_ASKPASS")))
             (executable-find "git-gui--askpass"))
    (setenv "GIT_ASKPASS" "git-gui--askpass")))
;;
(init-handler git config)
;;
(init-package git)
