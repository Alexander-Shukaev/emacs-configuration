(eval-when-compile
  (require 'init-package))
;;
(init-handler company-lsp prepare
  (init-load company  :no-error)
  (init-load lsp-mode :no-error))
;;
(init-handler company-lsp init)
;;
(init-handler company-lsp config)
;;
(init-package company-lsp
  ;; -------------------------------------------------------------------------
  ;; DEPRECATED:
  ;;
  ;; See [1]:
  ;;
  :disabled
  ;; -------------------------------------------------------------------------
  )
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/scalameta/metals/issues/2641'
;;  ==========================================================================
;;  }}} References
