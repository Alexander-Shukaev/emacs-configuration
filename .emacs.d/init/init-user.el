(eval-when-compile
  (require 'init-package-macros))
;;
(eval-and-compile
  (init-load after-init))
;;
(defun init-user-setup ()
  (defcustom init-user-name
    (user-login-name)
    "User (full) name."
    :group 'init
    :type 'string)
  (put 'init-user-name  'safe-local-variable #'stringp)
  ;;
  (defcustom init-user-email
    user-mail-address
    "User email (address)."
    :group 'init
    :type 'string)
  (put 'init-user-email 'safe-local-variable #'stringp)
  ;;
  (defcustom init-user-url
    (concat "http://" mail-host-address)
    "User URL (address)."
    :group 'init
    :type 'string)
  (put 'init-user-url   'safe-local-variable #'stringp))
(after-init (init-user-setup))
;;
(provide 'init-user)
