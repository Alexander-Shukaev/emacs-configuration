(eval-when-compile
  (require 'init-package))
;;
(init-handler term prepare
  (init-load comint :no-error)
  (init-load shell  :no-error))
;;
(init-handler term init
  (init-load tramp :no-error)
  ;;
  (after-load tramp
    (setq-default term-prompt-regexp tramp-shell-prompt-pattern)))
;;
(init-handler term config)
;;
(init-package term
  :built-in)
