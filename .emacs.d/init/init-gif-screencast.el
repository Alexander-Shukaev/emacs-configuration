(eval-when-compile
  (require 'init-package))
;;
(init-handler gif-screencast init)
;;
(init-handler gif-screencast config)
;;
(init-handler gif-screencast mode-map
  (let ((keymap gif-screencast-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      "<XF86AudioPlay>" #'gif-screencast-toggle-pause
      "<XF86Eject>"     #'gif-screencast-stop)))
;;
(init-package gif-screencast)
