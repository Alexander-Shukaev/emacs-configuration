(eval-when-compile
  (require 'init-package))
;;
(init-handler elint init)
;;
(init-handler elint config)
;;
(init-package elint
  :built-in)
