(eval-when-compile
  (require 'init-package))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Try [1] instead of SSH.
;; ---------------------------------------------------------------------------
;; NOTE:
;;
;; As per [2], command `tramp-cleanup-connection' flushes remote connection,
;; what also cleans the password cache (see [3]) stored in variable
;; `password-data' (provided by `password-cache.el').
;; ---------------------------------------------------------------------------
;;
(init-handler tramp prepare
  (init-load shell :no-error))
;;
(init-handler tramp init
  (setq-default
    remote-file-name-inhibit-cache
    60; seconds
    ;; -----------------------------------------------------------------------
    tramp-auto-save-directory
    nil
    ;; -----------------------------------------------------------------------
    tramp-backup-directory-alist
    `((".*" . ,(abbreviate-file-name
                (expand-directory "tramp/" user-backup-directory))))
    ;; -----------------------------------------------------------------------
    tramp-completion-reread-directory-timeout
    nil
    ;; -----------------------------------------------------------------------
    ;; TODO:
    ;;
    ;; tramp-copy-size-limit
    ;; nil
    ;; -----------------------------------------------------------------------
    tramp-debug-buffer
    nil
    ;; -----------------------------------------------------------------------
    tramp-debug-to-file
    nil
    ;; -----------------------------------------------------------------------
    tramp-default-method
    (or (if (eq system-type 'windows-nt)
            (or (and (executable-find "pscp" ) "pscp" )
                (and (executable-find "plink") "plink"))
          (or (and (executable-find "ssh") "ssh")
              (and (executable-find "scp") "scp")))
        "ftp")
    ;; -----------------------------------------------------------------------
    ;; TODO: No file on 'sudo'?
    ;;
    tramp-histfile-override
    "~/.tramp_history"
    ;; -----------------------------------------------------------------------
    ;; TODO:
    ;;
    ;; tramp-inline-compress-start-size
    ;; nil
    ;; -----------------------------------------------------------------------
    ;; TODO: With 'sudo' has 'root:root' ownership:
    ;;
    tramp-persistency-file-name
    (abbrev-file-name "tramp" user-cache-directory)
    ;; -----------------------------------------------------------------------
    tramp-remote-path
    '(tramp-default-remote-path
      "/usr/local/sbin"
      "/usr/local/bin"
      "/usr/sbin"
      "/usr/bin"
      "/local/sbin"
      "/local/bin"
      "/sbin"
      "/bin")
    ;; -----------------------------------------------------------------------
    tramp-verbose
    3)
  (when (bound-and-true-p init-bb)
    (setq-default tramp-shell-prompt-pattern
      "[^]#$%>\n]*#?[]]?[#$%>] *\\(\e\\[[0-9;]*[a-zA-Z] *\\)*")))
;;
(init-handler tramp config
  (add-to-list 'tramp-default-proxies-alist
               '(nil "\\`root\\'" "/ssh:%h:"))
  (add-to-list 'tramp-default-proxies-alist
               `(,(regexp-quote (system-name)) nil nil))
  (add-to-list 'tramp-gvfs-methods
               "ftp")
  (add-to-list 'tramp-remote-path
               'tramp-own-remote-path)
  (when (bound-and-true-p init-bb)
    (setf tramp-methods (assoc-delete-all "tk" tramp-methods))
    (add-to-list 'tramp-methods
                 (cond ((init-wsl-p)
                        '("tk"
                          (tramp-login-program      "ssh")
                          (tramp-login-args         (("-l" "%u")
                                                     ("-p" "%p")
                                                     ("%c")
                                                     ("-e" "none")
                                                     ("%h")))
                          (tramp-async-args         (("-q")))
                          (tramp-direct-async       t)
                          (tramp-remote-shell       "/bin/bash")
                          (tramp-remote-shell-args  ("-c"))
                          (tramp-default-port       22)
                          (tramp-connection-timeout 30)))
                       ((and (eq system-type 'windows-nt)
                             (executable-find "plink"))
                        '("tk"
                          (tramp-login-program      "plink")
                          (tramp-login-args         (("-l" "%u")
                                                     ("-P" "%p")
                                                     ("-ssh")
                                                     ("-t")
                                                     ("10.117.4.48")
                                                     ("\"")
                                                     ("inline")
                                                     ("%h")
                                                     ("\"")))
                          (tramp-remote-shell       "/bin/bash")
                          (tramp-remote-shell-args  ("-c"))
                          (tramp-default-port       22)
                          (tramp-connection-timeout 10)))
                       (t
                        '("tk"
                          (tramp-login-program      "ssh")
                          (tramp-login-args         (("-l" "%u")
                                                     ("-p" "%p")
                                                     ("%c")
                                                     ("-e" "none")
                                                     ("-t")
                                                     ("10.117.66.29")
                                                     ("\"")
                                                     ("inline")
                                                     ("%h")
                                                     ("\"")))
                          (tramp-remote-shell       "/bin/bash")
                          (tramp-remote-shell-args  ("-c"))
                          (tramp-default-port       22)
                          (tramp-connection-timeout 30))))))
  ;;
  (init-defad tramp-open-shell
      (:around (function vec &rest ...) init/:around)
    #$
    (let ((tramp-remote-process-environment
           (if (and (equal (tramp-file-name-method vec) "sudo")
                    (equal (tramp-file-name-host   vec) (system-name)))
               (prog1
                   process-environment
                 (message "Overridden (locally) \
the default value of variable `%s' (by the current value of variable `%s'):
- current: %s
- default: %s"
                          'tramp-remote-process-environment
                          'process-environment
                          process-environment
                          tramp-remote-process-environment))
             tramp-remote-process-environment)))
      (apply function vec ...))))
;;
(init-package tramp
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://mosh.org'
;;  [2] Info node `(tramp) Cleanup remote connections'
;;  [3] Info node `(tramp) Password handling'
;;  ==========================================================================
;;  }}} References
