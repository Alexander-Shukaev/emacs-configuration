(eval-when-compile
  (require 'init-package))
;;
(init-handler xwidget init)
;;
(init-handler xwidget config
  (setenv "SNAP"          "1")
  (setenv "SNAP_NAME"     "1")
  (setenv "SNAP_REVISION" "1")
  ;;
  (init-defun init-xwidget-webkit-mode-setup ()
    #$
    (setq-local  left-fringe-width 0)
    (setq-local right-fringe-width 0)
    (set-window-buffer (selected-window) (current-buffer)))
  (add-hook 'xwidget-webkit-mode-hook #'init-xwidget-webkit-mode-setup))
;;
(init-package xwidget
  :built-in
  :when
  (featurep 'xwidget-internal))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] `view-emacs-problems'
;;  ==========================================================================
;;  }}} References
