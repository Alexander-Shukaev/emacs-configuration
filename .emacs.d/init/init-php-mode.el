(eval-when-compile
  (require 'init-package))
;;
(init-handler php-mode init
  (setq-default php-mode-coding-style 'wordpress))
;;
(init-handler php-mode config
  (require 'php-extras nil :no-error)
  ;;
  (after-load php-extras
    (init-call php-mode map))
  ;;
  (init-defun init-php-mode-wordpress-setup ()
    #$
    (setq-local indent-tabs-mode nil))
  (add-hook 'php-mode-wordpress-hook #'init-php-mode-wordpress-setup)
  ;;
  (init-defun init-php-mode-setup ()
    #$
    (setq-local require-final-newline t))
  (add-hook 'php-mode-hook #'init-php-mode-setup)
  ;;
  (after-load init-ac-php-core
    (add-hook 'php-mode-hook #'ac-php-core-eldoc-setup))
  ;;
  (after-load (init-company-php company)
    (init-defun init-php-mode-company-setup ()
      #$
      (make-local-variable 'company-backends)
      (if (bound-and-true-p php-mode)
          (add-to-list 'company-backends #'company-ac-php-backend)
        (setq-local company-backends (delq #'company-ac-php-backend
                                           company-backends))))
    (add-hook 'php-mode-hook #'init-php-mode-company-setup)))
;;
(init-handler php-mode map
  (let ((keymap php-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap)))
;;
(init-package php-mode
  :unless
  (bound-and-true-p init-bb))
