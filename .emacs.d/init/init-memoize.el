(eval-when-compile
  (require 'init-package))
;;
(init-handler memoize init
  (setq-default memoize-default-timeout "24 hours"))
;;
(init-handler memoize config
  (put 'defmemoize 'lisp-indent-function 2))
;;
(init-package memoize)
