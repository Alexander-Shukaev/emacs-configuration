(eval-when-compile
  (require 'init-package))
;;
(init-handler ycmd preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler ycmd prepare
  (init-load ycmd-eldoc :no-error)
  ;;
  (defun init-ycmd-setup ()
    (let (debug-on-error)
      (with-demoted-errors "ycmd: error: %s"
        (global-ycmd-mode)))))
;;
(init-handler ycmd init
  (setq-default
    ycmd-extra-conf-whitelist      '("~/*")
    ycmd-force-semantic-completion nil
    ycmd-global-config             "~/.ycm_extra_conf.py"
    ;; -----------------------------------------------------------------------
    ycmd-global-modes              t
    ;;
    ;; CAUTION:
    ;;
    ;; It is tempting to set `ycmd-global-modes' as
    ;;
    ;; '(not emacs-lisp-mode)
    ;;
    ;; but turns out to be error-prone because it means that the reasonable
    ;; default of `ycmd-file-type-map' is no longer considered at all and one
    ;; ends up having `ycmd-mode' turned on everywhere except for
    ;; `emacs-lisp-mode'.  This is clearly unacceptable since it can
    ;; potentially lead to terrible performance issues (including occasional
    ;; stutters and/or complete freezes) and communication corruption between
    ;; Emacs and YCMD server due to obvious parsing errors.  As a result,
    ;; prefer
    ;;
    ;;   (assq-delete-all 'emacs-lisp-mode ycmd-file-type-map)
    ;;
    ;; as done further.
    ;; -----------------------------------------------------------------------
    ycmd-request-log-level         'verbose
    ycmd-request-message-level     'verbose
    ;; TODO:
    ycmd-server-command `(,(cond ((bound-and-true-p init-bb)
                                  "python3")
                                 (t
                                  "python"))
                          ,(cond ((memq system-type '(cygwin windows-nt))
                                  "C:/Tools/x64/YCMD/ycmd/")
                                 ((bound-and-true-p init-bb)
                                  "/usr/local/share/ycmd/ycmd/")
                                 (t
                                  "/usr/share/vim/vimfiles/third_party/ycmd/ycmd/"))))
  (when (bound-and-true-p init-bb)
    (add-to-list 'ycmd-extra-conf-whitelist "/workspace/users/ashukaev/*"))
  ;;
  (after-init (init-ycmd-setup)))
;;
(init-handler ycmd config
  (dolist (mode '(emacs-lisp-mode php-mode))
    (assq-delete-all mode ycmd-file-type-map))
  ;;
  (when (> emacs-major-version 27)
    (init-assert (fboundp 'ycmd--encode-string))
    (init-defad ycmd--encode-string
        (:override (s) init/:override)
      #$
      (encode-coding-string s 'utf-8)))
  ;;
  (init-defad ycmd--maybe-enable-mode
      (:around (function &rest ...) init/:around)
    #$
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; Good sources for inspiration on various interesting criteria to
    ;; implement or customize the `turn-on' function for a global minor mode
    ;; (see `define-globalized-minor-mode') are functions `company-mode-on',
    ;; `flycheck-may-enable-mode', and `ycmd--maybe-enable-mode'.
    ;; -----------------------------------------------------------------------
    (when (and (not noninteractive)
               (init-interesting-buffer-p)
               ;; ------------------------------------------------------------
               ;; BUG:
               ;;
               ;; It does not look right that `ycmd--get-basic-request-data'
               ;; may return a request with property 'filepath' set to the
               ;; empty string in case of non-file-visiting buffer.  Thus,
               ;; explicitly allow/enable/turn on `ycmd-mode' only in
               ;; file-visiting buffers:
               ;;
               (buffer-file-name)
               ;; ------------------------------------------------------------
               (not (file-remote-p (buffer-file-name))))
      (apply function ...)))
  ;;
  (init-defad ycmd--command-request
      (:around (function &rest ...) init/:around)
    #$
    (when (bound-and-true-p ycmd-mode)
      (apply function ...)))
  ;;
  (init-defad ycmd-get-completions
      (:around (function &rest ...) init/:around)
    #$
    (when (bound-and-true-p ycmd-mode)
      (apply function ...)))
  ;;
  ;; -------------------------------------------------------------------------
  (init-defad ycmd--send-completer-available-request
      (:around (function &rest ...) init/:around)
    #$
    (when (bound-and-true-p ycmd-mode)
      (apply function ...)))
  ;;
  ;; BUG:
  ;;
  ;; The following code in `ycmd--send-completer-available-request',
  ;;
  ;;   (when (consp file-types)
  ;;     (setcdr file-types (ycmd-major-mode-to-file-types mode)))
  ;;
  ;; ... is a genuine bug because it may set property 'filetypes' to 'null',
  ;; while as per function '_MissingFieldsForFileData' in [1], it should be an
  ;; array of at least one element.
  ;; -------------------------------------------------------------------------
  ;;
  (init-defad ycmd-semantic-completer-available-p
      (:around (function &rest ...) init/:around)
    #$
    (when (bound-and-true-p ycmd-mode)
      (apply function ...)))
  ;;
  (after-load init-ycmd-eldoc
    (init-defun init-ycmd-eldoc-setup ()
      #$
      (if (bound-and-true-p ycmd-mode)
          (ycmd-eldoc-mode)
        (when (bound-and-true-p ycmd-eldoc-mode)
          (ycmd-eldoc-mode -1))))
    (add-hook 'ycmd-mode-hook #'init-ycmd-eldoc-setup))
  ;;
  (after-load (init-company-ycmd company)
    (init-defun init-ycmd-company-setup ()
      #$
      (make-local-variable 'company-backends)
      (if (bound-and-true-p ycmd-mode)
          (company-ycmd-setup)
        (setq-local company-backends (delq #'company-ycmd company-backends))))
    (add-hook 'ycmd-mode-hook #'init-ycmd-company-setup))
  ;;
  (after-load (init-flycheck-ycmd flycheck)
    (init-defun init-ycmd-flycheck-setup ()
      #$
      (make-local-variable 'flycheck-checkers)
      (if (bound-and-true-p ycmd-mode)
          (flycheck-ycmd-setup)
        (setq-local flycheck-checkers (delq 'ycmd flycheck-checkers))))
    (add-hook 'ycmd-mode-hook #'init-ycmd-flycheck-setup)))
;;
(init-handler ycmd command-map
  (let ((keymap ycmd-command-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap)))
;;
(init-handler ycmd mode-map
  (let ((keymap ycmd-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      [remap find-function-at-point] #'ycmd-goto
      [remap find-variable-at-point] #'ycmd-goto))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd ycmd-mode-map
      :motion
      [remap find-function-at-point] #'ycmd-goto
      [remap find-variable-at-point] #'ycmd-goto)))
;;
(init-package ycmd
  :diminish
  (ycmd-mode
   global-ycmd-mode))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `file:///usr/share/vim/vimfiles/third_party/ycmd/ycmd/request_validation.py'
;;  ==========================================================================
;;  }}} References
