(eval-when-compile
  (require 'init-package))
;;
(init-handler wgrep preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler wgrep init
  (setq-default
    wgrep-auto-save-buffer t
    wgrep-enable-key       (kbd "C-w"))
  ;;
  (after-load grep
    (setq-default wgrep-line-file-regexp (replace-regexp-in-string
                                          "?2:" "?3:"
                                          (replace-regexp-in-string
                                           "?3:" "?:"
                                           (car (car grep-regexp-alist))
                                           t :literal)
                                          t :literal))))
;;
(init-handler wgrep config
  (after-load (init-devil devil)
    (init-defun init-wgrep-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "wg[rep]"   #'wgrep-change-to-wgrep-mode
        ;;
        "finish"    #'wgrep-finish-edit
        ;;
        "c[ommit]"  "finish"
        "w[rite]"   "finish"
        ;;
        "cancel"    #'wgrep-abort-changes
        ;;
        "a[bort]"   "cancel"
        "bd[elete]" "cancel"
        "bk[ill]"   "cancel"
        ;;
        ;; TODO: Is this conceptually valid?
        "q[uit]"    #'wgrep-exit))
    (add-hook 'wgrep-setup-hook #'init-wgrep-devil-setup))
  ;;
  (after-load (init-evil evil)
    (advice-add #'wgrep-change-to-wgrep-mode :after #'evil-normal-state)
    (advice-add #'wgrep-to-original-mode     :after #'evil-motion-state)))
;;
(init-handler wgrep mode-map
  (let ((keymap wgrep-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap))
  ;;
  (init-defad wgrep-change-to-wgrep-mode
      (:before (&rest ...) init/:before)
    #$
    (set-keymap-parent wgrep-mode-map wgrep-original-mode-map))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd wgrep-mode-map
      :motion
      "q"          #'wgrep-exit
      ;;
      :normal
      "  <delete>" #'wgrep-mark-deletion
      "<S-delete>" #'wgrep-mark-deletion
      ;;
      "c"          #'wgrep-remove-change
      "C"          #'wgrep-remove-all-change)))
;;
(init-package wgrep)
