(eval-when-compile
  (require 'init-package))
;;
(init-handler evil init
  (after-init (evil-mode)))
;;
(init-handler evil config)
;;
(init-package evil)
