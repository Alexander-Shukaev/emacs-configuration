(eval-when-compile
  (require 'init-package))
;;
(init-handler eshell init)
;;
(init-handler eshell config)
;;
(init-package eshell
  :built-in)
