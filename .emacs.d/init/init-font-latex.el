(eval-when-compile
  (require 'init-package))
;;
(init-handler font-latex init
  (setq-default
    font-latex-match-reference-keywords-local    nil
    font-latex-match-sectioning-0-keywords-local nil
    font-latex-match-sectioning-1-keywords-local nil
    font-latex-match-sectioning-2-keywords-local nil
    font-latex-match-sectioning-3-keywords-local nil
    font-latex-match-sectioning-4-keywords-local nil
    font-latex-match-sectioning-5-keywords-local nil)
  ;;
  (setq-default
    font-latex-match-warning-keywords
    '(("autoinsertnextspace" "")
      ("begingroup"          "")
      ("bgroup"              "")
      ("blank"               "")
      ("crlf"                "")
      ("def"                 "\\")
      ("define"              "")
      ("dimexpr"             "\\")
      ("displaystyle"        "")
      ("egroup"              "")
      ("endgroup"            "")
      ("expandafter"         "\\")
      ("left"                "")
      ("let"                 "\\")
      ("nowhitespace"        "")
      ("placebookmarks"      "")
      ("placecombinedlist"   "")
      ("placecontent"        "")
      ("placefigure"         "")
      ("placeformula"        "")
      ("placepublications"   "")
      ("placetable"          "")
      ("protect"             "")
      ("protected"           "\\")
      ("relax"               "")
      ("right"               "")
      ("space"               "")
      ("startplacefigure"    "")
      ("startplaceformula"   "")
      ("startplacetable"     "")
      ("stopplacefigure"     "")
      ("stopplaceformula"    "")
      ("stopplacetable"      "")
      ("the"                 "\\")
      ("unprotect"           "")
      ("unskip"              ""))
    ;; font-latex-match-function-keywords
    ;; '(("defineconversion"       "")
    ;;   ("definehead"             "")
    ;;   ("definemakeup"           "")
    ;;   ("definemathmatrix"       "")
    ;;   ("definereferenceformat"  "")
    ;;   ("definesymbol"           "")
    ;;   ("definesynonyms"         "")
    ;;   ("definetabulate"         "")
    ;;   ("definetyping"           "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("setupTABLE"             "")
    ;;   ("setupalign"             "")
    ;;   ("setupbar"               "")
    ;;   ("setupbibtex"            "")
    ;;   ("setupblank"             "")
    ;;   ("setupbodyfont"          "")
    ;;   ("setupcaption"           "")
    ;;   ("setupcaptions"          "")
    ;;   ("setupcite"              "")
    ;;   ("setupcombinations"      "")
    ;;   ("setupcombinedlist"      "")
    ;;   ("setupdocument"          "")
    ;;   ("setupexternalfigures"   "")
    ;;   ("setupformulas"          "")
    ;;   ("setupformulas"          "")
    ;;   ("setuphead"              "")
    ;;   ("setupindenting"         "")
    ;;   ("setupinteraction"       "")
    ;;   ("setupinterlinespace"    "")
    ;;   ("setupitemize"           "")
    ;;   ("setuplabeltext"         "")
    ;;   ("setuplayout"            "")
    ;;   ("setuplinenumbering"     "")
    ;;   ("setuplist"              "")
    ;;   ("setupmakeup"            "")
    ;;   ("setupmathematics"       "")
    ;;   ("setuppagenumbering"     "")
    ;;   ("setuppapersize"         "")
    ;;   ("setuppublicationlayout" "")
    ;;   ("setuppublicationlist"   "")
    ;;   ("setuppublications"      "")
    ;;   ("setupquotation"         "")
    ;;   ("setupsynonyms"          "")
    ;;   ("setuptabulate"          "")
    ;;   ("setuptyping"            "")
    ;;   ("setupurl"               "")
    ;;   ("setupwhitespace"        "")
    ;;   ;; -----------------------------------------------------------------
    ;;   (    "language"           "")
    ;;   ("mainlanguage"           "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("setmainfont"            "")
    ;;   ("setmathfont"            "")
    ;;   ("setmonofont"            "")
    ;;   ("setsansfont"            "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("switchtobodyfont"       "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("framed"                 "")
    ;;   ("normal"                 "")
    ;;   ("scale"                  "")
    ;;   ("symbol"                 "")
    ;;   ("underbar"               "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("doifnextcharelse"       "")
    ;;   ("doifsomethingelse"      "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("dosingleargument"       "")
    ;;   ("dodoubleargument"       "")
    ;;   ("dotripleargument"       "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("tikzset"                "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("usemodule"              "")
    ;;   ("usetikzlibrary"         ""))
    ;; font-latex-match-variable-keywords
    ;; '(("documentvariable"        "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("currentdate"             "")
    ;;   ;; -----------------------------------------------------------------
    ;;   (   "combinationparameter" "")
    ;;   ("externalfigureparameter" "")
    ;;   (           "urlparameter" "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("delimiterfactor"         "")
    ;;   ("nulldelimiterspace"      "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("backspace"               "")
    ;;   ( "topspace"               "")
    ;;   ("paperheight"             "")
    ;;   ("paperwidth"              "")
    ;;   ( "textheight"             "")
    ;;   ( "textwidth"              "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("everymath"               ""))
    ;; font-latex-match-reference-keywords
    ;; '(("cite" "")
    ;;   ("from" ""))
    ;; font-latex-match-math-command-keywords
    ;; '(("math"           "")
    ;;   ("mathbb"         "")
    ;;   ("mathcal"        "")
    ;;   ("mathss"         "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("langle"         "")
    ;;   ("mfunction"      "")
    ;;   ("nabla"          "")
    ;;   ("rangle"         "")
    ;;   ("text"           "")
    ;;   ("vec"            "")
    ;;   ;; -----------------------------------------------------------------
    ;;   ("integers"       "")
    ;;   ("naturalnumbers" "")
    ;;   ("reals"          ""))
    font-latex-match-sectioning-0-keywords
    '(("part" "[{"))
    font-latex-match-sectioning-1-keywords
    '(("chapter" "[{")
      ("title"   "[{"))
    font-latex-match-sectioning-2-keywords
    '(("section" "[{")
      ("subject" "[{"))
    font-latex-match-sectioning-3-keywords
    '(("subsection" "[{")
      ("subsubject" "[{"))
    font-latex-match-sectioning-4-keywords
    '(("subsubsection" "[{")
      ("subsubsubject" "[{"))))
;;
(init-handler font-latex config
  (face-spec-reset-face 'font-latex-sedate-face)
  (set-face-attribute   'font-latex-sedate-face nil
                        :inherit 'font-lock-keyword-face))
;;
(init-package font-latex
  :ensure auctex)
