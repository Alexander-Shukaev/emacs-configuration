(eval-when-compile
  (require 'init-package))
;;
(init-handler ac-php-core init)
;;
(init-handler ac-php-core config)
;;
(init-package ac-php-core
  :unless
  (bound-and-true-p init-bb))
