(eval-when-compile
  (require 'init-package))
;;
(init-handler epa prepare
  (init-load epg :no-error)
  ;;
  (define-minor-mode epa-armor-mode
    "\
Toggle ASCII armored output in the current buffer.

With a prefix argument ARG, enable the mode if ARG is positive,
and disable it otherwise.  If called from Lisp, enable the mode
if ARG is omitted or nil.

See `epa-armor'."
    :group 'epa
    (require 'epa)
    (if (bound-and-true-p epa-armor-mode)
        (setq-local epa-armor t)
      (kill-local-variable 'epa-armor))))
;;
(init-handler epa init
  (setq-default epa-pinentry-mode 'loopback))
;;
(init-handler epa config
  (put 'epa-armor 'permanent-local t))
;;
(init-package epa
  :built-in
  :variables
  (epa-armor))
