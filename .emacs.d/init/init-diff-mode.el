(eval-when-compile
  (require 'init-package))
;;
(init-handler diff-mode preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler diff-mode prepare
  (defcustom init-diff-mode-face-spec-alist
    nil
    "Association list of face specifications in Diff mode."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type list))
  ;;
  (defcustom init-diff-mode-face-alias-alist
    '((diff-header            . magit-diff-hunk-heading)
      (diff-context           . magit-diff-context)
      (diff-added             . magit-diff-added)
      (diff-removed           . magit-diff-removed)
      (diff-changed           . smerge-refined-changed)
      (diff-refine-added      . magit-diff-added-highlight)
      (diff-refine-removed    . magit-diff-removed-highlight)
      (diff-refine-changed    . ediff-fine-diff-C)
      (diff-indicator-added   . magit-diffstat-added)
      (diff-indicator-removed . magit-diffstat-removed))
    "Association list of face aliases in Diff mode."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type symbol)))
;;
(init-handler diff-mode init)
;;
(init-handler diff-mode config
  (add-hook 'diff-mode-hook #'diff-auto-refine-mode)
  ;;
  (after-load (init-solarized-theme solarized-theme magit-diff)
    (init-defun init-diff-mode-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (dolist (entry init-diff-mode-face-spec-alist)
          (let ((face (car entry))
                (spec (cdr entry)))
            (put face 'theme-face nil)
            (face-spec-set face spec)))
        (dolist (entry init-diff-mode-face-alias-alist)
          (let ((face  (car entry))
                (alias (cdr entry)))
            (put face 'theme-face nil)
            (put face 'face-alias alias)))))
    (init-diff-mode-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...)
                init-diff-mode-solarized-theme-setup/:after)
      #$
      (init-diff-mode-solarized-theme-setup theme))))
;;
(init-handler diff-mode map
  (init-clear-keymap diff-mode-map))
;;
(init-handler diff-mode minor-mode-map
  (init-clear-keymap diff-minor-mode-map))
;;
(init-package diff-mode
  :built-in)
