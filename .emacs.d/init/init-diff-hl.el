(eval-when-compile
  (require 'init-package))
;;
(init-handler diff-hl preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler diff-hl prepare
  (init-load diff-mode)
  ;;
  (defcustom init-diff-hl-face-spec-alist
    nil
    "Association list of face specifications in Diff Hl."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type list))
  ;;
  (defcustom init-diff-hl-face-alias-alist
    '((diff-hl-insert . magit-diff-added)
      (diff-hl-delete . magit-diff-removed)
      (diff-hl-change . ediff-current-diff-C))
    "Association list of face aliases in Diff Hl."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type symbol)))
;;
(init-handler diff-hl init
  (after-init (global-diff-hl-mode)))
;;
(init-handler diff-hl config
  (after-load (init-solarized-theme solarized-theme magit-diff)
    (init-defun init-diff-hl-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (dolist (entry init-diff-hl-face-spec-alist)
          (let ((face (car entry))
                (spec (cdr entry)))
            (put face 'theme-face nil)
            (face-spec-set face spec)))
        (dolist (entry init-diff-hl-face-alias-alist)
          (let ((face  (car entry))
                (alias (cdr entry)))
            (put face 'theme-face nil)
            (put face 'face-alias alias)))))
    (init-diff-hl-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...)
                init-diff-hl-solarized-theme-setup/:after)
      #$
      (init-diff-hl-solarized-theme-setup theme))))
;;
(init-package diff-hl
  ;; TODO:
  :disabled)
