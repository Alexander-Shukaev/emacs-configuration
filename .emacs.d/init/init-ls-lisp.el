(eval-when-compile
  (require 'init-package))
;;
(init-handler ls-lisp init
  (setq-default
    ls-lisp-use-insert-directory-program (and (executable-find "ls") t)))
;;
(init-handler ls-lisp config)
;;
(init-package ls-lisp
  :built-in)
