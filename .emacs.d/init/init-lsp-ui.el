(eval-when-compile
  (require 'init-package))
;;
(init-handler lsp-ui prepare
  (init-load lsp-mode :no-error))
;;
(init-handler lsp-ui init)
;;
(init-handler lsp-ui config)
;;
(init-package lsp-ui)
