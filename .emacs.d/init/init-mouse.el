(eval-when-compile
  (require 'init-package))
;;
(init-handler mouse init
  (setq-default
    mouse-autoselect-window t
    ;;
    ;; As per [1]:
    mouse-drag-copy-region  t
    mouse-yank-at-point     t))
;;
(init-handler mouse config)
;;
(init-package mouse
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(emacs) Clipboard'
;;  ==========================================================================
;;  }}} References
