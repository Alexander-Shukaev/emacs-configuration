(eval-when-compile
  (require 'init-package))
;;
;; ---------------------------------------------------------------------------
;; BUG:
;;
;; It is expected that `global-mode-string' is either nil or list, otherwise
;; `display-battery-mode' signals error with the following message:
;;
;;   display-battery-mode: Wrong type argument: listp, ...
;; ---------------------------------------------------------------------------
;;
(init-handler battery init)
;;
(init-handler battery config
  (init-defad battery-linux-proc-apm
      (:around (function &rest ...) init/:around)
    #$
    (let (debug-on-signal)
      (apply function ...)))
  ;;
  (init-defad battery-linux-proc-acpi
      (:around (function &rest ...) init/:around)
    #$
    (let (debug-on-signal)
      (apply function ...)))
  ;;
  (init-defad battery-linux-sysfs
      (:around (function &rest ...) init/:around)
    #$
    (let (debug-on-signal)
      (apply function ...)))
  ;;
  (init-defad battery-pmset
      (:around (function &rest ...) init/:around)
    #$
    (let (debug-on-signal)
      (apply function ...)))
  ;;
  (init-defad battery-search-for-one-match-in-files
      (:around (function &rest ...) init/:around)
    #$
    (let (debug-on-signal)
      (apply function ...))))
;;
(init-package battery
  :built-in)
