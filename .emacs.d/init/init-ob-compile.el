(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-compile prepare
  (init-load ob :no-error))
;;
(init-handler ob-compile init
  (after-load org
    (dolist (language '((compile . t)))
      (add-to-list 'org-babel-load-languages language))))
;;
(init-handler ob-compile config)
;;
(init-package ob-compile)
