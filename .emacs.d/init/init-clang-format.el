(eval-when-compile
  (require 'init-package))
;;
(init-handler clang-format init)
;;
(init-handler clang-format config)
;;
(init-package clang-format)
