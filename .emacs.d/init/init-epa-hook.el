(eval-when-compile
  (require 'init-package))
;;
(init-handler epa-hook init
  (setq-default
    auto-encryption-mode       nil
    epa-file-inhibit-auto-save t
    epa-file-name-regexp       (concat "\\.\\(asc\\|gpg\\|pgp\\)"
                                       "\\(~\\|\\.~[0-9]+~\\)?\\'")))
;;
(init-handler epa-hook config
  (init-assert (fboundp 'epa-file-find-file-hook))
  (fmakunbound 'epa-file-find-file-hook)
  (init-defun epa-file-find-file-hook ()
    #$
    (when (and epa-file-inhibit-auto-save
               buffer-file-name
               (string-match-p epa-file-name-regexp buffer-file-name))
      (auto-save-mode -1)))
  ;;
  (init-assert (fboundp 'epa-file-name-regexp-update))
  (fmakunbound 'epa-file-name-regexp-update)
  (init-defun epa-file-name-regexp-update ()
    #$
    (interactive)
    (setcar epa-file-handler               epa-file-name-regexp)
    (setcar epa-file-auto-mode-alist-entry epa-file-name-regexp))
  ;;
  (when (bound-and-true-p auto-encryption-mode)
    (auto-encryption-mode -1))
  ;;
  (epa-file-name-regexp-update)
  ;;
  (after-init (epa-file-name-regexp-update))
  (after-init (auto-encryption-mode)))
;;
(init-package epa-hook
  :built-in)
