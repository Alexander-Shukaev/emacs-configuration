(eval-when-compile
  (require 'init-package))
;;
(init-handler org-superstar init)
;;
(init-handler org-superstar config)
;;
(init-package org-superstar
  :unless
  (version< emacs-version "26.1")
  :when
  (bound-and-true-p inhibit-compacting-font-caches))
