(eval-when-compile
  (require 'init-package))
;;
(init-handler minibuffer preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler minibuffer prepare
  (defcustom init-minibuffer-gc-cons-threshold-mode
    t
    "\
If non-nil, use `minibuffer-gc-cons-threshold-mode'."
    :group 'init
    :type 'boolean)
  ;;
  (defcustom minibuffer-gc-high-cons-threshold
    (max (* 100 (or (bound-and-true-p default-gc-cons-threshold) 0))
         ;; Hexadecimal: 10000000
         (eval-when-compile (* 64 1024 1024)))
    "\
Value of `gc-cons-threshold' used by `minibuffer-gc-cons-threshold-mode'.

See `minibuffer-gc-cons-threshold-setup'."
    :group 'minibuffer
    :type 'integer)
  ;;
  (defvar-local minibuffer-gc-cons-threshold
    nil
    "\
Cached latest buffer-local value of `gc-cons-threshold'.

See `minibuffer-gc-cons-threshold-setup'
and `minibuffer-gc-cons-threshold-exit'.")
  (put 'minibuffer-gc-cons-threshold 'permanent-local t)
  ;;
  (defun minibuffer-gc-cons-threshold-setup ()
    "\
Set `gc-cons-threshold' to the value of `minibuffer-gc-high-cons-threshold'.

See `minibuffer-gc-cons-threshold'."
    (setq-local minibuffer-gc-cons-threshold gc-cons-threshold)
    (setq gc-cons-threshold (max gc-cons-threshold
                                 minibuffer-gc-high-cons-threshold)))
  ;;
  (defun minibuffer-gc-cons-threshold-exit ()
    "\
Set `gc-cons-threshold' to the default value (i.e. restore).

See `minibuffer-gc-cons-threshold'."
    (when (and (bound-and-true-p  minibuffer-gc-cons-threshold)
               (local-variable-p 'minibuffer-gc-cons-threshold))
      (when (= gc-cons-threshold minibuffer-gc-high-cons-threshold)
        (setq gc-cons-threshold minibuffer-gc-cons-threshold))
      (kill-local-variable 'minibuffer-gc-cons-threshold)))
  ;;
  (define-minor-mode minibuffer-gc-cons-threshold-mode
    ;; -----------------------------------------------------------------------
    ;; TODO:
    ;;
    ;; Compatibility with `gcmh-mode' (can both be active at the same time?)
    ;; or error with `define-advice' applied to both.
    ;; -----------------------------------------------------------------------
    "\
Toggle value of `gc-cons-threshold' when minibuffer is active.

With a prefix argument ARG, enable the mode if ARG is positive,
and disable it otherwise.  If called from Lisp, enable the mode
if ARG is omitted or nil.

See `minibuffer-gc-cons-threshold-setup'
and `minibuffer-gc-cons-threshold-exit'."
    :group 'minibuffer
    :global t
    (require 'minibuffer)
    (if (bound-and-true-p minibuffer-gc-cons-threshold-mode)
        (progn
          (add-hook 'minibuffer-setup-hook
                    #'minibuffer-gc-cons-threshold-setup)
          (add-hook 'minibuffer-exit-hook
                    #'minibuffer-gc-cons-threshold-exit)
          (advice-add #'abort-recursive-edit
                      :after
                      #'minibuffer-gc-cons-threshold-exit
                      '((depth . -100))))
      (remove-hook 'minibuffer-setup-hook
                   #'minibuffer-gc-cons-threshold-setup)
      (remove-hook 'minibuffer-exit-hook
                   #'minibuffer-gc-cons-threshold-exit)
      (advice-remove #'abort-recursive-edit
                     #'minibuffer-gc-cons-threshold-exit))))
;;
(init-handler minibuffer init
  (init-load gc)
  ;;
  (setq-default
    completion-auto-help              'lazy
    enable-recursive-minibuffers      t
    history-delete-duplicates         t
    history-length                    t
    ;; -----------------------------------------------------------------------
    minibuffer-auto-raise             (not (bound-and-true-p
                                            x-wait-for-event-timeout))
    ;;
    ;; NOTE:
    ;;
    ;; As described in Emacs bug #29095, `minibuffer-auto-raise' will cause
    ;; delays of `x-wait-for-event-timeout' when set and substantially
    ;; slowdown the initialization (startup) process of Emacs.  Hence, make
    ;; sure that `minibuffer-auto-raise' is nil if `x-wait-for-event-timeout'
    ;; is bound and non-nil.
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; As noted in [1], `minibuffer-completing-file-name' is not supposed to
    ;; be set from Emacs Lisp as it is an internal variable used in the
    ;; `minibuffer.c' source file of Emacs to determine whether to use
    ;; `minibuffer-local-filename-completion-map' or
    ;; `minibuffer-local-completion-map'.  It is only supposed to be read from
    ;; Emacs Lisp, albeit onset to be deprecated as its exposure to Emacs Lisp
    ;; as a writable variable is dangerous and smells, e.g. can be a source of
    ;; weird bugs (see [1]).
    ;; -----------------------------------------------------------------------
    minibuffer-gc-high-cons-threshold init-gc-high-cons-threshold
    minibuffer-prompt-properties      '(read-only
                                        t
                                        cursor-intangible
                                        t
                                        face
                                        minibuffer-prompt)
    resize-mini-windows               t)
  ;;
  (after-load init-gcmh
    (setq-default init-minibuffer-gc-cons-threshold-mode nil))
  ;;
  (init-assert (>= minibuffer-gc-high-cons-threshold
                   (or (bound-and-true-p default-gc-cons-threshold) 0)))
  ;;
  (when init-minibuffer-gc-cons-threshold-mode
    (after-init
      (when init-minibuffer-gc-cons-threshold-mode
        (minibuffer-gc-cons-threshold-mode)))))
;;
(init-handler minibuffer config
  (init-defun minibuffer-erase-exit ()
    #$
    (unless (> (minibuffer-depth) 1)
      (init-assert (minibufferp))
      (let ((inhibit-read-only t))
        (erase-buffer))))
  (add-hook 'minibuffer-exit-hook #'minibuffer-erase-exit)
  (advice-add #'abort-recursive-edit
              :after
              #'minibuffer-erase-exit
              '((depth . -100)))
  ;;
  (init-defun minibuffer-inactive-mode-setup ()
    #$
    ;; Make `try-expand-dabbrev' from `hippie-expand' work in minibuffer by
    ;; redefining syntax for '/' as per `he-dabbrev-beg':
    (set-syntax-table (let ((table (make-syntax-table)))
                        (modify-syntax-entry ?/ "." table)
                        table)))
  (add-hook 'minibuffer-inactive-mode-hook #'minibuffer-inactive-mode-setup)
  ;;
  (init-defad abort-recursive-edit
      ;; TODO: I don't understand why `:after' is not run:
      (:before (&rest ...) init/:before)
    #$
    (let ((buffer (get-buffer "*Completions*")))
      (when (buffer-live-p buffer)
        (delete-windows-on buffer)
        (let (kill-buffer-query-functions)
          (kill-buffer buffer)))))
  ;;
  (after-load "bindings"
    (let ((keymap minibuffer-local-map))
      (init-assert (null (keymap-parent keymap)))
      (init-clear-keymap    keymap :recursively-p)
      (init-define-keys-kbd keymap
        "C-g" #'abort-recursive-edit
        ;;
        "<XF86Forward>" #'next-history-element
        "<XF86Back>   " #'previous-history-element
        ;;
        "<next> " #'next-complete-history-element
        "<prior>" #'previous-complete-history-element
        ;;
        "<down>" #'next-complete-history-element
        "<up>  " #'previous-complete-history-element
        ;;
        "<S-down>" #'next-matching-history-element
        "<S-up>  " #'previous-matching-history-element
        ;;
        "<C-down>" #'next-matching-history-element
        "<C-up>  " #'previous-matching-history-element
        ;;
        "RET" #'exit-minibuffer
        "LFD" #'exit-minibuffer
        ;;
        "TAB" #'self-insert-command))
    ;;
    (after-load (init-devil devil)
      (devil-define-keys-kbd minibuffer-local-map
        :motion
        "q" "C-g")))
  ;;
  (let ((keymap minibuffer-local-ns-map))
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map))
    (init-clear-keymap    keymap)
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map))
    (init-define-keys-kbd keymap
      "TAB" #'exit-minibuffer
      "SPC" #'exit-minibuffer
      ;;
      "?" #'self-insert-and-exit))
  ;;
  (let ((keymap minibuffer-local-completion-map))
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map))
    (init-clear-keymap    keymap)
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map))
    (init-define-keys-kbd keymap
      "TAB" #'minibuffer-complete
      "SPC" #'minibuffer-complete-word
      ;;
      "?" #'minibuffer-completion-help))
  ;;
  (let ((keymap minibuffer-local-must-match-map))
    (init-assert (eq (keymap-parent keymap) minibuffer-local-completion-map))
    (init-clear-keymap    keymap)
    (init-assert (eq (keymap-parent keymap) minibuffer-local-completion-map))
    (init-define-keys-kbd keymap
      "RET" #'minibuffer-complete-and-exit
      "LFD" #'minibuffer-complete-and-exit))
  ;;
  (let ((keymap minibuffer-local-isearch-map))
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map))
    (init-clear-keymap    keymap)
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map)))
  ;;
  (let ((keymap minibuffer-inactive-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd minibuffer-inactive-mode-map
      :motion
      "q" "C-g"))
  ;;
  (when (featurep 'delsel)
    (unload-feature 'delsel :force)
    (require 'delsel)))
;;
(init-package minibuffer
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/abo-abo/swiper/issues/1144'
;;  ==========================================================================
;;  }}} References
