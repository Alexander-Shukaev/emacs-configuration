(eval-when-compile
  (require 'init-package))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; In addition to 'systemd' service 'emacs.service' [1] that runs Emacs as a
;; daemon, implement 'systemd' socket 'emacs.socket' as per [2].
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Option '-T' provides a cool feature.
;; ---------------------------------------------------------------------------
;;
(init-handler server prepare
  (defun init-server-setup ()
    (if (eq system-type 'windows-nt)
        (server-start)
      (require 'server)
      ;; ---------------------------------------------------------------------
      ;; [RESOLVED] bug#31859:
      ;;
      (unless (or (process-live-p server-process)
                  (server-running-p))
        (server-start))
      ;; ---------------------------------------------------------------------
      )))
;;
(init-handler server init
  ;; -------------------------------------------------------------------------
  ;; CAUTION:
  ;;
  ;; As per [3], if either `server-auth-dir' or `server-socket-dir' (as per
  ;; `server-use-tcp') is set to a non-standard directory, or if `server-name'
  ;; is set to a non-standard absolute file name, 'emacsclient' needs to be
  ;; specified the corresponding absolute file name using either option '-f'
  ;; (or environment variable `EMACS_SERVER_FILE') to identify the server file
  ;; or option '-s' to identify the socket name as per [4] because the default
  ;; `server-auth-dir' and `server-socket-dir' are hard-coded in 'emacsclient'
  ;; to be used as the directories for resolving relative file names
  ;; (`server-name'), and 'emacsclient' does not even read or load
  ;; `user-init-file'.  Similarly, if only `server-name' is set to a
  ;; non-standard relative file name, 'emacsclient' needs to be specified the
  ;; same relative file name using either option '-f' (or environment variable
  ;; `EMACS_SERVER_FILE') to identify the server file or option '-s' to
  ;; identify the socket name as per [4].  To conclude, it appears wise, much
  ;; more user-friendly, and much less error-prone to avoid redundant
  ;; customization in order to leave all these variables intact (set to their
  ;; default values).
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; Review `server-socket-dir' again, now uses environment variable
  ;; `XDG_RUNTIME_DIR' if set.
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; Honestly, `xdg-runtime-dir' (or environment variable `XDG_RUNTIME_DIR'),
  ;; e.g. '/run/user/${UID}', appears to be far more idiomatic and
  ;; conventional value for the default `server-socket-dir' (and maybe even
  ;; `server-auth-dir').
  ;; -------------------------------------------------------------------------
  ;;
  (after-init (init-server-setup)))
;;
(init-handler server config)
;;
(init-package server
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `file:///etc/systemd/user/emacs.service'
;;  [2] Info node `(emacs) Emacs Server'
;;  [3] Info node `(emacs) TCP Emacs server'
;;  [4] Info node `(emacs) emacsclient Options'
;;  ==========================================================================
;;  }}} References
