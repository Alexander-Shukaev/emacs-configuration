(eval-when-compile
  (require 'init-package))
;;
(init-handler fill-column-indicator preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler fill-column-indicator init
  (setq-default
    fci-always-use-textual-rule t
    fci-handle-line-move-visual t
    fci-handle-truncate-lines   t
    ;;        Name: BOX DRAWINGS LIGHT VERTICAL
    ;;   Character: │
    ;; Hexadecimal: 2502
    fci-rule-character          #x2502
    fci-rule-width              1))
;;
(init-handler fill-column-indicator config
  (after-load (init-solarized-theme solarized-theme)
    (init-defun init-fci-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (setq-default fci-rule-color "#880000")))
    (init-fci-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...) init-fci-solarized-theme-setup/:after)
      #$
      (init-fci-solarized-theme-setup theme))))
;;
(init-package fill-column-indicator
  ;; -------------------------------------------------------------------------
  ;; DEPRECATED:
  ;;
  ;; Because of `display-fill-column-indicator' (Emacs 27.1):
  ;;
  :disabled
  ;;
  ;; Also sometimes slow and sluggish.  In particular, this is a performance
  ;; killer on large buffers, e.g. (thanks to `profiler-report'):
  ;;
  ;; - `redisplay_internal' (C function)                            59351  56%
  ;;  - `fci-update-window-for-scroll'                              59285  56%
  ;;   - `fci-redraw-window'                                        59285  56%
  ;;    - `fci-redraw-region'                                       59283  56%
  ;;       `fci-put-overlays-region'                                58367  55%
  ;;
  ;; and
  ;;
  ;; - `command-execute'                                            43059  40%
  ;;  - `call-interactively'                                        43059  40%
  ;;   - `funcall-interactively'                                    43059  40%
  ;;    - `counsel-M-x'                                             38137  36%
  ;;     - `ivy-read'                                               37904  36%
  ;;      - `read-from-minibuffer'                                  37593  35%
  ;;       - `ivy--minibuffer-setup'                                36139  34%
  ;;        - `ivy--exhibit'                                        36139  34%
  ;;         - `ivy--insert-minibuffer'                             36104  34%
  ;;          - `ivy--resize-minibuffer-to-fit'                     36104  34%
  ;;           - `window-resize'                                    36103  34%
  ;;            - `window--resize-mini-window'                      36103  34%
  ;;             - `run-window-configuration-change-hook'           36103  34%
  ;;              - `fci-redraw-frame'                              36102  34%
  ;;               - `fci-delete-unneeded'                          36040  34%
  ;; -------------------------------------------------------------------------
  )
