(eval-when-compile
  (require 'init-package))
;;
(init-handler longlines init
  (setq-default longlines-show-hard-newlines t))
;;
(init-handler longlines config
  (init-defad longlines-post-command-function
      (:around (function &rest ...) init/:around)
    #$
    (unless buffer-read-only
      (apply function ...))))
;;
(init-package longlines
  :built-in
  :diminish
  (longlines-mode))
