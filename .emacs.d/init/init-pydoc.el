(eval-when-compile
  (require 'init-package))
;;
(init-handler pydoc init)
;;
(init-handler pydoc config)
;;
(init-handler pydoc mode-map
  (let ((keymap pydoc-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-assert (null (keymap-parent keymap)))))
;;
(init-package pydoc)
