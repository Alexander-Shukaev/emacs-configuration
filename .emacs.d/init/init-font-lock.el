(eval-when-compile
  (require 'init-package))
;;
(init-handler font-lock prepare
  (init-negate (boundp 'font-lock-operator-face))
  (defvar font-lock-operator-face
    'font-lock-operator-face
    "Face name to use for operators.")
  ;;
  (if (> emacs-major-version 29)
      (init-assert (facep 'font-lock-operator-face))
    (init-negate (facep 'font-lock-operator-face))
    (defface font-lock-operator-face
      '((((class grayscale)
          (background light))
         :foreground "DimGray"
         :weight bold)
        (((class grayscale)
          (background dark))
         :foreground "LightGray"
         :weight bold)
        (((class color)
          (min-colors 88)
          (background light))
         :foreground "red")
        (((class color)
          (min-colors 88)
          (background dark))
         :foreground "red")
        (((class color)
          (min-colors 16)
          (background light))
         :foreground "red")
        (((class color)
          (min-colors 16)
          (background dark))
         :foreground "red")
        (((class color)
          (min-colors 8)
          (background light))
         :foreground "red")
        (((class color)
          (min-colors 8)
          (background dark))
         :foreground "red")
        (t
         :weight bold))
      "Font Lock mode face used to highlight operators."
      :group 'font-lock-faces)))
;;
(init-handler font-lock init
  (setq-default
    font-lock-maximum-decoration '((c-mode   . 2)
                                   (c++-mode . 2)
                                   (t        . t))
    font-lock-multiline          nil))
;;
(init-handler font-lock config
  (init-defad font-lock-fontify-region
      (:around (function &rest ...) init/:around)
    #$
    (let ((non-essential t))
      (apply function ...))))
;;
(init-package font-lock
  :built-in)
