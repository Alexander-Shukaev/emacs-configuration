(eval-when-compile
  (require 'init-package))
;;
(init-handler ediff-util init)
;;
(init-handler ediff-util config
  (init-inhibit ediff-util mode-map)
  ;;
  (init-defad ediff-setup-keymap
      (:after (&rest ...) init/:after)
    #$
    (init-ediff-util/:mode-map)))
;;
(init-handler ediff-util mode-map
  (let ((keymap ediff-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-define-keys-kbd keymap
      "TAB"       #'ediff-next-difference
      "<backtab>" #'ediff-previous-difference
      ;;
      "SPC"       #'ediff-next-difference
      "DEL"       #'ediff-previous-difference
      ;;
      "C-n"       #'ediff-next-difference
      "C-p"       #'ediff-previous-difference
      ;;
      "n"         #'ediff-next-difference
      "p"         #'ediff-previous-difference
      ;;
      "j"         nil
      "j"         "v"
      "k"         "V")))
;;
(init-package ediff-util
  :built-in)
