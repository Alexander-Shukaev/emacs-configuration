(eval-when-compile
  (require 'init-package))
;;
(init-handler gcmh init
  (init-load gc)
  ;;
  (setq-default
    gcmh-high-cons-threshold init-gc-high-cons-threshold
    gcmh-low-cons-threshold  init-gc-low-cons-threshold
    gcmh-idle-delay          (or (bound-and-true-p gc-idle-delay) 90); seconds
    gcmh-verbose             (bound-and-true-p garbage-collection-messages))
  ;;
  (after-init (gcmh-mode)))
;;
(init-handler gcmh config)
;;
(init-package gcmh
  ;; -------------------------------------------------------------------------
  ;; DEPRECATED:
  ;;
  ;; Because of `gc-idle-garbage-collect':
  ;;
  :disabled
  ;; -------------------------------------------------------------------------
  :diminish
  (gcmh-mode))
