(eval-when-compile
  (require 'init-package))
;;
(init-handler python init
  (when (zerop (length (getenv "PYTEST_ADDOPTS")))
    (setenv "PYTEST_ADDOPTS" "--color='yes'")))
;;
(init-handler python config
  (init-defun python-mode-setup ()
    #$
    (setq-local comment-start "#"))
  (add-hook 'python-mode-hook #'python-mode-setup)
  ;;
  (after-load init-electric-operator
    (add-hook 'python-mode-hook #'electric-operator-mode))
  ;;
  (after-load init-highlight-operators
    (add-hook 'python-mode-hook #'highlight-operators-mode)))
;;
(init-package python
  :built-in
  :mode
  (("/SConscript\\'" . python-mode)
   ("/SConstruct\\'" . python-mode)))
