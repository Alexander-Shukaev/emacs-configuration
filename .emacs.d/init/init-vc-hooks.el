(eval-when-compile
  (require 'init-package))
;;
(init-handler vc-hooks init
  (setq-default
    vc-follow-symlinks  t
    vc-handled-backends nil))
;;
(init-handler vc-hooks config
  (after-load tramp
    (setq-default vc-ignore-dir-regexp
      (format "\\(%s\\)\\|\\(%s\\)"
              vc-ignore-dir-regexp
              tramp-file-name-regexp))))
;;
(init-package vc-hooks
  :built-in)
