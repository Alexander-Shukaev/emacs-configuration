(eval-when-compile
  (require 'init-package))
;;
(init-handler files preface
  (eval-when-compile
    (require 'cl-lib)))
;;
(init-handler files prepare
  (defun init-split-filename-line-column
      (filename)
    (let ((line   1)
          (column 0))
      (save-match-data
        (cond ((string-match "^\\(.+\\):\\([0-9]+\\):\\([0-9]+\\):?$"
                             filename)
               (setq line     (string-to-number (or (match-string 2 filename)
                                                    "1")))
               (setq column   (string-to-number (or (match-string 3 filename)
                                                    "0")))
               (setq filename (match-string 1 filename)))
              ((string-match "^\\(.+\\):\\([0-9]+\\):?:?$"
                             filename)
               (setq line     (string-to-number (or (match-string 2 filename)
                                                    "1")))
               (setq filename (match-string 1 filename)))))
      (list filename line column))))
;;
(init-handler files init
  (put 'dir-locals-company 'safe-local-variable #'stringp)
  (put 'dir-locals-project 'safe-local-variable #'stringp)
  ;;
  (setq-default
    auto-save-default                     nil
    auto-save-file-name-transforms        `((".*"
                                             ,user-backup-directory
                                             :uniquify))
    auto-save-list-file-prefix            user-backup-directory
    backup-by-copying                     nil
    backup-by-copying-when-linked         t
    backup-by-copying-when-mismatch       t
    backup-directory-alist                `((".*"
                                             . ,(abbreviate-file-name
                                                 (expand-directory
                                                  "local/"
                                                  user-backup-directory))))
    enable-remote-dir-locals              nil
    find-file-suppress-same-file-warnings t
    find-file-visit-truename              t
    large-file-warning-threshold          (eval-when-compile (* 8 1024 1024))
    make-backup-files                     t
    require-final-newline                 t))
;;
(init-handler files config
  (init-assert (boundp 'list-directory-brief-switches))
  (makunbound 'list-directory-brief-switches)
  ;;
  (init-assert (boundp 'list-directory-verbose-switches))
  (makunbound 'list-directory-verbose-switches)
  ;;
  (init-assert (fboundp 'list-directory))
  (fmakunbound 'list-directory)
  ;;
  (when (bound-and-true-p init-bb)
    (dolist (auto-mode (eval-when-compile
                         (mapcar #'(lambda
                                       (extension)
                                     `(,(macroexpand `(rx ,extension eos))
                                       . js-mode))
                                 '(".fix" ".protocol" "_mapping.script"))))
      (add-to-list 'auto-mode-alist auto-mode)))
  ;;
  (add-to-list 'safe-local-variable-values
               '(eval setq-local fill-column 78))
  (add-to-list 'safe-local-variable-values
               '(eval setq-local fill-column 79))
  (when (bound-and-true-p init-bb)
    (add-to-list 'safe-local-variable-values
                 '(eval setq-local clang-format-executable
                        (or (executable-find "bde-format") "bde-format"))))
  ;;
  (after-init
    (init-defun init-hack-dir-local-variables-non-file-buffer ()
      #$
      (when (and (not noninteractive)
                 (init-interesting-buffer-p)
                 (not (eq (get major-mode 'mode-class) 'special)))
        (hack-dir-local-variables-non-file-buffer)))
    (add-hook 'after-change-major-mode-hook
              #'init-hack-dir-local-variables-non-file-buffer)
    ;;
    (init-defad normal-mode
        (:around (function &rest ...) init/:around)
      #$
      ;; ---------------------------------------------------------------------
      (init-ignore-progn
        (let ((after-change-major-mode-hook
               (remq #'init-hack-dir-local-variables-non-file-buffer
                     after-change-major-mode-hook)))
          (apply function ...)))
      ;;
      ;; NOTE:
      ;;
      ;; Results in the (annoying) message: "Making
      ;; after-change-major-mode-hook buffer-local while locally let-bound!".
      ;; ---------------------------------------------------------------------
      (remove-hook 'after-change-major-mode-hook
                   #'init-hack-dir-local-variables-non-file-buffer)
      (unwind-protect
          (apply function ...)
        (add-hook 'after-change-major-mode-hook
                  #'init-hack-dir-local-variables-non-file-buffer)))
    ;;
    (init-defad hack-dir-local-variables
        (:around (function &rest ...) init/:around)
      #$
      ;; ---------------------------------------------------------------------
      ;; TODO:
      ;;
      ;; Should this be rather done upon buffer temporary buffer creation?
      ;; E.g. where the major mode for it is set, wherever that is.
      ;;
      (init-ignore-progn
        (when (string-match-p (eval-when-compile
                                (format "^%s\\b" (regexp-quote " *temp")))
                              (buffer-name))
          (setq-local enable-dir-local-variables nil)))
      ;; ---------------------------------------------------------------------
      (unless enable-remote-dir-locals
        (let ((path (or (buffer-file-name) default-directory)))
          (when (or (not (file-remote-p path))
                    (and (equal (file-remote-p path 'method) "sudo")
                         (equal (file-remote-p path 'host)   (system-name))))
            (setq-local enable-remote-dir-locals t))))
      (when (and enable-local-variables
                 enable-dir-local-variables
                 enable-remote-dir-locals)
        (let* ((dir-or-cache      (dir-locals-find-file
                                   (or (buffer-file-name)
                                       default-directory)))
               (default-directory (if (stringp dir-or-cache)
                                      dir-or-cache
                                    (car-safe dir-or-cache)))
               (buffer-file-name  nil))
          (when default-directory
            (setq default-directory (file-name-directory
                                     (directory-file-name
                                      (file-name-directory
                                       default-directory))))
            (apply function ...))))
      (apply function ...)))
  ;;
  (init-defad abbreviate-file-name
      (:around (function &rest ...) init/:around)
    #$
    (let ((non-essential t))
      ;; ---------------------------------------------------------------------
      ;; CAUTION:
      ;;
      ;; Locally let-binding `tramp-mode' to nil before
      ;; `tramp-autoload-file-name-handler' (which is injected to
      ;; `file-name-handler-alist' by
      ;; `tramp-register-autoload-file-name-handlers') was ever called will
      ;; result in calling `tramp-unload-file-name-handlers' and that will
      ;; prevent `tramp' from being automatically loaded forever.  Instead,
      ;; prefer locally let-binding `non-essential' to t.
      ;; ---------------------------------------------------------------------
      ;; NOTE:
      ;;
      ;; The above approach (locally let-binding `non-essential' to t) is more
      ;; reliable and cleaner than the following:
      ;;
      (init-ignore-progn
        ((inhibit-file-name-handlers  '(tramp-file-name-handler
                                        tramp-vc-file-name-handler
                                        tramp-autoload-file-name-handler
                                        tramp-completion-file-name-handler))
         (inhibit-file-name-operation function)))
      ;; ---------------------------------------------------------------------
      (apply function ...)))
  ;;
  (init-defad find-file
      (:around (function filename &rest ...) init/:around)
    #$
    (let* ((split  (init-split-filename-line-column filename))
           (line   (nth 1 split))
           (column (nth 2 split)))
      (setq filename (nth 0 split))
      (when (and (not (called-interactively-p 'any))
                 (bound-and-true-p init-ffap--find-file-at-point-p)
                 (bound-and-true-p ffap-string-at-point)
                 (not (zerop (length ffap-string-at-point))))
        (setq split (init-split-filename-line-column ffap-string-at-point))
        (when (equal (expand-file-name (nth 0 split))
                     (expand-file-name filename))
          (when (and (= line   1)
                     (= column 0))
            (setq line   (nth 1 split))
            (setq column (nth 2 split)))))
      (apply function filename ...)
      (when (> line   1)
        (goto-char (point-min))
        (forward-line (1- line)))
      (when (> column 0)
        (move-to-column column))))
  ;;
  (init-defun init-file-setup ()
    #$
    (when (file-remote-p default-directory)
      (setq-local file-precious-flag t)))
  (add-hook 'find-file-hook #'init-file-setup :append)
  ;;
  (after-load init-user
    (add-to-list 'safe-local-variable-values
                 '(eval when
                        (bound-and-true-p init-user-url)
                        (setq-local init-user-email init-user-url))))
  ;;
  (after-load init-autorevert
    (init-defun init-file-auto-revert-tail-setup ()
      #$
      (when (and buffer-file-name
                 (string-match-p (concat "\\.log\\'"
                                         (when (bound-and-true-p init-bb)
                                           "\\|/rts/rtd/tmp/"))
                                 buffer-file-name))
        (auto-revert-tail-mode)))
    (add-hook 'find-file-hook
              #'init-file-auto-revert-tail-setup
              :append))
  ;;
  (after-load init-epa-file
    (init-defun init-file-write-contents-function ()
      #$
      (setq-local buffer-read-only t)
      (barf-if-buffer-read-only))
    ;;
    (init-defun init-file-epa-file-buffer-armor-type-setup ()
      #$
      (when (epa-file-buffer-armor-type-p)
        (setq-local buffer-read-only t)
        (add-hook 'write-contents-functions
                  #'init-file-write-contents-function
                  nil
                  :local)))
    (add-hook 'find-file-hook
              #'init-file-epa-file-buffer-armor-type-setup
              :append))
  ;;
  (after-load init-header2
    (add-hook 'write-file-functions #'auto-update-file-header))
  ;;
  (after-load init-whitespace
    (add-hook 'before-save-hook #'whitespace-cleanup))
  ;;
  (after-load init-magit
    (when init-magit-after-save-refresh-status
      (after-load magit
        (when init-magit-after-save-refresh-status
          (add-hook 'after-save-hook #'magit-after-save-refresh-status)))))
  ;;
  (after-load vc-hooks
    (cl-macrolet
        ((define-remove-hook ()
           (if (version< emacs-version "25.1")
               '(remove-hook 'find-file-hook #'vc-find-file-hook)
             '(remove-hook 'find-file-hook #'vc-refresh-state))))
      (define-remove-hook))))
;;
(init-package files
  :built-in)
