(eval-when-compile
  (require 'init-package))
;;
(eval-and-compile
  (init-load devil-load-path))
;;
(init-handler devil-search init
  (setq-default
    devil-search-echo-duration           nil
    devil-search-highlight-duration      nil
    devil-search-lazy-highlight-duration nil
    ;;
    devil-search-hide-immediately-p      t
    devil-search-invisible-p             t))
;;
(init-handler devil-search config)
;;
(init-package devil-search
  :load-path init-devil-load-path
  :ensure evil
  :ensure memoize)
