(eval-when-compile
  (require 'init-package))
;;
(init-handler hl-todo init
  (setq-default hl-todo-activate-in-modes '(fundamental-mode
                                            prog-mode
                                            text-mode))
  ;;
  (after-init (global-hl-todo-mode)))
;;
(init-handler hl-todo config)
;;
(init-handler hl-todo mode-map
  (init-clear-keymap hl-todo-mode-map))
;;
(init-package hl-todo
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Blows up memory consumption and simply unable to load large files.
  ;; -------------------------------------------------------------------------
  )
