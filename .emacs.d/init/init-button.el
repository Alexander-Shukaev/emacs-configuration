(eval-when-compile
  (require 'init-package))
;;
(init-handler button preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler button init)
;;
(init-handler button config
  (after-load evil
    (evil-declare-motion #'forward-button)
    (evil-declare-motion #'backward-button)))
;;
(init-handler button buffer-map
  (let ((keymap button-buffer-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      "TAB"       #'forward-button
      "<backtab>" #'backward-button))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd button-buffer-map
      :motion
      "TAB"       #'forward-button
      "<backtab>" #'backward-button)
    ;;
    (devil-repeat-motions-kbd
      :keymap button-buffer-map
      "TAB"       "TAB" "<backtab>"
      "<backtab>" "TAB" "<backtab>")))
;;
(init-package button
  :built-in)
