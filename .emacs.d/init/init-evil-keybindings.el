(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-keybindings init)
;;
(init-handler evil-keybindings config
  (init-eval-and-compile-when-compile
    (init-negate (init-load-history "evil-keybindings")))
  (init-negate (bound-and-true-p evil-want-keybinding))
  (init-negate (bound-and-true-p evil-emacs-state-modes))
  (init-negate (bound-and-true-p evil-insert-state-modes))
  (init-negate (bound-and-true-p evil-motion-state-modes)))
;;
(init-package evil-keybindings
  :ensure evil)
