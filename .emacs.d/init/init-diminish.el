(eval-when-compile
  (require 'init-package))

(init-handler diminish init)

(init-handler diminish config)

(init-package diminish)
