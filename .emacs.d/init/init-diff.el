(eval-when-compile
  (require 'init-package))
;;
(init-handler diff init
  (setq-default diff-switches "-u"))
;;
(init-handler diff config)
;;
(init-package diff
  :built-in)
