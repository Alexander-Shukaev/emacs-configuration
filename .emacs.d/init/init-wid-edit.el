(eval-when-compile
  (require 'init-package))
;;
(init-handler wid-edit preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler wid-edit init)
;;
(init-handler wid-edit config
  (after-load evil
    (evil-declare-motion #'widget-forward)
    (evil-declare-motion #'widget-backward)))
;;
(init-handler wid-edit widget-keymap
  (put 'widget-backward :advertised-binding (kbd "<backtab>"))
  ;;
  (let ((keymap widget-keymap))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      "<mouse-1>" #'widget-button-click
      "<mouse-2>" #'widget-button-click
      ;;
      "RET"       #'widget-button-press
      ;;
      "TAB"       #'widget-forward
      "<backtab>" #'widget-backward))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd widget-keymap
      :motion
      "TAB"       #'widget-forward
      "<backtab>" #'widget-backward)
    ;;
    (devil-repeat-motions-kbd
      :keymap widget-keymap
      "TAB"       "TAB" "<backtab>"
      "<backtab>" "TAB" "<backtab>")))
;;
(init-package wid-edit
  :built-in)
