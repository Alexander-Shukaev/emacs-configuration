(eval-when-compile
  (require 'init-package))
;;
(init-handler nlinum init
  (after-init (global-nlinum-mode)))
;;
(init-handler nlinum config
  ;; Prevent occasional slanting of line numbers:
  (set-face-attribute 'linum nil
                      :slant 'normal))
;;
(init-package nlinum
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Slow and sluggish.
  ;; -------------------------------------------------------------------------
  )
