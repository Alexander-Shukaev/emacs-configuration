(eval-when-compile
  (require 'init-package))
;;
(init-handler zmq init)
;;
(init-handler zmq config
  (init-assert (fboundp 'zmq--download-module))
  (init-defad zmq--download-module
      (:around (function &rest ...) init/:around)
    #$
    (let (debug-on-error)
      (with-demoted-errors "zmq: error: %s"
        (apply function ...)))))
;;
(init-package zmq)
