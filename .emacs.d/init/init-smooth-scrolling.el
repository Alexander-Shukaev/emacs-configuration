(eval-when-compile
  (require 'init-package))
;;
(init-handler smooth-scrolling init
  (init-load scroll :no-error)
  ;;
  (setq-default
    smooth-scroll-margin         scroll-margin
    smooth-scroll-strict-margins nil)
  ;;
  (after-init (smooth-scrolling-mode)))
;;
(init-handler smooth-scrolling config)
;;
(init-package smooth-scrolling
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Scrolling down (moving point up) is more or less fine but scrolling up
  ;; (moving point down) is extremely sluggish and stuttering.
  ;; -------------------------------------------------------------------------
  )
