(eval-when-compile
  (require 'init-package))
;;
(init-handler xclip init
  (when (init-wsl-p)
    (setq-default
      xclip-method  'powershell
      xclip-program "clip.exe"))
  ;;
  (when (getenv "WAYLAND_DISPLAY")
    ;; TODO: Steals focus!
    (if (executable-find "wl-copy")
        (setq-default
          xclip-method  'wl-copy
          xclip-program "wl-copy")
      (when (init-wsl-p)
        (lwarn 'init
               :warning
               "Interprogram copy/paste is likely broken: %s\n%s"
               "Install `wl-copy' (from `wl-clipboard')"
               (init-backtrace-to-string)))))
  ;;
  (after-init (xclip-mode)))
;;
(init-handler xclip config
  (fmakunbound 'xclip--setup)
  ;;
  (defvar init-xclip-last-selected-text-clipboard
    nil
    "The value of the CLIPBOARD X selection from `xclip'.")
  ;;
  (defvar init-xclip-last-selected-text-primary
    nil
    "The value of the PRIMARY X selection from `xclip'.")
  ;;
  (init-defun init-xclip-select-text
      (text)
    #$
    "See `x-select-text'."
    (xclip-set-selection 'primary text)
    (setq init-xclip-last-selected-text-primary text)
    (when xclip-select-enable-clipboard
      (xclip-set-selection 'clipboard text)
      (setq init-xclip-last-selected-text-clipboard text)))
  ;;
  (init-defun init-xclip-selection-value ()
    #$
    "See `x-selection-value'."
    (let ((text (when xclip-select-enable-clipboard
                  (if (init-wsl-p)
                      (let ((coding-system-for-read 'dos))
                        (xclip-get-selection 'CLIPBOARD))
                    (xclip-get-selection 'CLIPBOARD)))))
      (setq text
        (cond ((or (not text) (string= text ""))
               (setq init-xclip-last-selected-text-clipboard nil))
              ((eq text init-xclip-last-selected-text-clipboard)
               nil)
              ((string= text init-xclip-last-selected-text-clipboard)
               (setq init-xclip-last-selected-text-clipboard text)
               nil)
              (t
               (setq init-xclip-last-selected-text-clipboard text))))
      (or text
          (when (and (memq xclip-method '(xsel xclip))
                     (or (eq window-system 'x)
                         (getenv "DISPLAY")))
            (setq text (with-output-to-string
                         (call-process xclip-program   nil
                                       standard-output nil "-o")))
            (setq text
              (cond ((or (not text) (string= text ""))
                     (setq init-xclip-last-selected-text-primary nil))
                    ((eq text init-xclip-last-selected-text-primary)
                     nil)
                    ((string= text init-xclip-last-selected-text-primary)
                     (setq init-xclip-last-selected-text-primary text)
                     nil)
                    (t
                     (setq init-xclip-last-selected-text-primary text))))
            text))))
  ;;
  (init-defun init-xclip-setup ()
    #$
    (if (bound-and-true-p xclip-mode)
        (setq
          interprogram-cut-function   #'init-xclip-select-text
          interprogram-paste-function #'init-xclip-selection-value)
      (setq
        interprogram-cut-function   #'x-select-text
        interprogram-paste-function #'x-selection-value)))
  ;; As per `tty-run-terminal-initialization' and [1]:
  (dolist (hook '(terminal-init-xterm-hook window-setup-hook))
    (add-hook hook #'init-xclip-setup)))
;;
(init-package xclip)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(elisp) Terminal-Specific'
;;  ==========================================================================
;;  }}} References
