(eval-when-compile
  (require 'init-package))
;;
(init-handler company-flx prepare
  (init-load company :no-error)
  (init-load flx     :no-error))
;;
(init-handler company-flx init
  (setq-default company-flx-limit 256)
  ;;
  (after-load company
    (after-init (company-flx-mode))))
;;
(init-handler company-flx config)
;;
(init-package company-flx
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Sometimes slow and sluggish.
  ;; -------------------------------------------------------------------------
  )
