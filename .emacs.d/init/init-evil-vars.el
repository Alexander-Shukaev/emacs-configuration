(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-vars preface
  (unless (fboundp 'evil-update-insert-state-bindings)
    (fset 'evil-update-insert-state-bindings #'ignore)))
;;
(init-handler evil-vars init
  (init-load indent)
  ;;
  (setq-default
    evil-auto-balance-windows          nil
    evil-cross-lines                   t
    evil-default-state                 'normal
    evil-disable-insert-state-bindings t
    evil-echo-state                    nil
    evil-flash-delay                   60; seconds
    evil-kill-on-visual-paste          nil
    evil-mode-line-format              'before
    evil-overriding-maps               nil
    evil-respect-visual-line-mode      t
    evil-shift-width                   init-indent-width
    evil-want-Y-yank-to-eol            t
    evil-want-fine-undo                t
    evil-want-integration              t
    evil-want-keybinding               nil
    evil-want-minibuffer               t))
;;
(init-handler evil-vars config
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; Actually, I don't think this will work if one explicitly loads
  ;; `evil-maps', for example, because this code will execute already during
  ;; the loading of `evil-maps' (since it requires `evil-vars' itself):
  ;;
  (dolist (feature '(;; TODO:
                     ;; evil-search
                     evil-maps
                     evil-keybindings))
    (add-to-list 'features feature))
  ;; -------------------------------------------------------------------------
  ;;
  (after-load "isearch"
    (init-assert (boundp 'evil-ex-search-history))
    (set         'evil-ex-search-history  regexp-search-ring)
    (defvaralias 'evil-ex-search-history 'regexp-search-ring)))
;;
(init-package evil-vars
  :ensure evil)
