(eval-when-compile
  (require 'init-package))
;;
(init-handler ansi-color preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler ansi-color prepare
  (defconst init-ansi-color-black
    "black"
    "ANSI color of black.")
  ;;
  (defconst init-ansi-color-red
    "red3"
    "ANSI color of red.")
  ;;
  (defconst init-ansi-color-green
    "green3"
    "ANSI color of green.")
  ;;
  (defconst init-ansi-color-yellow
    "yellow3"
    "ANSI color of yellow.")
  ;;
  (defconst init-ansi-color-blue
    "blue3"
    "ANSI color of blue.")
  ;;
  (defconst init-ansi-color-magenta
    "magenta3"
    "ANSI color of magenta.")
  ;;
  (defconst init-ansi-color-cyan
    "cyan3"
    "ANSI color of cyan.")
  ;;
  (defconst init-ansi-color-white
    "gray90"
    "ANSI color of white.")
  ;;
  (defconst init-ansi-color-names-vector
    [init-ansi-color-black
     init-ansi-color-red
     init-ansi-color-green
     init-ansi-color-yellow
     init-ansi-color-blue
     init-ansi-color-magenta
     init-ansi-color-cyan
     init-ansi-color-white]
    "Colors used to initialize `ansi-color-names-vector'."))
;;
(init-handler ansi-color init
  (setq-default
    ansi-color-for-comint-mode      t
    ansi-color-for-compilation-mode (or (fboundp 'ansi-color-compilation-filter)
                                        (fboundp 'ansi-color-process-output)))
  (unless (car-safe (custom-variable-theme-value 'ansi-color-names-vector))
    (setq-default ansi-color-names-vector    init-ansi-color-names-vector)))
;;
(init-handler ansi-color config
  (init-assert (bound-and-true-p ansi-color-control-seq-regexp))
  (setq-default ansi-color-control-seq-regexp
    (format "%s\\|%s" ansi-color-control-seq-regexp "\a"))
  ;;
  (init-defun init-ansi-color--face-setup
      (alist)
    #$
    (dolist (entry alist)
      (face-spec-set (car entry) `((t
                                    :foreground ,(cdr entry)
                                    :background ,(cdr entry))))))
  ;;
  (after-load (init-solarized-theme solarized-theme)
    (init-defun init-ansi-color-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (face-spec-reset-face 'ansi-color-bold)
        (face-spec-set 'ansi-color-bold
                       '((t
                          :weight bold)))
        (init-ansi-color--face-setup
         `((ansi-color-black          . ,init-ansi-color-black )
           (ansi-color-red            . ,init-solarized-red    )
           (ansi-color-green          . ,init-solarized-green  )
           (ansi-color-yellow         . ,init-solarized-yellow )
           (ansi-color-blue           . ,init-solarized-blue   )
           (ansi-color-magenta        . ,init-solarized-magenta)
           (ansi-color-cyan           . ,init-solarized-cyan   )
           (ansi-color-white          . ,init-ansi-color-white )
           ;;
           (ansi-color-bright-black   . ,init-ansi-color-black )
           (ansi-color-bright-red     . ,init-solarized-red    )
           (ansi-color-bright-green   . ,init-solarized-green  )
           (ansi-color-bright-yellow  . ,init-solarized-yellow )
           (ansi-color-bright-blue    . ,init-solarized-blue   )
           (ansi-color-bright-magenta . ,init-solarized-magenta)
           (ansi-color-bright-cyan    . ,init-solarized-cyan   )
           (ansi-color-bright-white   . ,init-ansi-color-white )))))
    (init-ansi-color-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...)
                init-ansi-color-solarized-theme-setup/:after)
      #$
      (init-ansi-color-solarized-theme-setup theme))))
;;
(init-package ansi-color
  :built-in
  :variables
  (ansi-color-control-seq-regexp))
