(eval-when-compile
  (require 'init-package))
;;
(init-handler json-reformat init
  (init-load indent)
  ;;
  (setq-default json-reformat:indent-width init-indent-width))
;;
(init-handler json-reformat config)
;;
(init-package json-reformat)
