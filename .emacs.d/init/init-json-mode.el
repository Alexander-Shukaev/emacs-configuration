(eval-when-compile
  (require 'init-package))
;;
(init-handler json-mode prepare
  (init-load js            :no-error)
  (init-load json-reformat :no-error))
;;
(init-handler json-mode init)
;;
(init-handler json-mode config
  (after-load (init-json-reformat devil)
    (init-defun init-json-mode-json-reformat-devil-setup ()
      #$
      (setq-local devil-format-region-function #'json-reformat-region))
    (add-hook 'json-mode-hook #'init-json-mode-json-reformat-devil-setup))
  ;;
  (after-load init-electric-operator
    (add-hook 'json-mode-hook #'electric-operator-mode))
  ;;
  (after-load init-highlight-operators
    (add-hook 'json-mode-hook #'highlight-operators-mode)))
;;
(init-handler json-mode map
  (let ((keymap json-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap)))
;;
(init-package json-mode)
