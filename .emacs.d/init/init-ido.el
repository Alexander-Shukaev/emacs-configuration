(eval-when-compile
  (require 'init-package))
;;
(init-handler ido init)
;;
(init-handler ido config
  (init-assert (fboundp 'ido-list-directory))
  (fmakunbound 'ido-list-directory))
;;
(init-package ido)
