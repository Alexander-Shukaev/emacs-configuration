(eval-when-compile
  (require 'init-package))
;;
(init-handler windmove init)
;;
(init-handler windmove config
  (init-assert (fboundp 'windmove-find-other-window))
  (fmakunbound 'windmove-find-other-window)
  (defun windmove-find-other-window
      (dir &optional arg window)
    "\
Return the window object in direction DIR.

DIR, ARG, and WINDOW are handled as by `windmove-other-window-loc'."
    (window-in-direction (cond
                          ((eq dir 'up)   'above)
                          ((eq dir 'down) 'below)
                          (t dir))
                         window :ignore arg windmove-wrap-around t)))
;;
(init-package windmove
  :built-in)
