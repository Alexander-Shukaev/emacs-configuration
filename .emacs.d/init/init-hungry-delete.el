(eval-when-compile
  (require 'init-package))
;;
(init-handler hungry-delete init
  (setq-default hungry-delete-join-reluctantly t)
  ;;
  (after-init (global-hungry-delete-mode)))
;;
(init-handler hungry-delete config
  (dolist (mode '(calc-mode
                  help-mode
                  minibuffer-inactive-mode
                  minibuffer-mode))
    (add-to-list 'hungry-delete-except-modes mode)))
;;
(init-handler hungry-delete mode-map)
;;
(init-package hungry-delete
  :diminish
  (global-hungry-delete-mode
   hungry-delete-mode))
