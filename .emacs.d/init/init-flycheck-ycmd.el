(eval-when-compile
  (require 'init-package))
;;
(init-handler flycheck-ycmd prepare
  (init-load flycheck :no-error)
  (init-load ycmd     :no-error))
;;
(init-handler flycheck-ycmd init)
;;
(init-handler flycheck-ycmd config)
;;
(init-package flycheck-ycmd)
