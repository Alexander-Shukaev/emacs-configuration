(eval-when-compile
  (require 'init-package))
;;
(init-handler highlight-indentation init
  (init-load indent)
  ;;
  (setq-default highlight-indentation-offset init-indent-width))
;;
(init-handler highlight-indentation config)
;;
(init-package highlight-indentation
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Slow and sluggish.
  ;; -------------------------------------------------------------------------
  :diminish
  (highlight-indentation-mode
   highlight-indentation-current-column-mode))
