(eval-when-compile
  (require 'init-package))
;;
(init-handler pydoc-info init
  (after-init (pydoc-info-add-help '("python" "sphinx"))))
;;
(init-handler pydoc-info config)
;;
(init-package pydoc-info
  :load-path "lisp/pydoc-info")
