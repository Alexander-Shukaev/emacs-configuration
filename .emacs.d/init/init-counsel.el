(eval-when-compile
  (require 'init-package))
;;
(init-handler counsel preface
  (eval-when-compile
    (require 'cl-lib)))
;;
;; TODO: Start using `counsel-bookmark', `counsel-compilation-errors',
;; `counsel-compile'!
(init-handler counsel init
  (init-load files :no-error)
  ;;
  (setq-default
    ;; -----------------------------------------------------------------------
    counsel-async-filter-update-time
    100000
    ;; -----------------------------------------------------------------------
    counsel-dired-jump-args
    ". -name '.git' -prune -o -type d -print | cut -c 3-"
    ;; -----------------------------------------------------------------------
    counsel-file-jump-args
    ". -name '.git' -prune -o -type f -print | cut -c 3-"
    ;; -----------------------------------------------------------------------
    counsel-find-file-at-point
    t
    ;; -----------------------------------------------------------------------
    counsel-find-file-ignore-regexp
    "\\(?:\\(?:\\`\\|[/\\]\\)[#.]\\|[#~]\\'\\)"
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; In fact, `swiper' is slower than `grep' on large files, so setting this
    ;; to `large-file-warning-threshold' does not make any sense.  Hence,
    ;; prefer `grep' always:
    ;;
    counsel-grep-swiper-limit
    0
    ;; -----------------------------------------------------------------------
    counsel-mode-override-describe-bindings
    t
    ;; -----------------------------------------------------------------------
    counsel-preselect-current-file
    t
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; When `counsel-recentf-include-xdg-list' is non-nil, it noticeably slows
    ;; down `counsel-recentf'.
    ;;
    counsel-recentf-include-xdg-list
    nil
    ;; -----------------------------------------------------------------------
    counsel--git-grep-count-threshold
    0)
  ;;
  (after-load init-helpful
    (setq-default
      ;; TODO: `helpful-key', `helpful-face'.
      counsel-descbinds-function         #'helpful-callable
      counsel-describe-function-function #'helpful-callable
      counsel-describe-symbol-function   #'helpful-symbol
      counsel-describe-variable-function #'helpful-variable))
  ;;
  (after-init (counsel-mode)))
;;
(init-handler counsel config
  (setq-default
    counsel-ag-base-command `("ag"
                              "--vimgrep"
                              "--follow"
                              "--hidden"
                              "--skip-vcs-ignores"
                              "--silent"
                              "--smart-case"
                              "--width" ,(number-to-string
                                          (* 2 fill-column))
                              "%s")
    counsel-rg-base-command `("rg"
                              "--with-filename"
                              "--no-heading"
                              "--color" "never"
                              "--follow"
                              "--hidden"
                              "--no-ignore-vcs"
                              "--no-messages"
                              "--smart-case"
                              "--max-columns" ,(number-to-string
                                                (* 2 fill-column))
                              "--max-columns-preview"
                              "%s"))
  ;;
  (add-hook 'counsel-grep-post-action-hook #'recenter)
  ;;
  (defun counsel-recentd ()
    "Find a directory on `recentf-list'."
    (interactive)
    (unless (fboundp 'recentf-mode)
      (require 'recentf))
    (recentf-mode)
    (ivy-read "Recentd: "
              (delete-dups (mapcar #'(lambda
                                         (file)
                                       (let ((non-essential t))
                                         (if (file-directory-p file)
                                             file
                                           (file-name-directory file))))
                                   (mapcar #'substring-no-properties
                                           (bound-and-true-p recentf-list))))
              :action        #'(lambda
                                   (directory)
                                 (with-ivy-window
                                   (find-file directory)))
              :caller        'counsel-recentd
              :sort          nil
              :initial-input nil))
  (ivy-set-actions
   'counsel-recentd
   '(("j" find-file-other-window "other window")
     ("x" counsel-find-file-extern "open externally")))
  ;;
  ;; TODO: With [1] should not be necessary anymore:
  (init-defad counsel-grep-function
      (:around (function string) init/:around)
    #$
    (save-match-data
      (funcall function string)))
  ;;
  ;; See [2, 3]:
  (init-defad counsel-rg
      (:around (function &rest ...) init/:around)
    #$
    (let ((process-exit-status-function
           (symbol-function 'process-exit-status)))
      (cl-letf (((symbol-function 'process-exit-status)
                 #'(lambda
                       (process)
                     (let ((status (funcall process-exit-status-function
                                            process)))
                       (if (= status 2) 0 status)))))
        (apply function ...))))
  ;;
  (init-defad counsel-switch-buffer
      (:around (function &rest ...) init/:around)
    #$
    (let ((non-essential t))
      (apply function ...)))
  ;;
  (init-defad counsel-switch-buffer-other-window
      (:around (function &rest ...) init/:around)
    #$
    (let ((non-essential t))
      (apply function ...)))
  ;;
  (after-load "isearch"
    (dolist (variable '(counsel-grep-history counsel-git-grep-history))
      (init-assert (boundp variable))
      (set         variable  regexp-search-ring)
      (defvaralias variable 'regexp-search-ring))))
;;
(init-handler counsel mode-map
  (let ((keymap counsel-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-define-keys-kbd keymap
      [remap  switch-to-buffer] #'counsel-switch-buffer
      [remap ivy-switch-buffer] #'counsel-switch-buffer
      ;;
      [remap  switch-to-buffer-other-window] #'counsel-switch-buffer-other-window
      [remap ivy-switch-buffer-other-window] #'counsel-switch-buffer-other-window)))
;;
(init-handler counsel ag-map
  (let ((keymap counsel-ag-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      [remap query-replace]        #'counsel-git-grep-query-replace
      [remap query-replace-regexp] #'counsel-git-grep-query-replace)))
;;
(init-handler counsel git-grep-map
  (let ((keymap counsel-git-grep-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      [remap query-replace]        #'counsel-git-grep-query-replace
      [remap query-replace-regexp] #'counsel-git-grep-query-replace)))
;;
(init-package counsel
  :commands
  (counsel-recentd)
  :diminish
  (counsel-mode))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/abo-abo/swiper/commit/1396d7be50c7b885579ff3e41a4320798f0aedf3'
;;  [2] URL `http://github.com/doomemacs/doomemacs/commit/234cc27b77d2da72841225a5770961a4229f70e7'
;;  [3] URL `http://endlessparentheses.com/understanding-letf-and-how-it-replaces-flet.html'
;;  ==========================================================================
;;  }}} References
