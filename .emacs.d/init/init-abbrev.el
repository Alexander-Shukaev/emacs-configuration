(eval-when-compile
  (require 'init-package))
;;
(init-handler abbrev init)
;;
(init-handler abbrev config)
;;
(init-handler abbrev edit-abbrevs-mode-map
  (init-clear-keymap edit-abbrevs-mode-map))
;;
(init-package abbrev
  :built-in
  :diminish
  (abbrev-mode))
