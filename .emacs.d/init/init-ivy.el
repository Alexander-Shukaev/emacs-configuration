(eval-when-compile
  (require 'init-package))
;;
(init-handler ivy preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler ivy init
  (init-load scroll :no-error)
  ;;
  (setq-default
    ivy-count-format         "%d/%d "
    ivy-extra-directories    nil
    ivy-flx-limit            256
    ivy-height               (1+ (* 2 scroll-margin))
    ivy-initial-inputs-alist nil
    ivy-re-builders-alist    '((t . ivy--regex-plus))
    ivy-use-virtual-buffers  t
    ivy-wrap                 t)
  ;;
  (after-init (ivy-mode)))
;;
(init-handler ivy config
  (face-spec-reset-face 'ivy-virtual)
  (set-face-attribute   'ivy-virtual nil
                        :inherit 'font-lock-comment-face)
  ;;
  (add-to-list 'ivy-sort-functions-alist
               '(ffap-read-file-or-url-internal
                 . ivy-sort-file-function-default))
  ;;
  (ivy-set-display-transformer
   'ffap-read-file-or-url-internal 'ivy-read-file-transformer)
  ;;
  (init-defun ivy-find-file-or-immediate-done ()
    #$
    (interactive)
    (if (null ivy--directory)
        (ivy-immediate-done)
      (delete-minibuffer-contents)
      (ivy--done (concat ivy--directory ivy-text))))
  ;;
  (init-defad ivy-switch-buffer
      (:around (function &rest ...) init/:around)
    #$
    (let ((non-essential t))
      (apply function ...)))
  ;;
  (init-defad ivy-switch-buffer-other-window
      (:around (function &rest ...) init/:around)
    #$
    (let ((non-essential t))
      (apply function ...)))
  ;;
  (after-load evil
    (evil-set-initial-state 'ivy-occur-mode 'motion)
    ;;
    (evil-declare-motion #'ivy-occur-next-line)
    (evil-declare-motion #'ivy-occur-previous-line)
    ;;
    (evil-set-type #'ivy-occur-next-line     'line)
    (evil-set-type #'ivy-occur-previous-line 'line))
  ;;
  (after-load init-wgrep
    (add-hook 'ivy-occur-grep-mode-hook #'wgrep-setup)))
;;
(init-handler ivy minibuffer-map
  (let ((keymap ivy-minibuffer-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (set-keymap-parent    keymap minibuffer-local-map)
    (init-define-keys-kbd keymap
      [remap backward-delete-char-untabify]     #'ivy-backward-delete-char
      [remap backward-kill-word]                #'ivy-backward-kill-word
      [remap beginning-of-buffer]               #'ivy-beginning-of-buffer
      [remap delete-backward-char]              #'ivy-backward-delete-char
      [remap delete-char]                       #'ivy-delete-char
      [remap describe-mode]                     #'ivy-help
      [remap end-of-buffer]                     #'ivy-end-of-buffer
      [remap exit-minibuffer]                   #'ivy-done
      [remap forward-char]                      #'ivy-forward-char
      [remap kill-line]                         #'ivy-kill-line
      [remap kill-ring-save]                    #'ivy-kill-ring-save
      [remap kill-word]                         #'ivy-kill-word
      [remap minibuffer-complete-and-exit]      #'ivy-partial-or-done
      [remap next-complete-history-element]     #'ivy-next-history-element
      [remap next-history-element]              #'ivy-next-history-element
      [remap next-line]                         #'ivy-next-line
      [remap previous-complete-history-element] #'ivy-previous-history-element
      [remap previous-history-element]          #'ivy-previous-history-element
      [remap previous-line]                     #'ivy-previous-line
      [remap scroll-down-command]               #'ivy-scroll-down-command
      [remap scroll-up-command]                 #'ivy-scroll-up-command
      ;;
      "C-'"                                     #'ivy-avy
      "C-:"                                     #'ivy-restrict-to-matches
      "C-a"                                     #'ivy-read-action
      "C-e"                                     #'ivy-find-file-or-immediate-done
      "C-f"                                     #'ivy-find-file-or-immediate-done
      "C-n"                                     #'ivy-next-line-and-call
      "C-o"                                     #'ivy-occur
      "C-p"                                     #'ivy-previous-line-and-call
      "C-r"                                     #'ivy-reverse-i-search
      "C-s"                                     #'ivy-next-line-or-history
      "C-y"                                     #'ivy-yank-word
      ;;
      "SPC"                                     #'self-insert-command
      ;;
      "TAB"                                     #'ivy-next-line
      "<backtab>"                               #'ivy-previous-line
      ;;
      ;; TODO: `ivy-dispatching-done', `ivy-partial-or-done', `ivy-partial',
      ;; conflict with `minibuffer-complete-and-exit', <M-RET>?
      "LFD"                                     #'ivy-immediate-done
      "RET"                                     #'ivy-alt-done)))
;;
(init-handler ivy mode-map
  (let ((keymap ivy-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      ;; TODO: Other frame?
      [remap switch-to-buffer-other-window] #'ivy-switch-buffer-other-window
      [remap switch-to-buffer]              #'ivy-switch-buffer)))
;;
(init-handler ivy switch-buffer-map)
;;
(init-handler ivy occur-mode-map
  (let ((keymap ivy-occur-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (set-keymap-parent    keymap special-mode-map)
    (init-define-keys-kbd keymap
      [remap next-line    ] #'ivy-occur-next-line
      [remap previous-line] #'ivy-occur-previous-line
      ;;
      [remap revert-buffer] #'ivy-occur-revert-buffer
      ;;
      "<mouse-2>"           #'ivy-occur-click
      ;;
      "RET"                 #'ivy-occur-press-and-switch
      ;;
      "TAB"                 #'ivy-occur-next-line
      "<backtab>"           #'ivy-occur-previous-line
      ;;
      "a"                   #'ivy-occur-read-action
      "c"                   #'ivy-occur-toggle-calling
      "d"                   #'ivy-occur-dispatch
      "x"                   #'ivy-occur-press))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd ivy-occur-mode-map
      :motion
      [remap devil-line-forward ] #'ivy-occur-next-line
      [remap devil-line-backward] #'ivy-occur-previous-line
      ;;
      "TAB"                       #'ivy-occur-next-line
      "<backtab>"                 #'ivy-occur-previous-line)
    ;;
    (devil-repeat-motions-kbd
      :keymap ivy-occur-mode-map
      "TAB"       "TAB" "<backtab>"
      "<backtab>" "TAB" "<backtab>"))
  ;;
  (after-load evil
    (evil-make-overriding-map ivy-occur-mode-map 'motion)))
;;
(init-handler ivy occur-grep-mode-map
  (let ((keymap ivy-occur-grep-mode-map))
    (init-assert (null  (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (setcdr keymap (cdr (copy-keymap ivy-occur-mode-map)))
    (init-assert (equal (keymap-parent keymap) special-mode-map)))
  (after-load grep
    (set-keymap-parent ivy-occur-grep-mode-map grep-mode-map)))
;;
(init-package ivy
  :diminish
  (ivy-mode))
