(eval-when-compile
  (require 'init-package))
;;
(init-handler protobuf-mode init)
;;
(init-handler protobuf-mode config
  (defconst init-protobuf-mode-hooks
    '(protobuf-mode-hook))
  ;;
  (after-load (init-clang-format devil)
    (init-defun init-protobuf-mode-clang-format-devil-setup ()
      #$
      (setq-local devil-format-region-function #'clang-format-region))
    (dolist (hook init-protobuf-mode-hooks)
      (add-hook hook #'init-protobuf-mode-clang-format-devil-setup)))
  ;;
  (after-load init-electric-operator
    (dolist (hook init-protobuf-mode-hooks)
      (add-hook hook #'electric-operator-mode)))
  ;;
  (after-load init-highlight-operators
    (dolist (hook init-protobuf-mode-hooks)
      (add-hook hook #'highlight-operators-mode))))
;;
(init-handler protobuf-mode map
  (init-clear-keymap protobuf-mode-map))
;;
(init-package protobuf-mode
  :mode
  (("\\.proto\\'" . protobuf-mode)))
