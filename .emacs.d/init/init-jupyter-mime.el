(eval-when-compile
  (require 'init-package))
;;
(init-handler jupyter-mime prepare
  (init-load ansi-color :no-error))
;;
(init-handler jupyter-mime init)
;;
(init-handler jupyter-mime config
  (init-defun init-jupyter-ansi-color-apply ()
    #$
    (when (org-babel-jupyter-language-p
           (car (or (org-babel-get-src-block-info :light) '(""))))
      (let* ((r (org-babel-where-is-src-block-result))
             (result (when r
                       (save-excursion
                         (goto-char r)
                         (org-element-context)))))
        (when result
          (jupyter-ansi-color-apply-on-region (org-element-property :begin
                                                                    result)
                                              (org-element-property :end
                                                                    result))
          (save-match-data
            (save-excursion
              (goto-char r)
              (when (let (case-fold-search)
                      (search-forward "# [goto error]"
                                      (org-element-property :end result) t))
                (add-text-properties (match-beginning 0)
                                     (match-end       0)
                                     (list 'help-echo
                                           "Click to jump to error."
                                           'face
                                           'link
                                           'keymap
                                           jupyter-org-goto-error-map))))))
        t)))
  ;;
  (after-load ansi-color
    (init-defad jupyter-ansi-color-apply-on-region
        (:override (begin end) init/:override)
      #$
      (ansi-color-apply-on-region begin end :preserve-sequences))))
;;
(init-package jupyter-mime
  :unless
  (< emacs-major-version 26)
  :ensure jupyter)
