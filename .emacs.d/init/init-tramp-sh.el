(eval-when-compile
  (require 'init-package))
;;
(init-handler tramp-sh prepare
  (init-load tramp :no-error))
;;
(init-handler tramp-sh init)
;;
(init-handler tramp-sh config
  (setq-default tramp-use-connection-share
    (and tramp-use-connection-share (not (bound-and-true-p init-bb))))
  ;;
  (if (bound-and-true-p tramp-device-escape-sequence-regexp)
      (setq-default tramp-device-escape-sequence-regexp
        (format "%s\\|%s" tramp-device-escape-sequence-regexp "\a"))
    (init-assert (bound-and-true-p ansi-color-control-seq-regexp)))
  ;;
  (init-defad tramp-sh-handle-delete-file
      (:around (function filename &rest ...) init/:around)
    #$
    (if (and (equal (file-remote-p filename 'user) (user-login-name))
             (equal (file-remote-p filename 'host) (system-name)))
        (apply function filename ...)
      (let (delete-by-moving-to-trash)
        (apply function filename ...))))
  ;;
  (init-defad tramp-ssh-controlmaster-options
      (:around (function vec) init/:around)
    #$
    (let ((options (funcall function vec)))
      (unless (zerop (length options))
        (message "%s" options)
        (when (string-match-p "ControlMaster=auto" options)
          (if (string-match-p "ControlPersist=no"  options)
              (setq options
                (replace-regexp-in-string (regexp-quote "ControlPersist=no")
                                          "ControlPersist=24h"
                                          options
                                          nil
                                          :literal))
            (setq options (concat options " -o ControlPersist=24h")))))
      (message "`%s': \"%s\"" 'tramp-ssh-controlmaster-options options)
      options)))
;;
(init-package tramp-sh
  :built-in)
