(eval-when-compile
  (require 'init-package))
;;
(init-handler col-highlight init
  (setq-default col-highlight-vline-face-flag t))
;;
(init-handler col-highlight config
  (face-spec-reset-face 'col-highlight)
  (set-face-attribute   'col-highlight nil
                        :inherit 'hl-line))
;;
(init-package col-highlight
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Sometimes slow and sluggish.
  ;; -------------------------------------------------------------------------
  :unless
  (< emacs-major-version 25))
