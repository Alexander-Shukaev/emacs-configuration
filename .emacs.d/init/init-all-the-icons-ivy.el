(eval-when-compile
  (require 'init-package))
;;
(init-handler all-the-icons-ivy init)
;;
(init-handler all-the-icons-ivy config)
;;
(init-package all-the-icons-ivy
  :disabled)
