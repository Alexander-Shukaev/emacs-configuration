(eval-when-compile
  (require 'init-package))
;;
(init-handler dired preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler dired init
  (setq-default dired-auto-revert-buffer t)
  ;;
  ;; If `init-ls-lisp' exists, then `require' it in hope that it sets
  ;; `ls-lisp-use-insert-directory-program' properly:
  (init-load ls-lisp :no-error)
  ;;
  (setq-default
    dired-listing-switches
    (if (bound-and-true-p ls-lisp-use-insert-directory-program)
        (mapconcat #'identity '("--almost-all"
                                ;; "--dereference"
                                "--group-directories-first"
                                "--human-readable"
                                "--indicator-style=slash"
                                "-l"
                                "-v")
                   " ")
      "-ahlv")))
;;
(init-handler dired config
  (require 'dired-x nil :no-error)
  (let ((features (remq 'dired features)))
    (require 'woman nil :no-error))
  ;;
  (after-load dired-x
    (init-call dired mode-map))
  ;;
  (after-load woman
    (init-call dired mode-map))
  ;;
  (after-load evil
    (evil-set-initial-state 'dired-mode 'motion)
    ;;
    (evil-declare-motion #'dired-next-line)
    (evil-declare-motion #'dired-previous-line)
    ;;
    (evil-declare-motion #'dired-next-dirline)
    (evil-declare-motion #'dired-prev-dirline)
    ;;
    (evil-set-type #'dired-next-line     'line)
    (evil-set-type #'dired-previous-line 'line)
    ;;
    (evil-set-type #'dired-next-dirline 'line)
    (evil-set-type #'dired-prev-dirline 'line))
  ;;
  (after-load init-dired-details
    (init-defad dired-revert
        (:before (&rest ...) init-dired-details/:before)
      #$
      (dired-details-delete-overlays))
    (add-hook 'dired-after-readin-hook #'dired-details-activate))
  ;;
  (after-load init-dired-k
    (add-hook 'dired-initial-position-hook #'dired-k)
    (add-hook 'dired-after-readin-hook     #'dired-k-no-revert))
  ;;
  (after-load init-all-the-icons-dired
    (add-hook 'dired-mode-hook #'all-the-icons-dired-mode))
  ;;
  (init-defun init-dired-mode-setup ()
    #$
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; As per bug#59151:
    ;;
    (when (file-remote-p default-directory)
      ;; ---------------------------------------------------------------------
      ;; TODO:
      ;;
      ;; Similarly to `init-company-turn-off-fci-maybe', `font-lock-mode' need
      ;; to be turned off/on gracefully by tracking via buffer-local
      ;; variables:
      ;;
      (when (fboundp 'font-lock-mode)
        (font-lock-mode -1))
      ;; ---------------------------------------------------------------------
      )
    ;; -----------------------------------------------------------------------
    )
  (add-hook 'dired-mode-hook #'init-dired-mode-setup :append))
;;
(init-handler dired mode-map
  (let ((keymap dired-mode-map))
    (init-assert (equal (keymap-parent keymap) special-mode-map))
    (init-clear-keymap    keymap)
    (init-assert (equal (keymap-parent keymap) special-mode-map))
    (init-define-keys-kbd keymap
      "<follow-link>"  'mouse-face
      "<mouse-2>"     #'dired-mouse-find-file-other-window
      ;;
      ;; TODO: [remaps]
      ;; "C-SPC" #'dired-find-file-other-window
      "RET"   #'dired-find-file
      ;; TODO: Remove?
      ;; "RET" #'dired-find-alternate-file
      ;;
      "u"  #'dired-up-directory
      ".." #'dired-up-directory
      ;; TODO: Remove?
      ;; ".." #'(lambda ()
      ;;          (interactive)
      ;;          (find-alternate-file (file-name-directory
      ;;                                (directory-file-name
      ;;                                 (dired-current-directory)))))
      ;;
      "d" #'dired-display-file
      "c" #'dired-create-directory
      ;;
      "TAB"       #'dired-next-dirline
      "<backtab>" #'dired-prev-dirline
      ;;
      ;; TODO: Remove?
      ;; [remap quit-window] #'kill-this-buffer
      ;;
      [remap describe-mode] #'dired-summary
      [remap     next-line] #'dired-next-line
      [remap previous-line] #'dired-previous-line)
    (when (fboundp 'dired-hide-details-mode)
      (init-define-keys-kbd keymap
        "#" #'dired-hide-details-mode)))
  ;; TODO: Remove?
  ;; (put 'dired-find-alternate-file 'disabled nil)
  ;;
  (after-load dired-x
    (init-define-keys-kbd dired-mode-map
      [remap find-file]              #'dired-x-find-file
      [remap find-file-other-window] #'dired-x-find-file-other-window))
  ;;
  (after-load (init-dired-x dired-x)
    (init-define-keys-kbd dired-mode-map
      [remap find-file-other-frame] #'dired-x-find-file-other-frame))
  ;;
  (after-load init-dired-details
    (init-define-keys-kbd dired-mode-map
      "#" #'dired-details-toggle))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd dired-mode-map
      :motion
      [remap devil-line-forward ] #'dired-next-line
      [remap devil-line-backward] #'dired-previous-line)))
;;
(init-package dired
  :built-in)
