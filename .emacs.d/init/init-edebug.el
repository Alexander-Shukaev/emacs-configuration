(eval-when-compile
  (require 'init-package))
;;
(init-handler edebug init
  (setq-default
    edebug-inhibit-emacs-lisp-mode-bindings t
    global-edebug-prefix                    (kbd "M-d")))
;;
(init-handler edebug config)
;;
(init-handler edebug edebug-global-map
  (let ((keymap (or (bound-and-true-p edebug-global-map)
                    (with-no-warnings global-edebug-map))))
    (init-assert (null (keymap-parent keymap)))
    (init-define-keys-kbd keymap
      "C-g" #'abort-recursive-edit
      ;;
      "RET" #'edebug-set-initial-mode
      ;;
      "n" #'edebug-next-mode
      ;;
      "B" #'edebug-unset-breakpoint)))
;;
(init-package edebug
  :built-in)
