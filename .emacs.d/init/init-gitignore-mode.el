(eval-when-compile
  (require 'init-package))
;;
(init-handler gitignore-mode init)
;;
(init-handler gitignore-mode config
  (init-defun gitignore-mode-setup ()
    #$
    (setq-local comment-start "#"))
  (add-hook 'gitignore-mode-hook #'gitignore-mode-setup))
;;
(init-package gitignore-mode
  :ensure git-modes
  :mode
  (("/\\.ignore\\'" . gitignore-mode)))
