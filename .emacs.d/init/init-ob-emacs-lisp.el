(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-emacs-lisp prepare
  (init-load ob :no-error))
;;
(init-handler ob-emacs-lisp init
  (after-load org
    (dolist (language '((emacs-lisp . t)))
      (add-to-list 'org-babel-load-languages language))))
;;
(init-handler ob-emacs-lisp config)
;;
(init-package ob-emacs-lisp
  :built-in)
