(eval-when-compile
  (require 'init-package))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Suggest Emacs developers to inherit `completion-list-mode' from
;; `special-mode' and to modularize the monstrous `simple'.
;; ---------------------------------------------------------------------------
;;
(init-handler completion-list preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler completion-list prepare
  (init-load simple))
;;
(init-handler completion-list init
  (after-load simple
    (require 'completion-list)))
;;
(init-handler completion-list config
  (after-load evil
    (evil-set-initial-state 'completion-list-mode 'motion)
    ;;
    (evil-declare-motion 'next-completion)
    (evil-declare-motion 'previous-completion)))
;;
(init-handler completion-list mode-map
  (let ((keymap completion-list-mode-map))
    (if (< emacs-major-version 28)
        (progn
          (init-assert (null (keymap-parent keymap)))
          (init-clear-keymap keymap)
          (set-keymap-parent keymap special-mode-map))
      (init-assert (equal (keymap-parent keymap) special-mode-map))
      (init-clear-keymap keymap)
      (init-assert (equal (keymap-parent keymap) special-mode-map)))
    (init-define-keys-kbd keymap
      "<follow-link>" 'mouse-face
      ;;
      "<mouse-2>" #'choose-completion
      ;;
      "<right>" #'next-completion
      "<left> " #'previous-completion
      ;;
      "TAB"       #'next-completion
      "<backtab>" #'previous-completion
      ;;
      "RET" #'choose-completion))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd completion-list-mode-map
      :motion
      [remap devil-char-forward ] #'next-completion
      [remap devil-char-backward] #'previous-completion
      ;;
      "TAB"       #'next-completion
      "<backtab>" #'previous-completion)
    ;;
    (devil-repeat-motions-kbd
      :keymap completion-list-mode-map
      "TAB"       "TAB" "<backtab>"
      "<backtab>" "TAB" "<backtab>")))
;;
(init-package completion-list
  :load-path "lisp/completion-list")
