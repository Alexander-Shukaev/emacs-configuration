(eval-when-compile
  (require 'init-package))
;;
(init-handler dired-details init)
;;
(init-handler dired-details config
  (init-assert (fboundp 'dired-details-install))
  (fmakunbound 'dired-details-install))
;;
(init-package dired-details
  :when
  (version< emacs-version "24.4")
  :commands
  (dired-details-activate
   dired-details-delete-overlays
   dired-details-toggle))
