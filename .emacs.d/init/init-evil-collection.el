(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-collection preface
  (after-load warnings
    (add-to-list 'warning-suppress-log-types '(evil-collection))))
;;
(init-handler evil-collection prepare
  (init-load annalist :no-error)
  ;;
  (init-load evil :no-error))
;;
(init-handler evil-collection init
  (setq-default
    evil-collection-setup-debugger-keys        nil
    evil-collection-setup-minibuffer           nil
    ;;
    evil-collection-want-find-usages-bindings  t
    evil-collection-want-unimpaired-p          nil
    ;;
    evil-collection-calendar-want-org-bindings nil
    ;;
    evil-collection-mode-list                  nil))
;;
(init-handler evil-collection config
  (init-eval-and-compile-when-compile
    (init-assert (init-load-history "evil-integration"))
    (init-negate (init-load-history "evil-keybindings")))
  (if (bound-and-true-p evil-disable-insert-state-bindings)
      (init-assert (equal evil-collection-state-denylist '(insert)))
    (init-negate evil-collection-state-denylist)))
;;
(init-package evil-collection)
