(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-makefile prepare
  (init-load ob :no-error))
;;
(init-handler ob-makefile init
  (after-load org
    (dolist (language '((makefile . t)))
      (add-to-list 'org-babel-load-languages language))))
;;
(init-handler ob-makefile config)
;;
(init-package ob-makefile
  :built-in)
