(require 'macroexp)
;;
(eval-when-compile
  (require 'cl-lib))
;;
;; ---------------------------------------------------------------------------
;;;###autoload
(eval-and-compile
  (define-advice eval-when-compile
      (:filter-return (expression) init/:filter-return)
    (if (and (listp             expression)
             (eq      (car-safe expression) 'quote)
             (stringp (cadr     expression)))
        (cadr expression)
      expression)))
;;
;; NOTE:
;;
;; The above advice is useful to, for example, allow `eval-when-compile' as
;; DOCSTRING for `defun', `defmacro', etc.
;; ---------------------------------------------------------------------------
;;
(defmacro init-assert
    (form)
  "\
Signal `error' if FORM evaluates to nil."
  (declare (debug  t)
           (indent defun))
  (macroexpand `(cl-assert (identity ,form) :show-args)))
;;
(defmacro init-reject
    (form)
  "\
Signal `error' if FORM evaluates to t."
  (declare (debug  t)
           (indent defun))
  (macroexpand `(cl-assert (not      ,form) :show-args)))
(eval-and-compile
  (defalias 'init-negate #'init-reject))
;;
(defmacro init-display-graphic-p ()
  "\
Compatibility wrapper for `display-graphic-p'.

In older versions of Emacs, `display-graphic-p' did not exist.
In Emacs 23 and newer, it does."
  (declare (debug t))
  (if (< emacs-major-version 23)
      '(or window-system (getenv "DISPLAY") (getenv "WAYLAND_DISPLAY"))
    '(display-graphic-p)))
;;
(defsubst init-wsl-p ()
  "\
Return t if running Emacs instance on WSL."
  (and (bound-and-true-p operating-system-release)
       (string-match-p "-[Mm]icrosoft" operating-system-release)
       t))
;;
(defmacro init-ignore-progn
    (&rest body)
  "\
Ignore BODY."
  (declare (debug  t)
           (indent defun))
  nil)
;;
(defmacro init-backtrace
    (&optional base)
  "\
Compatibility wrapper for `backtrace'.

If non-nil, BASE should be a function, and
frames before its nearest activation frame are discarded."
  (declare (debug t))
  (when (and (listp base)
             (eq (car-safe base) 'quote))
    (setq base (cadr base)))
  (if (and (fboundp 'mapbacktrace)
           (fboundp 'backtrace--print-frame))
      `(mapbacktrace #'backtrace--print-frame ',(or base 'mapbacktrace))
    '(backtrace)))
;;
;; TODO: Utilize `backtrace' library in recent version(s) of Emacs?
(defsubst init-backtrace-to-string ()
  "\
Return a trace of (Lisp) function calls currently active as string."
  (with-temp-buffer
    (let ((backtrace-fontify               nil)
          (print-circle                    t)
          (print-escape-control-characters t)
          (print-escape-newlines           t)
          (print-level                     (or print-level 8))
          (standard-output                 (current-buffer)))
      (init-backtrace 'init-backtrace-to-string)
      (buffer-substring-no-properties (point-min) (point-max)))))
;;
(defsubst init-current-call-stack ()
  "\
Return frames of the current call stack."
  (let ((frames)
        (frame)
        (index 5))
    (while (setq frame (backtrace-frame index))
      (push frame frames)
      (cl-incf index))
    (cl-remove-if-not 'car frames)))
;;
;; TODO: Refactor relevant `lwarn' calls to this one (see `devil-warning'):
(defun init-warning
    (level format-string &rest args)
  "\
Display a warning message with trace of (Lisp) function calls.

See `lwarn' and `init-backtrace-to-string'."
  (setq format-string (concat format-string "\n  %S"))
  (setq args          (nconc args (list (current-buffer))))
  (let ((backtrace-string (init-backtrace-to-string)))
    (unless (zerop (length backtrace-string))
      (setq format-string (concat format-string "\n%s"))
      (setq args          (nconc args (list backtrace-string)))))
  (apply #'lwarn 'init level format-string args))
;;
(defmacro init-current-function ()
  "\
Return symbol of the current function at expansion time."
  (declare (debug t))
  (let ((frame (cl-find-if #'(lambda
                                 (frame)
                               (let (debug-on-error)
                                 (ignore-errors
                                   (eq (cl-caaddr frame) 'defalias))))
                           (reverse (init-current-call-stack)))))
    `',(cl-cadadr (cl-caddr frame))))
;;
(defsubst init-load-history
    (&optional library no-suffix path)
  "\
Return mapping(s) of loaded file name to symbols and features."
  (if library
      (let ((file (locate-library library no-suffix path)))
        (when file
          (setq file (expand-file-name file))
          (or (when (string-match-p "\\.el\\'" file)
                (assoc (concat file "c") load-history))
              (assoc file load-history))))
    load-history))
;;
(defsubst init-load-p ()
  "\
Return t if both `load-in-progress' and `load-file-name' are non-nil."
  (and load-in-progress load-file-name t))
;;
(defsubst init-byte-compile-p ()
  "\
Return t if `init-load-p' is nil and either `byte-compile-current-file'
or `comp-native-compiling' is non-nil."
  (unless (init-load-p)
    (and (or (bound-and-true-p byte-compile-current-file)
             (bound-and-true-p comp-native-compiling))
         t)))
(defalias 'init-compile-p #'init-byte-compile-p)
;;
(defmacro init-eval-and-compile-when-compile
    (&rest body)
  "\
Like `progn', but evaluates the body at compile time and at load time.
In interpreted code, this is entirely equivalent to `progn'."
  (declare (debug  t)
           (indent defun))
  `(,(if (init-compile-p)
         'eval-and-compile
       'progn)
    ,@body))
;;
(defmacro init-with-demoted-errors-when-called-interactively
    (format &rest body)
  "\
Run BODY and demote any errors to simple messages.

See `with-demoted-errors' and `called-interactively-p'."
  (declare (debug  t)
           (indent defun))
  `(if (called-interactively-p 'any)
       (with-demoted-errors ,format
         ,@body)
     ,@body))
;;
(defmacro init-require
    (feature &optional file no-error)
  "\
If feature FEATURE is not loaded, load it from file FILE.

If the optional third argument NO-ERROR is non-nil, return nil if
the file FILE is not found instead of signaling an error.  If the
required feature FEATURE is not provided by the file FILE, return
the `:disabled' keyword.  Normally the return value is FEATURE.

See `require'."
  (declare (debug t))
  (when (and (listp feature)
             (eq (car-safe feature) 'quote))
    (setq feature (cadr feature)))
  `(condition-case e
       (require ',feature ,file ',no-error)
     (error (if (and (stringp        (cadr e))
                     (string-match-p ,(mapconcat
                                       #'(lambda
                                             (s)
                                           (regexp-quote
                                            (format-message s feature)))
                                       '("failed to provide feature `%s'"
                                         "feature `%s' was not provided")
                                       "\\|")
                                     (cadr e)))
                :disabled
              (signal (car e) (cdr e))))))
;;
(defmacro init-without-after-load-when-compile
    (&rest body)
  (declare (debug  t)
           (indent defun))
  `(progn
     ,@(when (init-compile-p)
         `((eval-when-compile
             (defvar init--after-load-alist after-load-alist)
             (setq after-load-alist nil))))
     (unwind-protect
         (progn
           ,@body)
       ,@(when (init-compile-p)
           `((eval-when-compile
               (setq after-load-alist init--after-load-alist)))))))
;;
;; TODO: Can be more generic function disable/substitution macro:
(defmacro init-without-force-mode-line-update
    (&rest body)
  "\
Execute BODY without force redisplay of the current buffer's mode line.

See `force-mode-line-update'."
  (declare (debug  t)
           (indent defun))
  (let* ((         symbol 'force-mode-line-update)
         (  symbol-symbol (cl-gentemp (format "%s--symbol-"   symbol)))
         (function-symbol (cl-gentemp (format "%s--function-" symbol))))
    `(let* ((  ,symbol-symbol ',symbol)
            (,function-symbol (symbol-function ,symbol-symbol)))
       (fset ,symbol-symbol #'ignore)
       (unwind-protect
           (progn ,@body)
         (fset ,symbol-symbol ,function-symbol)))))
;;
(defmacro init-without-timers
    (&rest body)
  "\
Execute BODY without running timers.

See `timer-list', `timer-idle-list', and `timer-event-handler'."
  (declare (debug  t)
           (indent defun))
  (let* ((          symbol 'init-without-timers)
         (     list-symbol (cl-gentemp (format "%s--list-"      symbol)))
         (idle-list-symbol (cl-gentemp (format "%s--idle-list-" symbol))))
    `(let ((     ,list-symbol timer-list)
           (,idle-list-symbol timer-idle-list))
       (setq
         timer-list      nil
         timer-idle-list nil)
       (unwind-protect
           (progn ,@body)
         (dolist (symbol '(timer-list timer-idle-list))
           (let ((value (symbol-value symbol)))
             (when value
               (lwarn 'init
                      :warning
                      "Set the non-nil value of variable `%s': %s\n%s"
                      symbol
                      value
                      (init-backtrace-to-string)))))
         (setq
           timer-list      ,list-symbol
           timer-idle-list ,idle-list-symbol)))))
;;
(defmacro init-without-undo
    (&rest body)
  "\
Execute BODY without recording undo entries in `buffer-undo-list'."
  (declare (debug  t)
           (indent defun))
  `(let (buffer-undo-list)
     ;; TODO: Why not just bind `buffer-undo-list' to t?
     (buffer-disable-undo)
     (unwind-protect
         (progn ,@body)
       (buffer-enable-undo))))
;;
(defmacro init-defad
    (symbol args file &rest body)
  "\
Expand to `declare-function' followed by `define-advice'."
  (declare (debug      t)
           (doc-string 4)
           (indent     2))
  (let* ((arglist (nth 1 args))
         (name    (nth 2 args))
         (advice  (and name
                       (or (stringp name) (symbolp name))
                       (intern (format "%s@%s" symbol name)))))
    (macroexp-progn `(,@(when advice
                          `((declare-function ,advice ,file ,arglist)))
                      (define-advice ,symbol ,args ,@body)))))
;;
(defmacro init-defun
    (name arglist file &optional docstring &rest body)
  "\
Expand to `declare-function' followed by `defun'."
  (declare (debug      t)
           (doc-string 4)
           (indent     2))
  `(progn
     (declare-function ,name ,file ,arglist)
     (defun ,name ,arglist ,docstring ,@body)))
;;
(defmacro init-without-keymap-parent
    (keymap &rest body)
  "\
Execute BODY without the parent keymap of keymap KEYMAP."
  (declare (debug  t)
           (indent defun))
  (let* ((           symbol 'init-without-keymap-parent)
         (       map-symbol (cl-gentemp (format "%s--map-"        symbol)))
         (parent-map-symbol (cl-gentemp (format "%s--parent-map-" symbol))))
    `(let* ((       ,map-symbol ,keymap)
            (,parent-map-symbol (keymap-parent ,map-symbol)))
       (set-keymap-parent ,map-symbol nil)
       (unwind-protect
           (progn ,@body)
         (set-keymap-parent ,map-symbol ,parent-map-symbol)
         (init-assert
           (eq (keymap-parent ,map-symbol) ,parent-map-symbol))))))
;;
(eval-and-compile
  (defun init-define-key
      (keymap key def)
    (eval-when-compile
      (documentation 'define-key))
    (dolist (arg (list key def))
      (when (or (stringp arg)
                (vectorp arg))
        (init-negate (zerop (length arg)))))
    (when (and def (not (eq def #'undefined)))
      (let ((command (init-without-keymap-parent keymap
                       (init-key-command key keymap))))
        (init-assert (or (null  command)
                         (eq    command #'undefined)
                         (equal command def)))))
    (define-key keymap key def)))
;;
(defun init-define-key-kbd
    (keymap key def)
  (eval-when-compile
    (documentation 'init-define-key))
  (dolist (arg (list key def))
    (when (or (stringp arg)
              (vectorp arg))
      (init-negate (zerop (length arg))))
    (when (stringp arg)
      (init-negate (string-match-p "^[\s\t\n\f]+$" arg))))
  (init-define-key keymap
                   (if (stringp key) (kbd key) key)
                   (if (stringp def) (kbd def) def)))
;;
(defmacro init-define-keys
    (keymap &rest ...)
  "\
Apply `init-define-key' to KEYMAP and each two consecutive arguments.
KEYMAP is a keymap.
Return KEYMAP."
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(init-define-key keymap ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(let ((keymap ,keymap))
       ,@body
       keymap)))
;;
(defmacro init-define-keys-with-prefix
    (keymap key &rest ...)
  "\
Apply `init-define-keys' to a keymap with prefix KEY in KEYMAP.
If no such keymap exists, make new sparse keymap with prefix KEY
in KEYMAP and apply `init-define-keys' to it instead.
KEYMAP is a keymap.
Return a keymap with prefix KEY in KEYMAP."
  (declare (debug  t)
           (indent defun))
  (let* ((    symbol 'init-define-keys-with-prefix)
         (map-symbol (cl-gentemp (format "%s--map-" symbol))))
    `(let* ((keymap      ,keymap)
            (,map-symbol (or (lookup-key keymap ,key)
                             (make-sparse-keymap))))
       (init-define-key keymap ,key (init-define-keys ,map-symbol
                                      ,@...))
       ,map-symbol)))
;;
(defmacro init-define-keys-kbd
    (keymap &rest ...)
  "\
Apply `init-define-key-kbd' to KEYMAP and each two consecutive arguments.
KEYMAP is a keymap.
Return KEYMAP."
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(init-define-key-kbd keymap ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(let ((keymap ,keymap))
       ,@body
       keymap)))
;;
(defmacro init-define-keys-with-prefix-kbd
    (keymap key &rest ...)
  "\
Apply `init-define-keys-kbd' to a keymap with prefix KEY in KEYMAP.
If no such keymap exists, make new sparse keymap with prefix KEY
in KEYMAP and apply `init-define-keys-kbd' to it instead.
KEYMAP is a keymap.
Return a keymap with prefix KEY in KEYMAP."
  (declare (debug  t)
           (indent defun))
  (let* ((    symbol 'init-define-keys-with-prefix-kbd)
         (map-symbol (cl-gentemp (format "%s--map-" symbol))))
    `(let* ((keymap      ,keymap)
            (,map-symbol (or (lookup-key keymap ,(cond ((stringp key)
                                                        (kbd key))
                                                       ((vectorp key)
                                                        key)
                                                       (t
                                                        `(if (stringp ,key)
                                                             (kbd ,key)
                                                           ,key))))
                             (make-sparse-keymap))))
       (init-define-key-kbd keymap ,key (init-define-keys-kbd ,map-symbol
                                          ,@...))
       ,map-symbol)))
;;
(provide 'init-macros)
