(eval-when-compile
  (require 'init-package))
;;
(init-handler sync-recentf prepare
  (init-load recentf :no-error))
;;
(init-handler sync-recentf init)
;;
(init-handler sync-recentf config)
;;
(init-package sync-recentf
  ;; -------------------------------------------------------------------------
  ;; DEPRECATED:
  ;;
  ;; Because of `mneme':
  ;;
  :disabled
  ;; -------------------------------------------------------------------------
  )
