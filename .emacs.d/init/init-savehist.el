(eval-when-compile
  (require 'init-package))
;;
(init-handler savehist init
  (setq-default
    savehist-additional-variables    '(compile-command
                                       kill-ring
                                       search-ring
                                       regexp-search-ring)
    savehist-ignored-variables       '(command-history)
    savehist-save-minibuffer-history t
    savehist-autosave-interval       60; seconds
    )
  ;;
  (after-init (savehist-mode)))
;;
(init-handler savehist config)
;;
(init-package savehist
  :built-in)
