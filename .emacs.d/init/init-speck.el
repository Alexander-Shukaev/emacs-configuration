(eval-when-compile
  (require 'init-package))
;;
(init-handler speck init
  (setq-default
    speck-hunspell-program
    (executable-find "hunspell")
    ;;
    speck-engine
    (unless (zerop (length speck-hunspell-program))
      'Hunspell)
    ;;
    speck-hunspell-library-directory
    (if (or (eq system-type 'windows-nt)
            (zerop (length speck-hunspell-program)))
        ""
      (let* ((share-directory (expand-directory "share/"
                                                (file-name-directory
                                                 (directory-file-name
                                                  (file-name-directory
                                                   speck-hunspell-program)))))
             (directory (catch 'break
                          (dolist (directory
                                   `(,(expand-directory "hunspell/"
                                                        share-directory)
                                     ,(expand-directory "myspell/"
                                                        share-directory)))
                            (and (not (zerop (length directory)))
                                 (file-directory-p directory)
                                 (throw 'break directory))))))
        directory))
    ;;
    speck-hunspell-default-dictionary-name
    "en"
    ;;
    speck-hunspell-dictionary-alist
    `(("en" . ,(if (eq system-type 'windows-nt) "en_US-large" "en_US")))
    ;;
    speck-hunspell-language-options
    '(("en" utf-8 nil nil))
    ;;
    speck-hunspell-coding-system
    'utf-8))
;;
(init-handler speck config
  (init-defun init-speck-deactivate-all
      (&optional buffer-list)
    #$
    (setq buffer-list (or buffer-list (copy-sequence speck-buffer-list)))
    (save-current-buffer
      (dolist (buffer buffer-list)
        (when (buffer-live-p buffer)
          (set-buffer buffer)
          (speck-deactivate)))))
  ;;
  (init-defun init-speck-activate-all
      (&optional buffer-list)
    #$
    (setq buffer-list (or buffer-list (copy-sequence speck-buffer-list)))
    (save-current-buffer
      (dolist (buffer buffer-list)
        (when (buffer-live-p buffer)
          (set-buffer buffer)
          (let ((speck-retain-local-variables t))
            (speck-activate))))))
  ;;
  (init-defun init-speck-reactivate-all
      (&optional buffer-list)
    #$
    (setq buffer-list (or buffer-list (copy-sequence speck-buffer-list)))
    (when buffer-list
      (init-speck-deactivate-all buffer-list)
      ;; Hunspell occasionally hangs when restarting, maybe the
      ;; following helps.
      (sleep-for 0.1)
      (init-speck-activate-all buffer-list)))
  ;;
  (init-defun init-speck-reactivate
      (&optional process)
    #$
    (setq process (or process speck-process))
    (when (processp process)
      (let ((buffer-list (copy-sequence (assq process
                                              speck-process-buffer-alist))))
        (init-speck-reactivate-all buffer-list))))
  ;;
  (init-defun init-speck-reactivate-maybe
      (&optional process)
    #$
    (setq process (or process speck-process))
    (when (processp process)
      (unless (process-live-p process)
        (init-speck-reactivate process))))
  ;;
  (after-load loadhist
    (init-defun speck-unload-function ()
      #$
      "\
Unload function for Speck.

See `unload-feature' and the Info node `(elisp) Unloading'."
      ;; ---------------------------------------------------------------------
      ;; TODO:
      ;;
      ;; This kind of call to `unload-feature' for the corresponding feature
      ;; with `init-' prefix should be defined by `init-package' by default:
      ;;
      (init-ignore-progn
        (condition-case-unless-debug e
            (unload-feature 'init-speck :force)
          (error (lwarn 'init
                        :error
                        "Failed to unload feature `%s': %s\n%s"
                        'init-speck
                        (error-message-string e)
                        (init-backtrace-to-string))
                 nil)))
      ;;
      ;; BUG:
      ;;
      ;; See `TODO' above `init-load-advice/:before' for description of the
      ;; current deficiency of this solution because of what it is presently
      ;; disabled (as per `init-ignore-progn').  The bug can easily be
      ;; revealed when `unload-feature' is called for `speck' and then
      ;; `speck-mode' autoloaded function is called.
      ;; ---------------------------------------------------------------------
      (init-speck-deactivate-all)
      (dolist (symbol '(speck-engine
                        speck-hunspell-program
                        speck-hunspell-library-directory
                        speck-hunspell-default-dictionary-name
                        speck-hunspell-dictionary-alist
                        speck-hunspell-language-options
                        speck-hunspell-coding-system))
        (setq unload-function-defs-list (delq symbol
                                              unload-function-defs-list)))
      nil))
  ;;
  (init-defad speck-mode
      (:around (function &rest ...) init/:around)
    #$
    (when speck-engine
      (apply function ...)))
  ;;
  (init-defad speck-chunk
      (:around (function &rest ...) init/:around)
    #$
    (init-without-timers
      (apply function ...)))
  ;;
  (init-defad speck-chunk
      (:before (&rest ...) init/:before)
    #$
    (init-speck-reactivate-maybe))
  ;;
  (init-defad speck-word
      (:around (function &rest ...) init/:around)
    #$
    (init-without-timers
      (apply function ...)))
  ;;
  (init-defad speck-word
      (:before (&rest ...) init/:before)
    #$
    (init-speck-reactivate-maybe))
  ;;
  (init-defad speck-set-html-mode
      (:around (function &rest ...) init/:around)
    #$
    (init-without-timers
      (apply function ...)))
  ;;
  (init-defad speck-get-option
      (:around (function &rest ...) init/:around)
    #$
    (init-without-timers
      (apply function ...))))
;;
(init-handler speck mode-map
  (let ((keymap speck-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)))
;;
(init-handler speck overlay-map
  (let ((keymap speck-overlay-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      "<down-mouse-3>" #'ignore
      ;;
      "<mouse-3>"      #'speck-mouse-popup-menu
      ;;
      "<up-mouse-3>"   #'ignore)))
;;
(init-package speck)
