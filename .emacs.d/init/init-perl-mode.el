(eval-when-compile
  (require 'init-package))
;;
(init-handler perl-mode init
  (after-load init-cperl-mode
    (defalias 'perl-mode #'cperl-mode)))
;;
(init-handler perl-mode config)
;;
(init-package perl-mode
  :built-in)
