(eval-when-compile
  (require 'init-package))
;;
(init-handler frame preface
  (init-load font))
;;
(init-handler frame prepare
  (init-negate (fboundp 'set-frame-fullscreen))
  (defun set-frame-fullscreen
      (&optional frame)
    (interactive)
    (when (with-selected-frame (or frame (selected-frame))
            (and (if (fboundp 'frame-focus-state) (frame-focus-state) t)
                 (not noninteractive)
                 (init-display-graphic-p)))
      (modify-frame-parameters frame '((fullscreen . fullboth)))))
  ;;
  (defun init-frame-fullscreen-setup ()
    (if (boundp 'after-focus-change-function)
        (add-function :after
                      after-focus-change-function
                      #'set-frame-fullscreen)
      (add-hook 'focus-in-hook #'set-frame-fullscreen)))
  ;;
  (defun init-frame-font-setup
      (&optional frame)
    (unless frame (setq frame (selected-frame)))
    (with-selected-frame frame
      (when (and (if (fboundp 'frame-focus-state) (frame-focus-state) t)
                 (not noninteractive)
                 (init-display-graphic-p))
        (let ((font-backend (cdr (assoc 'font-backend default-frame-alist))))
          (if font-backend
              (when (eq frame frame-initial-frame)
                (modify-frame-parameters frame
                                         `((font-backend . ,font-backend)))
                (unless noninteractive
                  (message "Font Backend: `%s'" font-backend)))
            ;; ---------------------------------------------------------------
            ;; NOTE:
            ;;
            ;; As per bug#43177, a modern font backend (e.g. `ftcrhb') is a
            ;; performance game changer:
            ;;
            (when (and (> emacs-major-version 26)
                       (boundp 'cairo-version-string))
              (setq font-backend "ftcrhb"))
            ;; ---------------------------------------------------------------
            (when font-backend
              (add-to-list 'default-frame-alist
                           `(font-backend . ,font-backend))
              (modify-frame-parameters frame
                                       `((font-backend . ,font-backend)))
              (unless noninteractive
                (message "Font Backend: `%s'" font-backend)))))
        (let ((font (cdr (assoc 'font default-frame-alist))))
          (if font
              (when (eq frame frame-initial-frame)
                (set-frame-font font t t)
                (unless noninteractive
                  (message "Font: `%s'" font)))
            (let ((font-family (catch 'break
                                 (dolist (font-family init-font-families)
                                   (when (member font-family
                                                 (font-family-list))
                                     (throw 'break font-family))))))
              (setq font (when font-family
                           (format "%s-%d" font-family init-font-size))))
            (when font
              (add-to-list 'default-frame-alist `(font . ,font))
              (set-frame-font font t t)
              (unless noninteractive
                (message "Font: `%s'" font))))))))
  ;;
  (defun init-frame-keys-setup
      (&optional frame)
    (unless frame (setq frame (selected-frame)))
    (with-selected-frame frame
      (unless (or noninteractive (init-display-graphic-p))
        ;; In Emacs, <backspace> is supposed to be equivalent to <DEL>.
        ;; Hence, one way to have a setup that works in all contexts (and with
        ;; minimum configuration) is to have the <backspace> key generate
        ;; <DEL> when on the console or the terminal as per [1, 2, 3]:
        ;;
        (when (and (stringp (tty-type))
                   (boundp 'tty-erase-char))
          (let* ((symbol  'tty-erase-char)
                 (current (symbol-value symbol))
                 (correct ?\^?))
            (unless (eq current correct)
              (lwarn 'init
                     :emergency
                     "Expected the correct value of variable `%s':
- current: %s
- correct: %s\n%s"
                     symbol
                     current
                     correct
                     (init-backtrace-to-string))))))
      ;; TODO:
      )))
;;
(init-handler frame init
  (setq-default
    blink-cursor-blinks
    0
    ;; -----------------------------------------------------------------------
    blink-cursor-delay
    0.5; seconds
    ;; -----------------------------------------------------------------------
    blink-cursor-interval
    0.5; seconds
    ;; -----------------------------------------------------------------------
    focus-follows-mouse
    t
    ;; -----------------------------------------------------------------------
    frame-resize-pixelwise
    t
    ;; -----------------------------------------------------------------------
    frame-title-format
    '((:eval
       (let* ((frame-title-format)
              (path          (or (buffer-file-name)
                                 (file-name-as-directory default-directory)))
              (method-string (or (file-remote-p path 'method)
                                 ""))
              (user-string   (or (file-remote-p path 'user)
                                 (user-login-name)))
              (host-string   (or (file-remote-p path 'host)
                                 (system-name)))
              ;; -------------------------------------------------------------
              ;; [RESOLVED] bug#27315:
              ;;
              ;; On Emacs 26, function `abbreviate-file-name' triggers
              ;; `tramp-file-name-handler' if the supplied argument contains
              ;; matching TRAMP-compatible path.  As a result, accessing
              ;; remote file causes Emacs to hang with the "Tramp: Sending
              ;; Password" message in the minibuffer.  This can be avoided
              ;; either by locally let-binding `file-name-handler-alist' to
              ;; nil or not applying `abbreviate-file-name' to remote paths
              ;; altogether (since it looks to be pointless in this case
              ;; anyway):
              ;;
              (path-string   (or (file-remote-p path 'localname)
                                 (abbreviate-file-name path)))
              ;; -------------------------------------------------------------
              )
         `(,@(unless (zerop (length method-string))
               `(,method-string
                 ":"))
           ,user-string
           "@"
           ,host-string
           ":"
           ,path-string)))))
  ;;
  (let ((alpha 95))
    (when (init-wsl-p)
      (setq alpha (+ alpha 3)))
    (add-to-list 'default-frame-alist `(alpha ,alpha ,(- alpha 10))))
  ;;
  (when (daemonp)
    (init-negate (init-display-graphic-p))
    (init-negate (tty-type)))
  (unless (or noninteractive (daemonp))
    (when (init-display-graphic-p)
      (init-frame-font-setup))
    (init-frame-keys-setup))
  ;;
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; Simply became annoying:
  ;;
  (init-ignore-progn
    (after-init (init-frame-fullscreen-setup)))
  ;; -------------------------------------------------------------------------
  )
;;
(init-handler frame config
  (add-to-list 'initial-frame-alist '(fullscreen         . fullboth))
  (add-to-list 'initial-frame-alist '(fullscreen-restore . maximized))
  ;;
  (if (boundp 'after-focus-change-function)
      (add-function :after
                    after-focus-change-function
                    #'init-frame-font-setup)
    (add-hook 'focus-in-hook #'init-frame-font-setup))
  ;;
  (dolist (hook '(after-make-frame-functions))
    (add-hook hook #'init-frame-font-setup)
    (add-hook hook #'init-frame-keys-setup))
  ;;
  (after-init
    (auto-raise-mode)
    (unless (bound-and-true-p blink-cursor-mode)
      (unless (or noninteractive
                  no-blinking-cursor
                  (eq system-type 'ms-dos)
                  (not (memq window-system '(x w32 ns))))
        (lwarn 'init
               :warning
               "Disabled mode `%s'\n%s"
               'blink-cursor-mode
               (init-backtrace-to-string)))
      (unless (or noninteractive
                  emacs-basic-display
                  (eq system-type 'ms-dos))
        (blink-cursor-mode)))))
;;
(init-package frame
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(emacs) DEL Does Not Delete'
;;  [2] Info node `(efaq) Backspace invokes help'
;;  [3] URL `http://tldp.org/HOWTO/Keyboard-and-Console-HOWTO-5.html'
;;  ==========================================================================
;;  }}} References
