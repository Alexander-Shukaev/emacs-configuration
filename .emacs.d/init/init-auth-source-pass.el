(eval-when-compile
  (require 'init-package))
;;
(eval-when-compile
  (require 'cl-lib))
;;
(init-handler auth-source-pass prepare
  (init-load auth-source :no-error))
;;
(init-handler auth-source-pass init
  (after-init
    (unless (memq 'password-store (bound-and-true-p auth-sources))
      ;; TODO: `define-advice'
      (add-to-list 'auth-sources 'password-store :append)
      (auth-source-pass-enable)
      (init-negate (eq (car-safe auth-sources)        'password-store))
      (init-assert (eq (car-safe (last auth-sources)) 'password-store)))))
;;
(init-handler auth-source-pass config)
;;
(cl-macrolet
    ((define-package ()
       `(init-package auth-source-pass
          ,(unless (version< emacs-version "26.1")
             :built-in))))
  (define-package))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(auth) Help for users'
;;  [2] Info node `(auth) The Unix password store'
;;  [3] URL `http://www.passwordstore.org'
;;  ==========================================================================
;;  }}} References
