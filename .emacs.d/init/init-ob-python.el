(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-python prepare
  (init-load ob :no-error))
;;
(init-handler ob-python init
  (after-load org
    (dolist (language '((python . t)))
      (add-to-list 'org-babel-load-languages language))))
;;
(init-handler ob-python config)
;;
(init-package ob-python
  :built-in)
