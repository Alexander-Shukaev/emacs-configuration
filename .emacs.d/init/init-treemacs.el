(eval-when-compile
  (require 'init-package))
;;
(init-handler treemacs init)
;;
(init-handler treemacs config)
;;
(init-package treemacs)
