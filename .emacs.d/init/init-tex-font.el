(eval-when-compile
  (require 'init-package))

(init-handler tex-font init)

(init-handler tex-font config)

(init-package tex-font
  :ensure auctex)
