(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-C prepare
  (init-load ob :no-error))
;;
(init-handler ob-C init
  (after-load org
    (dolist (language '((C . t)))
      (add-to-list 'org-babel-load-languages language))))
;;
(init-handler ob-C config)
;;
(init-package ob-C
  :built-in)
