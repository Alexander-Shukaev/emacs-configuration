(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-surround preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler evil-surround prepare
  (init-load evil :no-error))
;;
(init-handler evil-surround init
  (setq-default evil-surround-pairs-alist
    '((?\# . ("#{" . "}"))
      (?\$ . ("${" . "}"))
      (?\% . ("%{" . "}"))
      ;;
      (?\( . ("(" . ")"))
      (?\) . ("(" . ")"))
      (?\< . ("<" . ">"))
      (?\> . ("<" . ">"))
      (?\[ . ("[" . "]"))
      (?\] . ("[" . "]"))
      (?\{ . ("{" . "}"))
      (?\} . ("{" . "}"))
      ;;
      (?\/ . ("\/" . "\/"))
      (?\\ . ("\\" . "\\"))
      ;;
      (?P  . ("(" . ")"))
      (?p  . ("(" . ")"))
      ;;
      (?A  . ("<" . ">"))
      (?a  . ("<" . ">"))
      ;;
      (?B  . ("[" . "]"))
      (?b  . ("[" . "]"))
      ;;
      (?C  . ("{" . "}"))
      (?c  . ("{" . "}"))
      ;;
      (?G  . ("`" . "'"))
      (?g  . ("`" . "'"))
      ;;
      (?Q  . ("`" . "'"))
      (?q  . ("`" . "'"))
      ;;
      (?T  . evil-surround-read-tag)
      (?t  . evil-surround-read-tag)
      ;;
      (?F  . evil-surround-function)
      (?f  . evil-surround-function)))
  ;;
  (after-load evil
    (after-init (global-evil-surround-mode))))
;;
(init-handler evil-surround config
  (after-load devil
    (dolist (operator '((devil-amend  . change)
                        (devil-delete . delete)))
      (add-to-list 'evil-surround-operator-alist operator :append))))
;;
(init-handler evil-surround mode-map
  (let ((keymap evil-surround-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap :recursively-p))
  ;;
  (after-load devil
    (dolist (keymap (list evil-normal-state-map evil-operator-state-map))
      (dolist (key '("s" "S"))
        (init-negate (init-without-keymap-parent keymap
                       (init-key-command key keymap)))))
    ;;
    (devil-define-keys-kbd evil-surround-mode-map
      :normal
      "s" #'evil-surround-region
      "S" #'evil-Surround-region
      :operator
      "s" #'evil-surround-edit
      "S" #'evil-Surround-edit)))
;;
(init-package evil-surround)
