(eval-when-compile
  (require 'init-package))
;;
(init-handler jupyter-org-extensions prepare
  (init-load jupyter-org-client :no-error)
  ;;
  (init-load ob :no-error))
;;
(init-handler jupyter-org-extensions init)
;;
(init-handler jupyter-org-extensions config)
;;
(init-package jupyter-org-extensions
  :unless
  (< emacs-major-version 26)
  :ensure jupyter
  :commands
  (jupyter-org-hydra/body))
