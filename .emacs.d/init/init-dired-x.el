(eval-when-compile
  (require 'init-package))
;;
(init-handler dired-x prepare
  ;; TODO: Do the same everywhere (not sure how that's useful as it does not
  ;; protect `:config' from redefining the already defined function):
  (init-negate (fboundp 'dired-x-find-file-other-frame)))
;;
(init-handler dired-x init
  (setq-default
    dired-bind-jump           nil
    dired-bind-man            nil
    dired-bind-info           nil
    dired-x-hands-off-my-keys t))
;;
(init-handler dired-x config
  (init-assert (fboundp 'dired-x-find-file))
  (init-assert (fboundp 'dired-x-find-file-other-window))
  ;;
  (init-defun dired-x-find-file-other-frame
      (filename)
    #$
    "Edit file FILENAME, in another frame.
Like `find-file-other-frame', except that when called interactively with
a prefix argument, it offers the filename near point as a default."
    (interactive (list (dired-x-read-filename-at-point "Find file: ")))
    (find-file-other-frame filename)))
;;
(init-package dired-x
  :built-in)
