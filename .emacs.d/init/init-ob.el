(eval-when-compile
  (require 'init-package))
;;
(init-handler ob prepare
  (init-load ob-async :no-error)
  (init-load ob-core  :no-error))
;;
(init-handler ob init)
;;
(init-handler ob config)
;;
(init-package ob
  :built-in)
