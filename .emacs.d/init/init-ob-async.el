(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-async init
  (setq-default ob-async-no-async-languages-alist '("R"
                                                    "ein-haskell"
                                                    "ein-julia"
                                                    "ein-python"
                                                    "ein-rust"
                                                    "ipython"
                                                    "jupyter-R"
                                                    "jupyter-julia"
                                                    "jupyter-python"
                                                    "python")))
;;
(init-handler ob-async config)
;;
(init-package ob-async)
