(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-maps init
  (setq-default evil-insert-state-bindings nil))
;;
(init-handler evil-maps config
  (init-eval-and-compile-when-compile
    (init-negate (init-load-history "evil-maps"))))
;;
(init-package evil-maps
  :ensure evil)
