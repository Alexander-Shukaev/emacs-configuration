(eval-when-compile
  (require 'init-package))
;;
(init-handler paren preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler paren init
  (setq-default show-paren-style 'expression)
  ;;
  (after-init (show-paren-mode)))
;;
(init-handler paren config
  (after-load (init-solarized-theme solarized-theme)
    (init-defun init-paren-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (set-face-attribute 'show-paren-match nil
                            :background (funcall (if (eq variant 'light)
                                                     #'init-color-sub
                                                   #'init-color-add)
                                                 base02
                                                 "#111111")
                            :foreground 'unspecified)))
    (init-paren-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...) init-paren-solarized-theme-setup/:after)
      #$
      (init-paren-solarized-theme-setup theme))))
;;
(init-package paren
  :built-in)
