(eval-when-compile
  (require 'init-package))
;;
(init-handler org-indent init)
;;
(init-handler org-indent config)
;;
(init-package org-indent
  :built-in
  :diminish
  (org-indent-mode))
