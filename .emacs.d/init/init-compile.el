(eval-when-compile
  (require 'init-package))
;;
(init-handler compile preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler compile init
  (setq-default
    ;; -----------------------------------------------------------------------
    compilation-scroll-output t
    ;;
    ;; NOTE:
    ;;
    ;; Unfortunately, the `first-error' option delivers a lot of false
    ;; positives due to imperfect parsing of output...
    ;; -----------------------------------------------------------------------
    ))
;;
(init-handler compile config
  ;; See [1]:
  (let* ((spec   (cdr (assoc 'guile-line
                             compilation-error-regexp-alist-alist)))
         (regexp (car spec)))
    (init-assert (equal regexp "^ *\\([0-9]+\\): *\\([0-9]+\\)"))
    (setcar spec (concat regexp "\\(?: *\\[.*\\]\\)")))
  (let* ((spec   (cdr (assoc 'cucumber
                             compilation-error-regexp-alist-alist)))
         (regexp (car spec)))
    (if (< emacs-major-version 28)
        (progn
          (init-assert (equal regexp "\\(?:^cucumber\
\\(?: -p [^[:space:]]+\\)?\\|#\\)\
\\(?: \\)\\([^(].*\\):\\([1-9][0-9]*\\)"))
          (setcar spec "\\(?:^cucumber\
\\(?: -p [^[:space:]]+\\)?\\|\\(?:^\\| \\)#\\)\
\\(?: \\)\\([^(].*\\):\\([1-9][0-9]*\\)"))
      (init-assert (equal regexp "\\(?:^\\(?:cucumber\
\\(?: -p [^[:space:]]+\\)?\\|     \\)\\|#\\) \
\\([^(].*\\):\\([1-9][0-9]*\\)"))
      (setcar spec "\\(?:^\\(?:cucumber\
\\(?: -p [^[:space:]]+\\)?\\|     \\)\\|\\(?:^\\| \\)#\\) \
\\([^(].*\\):\\([1-9][0-9]*\\)")))
  ;;
  (after-load evil
    (evil-set-initial-state 'compilation-mode 'motion)
    ;;
    (evil-declare-motion #'compilation-next-error)
    (evil-declare-motion #'compilation-previous-error)
    ;;
    (evil-declare-motion #'next-error-no-select)
    (evil-declare-motion #'previous-error-no-select)
    ;;
    (evil-declare-motion #'compilation-next-file)
    (evil-declare-motion #'compilation-previous-file)
    ;;
    (evil-set-type #'compilation-next-error     'line)
    (evil-set-type #'compilation-previous-error 'line)
    ;;
    (evil-set-type #'next-error-no-select     'line)
    (evil-set-type #'previous-error-no-select 'line)
    ;;
    (evil-set-type #'compilation-next-file     'line)
    (evil-set-type #'compilation-previous-file 'line))
  ;;
  (after-load (init-devil devil)
    (init-defun init-compile-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "k[ill]" #'kill-compilation))
    (dolist (hook '(compilation-minor-mode-hook
                    compilation-shell-minor-mode-hook
                    compilation-mode-hook))
      (add-hook hook #'init-compile-devil-setup)))
  ;;
  (after-load ansi-color
    (init-defun init-compile-ansi-color-process-output ()
      #$
      (when (eq major-mode 'compilation-mode)
        (let ((ansi-color-for-comint-mode (bound-and-true-p
                                           ansi-color-for-compilation-mode))
              (inhibit-read-only t))
          (ansi-color-process-output ""))))
    (add-hook 'compilation-filter-hook
              #'init-compile-ansi-color-process-output))
  ;;
  (after-load ob-compile
    (add-hook 'compilation-finish-functions #'ob-compile-save-file)))
;;
(init-handler compile minor-mode-map
  (let ((keymap compilation-minor-mode-map))
    (init-assert (equal (keymap-parent keymap) special-mode-map))
    (init-clear-keymap    keymap)
    (init-assert (equal (keymap-parent keymap) special-mode-map))
    (init-define-keys-kbd keymap
      [remap next-line    ] #'compilation-next-error
      [remap previous-line] #'compilation-previous-error
      ;;
      [remap revert-buffer] #'recompile
      ;;
      "<follow-link>"        'mouse-face
      "<mouse-2>"           #'compile-goto-error
      ;;
      "C-d"                 #'compilation-display-error
      "RET"                 #'compile-goto-error
      ;;
      "TAB"                 #'compilation-next-error
      "<backtab>"           #'compilation-previous-error
      ;;
      "C-n"                 #'next-error-no-select
      "C-p"                 #'previous-error-no-select
      ;;
      "}"                   #'compilation-next-file
      "{"                   #'compilation-previous-file))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd compilation-minor-mode-map
      :motion
      "}" #'compilation-next-file
      "{" #'compilation-previous-file)
    ;;
    (devil-repeat-motions-kbd
      :keymap compilation-minor-mode-map
      "}" "}" "{"
      "{" "}" "{")))
;;
(init-handler compile shell-minor-mode-map
  (let ((keymap compilation-shell-minor-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      "M-d"         #'compilation-display-error
      "M-RET"       #'compile-goto-error
      ;;
      "M-TAB"       #'compilation-next-error
      "<M-backtab>" #'compilation-previous-error
      ;;
      "C-n"         #'next-error-no-select
      "M-n"         #'next-error-no-select
      "C-p"         #'previous-error-no-select
      "M-p"         #'previous-error-no-select
      ;;
      "C-}"         #'compilation-next-file
      "M-}"         #'compilation-next-file
      "C-{"         #'compilation-previous-file
      "M-{"         #'compilation-previous-file))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd compilation-shell-minor-mode-map
      :motion
      "}" #'compilation-next-file
      "{" #'compilation-previous-file)
    ;;
    (devil-repeat-motions-kbd
      :keymap compilation-shell-minor-mode-map
      "}" "}" "{"
      "{" "}" "{")))
;;
(init-handler compile button-map
  ;; TODO: Looks useful?
  (init-clear-keymap compilation-button-map))
;;
(init-handler compile mode-map
  (let ((keymap compilation-mode-map))
    (init-assert (equal (keymap-parent keymap) special-mode-map))
    (init-clear-keymap keymap)
    (set-keymap-parent keymap compilation-minor-mode-map)))
;;
(init-handler compile mode-tool-bar-map
  (init-clear-keymap compilation-mode-tool-bar-map))
;;
(init-package compile
  :built-in
  :diminish
  (compilation-minor-mode
   compilation-shell-minor-mode))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/emacs-mirror/emacs/blob/master/test/lisp/progmodes/compile-tests.el'
;;  ==========================================================================
;;  }}} References
