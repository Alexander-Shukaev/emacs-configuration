(eval-when-compile
  (require 'init-package))
;;
(init-handler auth-source prepare
  (init-load password-cache :no-error))
;;
(init-handler auth-source init
  (setq-default
    auth-source-do-cache       (bound-and-true-p password-cache)
    auth-source-cache-expiry   (bound-and-true-p password-cache-expiry)
    auth-source-debug          t
    auth-source-gpg-encrypt-to t
    auth-sources               `("~/.authinfo"
                                 "~/.authinfo.gpg"
                                 "~/.netrc"
                                 ;; ------------------------------------------
                                 ;; BUG:
                                 ;;
                                 ;; For example, `counsel-rg' and
                                 ;; `counsel-git-grep' (and most likely all
                                 ;; the others) stop working via TRAMP
                                 ;; (e.g. method 'sudo') when search for
                                 ;; connection credentials reaches either of
                                 ;; these:
                                 ;;
                                 ,@(init-ignore-progn
                                     (when (featurep 'dbusbind)
                                       '(;
                                         default
                                         "secrets:session"
                                         "secrets:Login")))
                                 ;; ------------------------------------------
                                 )))
;;
(init-handler auth-source config
  (after-load auth-source-pass
    (unless (memq 'password-store auth-sources)
      ;; TODO: `define-advice'
      (add-to-list 'auth-sources 'password-store :append)
      (auth-source-pass-enable)
      (init-negate (eq (car-safe auth-sources)        'password-store))
      (init-assert (eq (car-safe (last auth-sources)) 'password-store)))))
;;
(init-package auth-source
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(auth) Help for users'
;;  [2] Info node `(auth) Secret Service API'
;;  [3] Info node `(auth) The Unix password store'
;;  [4] Info node `(tramp) Password handling'
;;  [5] URL `http://emacshorrors.com/posts/unexpected-security-features.html'
;;  [6] URL `http://www.passwordstore.org'
;;  ==========================================================================
;;  }}} References
