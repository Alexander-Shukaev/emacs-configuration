(eval-when-compile
  (require 'init-package))
;;
(init-handler rmail init)
;;
(init-handler rmail config
  (after-load ffap
    (add-hook 'rmail-mode-hook #'ffap-ro-mode-hook)))
;;
(init-package rmail
  :built-in)
