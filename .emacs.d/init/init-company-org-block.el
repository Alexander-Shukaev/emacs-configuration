(eval-when-compile
  (require 'init-package))
;;
(init-handler company-org-block prepare
  (init-load company :no-error)
  (init-load org     :no-error))
;;
(init-handler company-org-block init
  (setq-default
    company-org-block-auto-indent            t
    company-org-block-complete-at-bol        t
    company-org-block-edit-style             'auto
    company-org-block-explicit-lang-defaults t))
;;
(init-handler company-org-block config)
;;
(init-package company-org-block
  :commands
  (company-org-block))
