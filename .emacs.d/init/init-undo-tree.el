(eval-when-compile
  (require 'init-package))
;;
(init-handler undo-tree init
  (setq-default
    undo-tree-auto-save-history       (not (version< emacs-version "24.3"))
    undo-tree-enable-undo-in-region   nil
    undo-tree-history-directory-alist `((".*"
                                         . ,(abbreviate-file-name
                                             (expand-directory
                                              "undo-tree/"
                                              user-data-directory)))))
  ;;
  (after-init (global-undo-tree-mode)))
;;
(init-handler undo-tree config
  (init-assert (fboundp 'undo-tree-overridden-undo-bindings-p))
  (init-defad undo-tree-overridden-undo-bindings-p
      (:override (&rest ...) init/:override)
    #$
    nil)
  ;;
  (init-assert (fboundp 'undo-tree-update-menu-bar))
  (init-defad undo-tree-update-menu-bar
      (:override (&rest ...) init/:override)
    #$)
  (remove-hook 'menu-bar-update-hook #'undo-tree-update-menu-bar)
  ;;
  (init-defun init-undo-tree-mode-setup ()
    #$
    (when (or (memq major-mode '(Info-mode
                                 comint-mode
                                 compilation-mode
                                 completion-list-mode
                                 debugger-mode
                                 dired-mode
                                 help-mode
                                 ivy-occur-mode
                                 nxml-mode
                                 recentf-dialog-mode
                                 shell-mode
                                 special-mode
                                 term-mode
                                 vterm-mode))
              (derived-mode-p 'magit-mode)
              (derived-mode-p 'special-mode))
      (setq-local undo-tree-auto-save-history nil)))
  (add-hook 'undo-tree-mode-hook #'init-undo-tree-mode-setup))
;;
(init-handler undo-tree map
  (init-clear-keymap undo-tree-map :recursively-p)
  (init-define-keys-kbd undo-tree-map
    [remap redo]      #'undo-tree-redo
    [remap undo]      #'undo-tree-undo
    [remap undo-only] #'undo-tree-undo))
;;
(init-package undo-tree
  :diminish
  (undo-tree-mode
   global-undo-tree-mode))
