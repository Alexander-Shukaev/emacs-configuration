(eval-when-compile
  (require 'init-package))
;;
(init-handler dracula-theme prepare
  (init-load theme))
;;
(init-handler dracula-theme init
  (load-theme 'dracula :no-confirm))
;;
(init-handler dracula-theme config)
;;
(init-package dracula-theme
  :disabled)
