(eval-when-compile
  (require 'init-package))
;;
(eval-and-compile
  (init-load devil-load-path))
;;
(init-handler devil prepare
  (init-load evil :no-error))
;;
(init-handler devil init
  (after-init (devil-repeat-motion-mode)))
;;
(init-handler devil config)
;;
(init-package devil
  :load-path init-devil-load-path
  :ensure avy
  :ensure evil
  :ensure evil-easymotion
  :ensure memoize)
