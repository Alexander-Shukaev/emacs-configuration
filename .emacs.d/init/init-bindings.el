(eval-when-compile
  (require 'init-package))
;;
(init-handler "bindings" init)
;;
(init-handler "bindings" config)
;;
(init-handler "bindings" global-map)
;;
(init-package "bindings"
  :built-in)
