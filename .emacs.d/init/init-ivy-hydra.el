(eval-when-compile
  (require 'init-package))
;;
(init-handler ivy-hydra init)
;;
(init-handler ivy-hydra config)
;;
(init-package ivy-hydra)
