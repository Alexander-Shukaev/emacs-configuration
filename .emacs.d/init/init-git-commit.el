(eval-when-compile
  (require 'init-package))
;;
(init-handler git-commit preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler git-commit init
  (after-init (require 'git-commit)))
;;
(init-handler git-commit config
  (after-load init-undo-tree
    (init-defun init-git-commit-mode-undo-tree-setup ()
      #$
      (setq-local undo-tree-auto-save-history nil))
    (add-hook 'git-commit-mode-hook #'init-git-commit-mode-undo-tree-setup
              :append))
  ;;
  (after-load (init-devil devil)
    (init-defun init-git-commit-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "signoff"   #'git-commit-signoff
        "ack"       #'git-commit-ack
        "test"      #'git-commit-test
        "review"    #'git-commit-review
        "cc"        #'git-commit-cc
        "reported"  #'git-commit-reported
        "suggested" #'git-commit-suggested
        "ms[ave]"   #'git-commit-save-message
        "mn[ext]"   #'git-commit-next-message
        "mp[rev]"   #'git-commit-prev-message
        ;;
        "c[ommit]"  #'with-editor-finish))
    (add-hook 'git-commit-setup-hook #'init-git-commit-devil-setup))
  ;;
  (after-load (init-evil evil)
    (add-hook 'git-commit-setup-hook #'evil-insert-state)))
;;
(init-handler git-commit mode-map
  (let ((keymap git-commit-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap keymap :recursively-p)))
;;
(init-package git-commit
  :ensure magit)
