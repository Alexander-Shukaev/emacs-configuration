(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-states init
  (setq-default
    evil-emacs-state-cursor    '(bar    "magenta")
    evil-insert-state-cursor   '(bar    "yellow")
    evil-motion-state-cursor   '(box    "blue")
    evil-normal-state-cursor   '(box    "green")
    evil-operator-state-cursor '(hollow "white")
    evil-replace-state-cursor  '(hbar   "red")
    evil-visual-state-cursor   '(box    "cyan")))
;;
(init-handler evil-states config
  (defface evil-emacs-state-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "white"
       :background "magenta"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Emacs state tag."
    :group 'evil)
  ;;
  (defface evil-insert-state-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "yellow"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Insert state tag."
    :group 'evil)
  ;;
  (defface evil-motion-state-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "white"
       :background "blue"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Motion state tag."
    :group 'evil)
  ;;
  (defface evil-normal-state-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "green"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Normal state tag."
    :group 'evil)
  ;;
  (defface evil-operator-state-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "white"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Operator state tag."
    :group 'evil)
  ;;
  (defface evil-replace-state-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "white"
       :background "red"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Replace state tag."
    :group 'evil)
  ;;
  (defface evil-visual-state-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "cyan"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Visual state tag."
    :group 'evil)
  ;;
  (defface evil-visual-char-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "cyan"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Visual state `char' tag."
    :group 'evil)
  ;;
  (defface evil-visual-line-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "cyan"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Visual state `line' tag."
    :group 'evil)
  ;;
  (defface evil-visual-screen-line-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "cyan"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Visual state `screen-line' tag."
    :group 'evil)
  ;;
  (defface evil-visual-block-tag
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "black"
       :background "cyan"
       :weight bold)
      (t
       :weight bold))
    "Evil face used to highlight Visual state `block' tag."
    :group 'evil)
  ;;
  (dolist (tag-symbol `(evil-emacs-state-tag
                        evil-insert-state-tag
                        evil-motion-state-tag
                        evil-normal-state-tag
                        evil-operator-state-tag
                        evil-replace-state-tag
                        ,@(if (stringp evil-visual-state-tag)
                              '(evil-visual-state-tag)
                            '(evil-visual-char-tag
                              evil-visual-line-tag
                              evil-visual-screen-line-tag
                              evil-visual-block-tag))))
    (set-default tag-symbol (propertize (symbol-value tag-symbol)
                                        'face
                                        tag-symbol)))
  ;;
  (setq-default
    evil-normal-state-modes (delete-dups (append evil-emacs-state-modes
                                                 evil-insert-state-modes
                                                 evil-normal-state-modes
                                                 evil-motion-state-modes))
    ;;
    evil-emacs-state-modes  nil
    evil-insert-state-modes nil
    evil-motion-state-modes nil)
  ;;
  (eval-when-compile
    (when (< emacs-major-version 26)
      (defsubst caddr (x)
        "Return the `car' of the `cdr' of the `cdr' of X."
        (declare (compiler-macro internal--compiler-macro-cXXr))
        (car (cdr (cdr x))))))
  ;;
  (init-defun init-evil-states--color-setup
      (alist)
    #$
    (let (symbol)
      (dolist (entry alist)
        (when (setq symbol (intern-soft (format "evil-%s-cursor"
                                                (car entry))))
          (let ((cursor (default-value symbol)))
            (if (listp cursor)
                (while (consp cursor)
                  (when (stringp (car cursor))
                    (setcar cursor (cadr entry)))
                  (setq cursor (cdr cursor)))
              (when (stringp cursor)
                (set-default symbol (cadr entry))))))
        (when (setq symbol (intern-soft (format "evil-%s-tag"
                                                (car entry))))
          (face-spec-set symbol `((((class color))
                                   :background ,(cadr  entry)
                                   :foreground ,(caddr entry))))))))
  ;;
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; Switching between `init-evil-states-solarized-setup' and
  ;; `init-evil-states-ansi-color-setup' does not update colors of `powerline'
  ;; separators:
  ;;
  (after-load init-ansi-color
    (init-defun init-evil-states-ansi-color-setup ()
      #$
      (interactive)
      (init-evil-states--color-setup
       `((emacs-state        . (,init-ansi-color-magenta "white"))
         (insert-state       . (,init-ansi-color-yellow  "black"))
         (motion-state       . (,init-ansi-color-blue    "white"))
         (normal-state       . (,init-ansi-color-green   "black"))
         (operator-state     . (,init-ansi-color-white   "black"))
         (replace-state      . (,init-ansi-color-red     "white"))
         (visual-state       . (,init-ansi-color-cyan    "black"))
         (visual-char        . (,init-ansi-color-cyan    "black"))
         (visual-line        . (,init-ansi-color-cyan    "black"))
         (visual-screen-line . (,init-ansi-color-cyan    "black"))
         (visual-block       . (,init-ansi-color-cyan    "black")))))
    (unless (featurep 'init-solarized-theme)
      (init-evil-states-ansi-color-setup)))
  ;;
  (after-load init-solarized-theme
    (init-defun init-evil-states-solarized-setup ()
      #$
      (interactive)
      (init-evil-states--color-setup
       `((emacs-state        . (,init-solarized-magenta "black"))
         (insert-state       . (,init-solarized-yellow  "black"))
         (motion-state       . (,init-solarized-blue    "black"))
         (normal-state       . (,init-solarized-green   "black"))
         (operator-state     . (,init-solarized-white   "black"))
         (replace-state      . (,init-solarized-red     "black"))
         (visual-state       . (,init-solarized-cyan    "black"))
         (visual-char        . (,init-solarized-cyan    "black"))
         (visual-line        . (,init-solarized-cyan    "black"))
         (visual-screen-line . (,init-solarized-cyan    "black"))
         (visual-block       . (,init-solarized-cyan    "black")))))
    (init-evil-states-solarized-setup))
  ;; -------------------------------------------------------------------------
  )
;;
(init-package evil-states
  :ensure evil)
