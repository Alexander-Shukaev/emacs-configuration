(eval-when-compile
  (require 'init-package))
;;
(init-handler url-methods init)
;;
(init-handler url-methods config
  (dolist (scheme '("ftp" "http" "https" "rsync"))
    (url-scheme-register-proxy scheme))
  ;;
  (when (zerop (length (getenv "MAVEN_OPTS")))
    (let (maven-opts)
      (dolist (scheme '("http" "https"))
        (let ((proxy  (cdr (assoc scheme url-proxy-services)))
              (urlobj nil))
          (unless (zerop (length proxy))
            (save-match-data
              (cond ((string-match "^\\([^:]+\\):\\([0-9]+\\)$" proxy)
                     (setq urlobj (url-generic-parse-url nil))
                     (setf (url-type urlobj) scheme)
                     (setf (url-host urlobj) (match-string 1 proxy))
                     (setf (url-port urlobj)
                           (string-to-number (match-string 2 proxy))))
                    ((string-match url-nonrelative-link proxy)
                     (setq urlobj (url-generic-parse-url proxy))
                     (setf (url-type   urlobj) scheme)
                     (setf (url-target urlobj) nil))
                    (t
                     (setq urlobj (url-generic-parse-url nil))
                     (setf (url-type urlobj) scheme)
                     (setf (url-host urlobj) proxy))))
            (setq maven-opts (nconc maven-opts
                                    `(,(format "-D%s.proxyHost=%s"
                                               (url-type urlobj)
                                               (url-host urlobj))
                                      ,(format "-D%s.proxyPort=%d"
                                               (url-type urlobj)
                                               (url-port urlobj))))))))
      (setenv "MAVEN_OPTS" (mapconcat #'identity maven-opts " ")))))
;;
(init-package url-methods
  :built-in)
