(eval-when-compile
  (require 'init-package))
;;
(init-handler special preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler special prepare
  (init-load simple))
;;
(init-handler special init
  (after-load simple
    (require 'special)))
;;
(init-handler special config
  (after-load evil
    (evil-set-initial-state 'special-mode 'motion)))
;;
(init-handler special mode-map
  (let ((keymap special-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (suppress-keymap      keymap)
    (init-define-keys-kbd keymap
      "<"   #'beginning-of-buffer
      ">"   #'end-of-buffer
      ;;
      "SPC" #'next-line
      "DEL" #'previous-line
      ;;
      "C-n" #'next-line
      "C-p" #'previous-line
      ;;
      "?"   #'describe-mode
      ;;
      "q"   #'quit-window
      "G"   #'revert-buffer))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd special-mode-map
      :motion
      [remap next-line    ] #'devil-line-forward
      [remap previous-line] #'devil-line-backward)))
;;
(init-package special
  :load-path "lisp/special")
