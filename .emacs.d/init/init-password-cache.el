(eval-when-compile
  (require 'init-package))
;;
(init-handler password-cache init
  (setq-default
    password-cache        t
    password-cache-expiry nil))
;;
(init-handler password-cache config)
;;
(init-package password-cache
  :built-in)
