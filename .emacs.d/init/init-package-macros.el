;; ---------------------------------------------------------------------------
;; BUG:
;;
;; Nested `eval-and-compile' forms cause the innermost body to be evaluated
;; twice at compile time.  I believe it is a bug of Emacs because
;; `eval-when-compile' nested into `eval-and-compile' does not exhibit this
;; issue.  Be careful when using `init-package', i.e. don't use
;; `eval-and-compile' in `preface' body.
;; ---------------------------------------------------------------------------
;;
(require 'init-package-variables)
(require 'init-package-functions)
;;
(require 'init-macros)
;;
(require 'use-package)
;;
(require 'macroexp)
;;
(defmacro init-package-declare-handler
    (name phase &rest body)
  (declare (debug  t)
           (indent defun))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (let ((handler (init-package-handler-symbol name phase)))
    (put handler 'init-package-handler-body body)
    (setq name  (init-package-symbol    name))
    (setq phase (init-package-as-symbol phase))
    (let ((phases (get name 'init-package-phases)))
      (add-to-list 'phases phase)
      (put name 'init-package-phases phases)))
  nil)
(defalias 'init-declare-handler #'init-package-declare-handler)
(defalias 'init-handler         #'init-package-declare-handler)
;;
(defmacro init-package-define-handler
    (name phase)
  (declare (debug t))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (init-handler-defun-form name phase))
(defalias 'init-define-handler #'init-package-define-handler)
;;
(defmacro init-package-inline-handler
    (name phase)
  (declare (debug t))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (macroexp-progn (init-handler-body name phase)))
(defalias 'init-inline-handler #'init-package-inline-handler)
;;
(defmacro init-package-call-handler
    (name &optional phase)
  (declare (debug t))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (if phase
      (init-handler-call-form name phase)
    `(funcall (init-package-handler-symbol ',name
                                           (get (init-current-function)
                                                'init-package-phase)))))
(defalias 'init-call-handler #'init-package-call-handler)
(defalias 'init-call         #'init-package-call-handler)
;;
(defmacro init-package-inhibit-handler
    (name &optional phase)
  (declare (debug t))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (if phase
      (init-handler-inhibit-form name phase)
    `(put (init-package-handler-symbol ',name
                                       (get (init-current-function)
                                            'init-package-phase))
          'init-package-inhibit-handler
          t)))
(defalias 'init-inhibit-handler #'init-package-inhibit-handler)
(defalias 'init-inhibit         #'init-package-inhibit-handler)
;;
(defmacro init-package
    (name &rest args)
  (declare (debug  t)
           (indent defun))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (condition-case-unless-debug e
      (let* ((symbol (init-package-symbol name))
             (form   (init-package-form   name args))
             ;; --------------------------------------------------------------
             ;; CAUTION:
             ;;
             ;; The following call to the `macroexpand' function is absolutely
             ;; necessary to ensure possible errors signaled from `form' being
             ;; caught by the surrounding `condition-case' form (at expansion
             ;; time of `init-package'):
             ;;
             (body   (macroexp-unprogn    (macroexpand form)))
             ;; --------------------------------------------------------------
             )
        `(progn
           (defconst ,symbol
             ,(not (memq :disabled args))
             ,(format "Whether `init-package' for `%s' is enabled." name))
           ;; ----------------------------------------------------------------
           ;; CAUTION:
           ;;
           ;; Ensure that non-built-in packages do not load before their
           ;; corresponding files with `init-' prefix are loaded because this
           ;; could lead to the issue of not initializing their custom
           ;; variables properly.  For example, `evil-surround' requires
           ;; `evil'.  That is if somehow `evil-surround-mode' is enabled
           ;; (and, as a result, `evil' loaded) before `init-evil' is loaded,
           ;; `evil' will be left incorrectly customized.  Of course, this
           ;; could be later on fixed by customizing it (yet) again with
           ;; `custom' routines but this approach looks even more convoluted,
           ;; would be a nightmare to maintain, and is
           ;; simply not worth it.
           ;;
           ,@(unless (memq :built-in args)
               (let ((string (init-package-as-string name))
                     (symbol (init-package-as-symbol name)))
                 `((condition-case-unless-debug e
                       (progn
                         (init-negate (init-load-history  ,string))
                         (init-negate (featurep          ',symbol)))
                     (error (lwarn 'init-package
                                   :error
                                   "Failed to verify package (%s): %s\n%s"
                                   ,string
                                   (error-message-string e)
                                   (init-backtrace-to-string))
                            nil)))))
           ;; TODO: `eval-after-load' for `:disabled' with `error'.
           ;; TODO: Maybe `:banned' which also implies `:disabled' for that.
           ;; TODO: Maybe `:loadup' which also implies `:built-in' for that.
           ;; ----------------------------------------------------------------
           ;; CAUTION:
           ;;
           ;; The following snippet:
           ;;
           ;;   (init-without-after-load-when-compile
           ;;     ,@body)
           ;;
           ;; speeds up byte compilation but makes no sense functionally as it
           ;; results in potentially broken post-load initialization (see
           ;; `eval-after-load') for some packages.  Not worth it.
           ;; ----------------------------------------------------------------
           ,@body
           ,symbol))
    (error (lwarn 'init-package
                  :error
                  "Failed to define package (%s): %s\n%s"
                  name
                  (error-message-string e)
                  (init-backtrace-to-string))
           nil)))
;;
(defmacro init-package-load
    (name &optional no-error)
  "\
Load initialization package NAME (with `init-' prefix)."
  (declare (debug t))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (cond ((stringp name)
         (setq name (init-package-file name))
         `(let* ((byte-compile-current-file)
                 (comp-native-compiling)
                 (name (and (load ,name ',no-error :no-message) ,name)))
            (prog1
                name
              (cond ((null name)
                     (when (and (not noninteractive) force-load-messages)
                       (message "Loading %s...not found"  ,name)))
                    (t
                     (unless (or noninteractive force-load-messages)
                       (message "Loading %s...done"       ,name)))))))
        ((symbolp name)
         (setq name (init-package-feature name))
         `(let* ((byte-compile-current-file)
                 (comp-native-compiling)
                 (featurep (featurep ',name))
                 (name (init-require ,name nil ',no-error)))
            (prog1
                name
              (cond ((eq name :disabled)
                     (init-negate featurep)
                     (when (and (not noninteractive) force-load-messages)
                       (message "Loading %s...disabled"  ',name)))
                    ((null name)
                     (init-negate featurep)
                     (when (and (not noninteractive) force-load-messages)
                       (message "Loading %s...not found" ',name)))
                    ((not featurep)
                     (unless (or noninteractive force-load-messages)
                       (message "Loading %s...done"      ',name)))))))
        (t
         (signal 'wrong-type-argument
                 (list #'or
                       (list #'stringp name)
                       (list #'symbolp name))))))
(defalias 'init-load #'init-package-load)
;;
(defmacro init-package-using
    (name &optional no-error)
  "\
Load package NAME at compile and run time upon compile time expansion."
  (declare (debug  t)
           (indent defun))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (init-using-form name no-error))
(defalias 'init-using #'init-package-using)
;;
(defmacro init-package-using-when-compile
    (name &optional no-error)
  "\
Load package NAME at compile time upon compile time expansion."
  (declare (debug  t)
           (indent defun))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  (init-using-when-compile-form name no-error))
(defalias 'init-using-when-compile #'init-package-using-when-compile)
;;
(defmacro init-package-with
    (name &rest body)
  "\
Execute BODY only when package NAME is loaded."
  (declare (debug  t)
           (indent defun))
  (when (and (listp name)
             (eq (car-safe name) 'quote))
    (setq name (cadr name)))
  `(when (let ((byte-compile-current-file)
               (comp-native-compiling))
           ,(init-package-load-form name :no-error))
     ,@body))
(defalias 'init-with #'init-package-with)
;;
(provide 'init-package-macros)
