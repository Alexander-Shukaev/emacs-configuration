(eval-when-compile
  (require 'init-package))
;;
(init-handler bat-mode init)
;;
(init-handler bat-mode config
  (init-defun init-bat-mode-setup ()
    #$
    (setq-local comment-start ":: "))
  (add-hook 'bat-mode-hook #'init-bat-mode-setup))
;;
(init-package bat-mode
  :built-in)
