(eval-when-compile
  (require 'init-package))
;;
(init-handler php-extras init)
;;
(init-handler php-extras config)
;;
(init-package php-extras
  :unless
  (bound-and-true-p init-bb))
