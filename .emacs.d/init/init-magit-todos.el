(eval-when-compile
  (require 'init-package))
;;
(init-handler magit-todos init)
;;
(init-handler magit-todos config)
;;
(init-package magit-todos
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Slow and sluggish.
  ;; -------------------------------------------------------------------------
  :unless
  (version< emacs-version "25.2")
  :diminish
  (magit-todos-mode))
