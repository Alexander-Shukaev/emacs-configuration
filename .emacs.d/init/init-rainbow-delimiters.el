(eval-when-compile
  (require 'init-package))

(init-handler rainbow-delimiters init)

(init-handler rainbow-delimiters config)

(init-package rainbow-delimiters)
