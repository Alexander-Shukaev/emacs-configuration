(eval-when-compile
  (require 'init-package))
;;
(init-handler "isearch" preface
  (eval-when-compile
    (require 'cl-lib)))
;;
(init-handler "isearch" init
  (setq-default
    isearch-hide-immediately      t
    isearch-lazy-highlight        'all-windows
    regexp-search-ring-max        1024
    search-invisible              t
    search-nonincremental-instead t
    search-ring-max               1024
    search-ring-update            t
    search-whitespace-regexp      ".*"))
;;
(init-handler "isearch" config)
;;
(init-handler "isearch" mode-map
  (let ((keymap isearch-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-make-keymap keymap :recursively-p)
    (let ((i 0))
      (if (fboundp 'max-char)
          (set-char-table-range (nth 1 keymap)
                                (cons #x100 (max-char))
                                #'isearch-printing-char)
        (with-no-warnings
          ;; Obsolete in Emacs 23:
          (let ((list  (generic-character-list))
                (table (nth 1 keymap)))
            (while list
              (set-char-table-default table
                                      (car list)
                                      #'isearch-printing-char)
              (setq list (cdr list))))))
      (setq i ?\s)
      (while (< i 256)
        (define-key keymap (vector i) #'isearch-printing-char)
        (cl-incf i)))
    (init-assert (= help-char        ?\b))
    (init-assert (= meta-prefix-char ?\e))
    (init-define-keys-kbd keymap
      (char-to-string meta-prefix-char) (make-sparse-keymap)
      ;;
      ;; ---------------------------------------------------------------------
      ;; [RESOLVED] bug#20466:
      ;;
      "<backspace>" #'undefined
      ;; ---------------------------------------------------------------------
      ;;
      "   <delete>" #'isearch-delete-char
      "<kp-delete>" #'isearch-delete-char
      "        DEL" nil
      "        DEL" #'isearch-delete-char
      ;;
      "C-g" #'isearch-abort
      "C-s" #'isearch-repeat-forward
      "C-r" #'isearch-repeat-backward
      ;;
      "TAB"       #'isearch-repeat-forward
      "<backtab>" #'isearch-repeat-backward
      ;;
      ;; ---------------------------------------------------------------------
      ;; TODO: `isearch-cancel'
      ;; ---------------------------------------------------------------------
      ;;
      ;; ---------------------------------------------------------------------
      ;; TODO: `search-nonincremental-instead' has no effect?
      ;;
      "RET" #'isearch-exit
      ;; ---------------------------------------------------------------------
      ;;
      "C-q" #'isearch-quote-char
      ;;
      "LFD"   #'isearch-printing-char
      "S-SPC" #'isearch-printing-char
      ;;
      ;; ---------------------------------------------------------------------
      ;; TODO: `isearch-cancel', `isearch-del-char', `isearch-yank-char',
      ;; `isearch-yank-kill', `isearch-yank-line', `isearch-yank-pop',
      ;; `isearch-highlight-regexp', `isearch-complete'
      ;; ---------------------------------------------------------------------
      "C-y" #'isearch-yank-word-or-char
      ;;
      (char-to-string help-char) isearch-help-map
      "<help>"                   isearch-help-map
      "<f1>"                     isearch-help-map
      ;;
      "<down>" #'isearch-ring-advance
      "<up>  " #'isearch-ring-retreat
      ;;
      "<switch-frame>"       nil
      "<delete-frame>"       nil
      "<iconify-frame>"      nil
      "<make-frame-visible>" nil
      "<mouse-movement>"     nil
      "<language-change>"    nil
      ;;
      "C-\\" #'isearch-toggle-input-method
      "C-^"  #'isearch-toggle-specified-input-method
      ;;
      "<down-mouse-2>" nil
      "<mouse-2>"      #'isearch-mouse-2
      "<xterm-paste>"  #'isearch-xterm-paste
      ;;
      "C-e" #'isearch-edit-string
      ;;
      "C-o" #'isearch-occur)
    ;;
    (init-define-keys-with-prefix-kbd keymap "M-r"
      "R" #'isearch-query-replace-regexp
      "S" #'isearch-query-replace
      ;;
      "r" #'isearch-query-replace-regexp
      "s" #'isearch-query-replace)
    ;;
    (init-define-keys-with-prefix-kbd keymap "C-t"
      "c"   #'isearch-toggle-case-fold
      "i"   #'isearch-toggle-invisible
      "r"   #'isearch-toggle-regexp
      "w"   #'isearch-toggle-word
      "s"   #'isearch-toggle-symbol
      "SPC" #'isearch-toggle-lax-whitespace)))
;;
(init-handler "isearch" minibuffer-local-isearch-map
  (let ((keymap minibuffer-local-isearch-map))
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map))
    (init-clear-keymap    keymap)
    (init-assert (eq (keymap-parent keymap) minibuffer-local-map))
    (init-define-keys-kbd keymap
      "C-s" #'isearch-forward-exit-minibuffer
      "C-r" #'isearch-reverse-exit-minibuffer
      ;;
      "TAB" #'isearch-complete-edit)))
;;
(init-package "isearch"
  :built-in)
