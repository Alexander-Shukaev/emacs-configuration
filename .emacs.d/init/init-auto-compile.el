(eval-when-compile
  (require 'init-package))
;;
(init-handler auto-compile init
  (setq-default
    auto-compile-display-buffer    nil
    auto-compile-mode-line-counter t
    auto-compile-native-compile    nil)
  ;;
  (add-hook 'emacs-startup-hook #'auto-compile-on-load-mode :append)
  (add-hook 'emacs-startup-hook #'auto-compile-on-save-mode :append))
;;
(init-handler auto-compile config)
;;
(init-package auto-compile)
