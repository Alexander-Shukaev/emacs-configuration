(eval-when-compile
  (require 'init-package))
;;
(init-handler recentf preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler recentf init
  (setq-default
    recentf-auto-cleanup             300; seconds
    recentf-exclude                  '("\\`/sudo:.+@.+:/"
                                       ;;
                                       "\\`/dev/"
                                       "\\`/proc/"
                                       "\\`/run/"
                                       "\\`/sys/"
                                       "\\`/tmp/"
                                       "\\`/var/lib/docker"
                                       "\\`/var/run/"
                                       ;;
                                       "/\\.\\(authinfo\\|netrc\\)\\(\\.gpg\\|\\.asc\\)?\\'"
                                       "/\\.gitmodules\\.d/"
                                       "/\\.gnupg/"
                                       "/\\.luks/"
                                       "/\\.otp\\(/\\|\\..*\\'\\)"
                                       "/\\.ssh/"
                                       "/borg/keys/"
                                       "/borg/security/"
                                       "/keyrings/"
                                       "/keystore/"
                                       "/share/cmake/Helpers/"
                                       "/share/git/hooks/"
                                       "/share/make/scripts/"
                                       ;;
                                       "/COMMIT_EDITMSG\\'"
                                       "/COMMIT_MSG\\'")
    recentf-max-menu-items           10
    recentf-max-saved-items          nil
    recentf-menu-title               "Recent-Files-Menu"
    ;; TODO: Rename back to default, but make sure it has `emacs-lisp-mode' on
    ;; top:
    recentf-save-file                (locate-user-emacs-file ".recentf.el")
    recentf-show-file-shortcuts-flag t)
  ;;
  (after-init (recentf-mode))
  (after-init (after-load dired
                ;; TODO: It's a bug in `recentf-ext' that I have to defer it
                ;; manually until `dired' is loaded:
                (when (bound-and-true-p recentf-mode)
                  (require 'recentf-ext nil :no-error)))))
;;
(init-handler recentf config
  (add-to-list 'recentf-filename-handlers #'abbreviate-file-name)
  ;;
  (init-negate (featurep 'recentf-ext))
  (init-negate (featurep 'sync-recentf))
  ;;
  (init-defun init-recentf-ext-setup ()
    #$
    (when (and (bound-and-true-p recentf-mode)
               (boundp 'dired-mode-hook)
               (let ((name (and load-in-progress load-file-name)))
                 (not (and name (equal (file-name-sans-extension
                                        (file-name-nondirectory name))
                                       "recentf-ext")))))
      ;; TODO: It's a bug in `recentf-ext' that I have to defer it manually
      ;; until `dired' is loaded:
      (let ((recentf-mode-hook (remq #'init-recentf-ext-setup
                                     recentf-mode-hook)))
        (require 'recentf-ext nil :no-error))))
  (add-hook 'recentf-mode-hook #'init-recentf-ext-setup)
  ;;
  (init-defad recentf-cleanup
      (:around (function &rest ...) init/:around)
    #$
    (let ((non-essential t))
      (apply function ...)))
  ;;
  (when (or (require 'mneme        nil :no-error)
            (require 'sync-recentf nil :no-error))
    (init-reject (and (featurep 'mneme)
                      (featurep 'sync-recentf)))
    ;; TODO: On the start of Emacs after recompilation the history is empty
    ;; but on the subsequent restart it's back again.  Is this module not
    ;; loaded during compilation?
    (unless (numberp recentf-auto-cleanup)
      (setq-default recentf-auto-cleanup 300; seconds
        ))
    (when (bound-and-true-p recentf-mode)
      (recentf-auto-cleanup)
      (init-assert (timerp recentf-auto-cleanup-timer))))
  ;;
  (after-load evil
    (evil-set-initial-state 'recentf-dialog-mode 'motion)))
;;
(init-handler recentf dialog-mode-map
  (let ((keymap recentf-dialog-mode-map))
    (init-assert
      (if (< emacs-major-version 29)
          (eq (keymap-parent keymap) widget-keymap)
        (equal (keymap-parent keymap) (make-composed-keymap
                                       recentf--shortcuts-keymap
                                       widget-keymap))))
    (init-clear-keymap    keymap :recursively-p)
    (setcdr keymap (cdr (copy-keymap recentf--shortcuts-keymap)))
    (set-keymap-parent    keymap (make-composed-keymap (list special-mode-map
                                                             widget-keymap)))
    (init-define-keys-kbd keymap
      [remap quit-window] #'recentf-cancel-dialog
      ;;
      "<follow-link>" "RET"))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd recentf-dialog-mode-map
      :motion
      [remap next-line    ] #'devil-line-forward
      [remap previous-line] #'devil-line-backward))
  ;;
  ;; -------------------------------------------------------------------------
  (after-load evil
    (evil-make-overriding-map recentf-dialog-mode-map 'motion))
  ;;
  ;; NOTE:
  ;;
  ;; So that in Motion state, for example, \\[recentf-open-most-recent-file-0]
  ;; would stay properly key-mapped.
  ;; -------------------------------------------------------------------------
  )
;;
(init-package recentf
  :built-in
  :commands
  (recentf-open-files))
