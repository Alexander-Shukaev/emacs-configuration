(eval-when-compile
  (require 'init-package))
;;
(init-handler smerge-mode preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler smerge-mode prepare
  (init-load diff-mode)
  ;;
  (defcustom init-smerge-mode-face-spec-alist
    nil
    "Association list of face specifications in Smerge mode."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type list))
  ;;
  (defcustom init-smerge-mode-face-alias-alist
    '((smerge-base            . magit-diff-base)
      (smerge-lower           . magit-diff-added)
      (smerge-markers         . magit-diff-conflict-heading)
      (smerge-refined-added   . magit-diff-added-highlight)
      (smerge-refined-removed . magit-diff-removed-highlight)
      (smerge-upper           . magit-diff-removed))
    "Association list of face aliases in Smerge mode."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type symbol)))
;;
(init-handler smerge-mode init)
;;
(init-handler smerge-mode config
  (after-load (init-solarized-theme solarized-theme magit-diff)
    (init-defun init-smerge-mode-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (dolist (entry init-smerge-mode-face-spec-alist)
          (let ((face (car entry))
                (spec (cdr entry)))
            (put face 'theme-face nil)
            (face-spec-set face spec)))
        (dolist (entry init-smerge-mode-face-alias-alist)
          (let ((face  (car entry))
                (alias (cdr entry)))
            (put face 'theme-face nil)
            (put face 'face-alias alias)))))
    (init-smerge-mode-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...)
                init-smerge-mode-solarized-theme-setup/:after)
      #$
      (init-smerge-mode-solarized-theme-setup theme))))
;;
(init-package smerge-mode
  :built-in)
