(eval-when-compile
  (require 'init-package))
;;
(init-handler counsel-filter prepare
  (init-load counsel :no-error))
;;
(init-handler counsel-filter init)
;;
(init-handler counsel-filter config)
;;
(init-package counsel-filter
  :load-path "lisp/counsel-filter"
  :ensure counsel)
