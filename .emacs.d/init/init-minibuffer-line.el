(eval-when-compile
  (require 'init-package))
;;
(init-handler minibuffer-line preface
  (eval-when-compile
    (init-load macros)))
;;
(init-handler minibuffer-line prepare
  (init-load battery :no-error))
;;
(init-handler minibuffer-line init
  (setq-default
    minibuffer-line-format
    '((:eval
       (let* ((minibuffer-line-format)
              ;; -------------------------------------------------------------
              ;; CAUTION:
              ;;
              ;; If the variable `buffer-list-update-hook' contains the
              ;; function `minibuffer-line--update' and if the function
              ;; `format-time-string' is used for constructing the value of
              ;; `minibuffer-line-format' (like it is here), then the infinite
              ;; recursion may happen, for instance, after `gnus' invocation.
              ;; Avoid this issue by locally let-binding the variable
              ;; `buffer-list-update-hook' to nil.
              ;;
              (buffer-list-update-hook)
              ;; -------------------------------------------------------------
              (path (or (buffer-file-name)
                        (file-name-as-directory default-directory)))
              ;; -------------------------------------------------------------
              (method-string
               (propertize (or (file-remote-p path 'method)
                               "")
                           'face
                           'init-minibuffer-line-method))
              (user-string
               (let ((user (or (file-remote-p path 'user)
                               (user-login-name))))
                 (propertize user
                             'face
                             (if (equal "root" user)
                                 'init-minibuffer-line-root
                               'init-minibuffer-line-user))))
              (host-string
               (propertize (or (file-remote-p path 'host)
                               (system-name))
                           'face
                           'init-minibuffer-line-host))
              (path-string-list
               (let* ((path      (or (file-remote-p path 'localname)
                                     (abbreviate-file-name path)))
                      (home-dir  (when (string-prefix-p "~/" path)
                                   (progn
                                     (setq path (substring path 2))
                                     "~/")))
                      (file-name (file-name-nondirectory path))
                      (directory (if (equal "/" path)
                                     (progn
                                       (setq path nil)
                                       "/")
                                   (unless (zerop (length path))
                                     (prog1
                                         (when (zerop (length file-name))
                                           (file-name-as-directory
                                            (file-name-nondirectory
                                             (setq path
                                               (directory-file-name path)))))
                                       (setq path
                                         (file-name-directory path)))))))
                 `(,@(unless (zerop (length home-dir))
                       `(,(propertize home-dir
                                      'face
                                      'init-minibuffer-line-home-dir)))
                   ,@(unless (zerop (length path))
                       `(,(propertize path
                                      'face
                                      'init-minibuffer-line-path)))
                   ,@(unless (zerop (length directory))
                       `(,(propertize directory
                                      'face
                                      'init-minibuffer-line-directory)))
                   ,@(unless (zerop (length file-name))
                       `(,(propertize file-name
                                      'face
                                      'init-minibuffer-line-file-name))))))
              ;; -------------------------------------------------------------
              (battery-string
               (propertize (or
                            ;; -----------------------------------------------
                            (init-ignore-progn
                              (when (and (bound-and-true-p battery-mode-line-format)
                                         (bound-and-true-p battery-status-function))
                                (or (when (and (fboundp 'battery-update)
                                               (boundp  'battery-mode-line-string))
                                      (init-without-force-mode-line-update
                                        (init-without-timers
                                          (battery-update)))
                                      (init-format-quote battery-mode-line-string))
                                    (when (fboundp 'battery-format)
                                      (init-format-quote
                                       (battery-format battery-mode-line-format
                                                       (funcall
                                                        battery-status-function)))))))
                            ;;
                            ;; BUG:
                            ;;
                            ;; Slow and causes issues with `comint' password
                            ;; prompts as per example debugger backtrace:
                            ;;
                            ;;   ...
                            ;;   timer-event-handler([t 25846 10023 838403 nil #f(compiled-function (current-buf) #<bytecode ...>) (#<buffer *Async Shell Command*>) nil 727000 nil])
                            ;;   read-event(nil nil 0.001)
                            ;;   dbus-call-method(:system "org.freedesktop.UPower" "/org/freedesktop/UPower" "org.freedesktop.UPower" "EnumerateDevices" :timeout 1000)
                            ;;   battery--upower-devices()
                            ;;   battery-upower()
                            ;;   battery-update()
                            ;;   ...
                            ;;   minibuffer-line--update-maybe()
                            ;;   ...
                            ;;   select-window(#<window ...>)
                            ;;   ...
                            ;; -----------------------------------------------
                            "")
                           'face
                           'init-minibuffer-line-battery))
              ;; -------------------------------------------------------------
              (year-string
               (propertize (format-time-string "%Y")
                           'face
                           'init-minibuffer-line-year))
              (month-string
               (propertize (format-time-string "%m")
                           'face
                           'init-minibuffer-line-month))
              (day-string
               (propertize (format-time-string "%d")
                           'face
                           'init-minibuffer-line-day))
              (weekday-string
               (propertize (format-time-string "%A")
                           'face
                           'init-minibuffer-line-weekday))
              (hour-string
               (propertize (format-time-string "%H")
                           'face
                           'init-minibuffer-line-hour))
              (minute-string
               (propertize (format-time-string "%M")
                           'face
                           'init-minibuffer-line-minute))
              (second-string
               (propertize (format-time-string "%S")
                           'face
                           'init-minibuffer-line-second))
              ;; -------------------------------------------------------------
              (at-string
               (propertize "@"
                           'face
                           'init-minibuffer-line-at))
              (colon-string
               (propertize ":"
                           'face
                           'init-minibuffer-line-colon))
              (period-string
               (propertize "."
                           'face
                           'init-minibuffer-line-period))
              (space-string " ")
              ;; -------------------------------------------------------------
              (left-string-list
               `(,@(unless (zerop (length method-string))
                     `(,method-string
                       ,colon-string))
                 ,user-string
                 ,at-string
                 ,host-string
                 ,colon-string
                 ,@path-string-list))
              (right-string-list
               `(,@(unless (zerop (length battery-string))
                     `(,battery-string
                       ,space-string))
                 ,hour-string
                 ,colon-string
                 ,minute-string
                 ,space-string
                 ,weekday-string
                 ,space-string
                 ,day-string
                 ,period-string
                 ,month-string
                 ,period-string
                 ,year-string))
              (right-string
               (apply #'concat right-string-list))
              (pad-string
               (propertize " "
                           'display
                           `((space :align-to
                                    (- right
                                       right-fringe
                                       right-margin
                                       ,(if (init-display-graphic-p) 0 1)
                                       ,(string-width right-string)))))))
         `(,@left-string-list
           ,pad-string
           ,@right-string-list))))
    minibuffer-line-refresh-interval
    60; seconds
    )
  ;;
  (when (daemonp)
    (init-negate (init-display-graphic-p))
    (init-negate (tty-type)))
  (unless (or noninteractive (daemonp))
    (after-init (minibuffer-line-mode)))
  ;;
  (after-load frame
    (dolist (hook '(after-make-frame-functions))
      (add-hook hook #'minibuffer-line-mode))))
;;
(init-handler minibuffer-line config
  (after-load init-battery
    (require 'battery))
  ;;
  (defface init-minibuffer-line-method
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "green"
       :weight bold)
      (t
       :weight bold))
    "Minibuffer-line face used to highlight method."
    :group 'init)
  ;;
  (defface init-minibuffer-line-root
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "red")
      (t))
    "Minibuffer-line face used to highlight root."
    :group 'init)
  ;;
  (defface init-minibuffer-line-user
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "blue")
      (t))
    "Minibuffer-line face used to highlight user."
    :group 'init)
  ;;
  (defface init-minibuffer-line-host
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "blue")
      (t))
    "Minibuffer-line face used to highlight host."
    :group 'init)
  ;;
  (defface init-minibuffer-line-home-dir
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "yellow"
       :weight bold)
      (t
       :weight bold))
    "Minibuffer-line face used to highlight home directory."
    :group 'init)
  ;;
  (defface init-minibuffer-line-path
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "cyan")
      (t))
    "Minibuffer-line face used to highlight path."
    :group 'init)
  ;;
  (defface init-minibuffer-line-directory
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "blue"
       :weight bold)
      (t
       :weight bold))
    "Minibuffer-line face used to highlight directory."
    :group 'init)
  ;;
  (defface init-minibuffer-line-file-name
    '((((class grayscale)
        (background light))
       :foreground "DimGray"
       :weight bold)
      (((class grayscale)
        (background dark))
       :foreground "LightGray"
       :weight bold)
      (((class color))
       :foreground "white"
       :weight bold)
      (t
       :weight bold))
    "Minibuffer-line face used to highlight file name."
    :group 'init)
  ;;
  (defface init-minibuffer-line-battery
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "white")
      (t))
    "Minibuffer-line face used to highlight battery."
    :group 'init)
  ;;
  (defface init-minibuffer-line-date
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "green")
      (t))
    "Minibuffer-line face used to highlight date."
    :group 'init)
  ;;
  (defface init-minibuffer-line-year
    '((t
       (:inherit init-minibuffer-line-date)))
    "Minibuffer-line face used to highlight year."
    :group 'init)
  ;;
  (defface init-minibuffer-line-month
    '((t
       (:inherit init-minibuffer-line-date)))
    "Minibuffer-line face used to highlight month."
    :group 'init)
  ;;
  (defface init-minibuffer-line-day
    '((t
       (:inherit init-minibuffer-line-date)))
    "Minibuffer-line face used to highlight day."
    :group 'init)
  ;;
  (defface init-minibuffer-line-weekday
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "yellow")
      (t))
    "Minibuffer-line face used to highlight weekday."
    :group 'init)
  ;;
  (defface init-minibuffer-line-time
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "green")
      (t))
    "Minibuffer-line face used to highlight time."
    :group 'init)
  ;;
  (defface init-minibuffer-line-hour
    '((t
       (:inherit init-minibuffer-line-time)))
    "Minibuffer-line face used to highlight hour."
    :group 'init)
  ;;
  (defface init-minibuffer-line-minute
    '((t
       (:inherit init-minibuffer-line-time)))
    "Minibuffer-line face used to highlight minute."
    :group 'init)
  ;;
  (defface init-minibuffer-line-second
    '((t
       (:inherit init-minibuffer-line-time)))
    "Minibuffer-line face used to highlight second."
    :group 'init)
  ;;
  (defface init-minibuffer-line-delimiter
    '((((class grayscale)
        (background light))
       :foreground "DimGray")
      (((class grayscale)
        (background dark))
       :foreground "LightGray")
      (((class color))
       :foreground "magenta")
      (t))
    "Minibuffer-line face used to highlight delimiter."
    :group 'init)
  ;;
  (defface init-minibuffer-line-at
    '((t
       (:inherit init-minibuffer-line-delimiter)))
    "Minibuffer-line face used to highlight '@'."
    :group 'init)
  ;;
  (defface init-minibuffer-line-colon
    '((t
       (:inherit init-minibuffer-line-delimiter)))
    "Minibuffer-line face used to highlight ':'."
    :group 'init)
  ;;
  (defface init-minibuffer-line-period
    '((t
       (:inherit init-minibuffer-line-delimiter)))
    "Minibuffer-line face used to highlight '.'."
    :group 'init)
  ;;
  (defconst init-minibuffer-line-at
    (propertize "@"
                'face
                'init-minibuffer-line-at)
    "Minibuffer-line substring used to render '@'.")
  ;;
  (defconst init-minibuffer-line-colon
    (propertize ":"
                'face
                'init-minibuffer-line-colon)
    "Minibuffer-line substring used to render ':'.")
  ;;
  (defconst init-minibuffer-line-period
    (propertize "."
                'face
                'init-minibuffer-line-period)
    "Minibuffer-line substring used to render ':'.")
  ;;
  (defconst init-minibuffer-line-space
    " "
    "Minibuffer-line substring used to render ' '.")
  ;;
  (defsubst init-minibuffer-line-method
      (path)
    (propertize (or (file-remote-p path 'method)
                    "")
                'face
                'init-minibuffer-line-method))
  ;;
  (defsubst init-minibuffer-line-user
      (path)
    (let ((user (or (file-remote-p path 'user)
                    (user-login-name)
                    "")))
      (propertize user
                  'face
                  (if (equal "root" user)
                      'init-minibuffer-line-root
                    'init-minibuffer-line-user))))
  ;;
  (defsubst init-minibuffer-line-host
      (path)
    (propertize (or (file-remote-p path 'host)
                    (system-name)
                    "")
                'face
                'init-minibuffer-line-host))
  ;;
  (defsubst init-minibuffer-line-path
      (path)
    (let* ((path      (or (file-remote-p path 'localname)
                          (abbreviate-file-name path)))
           (home-dir  (when (string-prefix-p "~/" path)
                        (progn
                          (setq path (substring path 2))
                          "~/")))
           (file-name (file-name-nondirectory path))
           (directory (if (equal "/" path)
                          (progn
                            (setq path nil)
                            "/")
                        (unless (zerop (length path))
                          (prog1
                              (when (zerop (length file-name))
                                (file-name-as-directory
                                 (file-name-nondirectory
                                  (setq path (directory-file-name path)))))
                            (setq path (file-name-directory path)))))))
      (concat (unless (zerop (length home-dir))
                (propertize home-dir
                            'face
                            'init-minibuffer-line-home-dir))
              (unless (zerop (length path))
                (propertize path
                            'face
                            'init-minibuffer-line-path))
              (unless (zerop (length directory))
                (propertize directory
                            'face
                            'init-minibuffer-line-directory))
              (unless (zerop (length file-name))
                (propertize file-name
                            'face
                            'init-minibuffer-line-file-name)))))
  ;;
  (defsubst init-minibuffer-line-time
      (&optional format-string)
    (let (buffer-list-update-hook)
      ;; ---------------------------------------------------------------------
      ;; CAUTION:
      ;;
      ;; If the variable `buffer-list-update-hook' contains the function
      ;; `minibuffer-line--update' and if the function `format-time-string' is
      ;; used for constructing the value of `minibuffer-line-format' (like it
      ;; is here), then the infinite recursion may happen, for instance, after
      ;; `gnus' invocation.  Avoid this issue by locally let-binding the
      ;; variable `buffer-list-update-hook' to nil.
      ;; ---------------------------------------------------------------------
      (if format-string
          (propertize (format-time-string format-string)
                      'face
                      'init-minibuffer-line-time)
        (concat (propertize (format-time-string "%H")
                            'face
                            'init-minibuffer-line-hour)
                init-minibuffer-line-colon
                (propertize (format-time-string "%M")
                            'face
                            'init-minibuffer-line-minute)
                init-minibuffer-line-space
                (propertize (format-time-string "%A")
                            'face
                            'init-minibuffer-line-weekday)
                init-minibuffer-line-space
                (propertize (format-time-string "%d")
                            'face
                            'init-minibuffer-line-day)
                init-minibuffer-line-period
                (propertize (format-time-string "%m")
                            'face
                            'init-minibuffer-line-month)
                init-minibuffer-line-period
                (propertize (format-time-string "%Y")
                            'face
                            'init-minibuffer-line-year)))))
  ;;
  (defsubst init-minibuffer-line-battery
      (&optional default)
    (let (battery)
      (when (and (bound-and-true-p battery-mode-line-format)
                 (bound-and-true-p battery-status-function))
        (when (and (fboundp 'battery-update)
                   (boundp  'battery-mode-line-string))
          (init-without-force-mode-line-update
            (init-without-timers
              (battery-update)))
          (setq battery battery-mode-line-string)
          (when (and (zerop (length battery))
                     (fboundp 'battery-format))
            (setq battery (battery-format battery-mode-line-format
                                          (funcall battery-status-function)))
            (when (and (zerop (length battery))
                       (featurep 'battery))
              (setq battery default)))))
      (unless (zerop (length battery))
        (propertize battery
                    'face
                    'init-minibuffer-line-battery))))
  ;;
  ;; `mode-line-format'
  ;; `battery-mode-line-format'
  ;; `battery-mode-line-string'
  ;; `minibuffer-line-format'
  ;; `format-mode-line'
  ;; `battery-format'
  ;; `format-time-string'
  ;; %e, battery, and time all should be added to some list to be displayed
  ;;
  ;;
  ;;
  ;;
  ;;
  ;;
  (init-defun init-minibuffer-line-theme-l
      (&optional buffer)
    #$
    (with-current-buffer (or buffer (current-buffer))
      (let* ((path   (or (buffer-file-name)
                         (file-name-as-directory default-directory)))
             (method (init-minibuffer-line-method path))
             (user   (init-minibuffer-line-user   path))
             (host   (init-minibuffer-line-host   path)))
        (setq path (init-minibuffer-line-path path))
        `(,@(unless (zerop (length method))
              `(,method
                ,init-minibuffer-line-colon))
          ,@(unless (zerop (length user))
              `(,user
                ,@(when (zerop (length host))
                    `(,init-minibuffer-line-colon))))
          ,@(unless (zerop (length host))
              `(,init-minibuffer-line-at
                ,host
                ,init-minibuffer-line-colon))
          ,path))))
  ;;
  (init-defun init-minibuffer-line-theme
      (&optional time-format-string buffer)
    #$
    (let* ((theme-l (init-minibuffer-line-theme-l buffer))
           (theme-r (init-minibuffer-line-theme-r time-format-string)))
      (setq theme-l (apply #'concat theme-l))
      (setq theme-r (apply #'concat theme-r))
      (setq-local init-minibuffer-line-theme-string
        (concat theme-l
                (unless (zerop (length theme-r))
                  (let ((width (string-width theme-r)))
                    (unless (init-display-graphic-p)
                      (1+ width))
                    (format-mode-line (propertize " "
                                                  'display
                                                  `((space :align-to
                                                           (- right
                                                              right-fringe
                                                              right-margin
                                                              ,width)))))))
                theme-r)))
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; Take advantage of the implementation detail in `format-mode-line' that
    ;; is described in `mode-line-format':
    ;;
    ;; For any symbol other than t or nil, the symbol's value is processed as
    ;;  a mode line construct.  As a special exception, if that value is a
    ;;  string, the string is processed verbatim, without handling any
    ;;  %-constructs (see below).  Also, unless the symbol has a non-nil
    ;;  `risky-local-variable' property, all properties in any strings, as
    ;;  well as all `:eval' and `:propertize' forms in the value, are ignored.
    ;;
    ;; Therefore, in order to avoid the second reinterpretation of the string
    ;; value, instead of returning the string value itself, return the symbol
    ;; `init-minibuffer-line-theme-string' having that final resulting string
    ;; value:
    ;;
    'init-minibuffer-line-theme-string
    ;; -----------------------------------------------------------------------
    )
  ;;
  (init-ignore-progn
    ;; TODO: No effect?
    (init-defun init-minibuffer-line-quit-setup ()
      #$
      (if (bound-and-true-p minibuffer-line-mode)
          (progn
            (setq-default init-minibuffer-line-quit-error-message
              (get 'quit 'error-message))
            (put 'quit 'error-message ""))
        (put 'quit 'error-message init-minibuffer-line-quit-error-message)))
    (add-hook 'minibuffer-line-mode-hook #'init-minibuffer-line-quit-setup))
  ;;
  (init-defun minibuffer-line--update-maybe ()
    #$
    "Run `minibuffer-line--update' if `selected-window' returns appropriate."
    (interactive)
    (unless (window-minibuffer-p)
      (when (eq (selected-window) (frame-selected-window))
        (minibuffer-line--update))))
  ;;
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; Looks like `buffer-list-update-hook' does not run
  ;; `minibuffer-line--update-maybe' when windows of `sunrise-commander' are
  ;; involved:
  ;;
  (init-ignore-progn
    (add-hook 'buffer-list-update-hook #'minibuffer-line--update-maybe))
  ;;
  ;; Hence, directly advise function `select-window' instead:
  ;;
  (init-defad select-window
      (:after (window &optional norecord)
              init-minibuffer-line--update-maybe/:after)
    #$
    (unless norecord
      (minibuffer-line--update-maybe)))
  ;; -------------------------------------------------------------------------
  ;;
  (dolist (hook '(focus-in-hook
                  focus-out-hook
                  window-configuration-change-hook))
    (add-hook hook #'minibuffer-line--update))
  ;;
  (init-defun init-minibuffer-line--face-setup
      (alist)
    #$
    (dolist (entry alist)
      (face-spec-set (car entry) `((((class color))
                                    :foreground ,(cdr entry))))))
  ;;
  (after-load init-ansi-color
    (init-defun init-minibuffer-line-ansi-color-setup ()
      #$
      (interactive)
      (init-minibuffer-line--face-setup
       `((init-minibuffer-line-method    . ,init-ansi-color-green  )
         (init-minibuffer-line-root      . ,init-ansi-color-red    )
         (init-minibuffer-line-user      . ,init-ansi-color-blue   )
         (init-minibuffer-line-host      . ,init-ansi-color-blue   )
         (init-minibuffer-line-home-dir  . ,init-ansi-color-yellow )
         (init-minibuffer-line-path      . ,init-ansi-color-cyan   )
         (init-minibuffer-line-directory . ,init-ansi-color-blue   )
         (init-minibuffer-line-file-name . ,init-ansi-color-white  )
         (init-minibuffer-line-battery   . ,init-ansi-color-white  )
         (init-minibuffer-line-date      . ,init-ansi-color-green  )
         (init-minibuffer-line-weekday   . ,init-ansi-color-yellow )
         (init-minibuffer-line-time      . ,init-ansi-color-green  )
         (init-minibuffer-line-delimiter . ,init-ansi-color-magenta))))
    (unless (featurep 'init-solarized-theme)
      (init-minibuffer-line-ansi-color-setup)))
  ;;
  (after-load init-solarized-theme
    (init-defun init-minibuffer-line-solarized-setup ()
      #$
      (interactive)
      (init-minibuffer-line--face-setup
       `((init-minibuffer-line-method    . ,init-solarized-green  )
         (init-minibuffer-line-root      . ,init-solarized-red    )
         (init-minibuffer-line-user      . ,init-solarized-blue   )
         (init-minibuffer-line-host      . ,init-solarized-blue   )
         (init-minibuffer-line-home-dir  . ,init-solarized-yellow )
         (init-minibuffer-line-path      . ,init-solarized-cyan   )
         (init-minibuffer-line-directory . ,init-solarized-blue   )
         (init-minibuffer-line-file-name . ,init-solarized-white  )
         (init-minibuffer-line-battery   . ,init-solarized-white  )
         (init-minibuffer-line-date      . ,init-solarized-green  )
         (init-minibuffer-line-weekday   . ,init-solarized-yellow )
         (init-minibuffer-line-time      . ,init-solarized-green  )
         (init-minibuffer-line-delimiter . ,init-solarized-magenta))))
    (init-ignore-progn
      ;; TODO: Still valid?
      (advice-add #'solarized-setup
                  :after
                  #'init-minibuffer-line-solarized-setup))
    (init-minibuffer-line-solarized-setup)))
;;
(init-package minibuffer-line
  :variables
  (init-minibuffer-line-quit-error-message)
  :commands
  (init-minibuffer-line-theme))
