(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-jumps init
  (setq-default evil--jumps-buffer-targets "\\`\\'"))
;;
(init-handler evil-jumps config)
;;
(init-package evil-jumps
  :ensure evil)
