(eval-when-compile
  (require 'init-package))
;;
(init-handler goto-addr preface
  (eval-when-compile
    (init-load devil-common :no-error)
    ;;
    (require 'devil-common nil :no-error)))
;;
(init-handler goto-addr init)
;;
(init-handler goto-addr config)
;;
(init-handler goto-addr highlight-keymap
  (let ((keymap goto-address-highlight-keymap))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap)
    (init-define-keys-kbd keymap
      [remap find-file-at-point] #'goto-address-at-point
      ;;
      "<mouse-2>"                #'goto-address-at-point))
  ;;
  (after-load (init-devil devil)
    (devil-define-keys-kbd goto-address-highlight-keymap
      :motion
      [remap evil-find-file-at-point-with-line] #'goto-address-at-point
      ;;
      [remap find-file-at-point]                #'goto-address-at-point)))
;;
(init-package goto-addr
  :built-in)
