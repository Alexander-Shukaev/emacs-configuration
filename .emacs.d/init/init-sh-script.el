(eval-when-compile
  (require 'init-package))
;;
(init-handler sh-script init
  (init-load indent)
  ;;
  (setq-default
    sh-indentation  init-indent-width
    sh-basic-offset init-indent-width))
;;
(init-handler sh-script config
  (init-defun sh-mode-setup ()
    #$
    (setq-local comment-start "#"))
  (add-hook 'sh-mode-hook #'sh-mode-setup))
;;
(init-package sh-script
  :built-in
  :mode
  (("\\(/\\|\\`\\)\\.\\(\\(ba\\)?sh_\\)?alias\\'" . sh-mode)
   ("/PKGBUILD\\'"                                . sh-mode)
   ("/etc/profile\\'"                             . sh-mode)
   ("/etc/bash.bashrc\\'"                         . sh-mode)
   ("/etc/bash.bash_logout\\'"                    . sh-mode)
   ("/etc/bash_completion\\'"                     . sh-mode)))
