(eval-when-compile
  (require 'init-package))
;;
(defun init-global-define-key
    (key def)
  (eval-when-compile
    (documentation 'init-define-key))
  (init-define-key (current-global-map) key def))
;;
(defun init-global-define-key-kbd
    (key def)
  (eval-when-compile
    (documentation 'init-define-key-kbd))
  (init-define-key-kbd (current-global-map) key def))
;;
(defmacro init-global-define-keys-with-prefix
    (key &rest ...)
  (declare (debug  t)
           (indent defun))
  (eval-when-compile
    (documentation 'init-define-keys-with-prefix))
  `(init-define-keys-with-prefix (current-global-map) ,key ,@...))
;;
(defmacro init-global-define-keys-with-prefix-kbd
    (key &rest ...)
  (declare (debug  t)
           (indent defun))
  (eval-when-compile
    (documentation 'init-define-keys-with-prefix-kbd))
  `(init-define-keys-with-prefix-kbd (current-global-map) ,key ,@...))
;;
;; See `x-alternatives-map', `x-setup-function-keys',
;; `normal-erase-is-backspace-setup-frame', `normal-erase-is-backspace-mode':
(init-define-keys-kbd local-function-key-map
  "<S-backspace>"     "S-DEL"
  ;; -------------------------------------------------------------------------
  "<C-S-tab>"         "<C-backtab>"
  "<C-S-iso-lefttab>" "<C-backtab>"
  ;;
  ;; CAUTION:
  ;;
  ;; Unlike on Windows, <S-tab> is not recognized on Unix by default.  Thus,
  ;; use <backtab> instead.
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; WTF?
  ;;
  ;;   C-x             Prefix Command
  ;;   C-x ESC         Prefix Command
  ;;   C-x g           magit-status
  ;;   C-x C-<left>    previous-buffer
  ;;   C-x C-<right>   next-buffer
  ;;   C-x <left>      previous-buffer
  ;;   C-x <right>     next-buffer
  ;;   C-x M-g         magit-dispatch
  ;;   C-x @           Prefix Command
  ;; -------------------------------------------------------------------------
  "C-x"               nil)
;;
(init-assert (fboundp 'ESC-prefix))
(init-assert (keymapp  esc-map))
(init-assert (eq       esc-map (symbol-function 'ESC-prefix)))
;;
;; TODO: (C-down-mouse-3 menu-item
;; TODO: (tool-bar menu-item
;; TODO: (menu-bar keymap (options keymap (mule keymap (toggle-input-method menu-item "Toggle Input Method" toggle-input-method))))
(let* ((keymap              (make-keymap))
       (meta-prefix-key     (kbd (char-to-string meta-prefix-char)))
       (meta-prefix-command (global-key-binding  meta-prefix-key)))
  (dolist (command (nconc (mapcar #'(lambda
                                        (key)
                                      (global-key-binding (kbd key)))
                                  '("<XF86Back>"
                                    "<XF86Forward>"
                                    "<XF86WakeUp>"
                                    "<drag-n-drop>"
                                    "<delete-frame>"
                                    "<iconify-frame>"
                                    ;; ---------------------------------------
                                    ;; CAUTION:
                                    ;;
                                    ;; Ensure \\[handle-switch-frame] is
                                    ;; properly key-mapped to
                                    ;; `handle-switch-frame':
                                    ;;
                                    "<switch-frame>"
                                    ;;
                                    ;; Otherwise, `make-frame' results in
                                    ;; freeze.
                                    ;; ---------------------------------------
                                    "<make-frame-visible>"
                                    "<select-window>"))
                          '(self-insert-command
                            keyboard-quit
                            help-command
                            indent-for-tab-command
                            next-line
                            previous-line
                            quoted-insert
                            universal-argument
                            ;; -----------------------------------------------
                            ;; CAUTION:
                            ;;
                            ;; Don't inject the default key bindings from the
                            ;; `Control-X-prefix' function (that delivers them
                            ;; from the `ctl-x-map' variable) as they are
                            ;; convoluted, noisy, and clutter editing
                            ;; experience.
                            ;; -----------------------------------------------
                            ;; CAUTION:
                            ;;
                            ;; Don't inject the default key bindings from the
                            ;; `ESC-prefix' function (that delivers them from
                            ;; the `esc-map' variable) as they are convoluted,
                            ;; noisy, and clutter editing experience.
                            ;; -----------------------------------------------
                            toggle-input-method
                            abort-recursive-edit
                            negative-argument
                            digit-argument
                            ignore
                            ;; -----------------------------------------------
                            ;; CAUTION:
                            ;;
                            ;; Don't inject the default key bindings from the
                            ;; `repeat-complex-command' function because it
                            ;; additionally brings some from the
                            ;; `Control-X-prefix' function (that delivers them
                            ;; from the `ctl-x-map' variable) as they are
                            ;; convoluted, noisy, and clutter editing
                            ;; experience.
                            ;; -----------------------------------------------
                            execute-extended-command
                            mouse-start-secondary
                            mouse-set-point
                            mouse-set-secondary
                            mouse-set-region
                            mouse-drag-secondary
                            mouse-drag-region
                            mouse-drag-bottom-left-corner
                            mouse-drag-bottom-edge
                            mouse-drag-bottom-right-corner
                            mouse-drag-right-edge
                            mouse-drag-top-right-corner
                            mouse-drag-top-edge
                            mouse-drag-top-left-corner
                            mouse-drag-left-edge
                            mouse-drag-header-line
                            mouse-drag-mode-line
                            mouse-drag-vertical-line
                            mouse-select-window
                            mouse--down-1-maybe-follows-link
                            mouse--strip-first-event
                            mwheel-scroll
                            scroll-bar-toolkit-horizontal-scroll
                            scroll-bar-toolkit-scroll
                            toggle-frame-fullscreen)))
    (when command
      (substitute-key-definition command
                                 command
                                 keymap
                                 (current-global-map))))
  (setq-default global-map keymap)
  (use-global-map keymap)
  ;; -------------------------------------------------------------------------
  (init-ignore-progn
    (setq-default   meta-prefix-char ?\0)
    (setq           meta-prefix-key  (kbd (char-to-string meta-prefix-char)))
    (global-set-key meta-prefix-key  meta-prefix-command))
  ;;
  ;; CAUTION:
  ;;
  ;; Never bother to enable the above code as it will break mappings for
  ;; <M-x>, <right>, <left>, <down>, <up>, and many more in the terminal
  ;; variant of Emacs.  In general, resetting `meta-prefix-char' to another
  ;; value and/or remapping <ESC> in `global-map' (`current-global-map') is a
  ;; (really) bad idea [1, 2, 3].
  ;; -------------------------------------------------------------------------
  )
;;
(init-global-define-key-kbd "<mouse-8>" "<XF86Back>   ")
(init-global-define-key-kbd "<mouse-9>" "<XF86Forward>")
;;
(init-global-define-key-kbd "<left> " #'backward-char)
(init-global-define-key-kbd "<right>" #'forward-char)
;;
(init-global-define-key-kbd "   <delete>" #'delete-forward-char)
(init-global-define-key-kbd "<kp-delete>" #'delete-forward-char)
(init-global-define-key-kbd " <S-delete>" #'backward-delete-char)
;;
(init-global-define-key-kbd "DEL" #'backward-delete-char-untabify)
(init-global-define-key-kbd "RET" #'newline-and-indent)
;;
(init-global-define-key-kbd "C-t" #'tab-to-tab-stop)
;;
(init-global-define-key-kbd "M-i" #'toggle-input-method)
;;
(after-load simple
  (dolist (key '("M-:"))
    (init-global-define-key-kbd key #'execute-extended-command))
  (dolist (key '("M-!"))
    (init-global-define-key-kbd key #'shell-command))
  (dolist (key '("M-&"))
    (init-global-define-key-kbd key #'async-shell-command)))
;;
(after-load simple
  (init-global-define-key-kbd "M-e" #'eval-expression))
;;
(after-load ffap
  (init-global-define-keys-with-prefix-kbd "C-f"
    "RET" #'find-file-at-point
    ;;
    "C-/" #'dired-at-point
    "/"   #'dired-at-point
    ;;
    "C-e" #'ffap-other-frame
    "E"   #'ffap-other-window
    "e"   #'ffap
    ;;
    "A"   #'ffap-alternate-file-other-window
    "a"   #'ffap-alternate-file
    ;;
    "l"   #'ffap-literally
    ;;
    "p" #'find-file-at-point))
;;
(after-load files
  (init-global-define-keys-with-prefix-kbd "M-f"
    "C-f" #'find-file-other-frame
    "F"   #'find-file-other-window
    "f"   #'find-file
    ;;
    "l"   #'find-file-literally
    ;;
    "A"   #'find-alternate-file-other-window
    "a"   #'find-alternate-file
    ;;
    "C-e" "C-f"
    "E"   "F"
    "e"   "f"
    ;;
    "C-r" #'find-file-read-only-other-frame
    "R"   #'find-file-read-only-other-window
    "r"   #'find-file-read-only))

;; TODO: Implement by introducing translation alist (map) used by
;; `init-package' and `after-load':
;; TODO: `replace' and `text-mode' are features since `f70f9a5'.
;; TODO: `isearch' is feature since `fd912a80c'

;; '((global-set-key [S-mouse-3] 'ffap-at-mouse)
;;   (global-set-key [C-S-mouse-3] 'ffap-menu)
;;
(after-load "replace"
  (init-global-define-keys-with-prefix-kbd "M-o"
    "M" #'multi-occur-in-matching-buffers
    "m" #'multi-occur
    ;;
    "o" #'occur)
  ;;
  (init-global-define-keys-with-prefix-kbd "M-r"
    "M" #'map-query-replace-regexp
    "R" #'query-replace-regexp
    "S" #'query-replace
    ;;
    "r" #'replace-regexp
    "s" #'replace-string
    ;;
    "f" #'flush-lines
    "k" #'keep-lines
    ;;
    "?" #'how-many
    "h" #'how-many))
;;
(after-load init-dired
  (init-global-define-key-kbd "<f4>" #'dired-jump))
;;
(after-load init-easy-kill
  (init-global-define-key-kbd "<remap kill-ring-save>" #'easy-kill)
  (init-global-define-key-kbd "<remap mark-sexp>"      #'easy-mark))
;;
(after-load init-gif-screencast
  (init-global-define-key-kbd "<XF86AudioPlay>" #'gif-screencast))
;;
(after-load help
  (init-assert (fboundp 'help-command))
  (init-assert (keymapp  help-map))
  (init-assert (eq       help-map (symbol-function 'help-command)))
  (let* ((help-key     (kbd (char-to-string help-char)))
         (help-command (global-key-binding  help-key)))
    (init-assert (=     help-char    ?\b))
    (init-assert (equal help-key     (kbd "C-h")))
    (init-assert (eq    help-command 'help-command))))
;;
(defun init-global-esc-map-setup ()
  (init-assert (fboundp 'ESC-prefix))
  (init-assert (keymapp  esc-map))
  (init-assert (eq       esc-map (symbol-function 'ESC-prefix)))
  (let* ((meta-prefix-key     (kbd (char-to-string meta-prefix-char)))
         (meta-prefix-command (global-key-binding  meta-prefix-key)))
    (init-assert (=     meta-prefix-char    ?\e))
    (init-assert (equal meta-prefix-key     (kbd "ESC")))
    (if (keymapp meta-prefix-command)
        (init-global-define-key-kbd "ESC ESC" #'keyboard-escape-quit)
      ;; ---------------------------------------------------------------------
      ;; CAUTION:
      ;;
      ;; According to the above configuration, the following should never
      ;; happen (simply not desired at the moment):
      ;;
      (init-assert nil)
      ;; ---------------------------------------------------------------------
      (init-define-keys-kbd esc-map
        "ESC" nil
        "ESC" #'keyboard-escape-quit))))
;;
(after-load simple
  (after-init (init-global-esc-map-setup)))
;;
(after-load mouse
  (init-global-define-key-kbd "<M-mouse-2>" #'mouse-secondary-save-then-kill)
  (init-global-define-key-kbd "<M-mouse-3>" #'mouse-yank-secondary)
  ;;
  (init-global-define-key-kbd "<mouse-2>" #'mouse-save-then-kill)
  (init-global-define-key-kbd "<mouse-3>" #'mouse-yank-at-click))
;;
(provide 'init-global)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(elisp) Prefix Keys'
;;  [2] Info node `(elisp) Functions for Key Lookup'
;;  [3] Info node `(elisp) Translation Keymaps'
;;  [4] URL `http://www.emacswiki.org/emacs/ShiftedKeys'
;;  ==========================================================================
;;  }}} References
