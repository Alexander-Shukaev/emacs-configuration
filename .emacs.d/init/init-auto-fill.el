(eval-when-compile
  (require 'init-package))
;;
(init-handler auto-fill prepare
  (init-load simple))
;;
(init-handler auto-fill init
  (after-load simple
    (require 'auto-fill)))
;;
(init-handler auto-fill config)
;;
(init-package auto-fill
  :load-path "lisp/auto-fill"
  :commands
  (auto-fill-mode
   auto-fill-function)
  :diminish
  (auto-fill-function))
