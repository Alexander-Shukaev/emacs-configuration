(eval-when-compile
  (require 'init-package))
;;
(init-handler recentf-ext prepare
  (init-load recentf :no-error))
;;
(init-handler recentf-ext init)
;;
(init-handler recentf-ext config)
;;
(init-package recentf-ext)
