(eval-when-compile
  (require 'init-package))
;;
(init-handler mb-depth init
  (after-init (minibuffer-depth-indicate-mode)))
;;
(init-handler mb-depth config)
;;
(init-package mb-depth
  :built-in)
