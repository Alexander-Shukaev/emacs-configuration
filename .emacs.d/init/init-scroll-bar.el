(eval-when-compile
  (require 'init-package))
;;
(init-handler scroll-bar init
  (scroll-bar-mode -1))
;;
(init-handler scroll-bar config)
;;
(init-package scroll-bar
  :built-in
  :commands
  (scroll-bar-mode))
