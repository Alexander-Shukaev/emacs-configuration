(eval-when-compile
  (require 'init-package))
;;
(init-handler prog-mode init
  ;; -------------------------------------------------------------------------
  (init-ignore-progn
    (after-init (global-prettify-symbols-mode)))
  ;;
  ;; NOTE:
  ;;
  ;; Makes visual tracking of alignment and column limit annoying.
  ;; -------------------------------------------------------------------------
  )
;;
(init-handler prog-mode config
  (after-load init-auto-fill
    (add-hook 'prog-mode-hook #'auto-fill-mode                     :append))
  ;;
  (after-load init-display-fill-column-indicator
    (add-hook 'prog-mode-hook #'display-fill-column-indicator-mode :append))
  ;;
  (after-load init-fill-column-indicator
    (add-hook 'prog-mode-hook #'fci-mode                           :append))
  ;;
  (after-load init-bug-reference
    (add-hook 'prog-mode-hook #'bug-reference-prog-mode            :append))
  ;;
  (after-load init-goto-addr
    (add-hook 'prog-mode-hook #'goto-address-prog-mode             :append))
  ;;
  (after-load init-highlight-indentation
    (add-hook 'prog-mode-hook #'highlight-indentation-mode         :append))
  ;;
  (after-load init-highlight-numbers
    (add-hook 'prog-mode-hook #'highlight-numbers-mode             :append))
  ;;
  (after-load init-newcomment
    (init-defun init-prog-mode-auto-fill-setup ()
      #$
      (setq-local comment-auto-fill-only-comments t))
    (add-hook 'prog-mode-hook #'init-prog-mode-auto-fill-setup     :append))
  ;;
  (after-load init-rainbow-delimiters
    (add-hook 'prog-mode-hook #'rainbow-delimiters-mode            :append))
  ;;
  (after-load init-rainbow-mode
    (add-hook 'prog-mode-hook #'rainbow-mode                       :append))
  ;;
  (after-load init-speck
    (add-hook 'prog-mode-hook #'speck-mode                         :append)
    ;;
    (after-load speck
      (init-defun init-prog-mode-speck-setup ()
        #$
        (setq-local speck-syntactic t)))
    (add-hook 'prog-mode-hook #'init-prog-mode-speck-setup         :append))
  ;;
  (after-load init-whitespace
    (add-hook 'prog-mode-hook #'whitespace-mode                    :append)))
;;
(init-package prog-mode
  :built-in)
