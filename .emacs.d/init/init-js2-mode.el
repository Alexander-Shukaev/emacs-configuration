(eval-when-compile
  (require 'init-package))
;;
(init-handler js2-mode prepare
  (init-load js :no-error))
;;
(init-handler js2-mode init
  (init-load indent)
  ;;
  (setq-default js2-basic-offset init-indent-width))
;;
(init-handler js2-mode config
  (defconst init-js2-mode-hooks
    '(js2-jsx-mode-hook
      js2-mode-hook))
  ;;
  (after-load (init-clang-format devil)
    (init-defun init-js2-mode-clang-format-devil-setup ()
      #$
      (setq-local devil-format-region-function #'clang-format-region))
    (dolist (hook init-js2-mode-hooks)
      (add-hook hook #'init-js2-mode-clang-format-devil-setup)))
  ;;
  (after-load init-electric-operator
    (dolist (hook init-js2-mode-hooks)
      (add-hook hook #'electric-operator-mode)))
  ;;
  (after-load init-highlight-operators
    (dolist (hook init-js2-mode-hooks)
      (add-hook hook #'highlight-operators-mode))))
;;
(init-package js2-mode
  :interpreter
  (("node" . js2-jsx-mode)
   ("node" . js2-mode))
  :mode
  (("\\.jsx?\\'" . js2-jsx-mode)
   ("\\.js\\'"   . js2-mode)))
