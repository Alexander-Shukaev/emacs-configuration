(eval-when-compile
  (require 'init-package))
;;
(init-handler find-func init)
;;
(init-handler find-func config
  (unless find-function-C-source-directory
    (setq-default find-function-C-source-directory "")))
;;
(init-package find-func
  :built-in)
