(eval-when-compile
  (require 'init-package))
;;
(init-handler cmake-mode init
  (init-load indent)
  ;;
  (setq-default cmake-tab-width init-indent-width))
;;
(init-handler cmake-mode config
  (unless (memq 'cmake-font-lock-activate cmake-mode-hook)
    (after-load init-cmake-font-lock
      (init-defun init-cmake-mode-cmake-font-lock-setup ()
        #$
        (cmake-font-lock-activate))
      (add-hook 'cmake-mode-hook #'init-cmake-mode-cmake-font-lock-setup)))
  ;;
  (unless (eq (let* ((mode     (get 'cmake-mode 'derived-mode-parent))
                     (function (symbol-function mode)))
                (while (symbolp function)
                  (setq
                    mode     function
                    function (symbol-function mode)))
                mode)
              #'prog-mode)
    (after-load prog-mode
      (init-defun init-cmake-mode-prog-mode-setup ()
        #$
        (run-hooks 'prog-mode-hook))
      (add-hook 'cmake-mode-hook #'init-cmake-mode-prog-mode-setup)))
  ;;
  ;; -------------------------------------------------------------------------
  ;; CAUTION:
  ;;
  ;; Package `cmake-font-lock.el' deeply overrides `font-lock' defaults (see
  ;; `font-lock-defaults', `font-lock-refresh-defaults',
  ;; `cmake-font-lock-advanced-keywords', `cmake-font-lock-setup', and
  ;; `cmake-font-lock-activate') and, as a result, if `whitespace-mode' was
  ;; activated (as part of `prog-mode-hook') before call to
  ;; `cmake-font-lock-activate' (as part of `cmake-mode-hook'), then any
  ;; visible effect of the former will be erased by the latter as per
  ;; `cmake-font-lock.el'.  Hence, forcefully reapply `whitespace-mode'
  ;; afterwards:
  ;;
  (after-load init-whitespace
    (add-hook 'cmake-mode-hook #'whitespace-mode :append))
  ;; -------------------------------------------------------------------------
  )
;;
(init-package cmake-mode
  :mode
  (("/CMakeLists\\.txt\\'" . cmake-mode)
   ("\\.cmake\\'"          . cmake-mode)
   ("\\.CMake\\'"          . cmake-mode)
   ("\\.ctest\\'"          . cmake-mode)
   ("\\.CTest\\'"          . cmake-mode)))
