(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-terminal-cursor-changer init
  (setq-default etcc-use-blink t)
  ;;
  (after-load evil
    (after-init (evil-terminal-cursor-changer-activate))))
;;
(init-handler evil-terminal-cursor-changer config)
;;
(init-package evil-terminal-cursor-changer
  ;; -------------------------------------------------------------------------
  ;; DEPRECATED:
  ;;
  ;; Because of `term-cursor':
  ;;
  :disabled
  ;;
  ;; Also broken for too many terminal flavors and/or versions.
  ;; -------------------------------------------------------------------------
  )
