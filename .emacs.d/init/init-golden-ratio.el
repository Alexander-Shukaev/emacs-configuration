(eval-when-compile
  (require 'init-package))
;;
(init-handler golden-ratio init
  (setq-default golden-ratio-auto-scale nil)
  ;;
  (after-init (golden-ratio-mode)))
;;
(init-handler golden-ratio config
  (defvar golden-ratio-selected-window
    (frame-selected-window)
    "Selected window.")
  ;;
  (init-defun golden-ratio-set-selected-window
      (&optional window)
    #$
    "Set selected window to WINDOW."
    (setq-default golden-ratio-selected-window (or window
                                                   (frame-selected-window))))
  ;;
  (init-defun golden-ratio-selected-window-p
      (&optional window)
    #$
    "Return t if WINDOW is selected window."
    (eq (or window (selected-window))
        (default-value 'golden-ratio-selected-window)))
  ;;
  (init-defun golden-ratio-maybe
      (&optional arg)
    #$
    "Run `golden-ratio' if `golden-ratio-selected-window-p' returns nil."
    (interactive "p")
    (unless (golden-ratio-selected-window-p)
      (golden-ratio-set-selected-window)
      (golden-ratio arg)))
  ;;
  ;; -------------------------------------------------------------------------
  ;; CAUTION:
  ;;
  ;; Don't advise `select-window' explicitly with `golden-ratio'.  Otherwise,
  ;; the screen is updated/redrawn/renewed too frequently (probably because of
  ;; another package continuously calling `select-window' non-interactively).
  ;; Prefer `buffer-list-update-hook' instead:
  ;;
  (add-hook 'buffer-list-update-hook #'golden-ratio-maybe)
  ;; -------------------------------------------------------------------------
  (add-hook 'focus-in-hook           #'golden-ratio)
  (add-hook 'focus-out-hook          #'golden-ratio))
;;
(init-package golden-ratio
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; There are a couple of merges to do upstream till it gets useful.
  ;; -------------------------------------------------------------------------
  :diminish
  (golden-ratio-mode))
