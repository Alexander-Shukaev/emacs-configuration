(eval-when-compile
  (require 'init-package))
;;
(init-handler x-win prepare)
;;
(init-handler x-win init)
;;
(init-handler x-win config
  (init-assert (fboundp 'emacs-session-filename))
  (init-defad emacs-session-filename
      (:around (function session-id) init/:around)
    #$
    (let ((directory (expand-directory "session/" user-temporary-directory)))
      (condition-case e
          (progn
            (init-make-safe-directory directory)
            (expand-file-name session-id directory))
        (error (lwarn 'init
                      :error
                      "%s"
                      (error-message-string e))
               (funcall function session-id))))))
;;
(init-package x-win
  :built-in)
