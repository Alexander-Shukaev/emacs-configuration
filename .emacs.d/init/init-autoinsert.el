(eval-when-compile
  (require 'init-package))
;;
(init-handler autoinsert init
  (setq-default
    auto-insert-directory
    (abbreviate-file-name (init-user-expand-directory "auto-insert/"))
    ;;
    auto-insert-query
    nil
    ;;
    auto-insert-alist
    '(((bat-mode            . "Batch")        . ["template.bat"     init-auto-insert])
      ((c-mode              . "C")            . ["template.c"       init-auto-insert])
      ((c++-mode            . "C++")          . ["template.cpp"     init-auto-insert])
      ((cmake-mode          . "CMake")        . ["template.make"    init-auto-insert])
      ((context-mode        . "ConTeXt")      . ["template.context" init-auto-insert])
      ((makefile-gmake-mode . "GNU Makefile") . ["template.make"    init-auto-insert])
      ((python-mode         . "Python")       . ["template.make"    init-auto-insert])
      ((sh-mode             . "Shell")        . ["template.make"    init-auto-insert])
      ((yaml-mode           . "YAML")         . ["template.make"    init-auto-insert])))
  ;;
  (after-init (auto-insert-mode)))
;;
(init-handler autoinsert config
  (defsubst init-auto-insert--user-name
      (&optional value)
    (or (bound-and-true-p init-user-name) value "John Doe"))
  ;;
  (defsubst init-auto-insert--user-email
      (&optional value)
    (or (bound-and-true-p init-user-email) value "john.doe@example.com"))
  ;;
  (defsubst init-auto-insert--replace-file ()
    (save-match-data
      (save-excursion
        (while (ignore-errors (search-forward "FILE"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (if (buffer-file-name)
                               (file-name-nondirectory (buffer-file-name))
                             (buffer-name))
                           'fixedcase))))))
  ;;
  (defsubst init-auto-insert--replace-version ()
    (save-match-data
      (save-excursion
        (while (ignore-errors (search-forward "VERSION"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match "0.0.0"
                           'fixedcase))))))
  ;;
  (defsubst init-auto-insert--replace-updated ()
    (save-match-data
      (save-excursion
        (while (ignore-errors (search-forward "UPDATED"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (format-time-string "%F %A %T (%z)")
                           'fixedcase))))))
  ;;
  (defsubst init-auto-insert--replace-created ()
    (save-match-data
      (save-excursion
        (while (ignore-errors (search-forward "CREATED"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (format-time-string "%F %A %T (%z)")
                           'fixedcase))))))
  ;;
  (defsubst init-auto-insert--replace-author ()
    (save-match-data
      (save-excursion
        (while (ignore-errors (search-forward "AUTHOR.NAME"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (init-auto-insert--user-name)
                           'fixedcase)))
        (while (ignore-errors (search-forward "AUTHOR.EMAIL"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (init-auto-insert--user-email)
                           'fixedcase))))))
  ;;
  (defsubst init-auto-insert--replace-maintainer ()
    (save-match-data
      (save-excursion
        (while (ignore-errors (search-forward "MAINTAINER.NAME"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (init-auto-insert--user-name)
                           'fixedcase)))
        (while (ignore-errors (search-forward "MAINTAINER.EMAIL"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (init-auto-insert--user-email)
                           'fixedcase))))))
  ;;
  (defsubst init-auto-insert--replace-copyright ()
    (save-match-data
      (save-excursion
        (while (ignore-errors (search-forward "COPYRIGHT.YEAR"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (format-time-string "%Y")
                           'fixedcase)))
        (while (ignore-errors (search-forward "COPYRIGHT.NAME"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (init-auto-insert--user-name)
                           'fixedcase)))
        (while (ignore-errors (search-forward "COPYRIGHT.EMAIL"))
          (save-restriction
            (narrow-to-region (match-beginning 0) (match-end 0))
            (replace-match (init-auto-insert--user-email)
                           'fixedcase))))))
  ;;
  (init-defun init-auto-insert ()
    #$
    (init-auto-insert--replace-file)
    (init-auto-insert--replace-version)
    (init-auto-insert--replace-updated)
    (init-auto-insert--replace-created)
    (init-auto-insert--replace-author)
    (init-auto-insert--replace-maintainer)
    (init-auto-insert--replace-copyright))
  ;;
  (init-defad auto-insert
      (:before (&rest ...) init/:before)
    #$
    (when (and (bound-and-true-p dir-locals-company)
               (equal dir-locals-company "Presence"))
      (setq-local auto-insert-directory
        (abbreviate-file-name (expand-file-name "Presence/"
                                                auto-insert-directory)))
      (setq-local auto-insert-alist
        '(((c-mode              . "C")            . ["template.c"    init-auto-insert])
          ((c++-mode            . "C++")          . ["template.cpp"  init-auto-insert])
          ((cmake-mode          . "CMake")        . ["template.make" init-auto-insert])
          ((makefile-gmake-mode . "GNU Makefile") . ["template.make" init-auto-insert])
          ((python-mode         . "Python")       . ["template.make" init-auto-insert])
          ((sh-mode             . "Shell")        . ["template.make" init-auto-insert])
          ((yaml-mode           . "YAML")         . ["template.make" init-auto-insert]))))))
;;
(init-package autoinsert
  :built-in)
