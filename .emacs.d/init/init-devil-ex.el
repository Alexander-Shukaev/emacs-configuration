(eval-when-compile
  (require 'init-package))
;;
(eval-and-compile
  (init-load devil-load-path))
;;
(init-handler devil-ex init)
;;
(init-handler devil-ex config)
;;
(init-package devil-ex
  :load-path init-devil-load-path
  :ensure evil)
