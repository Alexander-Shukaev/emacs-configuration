(setq-default
  fast-but-imprecise-scrolling t
  scroll-conservatively        0
  scroll-margin                5
  scroll-step                  5)
;;
;; TODO:
;; (set 'auto-window-vscroll nil)
;; (set 'auto-save-interval 0)
;;
(provide 'init-scroll)
