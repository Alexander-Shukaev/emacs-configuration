(eval-when-compile
  (require 'init-package))
;;
(init-handler wgrep-rg init)
;;
(init-handler wgrep-rg config)
;;
(init-package wgrep-rg
  :ensure rg)
