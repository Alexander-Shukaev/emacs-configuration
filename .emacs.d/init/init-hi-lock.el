(eval-when-compile
  (require 'init-package))
;;
(init-handler hi-lock init)
;;
(init-handler hi-lock config)
;;
(init-package hi-lock
  :built-in
  :diminish
  (hi-lock-mode))
