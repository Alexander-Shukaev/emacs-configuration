(eval-when-compile
  (require 'init-package))
;;
(init-handler buffer-manager init
  ;; -------------------------------------------------------------------------
  ;; NOTE:
  ;;
  ;; `buff-menu.el' does not provide any feature (with `provide'), hence it is
  ;; impossible to use `unload-feature' here.  Furthermore, `buff-menu.el' is
  ;; always loaded before `user-init-file'.  As a result, it is impossible to
  ;; shadow it by putting empty (custom) `buff-menu.el' into some directory
  ;; from `load-path' which precedes another (standard) directory (from
  ;; `load-path') containing default (standard) `buff-menu.el' as it requires
  ;; modifying `load-path' (what once again is too late to do in
  ;; `user-init-file' anyway).
  ;; -------------------------------------------------------------------------
  (after-load "buff-menu"
    ;; TODO: Is there any better way to clean it up?  Yes, `load-history'!
    ;; TODO: Move to `buffer-manager':
    (dolist (function '(Buffer-menu-mode
                        buffer-menu
                        buffer-menu-other-window
                        list-buffers
                        Buffer-menu-toggle-files-only
                        Buffer-menu-sort
                        Buffer-menu-buffer
                        Buffer-menu-no-header
                        Buffer-menu-beginning
                        Buffer-menu-mark
                        Buffer-menu-unmark
                        Buffer-menu-backup-unmark
                        Buffer-menu--unmark
                        Buffer-menu-delete
                        Buffer-menu-delete-backwards
                        Buffer-menu-save
                        Buffer-menu-not-modified
                        Buffer-menu-execute
                        Buffer-menu-select
                        Buffer-menu-marked-buffers
                        Buffer-menu-isearch-buffers
                        Buffer-menu-isearch-buffers-regexp
                        Buffer-menu-multi-occur
                        Buffer-menu-visit-tags-table
                        Buffer-menu-1-window
                        Buffer-menu-this-window
                        Buffer-menu-other-window
                        Buffer-menu-switch-other-window
                        Buffer-menu-2-window
                        Buffer-menu-toggle-read-only
                        Buffer-menu-bury
                        Buffer-menu-view
                        Buffer-menu-view-other-window
                        list-buffers-noselect
                        Buffer-menu-mouse-select
                        list-buffers--refresh
                        tabulated-list-entry-size->
                        Buffer-menu--pretty-name
                        Buffer-menu--pretty-file-name
                        Buffer-menu-info-node-description))
      ;; TODO: Only once:
      ;; (init-assert (fboundp function))
      (fmakunbound function))
    ;; TODO: Clean up variables too!
    ))
;;
(init-handler buffer-manager config
  (face-spec-reset-face 'bm-buffer-name)
  (set-face-attribute   'bm-buffer-name nil
                        :inherit 'link)
  ;;
  (after-load evil
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    (evil-make-overriding-map bm-mode-map 'normal)
    ;;
    ;; So that in Visual state, for example, \\[bm-display-buffer-at] would
    ;; stay properly key-mapped.
    ;; -----------------------------------------------------------------------
    (evil-set-initial-state 'bm-mode 'motion)))
;;
(init-package buffer-manager
  :load-path "lisp/buffer-manager")
