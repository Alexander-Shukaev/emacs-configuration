(eval-when-compile
  (require 'init-package))
;;
(init-handler ediff preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler ediff prepare
  (init-load diff)
  (init-load ediff-util)
  ;;
  (defcustom init-ediff-face-spec-alist
    '((ediff-current-diff-C . ((((class color)
                                 (background light))
                                :background "#DDEEFF"
                                :foreground "#005588")
                               (((class color)
                                 (background dark))
                                :background "#005588"
                                :foreground "#DDEEFF")))
      (ediff-fine-diff-C .    ((((class color)
                                 (background light))
                                :background "#EEFFFF"
                                :foreground "#006699")
                               (((class color)
                                 (background dark))
                                :background "#006699"
                                :foreground "#EEFFFF"))))
    "Association list of face specifications in Ediff."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type list))
  ;;
  (defcustom init-ediff-face-alias-alist
    '((ediff-even-diff-A           . magit-diff-context-highlight)
      (ediff-even-diff-Ancestor    . magit-diff-context)
      (ediff-even-diff-B           . magit-diff-context-highlight)
      (ediff-even-diff-C           . magit-diff-context-highlight)
      (ediff-odd-diff-A            . magit-diff-context-highlight)
      (ediff-odd-diff-Ancestor     . magit-diff-context)
      (ediff-odd-diff-B            . magit-diff-context-highlight)
      (ediff-odd-diff-C            . magit-diff-context-highlight)
      (ediff-current-diff-A        . magit-diff-our)
      (ediff-current-diff-Ancestor . magit-diff-base)
      (ediff-current-diff-B        . magit-diff-their)
      (ediff-fine-diff-A           . magit-diff-removed-highlight)
      (ediff-fine-diff-Ancestor    . magit-diff-base-highlight)
      (ediff-fine-diff-B           . magit-diff-added-highlight))
    "Association list of face aliases in Ediff."
    :group 'init
    :type '(alist :key-type   symbol
                  :value-type symbol)))
;;
(init-handler ediff init
  (setq-default
    ediff-custom-diff-options   "-u"
    ediff-split-window-function #'split-window-horizontally
    ediff-window-setup-function #'ediff-setup-windows-plain))
;;
(init-handler ediff config
  (dolist (hook '(ediff-cleanup-hook
                  ediff-startup-hook
                  ediff-suspend-hook))
    (add-hook hook #'ediff-toggle-wide-display))
  ;;
  (after-load evil
    (dolist (mode '(ediff-mode
                    ediff-meta-mode))
      (evil-set-initial-state mode 'emacs)))
  ;;
  (after-load (init-solarized-theme solarized-theme magit-diff)
    (init-defun init-ediff-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (dolist (entry init-ediff-face-spec-alist)
          (let ((face (car entry))
                (spec (cdr entry)))
            (put face 'theme-face nil)
            (face-spec-set face spec)))
        (dolist (entry init-ediff-face-alias-alist)
          (let ((face  (car entry))
                (alias (cdr entry)))
            (put face 'theme-face nil)
            (put face 'face-alias alias)))))
    (init-ediff-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...)
                init-ediff-solarized-theme-setup/:after)
      #$
      (init-ediff-solarized-theme-setup theme))))
;;
(init-package ediff
  :built-in)
