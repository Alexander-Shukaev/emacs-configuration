(eval-when-compile
  (require 'init-package))
;;
(init-handler autorevert init
  (setq-default
    auto-revert-remote-files nil
    auto-revert-use-notify   t
    auto-revert-verbose      t))
;;
(init-handler autorevert config
  (init-defun init-auto-revert-tail-mode-setup ()
    #$
    (if (bound-and-true-p auto-revert-mode)
        (progn
          (setq-local buffer-read-only t)
          (let ((remote (file-remote-p default-directory)))
            (when (and (boundp 'tramp-verbose) remote)
              (setq-local tramp-verbose 2))
            (setq-local auto-revert-interval (if remote 1 0.5); seconds
              ))
          ;; -----------------------------------------------------------------
          ;; NOTE:
          ;;
          ;; Using `itail' for remote files instead:
          ;;
          (setq-local auto-revert-remote-files nil)
          ;; -----------------------------------------------------------------
          (setq-local auto-revert-verbose      nil)
          (auto-revert-set-timer)
          ;; -----------------------------------------------------------------
          ;; TODO:
          ;;
          ;; Similarly to `init-company-turn-off-fci-maybe', both
          ;; `font-lock-mode' and `show-smartparens-mode' need to be turned
          ;; off/on gracefully by tracking via buffer-local variables:
          ;;
          (when (fboundp 'font-lock-mode)
            (font-lock-mode        -1))
          (when (fboundp 'show-smartparens-mode)
            (show-smartparens-mode -1))
          ;; -----------------------------------------------------------------
          (goto-char (point-max)))
      (when (local-variable-p 'tramp-verbose)
        (kill-local-variable 'tramp-verbose))))
  (add-hook 'auto-revert-tail-mode-hook #'init-auto-revert-tail-mode-setup))
;;
(init-package autorevert
  :built-in
  :diminish
  (auto-revert-mode
   auto-revert-tail-mode
   global-auto-revert-mode))
