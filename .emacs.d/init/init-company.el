(eval-when-compile
  (require 'init-package))
;;
(init-handler company preface
  (eval-when-compile
    (require 'cl-lib)))
;;
(init-handler company prepare
  (defun init-company-idle-delay-setup ()
    (setq-local company-idle-delay
      (if (file-remote-p default-directory)
          nil
        (default-value 'company-idle-delay); seconds
        ))))
;;
(init-handler company init
  (setq-default
    company-begin-commands        '(self-insert-command)
    company-dabbrev-downcase      nil
    company-dabbrev-ignore-case   nil
    company-echo-delay            0; seconds
    company-frontends
    '(company-tng-frontend
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ;; Because of `company-tng':
      ;;
      ;; company-preview-frontend
      ;; ---------------------------------------------------------------------
      company-pseudo-tooltip-unless-just-one-frontend-with-delay
      company-echo-metadata-frontend)
    company-global-modes          t
    company-idle-delay            0.1; seconds
    company-minimum-prefix-length 2
    company-require-match         nil
    company-selection-wrap-around t)
  ;;
  (after-init (global-company-mode)))
;;
(init-handler company config
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; Because of `company-tng', <C-g> does not play well as it cancels
  ;; completion.
  ;; -------------------------------------------------------------------------
  ;; DEPRECATED:
  ;;
  ;; Because of `company-tng':
  ;;
  (init-ignore-progn
    (init-defun init-company-auto-complete-p ()
      #$
      (and (company-tooltip-visible-p)
           (company-explicit-action-p)))
    (setq-default company-auto-complete #'init-company-auto-complete-p))
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; As per [1]:
  ;;
  (after-load fill-column-indicator
    (init-defun init-company-turn-off-fci-maybe
        (&rest ...)
      #$
      (when (and (fboundp 'fci-mode)
                 (setq-local init-company-fci-mode
                   (bound-and-true-p fci-mode)))
        (fci-mode -1)))
    (dolist (hook '(company-completion-started-hook))
      (add-hook hook #'init-company-turn-off-fci-maybe))
    ;;
    (init-defun init-company-turn-on-fci-maybe
        (&rest ...)
      #$
      (when (and (fboundp 'fci-mode)
                 (bound-and-true-p  init-company-fci-mode)
                 (local-variable-p 'init-company-fci-mode))
        (kill-local-variable 'init-company-fci-mode)
        (fci-mode +1)))
    (dolist (hook '(company-completion-finished-hook
                    company-completion-cancelled-hook))
      (add-hook hook #'init-company-turn-on-fci-maybe)))
  ;; -------------------------------------------------------------------------
  )
;;
(init-handler company mode-map
  (let ((keymap company-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      [remap indent-for-tab-command] #'company-indent-or-complete-common)))
;;
(init-handler company active-map
  (let ((keymap company-active-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      [remap scroll-up-command]   #'company-next-page
      [remap scroll-down-command] #'company-previous-page
      ;;
      "<down-mouse-1>" #'ignore
      "<down-mouse-3>" #'ignore
      ;;
      "<mouse-1>" #'company-complete-mouse
      "<mouse-3>" #'company-select-mouse
      ;;
      "<up-mouse-1>" #'ignore
      "<up-mouse-3>" #'ignore
      ;;
      (char-to-string help-char) #'company-show-doc-buffer
      "<help>"                   #'company-show-doc-buffer
      "<f1>"                     #'company-show-doc-buffer
      ;; TODO:
      ;; [remap help-command] #'company-show-doc-buffer
      ;;
      "C-g" #'company-abort
      ;;
      "C-l" #'company-show-location
      ;;
      "C-f" #'company-filter-candidates
      "C-s" #'company-search-candidates
      ;;
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ;; Because of `company-tng':
      ;;
      ;; "TAB" #'company-select-next-if-tooltip-visible-or-complete-selection
      ;; ---------------------------------------------------------------------
      "TAB"       #'company-select-next
      "<backtab>" #'company-select-previous
      ;;
      ;; ---------------------------------------------------------------------
      ;; DEPRECATED:
      ;;
      ;; Because of `company-tng':
      ;;
      ;; "LFD" #'company-indent-or-complete-common
      ;; "RET" #'company-complete-selection
      ;; ---------------------------------------------------------------------
      )))
;;
(init-handler company search-map
  (let ((keymap company-search-map))
    (init-assert (null (keymap-parent keymap)))
    (init-make-keymap keymap :recursively-p)
    (let ((i 0))
      (if (fboundp 'max-char)
          (set-char-table-range (nth 1 keymap)
                                (cons #x100 (max-char))
                                #'company-search-printing-char)
        (with-no-warnings
          ;; Obsolete in Emacs 23:
          (let ((list  (generic-character-list))
                (table (nth 1 keymap)))
            (while list
              (set-char-table-default table
                                      (car list)
                                      #'company-search-printing-char)
              (setq list (cdr list))))))
      (define-key keymap [t]
        #'company-search-other-char)
      (while (< i ?\s)
        (define-key keymap (kbd (make-string 1 i))
          #'company-search-other-char)
        (cl-incf i))
      (while (< i 256)
        (define-key keymap (vector i)
          #'company-search-printing-char)
        (cl-incf i))
      (dotimes (i 10)
        (define-key keymap (kbd (format "<kp-%s>" i))
          #'company-search-keypad))
      (dotimes (i 10)
        (define-key keymap (kbd (format "C-%d" i))
          #'company-complete-number)))
    (init-define-keys-kbd keymap
      "   <delete>" #'company-search-delete-char
      "<kp-delete>" #'company-search-delete-char
      "        DEL" nil
      "        DEL" #'company-search-delete-char
      ;; ;;
      "C-g" nil
      "C-g" #'company-search-abort
      "C-s" nil
      "C-s" #'company-search-repeat-forward
      "C-r" nil
      "C-r" #'company-search-repeat-backward
      "C-f" nil
      "C-f" #'company-search-toggle-filtering
      ;;
      "TAB"       #'company-search-repeat-forward
      "<backtab>" #'company-search-repeat-backward)))
;;
(init-package company
  :variables
  (init-company-fci-mode)
  :diminish
  (company-mode
   global-company-mode))
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/company-mode/company-mode/issues/180'
;;  ==========================================================================
;;  }}} References
