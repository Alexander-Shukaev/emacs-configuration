(eval-when-compile
  (require 'init-package))
;;
(init-handler mneme prepare
  (init-load recentf :no-error))
;;
(init-handler mneme init)
;;
(init-handler mneme config)
;;
(init-package mneme
  :load-path "lisp/mneme")
