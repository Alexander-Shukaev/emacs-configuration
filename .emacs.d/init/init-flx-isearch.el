(eval-when-compile
  (require 'init-package))
;;
(init-handler flx-isearch init)
;;
(init-handler flx-isearch config)
;;
(init-package flx-isearch
  :disabled)
