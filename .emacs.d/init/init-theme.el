(require 'custom)
;;
(defvar init-theme
  nil
  "Theme currently being used and last loaded (see `load-theme').")
;;
(define-advice disable-theme
    (:after (theme &rest ...) init-theme/:after -100)
  (when (custom-theme-p theme)
    (put theme 'theme-settings      nil)
    (put theme 'theme-feature       nil)
    (put theme 'theme-documentation nil))
  (when (eq theme (default-value 'init-theme))
    (setq-default init-theme nil)))
;;
(define-advice load-theme
    (:before (theme &rest ...) init-theme/:before -100)
  (mapcar #'disable-theme custom-enabled-themes))
;;
(define-advice enable-theme
    (:after (theme &rest ...) init-theme/:after -100)
  (setq-default init-theme theme)
  (unless noninteractive
    (message "Theme: `%s'" theme)))
;;
(provide 'init-theme)
