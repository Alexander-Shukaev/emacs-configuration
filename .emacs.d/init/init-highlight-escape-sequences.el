(eval-when-compile
  (require 'init-package))
;;
(init-handler highlight-escape-sequences init
  (after-init (hes-mode)))
;;
(init-handler highlight-escape-sequences config)
;;
(init-package highlight-escape-sequences)
