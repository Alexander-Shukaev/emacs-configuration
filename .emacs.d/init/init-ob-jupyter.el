(eval-when-compile
  (require 'init-package))
;;
(init-handler ob-jupyter prepare
  (init-load jupyter-org-client     :no-error)
  (init-load jupyter-org-extensions :no-error)
  ;;
  (init-load ob :no-error))
;;
(init-handler ob-jupyter init
  (setq-default
    org-babel-default-header-args:jupyter-python
    '((:async   . "yes")
      (:results . "both")
      (:session . "jupyter-python")
      (:kernel  . "python3")
      (:pandoc  . t)
      (:exports . "both")
      (:cache   . "no")
      (:noweb   . "no")
      (:hlines  . "no")
      (:tangle  . "no")
      (:eval    . "never-export"))
    ;;
    org-babel-default-header-args:ipython
    org-babel-default-header-args:jupyter-python)
  ;;
  (after-load org
    (dolist (language '((jupyter . t)))
      (add-to-list 'org-babel-load-languages language :append)))
  ;;
  (after-load org-src
    (dolist (mode '(("ipython"        . python)
                    ("jupyter-python" . python)))
      (add-to-list 'org-src-lang-modes mode))))
;;
(init-handler ob-jupyter config
  (defvaralias
    'org-babel-default-header-args:ipython
    'org-babel-default-header-args:jupyter-python)
  (defalias 'org-babel-execute:ipython #'org-babel-execute:jupyter-python)
  ;;
  (org-babel-jupyter-override-src-block "python"))
;;
(init-package ob-jupyter
  :unless
  (< emacs-major-version 26)
  :ensure jupyter)
