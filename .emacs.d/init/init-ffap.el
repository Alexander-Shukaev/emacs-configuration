(eval-when-compile
  (require 'init-package))
;;
(init-handler ffap init)
;;
(init-handler ffap config
  (init-assert (fboundp 'ffap-list-directory))
  (fmakunbound 'ffap-list-directory)
  ;;
  (init-assert (eq (symbol-function 'ffap) #'find-file-at-point))
  (init-defad find-file-at-point
      (:around (function &rest ...) init/:around)
    #$
    (let ((init-ffap--find-file-at-point-p t))
      (apply function ...))))
;;
(init-package ffap)
