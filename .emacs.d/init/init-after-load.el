(eval-when-compile
  (require 'init-package))
;;
(init-handler after-load init)
;;
(init-handler after-load config)
;;
(init-package after-load
  :load-path "lisp/after-load")
