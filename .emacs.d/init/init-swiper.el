(eval-when-compile
  (require 'init-package))
;;
(init-handler swiper init
  (setq-default swiper-goto-start-of-match t))
;;
(init-handler swiper config
  (after-load "isearch"
    (init-assert (boundp 'swiper-history))
    (set         'swiper-history  regexp-search-ring)
    (defvaralias 'swiper-history 'regexp-search-ring)))
;;
(init-package swiper)
