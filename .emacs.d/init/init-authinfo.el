(eval-when-compile
  (require 'init-package))
;;
(init-handler authinfo init)
;;
(init-handler authinfo config)
;;
(init-package authinfo
  :load-path "lisp/authinfo")
