(eval-when-compile
  (require 'init-package))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Implement `interactive' toggling of `select-enable-clipboard'.
;; ---------------------------------------------------------------------------
;;
(init-handler select init
  (setq-default
    ;; TODO: Move from here:
    x-select-enable-clipboard-manager nil
    ;;
    ;; As per [1]:
    select-enable-clipboard t
    select-enable-primary   nil))
;;
(init-handler select config)
;;
(init-package select
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(emacs) Clipboard'
;;  ==========================================================================
;;  }}} References
