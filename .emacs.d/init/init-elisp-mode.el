(eval-when-compile
  (require 'init-package))
;;
(init-handler elisp-mode preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler elisp-mode init)
;;
(init-handler elisp-mode config
  (require 'edebug nil :no-error)
  ;;
  (after-load edebug
    (init-call elisp-mode mode-map))
  ;;
  (after-load (init-devil devil init-elint)
    (init-defun init-emacs-lisp-elint-devil-setup ()
      #$
      (devil-ex-define-cmds-local
        "buffer-elint"     #'elint-current-buffer
        "directory-elint"  #'elint-directory
        "file-elint"       #'elint-file
        ;;
        "bel[int]"         "buffer-elint"
        "del[int]"         "directory-elint"
        "fel[int]"         "file-elint"
        ;;
        "elf[unction]"   #'elint-defun
        "eli[nitialize]" #'elint-initialize))
    (add-hook 'emacs-lisp-mode-hook #'init-emacs-lisp-elint-devil-setup))
  ;;
  (after-load bug-reference
    (init-defun init-emacs-lisp-bug-reference-setup ()
      #$
      (setq-local bug-reference-bug-regexp (eval-when-compile
                                             (concat "\\("
                                                     "\\b\\(?:[Bb]ug ?#?\\)"
                                                     "\\("
                                                     "[0-9]+\\(?:#[0-9]+\\)?"
                                                     "\\)"
                                                     "\\)")))
      (setq-local bug-reference-url-format "http://bugs.gnu.org/%s"))
    (add-hook 'emacs-lisp-mode-hook #'init-emacs-lisp-bug-reference-setup))
  ;;
  (after-load speck
    (init-defun init-emacs-lisp-speck-setup ()
      #$
      ;; ---------------------------------------------------------------------
      ;; NOTE:
      ;;
      ;; As per `init-prog-mode-speck-setup':
      ;;
      (init-assert (and (local-variable-p 'speck-syntactic)
                        (eq speck-syntactic t)))
      ;; ---------------------------------------------------------------------
      (setq-local speck-face-inhibit-list '(font-lock-constant-face)))
    (add-hook 'emacs-lisp-mode-hook #'init-emacs-lisp-speck-setup))
  ;;
  (after-load (init-font-lock font-lock init-lisp-mode lisp-mode)
    (font-lock-add-keywords 'emacs-lisp-mode
                            init-lisp-mode-font-lock-keywords)
    ;; TODO: Why is it not inherited?  Should I now duplicate this everywhere?
    ;; WTF?
    (font-lock-add-keywords 'lisp-interaction-mode
                            init-lisp-mode-font-lock-keywords)))
;;
(init-handler elisp-mode mode-map
  (let ((keymap emacs-lisp-mode-map))
    (init-assert (eq (keymap-parent keymap) lisp-mode-shared-map))
    (init-clear-keymap                keymap)
    (init-assert (eq (keymap-parent keymap) lisp-mode-shared-map))
    (init-define-keys-with-prefix-kbd keymap "C-e"
      "C-b" #'eval-buffer
      "b"   #'eval-buffer
      ;;
      "C-r" #'eval-region
      "r"   #'eval-region
      ;;
      "C-f" #'eval-defun
      "f"   #'eval-defun
      ;;
      "C-p" #'eval-print-last-sexp
      "p"   #'eval-print-last-sexp
      ;;
      "C-s" #'eval-last-sexp
      "s"   #'eval-last-sexp)))
;;
(init-handler elisp-mode lisp-interaction-mode-map
  (let ((keymap lisp-interaction-mode-map))
    (init-assert (eq (keymap-parent keymap) lisp-mode-shared-map))
    (init-clear-keymap                keymap)
    (init-assert (eq (keymap-parent keymap) lisp-mode-shared-map))
    (init-define-keys-with-prefix-kbd keymap "C-e"
      "C-b" #'eval-buffer
      "b"   #'eval-buffer
      ;;
      "C-r" #'eval-region
      "r"   #'eval-region
      ;;
      "C-f" #'eval-defun
      "f"   #'eval-defun
      ;;
      "C-p" #'eval-print-last-sexp
      "p"   #'eval-print-last-sexp
      ;;
      "C-s" #'eval-last-sexp
      "s"   #'eval-last-sexp)))
;;
(init-package elisp-mode
  :built-in
  :when
  (> emacs-major-version 24))
