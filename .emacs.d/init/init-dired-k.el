(eval-when-compile
  (require 'init-package))
;;
(init-handler dired-k init
  (setq-default
    dired-k-human-readable t
    dired-k-style          'git))
;;
(init-handler dired-k config)
;;
(init-package dired-k
  ;; -------------------------------------------------------------------------
  :disabled
  ;;
  ;; TODO:
  ;;
  ;; Until outstanding issues are fixed:
  ;; - '.git/index.lock';
  ;; - Random TRAMP hangs;
  ;; - Proper status parsing (unintentionally triggers TRAMP).
  ;; -------------------------------------------------------------------------
  )
