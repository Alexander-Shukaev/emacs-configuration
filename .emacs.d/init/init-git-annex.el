(eval-when-compile
  (require 'init-package))
;;
(init-handler git-annex init
  (setq-default git-annex-commit nil)
  ;;
  (after-init (require 'git-annex)))
;;
(init-handler git-annex config
  (face-spec-reset-face 'git-annex-dired-annexed-available)
  (face-spec-set        'git-annex-dired-annexed-available
                        '((((class color)
                            (background light))
                           :foreground "light green")
                          (((class color)
                            (background dark))
                           :foreground "light green"))))
;;
(init-handler git-annex dired-map
  (let ((keymap git-annex-dired-map))
    (init-assert (null (keymap-parent keymap))))
  ;;
  (after-load (init-magit-annex magit-annex)
    (init-define-keys-kbd git-annex-dired-map
      "f" #'magit-annex-file-action)))
;;
(init-package git-annex)
