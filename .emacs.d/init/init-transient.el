(eval-when-compile
  (require 'init-package))
;;
(init-handler transient init)
;;
(init-handler transient config
  (transient-bind-q-to-quit))
;;
(init-package transient)
