(eval-when-compile
  (require 'init-package))
;;
(init-handler debug preface
  (eval-when-compile
    (require 'cl-lib)))
;;
(init-handler debug init)
;;
(init-handler debug config
  (after-load evil
    (evil-set-initial-state 'debugger-mode 'motion)))
;;
(init-handler debug mode-map
  ;; TODO:
  (let ((keymap debugger-mode-map))
    (cl-macrolet
        ((define-keymap-parent-p ()
           (if (< emacs-major-version 27)
               '(equal (keymap-parent keymap) button-buffer-map)
             '(eq (keymap-parent keymap) backtrace-mode-map))))
      (init-assert (define-keymap-parent-p)))))
;;
(init-package debug
  :built-in)
