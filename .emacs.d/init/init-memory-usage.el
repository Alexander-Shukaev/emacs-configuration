(eval-when-compile
  (require 'init-package))
;;
(init-handler memory-usage init)
;;
(init-handler memory-usage config)
;;
(init-package memory-usage)
