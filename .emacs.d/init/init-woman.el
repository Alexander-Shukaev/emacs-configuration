(eval-when-compile
  (require 'init-package))
;;
(init-handler woman prepare
  (init-load man :no-error))
;;
(init-handler woman init
  (setq-default woman-dired-keys nil))
;;
(init-handler woman config
  (init-assert (fboundp 'woman-dired-define-keys))
  (init-defad woman-dired-define-keys
      (:override (&rest ...) init/:override)
    #$)
  (remove-hook 'dired-mode-hook #'woman-dired-define-keys))
;;
(init-package woman
  :built-in)
