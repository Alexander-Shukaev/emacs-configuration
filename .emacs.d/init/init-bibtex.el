(eval-when-compile
  (require 'init-package))
;;
(init-handler bibtex init
  (setq-default
    bibtex-align-at-equal-sign            t
    bibtex-comma-after-last-field         t
    bibtex-field-delimiters               'braces
    bibtex-entry-delimiters               'braces
    bibtex-autokey-name-length            'all
    bibtex-autokey-names                  1
    bibtex-autokey-name-case-convert      #'downcase
    bibtex-autokey-name-separator         ""
    bibtex-autokey-name-year-separator    ":"
    bibtex-autokey-additional-names       ""
    bibtex-autokey-year-length            4
    bibtex-autokey-titlewords             0
    bibtex-autokey-titleword-first-ignore '("a"
                                            "an"
                                            "and"
                                            "if"
                                            "the")
    bibtex-entry-format                   '(opts-or-alts
                                            required-fields
                                            numerical-fields
                                            whitespace
                                            realign
                                            last-comma
                                            sort-fields)))
;;
(init-handler bibtex config
  (init-load indent)
  ;;
  (setq-default
    bibtex-autokey-prefix-string (format (format "\n%%%ds"
                                                 bibtex-field-indentation)
                                         "")
    bibtex-contline-indentation  (+ bibtex-text-indentation
                                    init-indent-width))
  ;;
  (init-defun init-bibtex-mode-setup ()
    (setq-local comment-start "%"))
  (add-hook 'bibtex-mode-hook #'init-bibtex-mode-setup))
;;
(init-package bibtex
  :built-in)
;;
;; TODO:
;; bibtex-entry
;; bibtex-clean-entry
;; bibtex-fill-entry
;; bibtex-reformat
;; bibtex-remove-delimiters
