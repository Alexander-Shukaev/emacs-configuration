(eval-when-compile
  (require 'init-package))
;;
(init-handler org-preview-html prepare
  (init-load xwidget :no-error))
;;
(init-handler org-preview-html init
  (setq-default org-preview-html-viewer (if (featurep 'xwidget-internal)
                                            'xwidget
                                          'eww)))
;;
(init-handler org-preview-html config)
;;
(init-package org-preview-html
  :unless
  (version< emacs-version "25.1"))
