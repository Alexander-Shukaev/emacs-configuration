(eval-when-compile
  (require 'init-package))
;;
(eval-and-compile
  (init-load devil-load-path))
;;
(init-handler devil-vars init
  (setq-default
    devil-async-shell-command-buffer   'new-buffer
    devil-backtrace-to-string-function #'init-backtrace-to-string
    devil-toggle-key                   "C-z"))
;;
(init-handler devil-vars config)
;;
(init-package devil-vars
  :load-path init-devil-load-path
  :ensure evil)
