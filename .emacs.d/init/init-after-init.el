(eval-when-compile
  (require 'init-package))
;;
(init-handler after-init init)
;;
(init-handler after-init config)
;;
(init-package after-init
  :load-path "lisp/after-init")
