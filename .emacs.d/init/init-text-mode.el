(eval-when-compile
  (require 'init-package))
;;
(init-handler "text-mode" init)
;;
(init-handler "text-mode" config
  (after-load init-auto-fill
    (add-hook 'text-mode-hook #'auto-fill-mode                     :append))
  ;;
  (after-load init-display-fill-column-indicator
    (add-hook 'text-mode-hook #'display-fill-column-indicator-mode :append))
  ;;
  (after-load init-fill-column-indicator
    (add-hook 'text-mode-hook #'fci-mode                           :append))
  ;;
  (after-load init-bug-reference
    (add-hook 'text-mode-hook #'bug-reference-mode                 :append))
  ;;
  (after-load init-goto-addr
    (add-hook 'text-mode-hook #'goto-address-mode                  :append))
  ;;
  (after-load init-highlight-indentation
    (add-hook 'text-mode-hook #'highlight-indentation-mode         :append))
  ;;
  (after-load init-highlight-numbers
    (add-hook 'text-mode-hook #'highlight-numbers-mode             :append))
  ;;
  (after-load init-newcomment
    (init-defun init-text-mode-auto-fill-setup ()
      #$
      (setq-local comment-auto-fill-only-comments nil))
    (add-hook 'text-mode-hook #'init-text-mode-auto-fill-setup     :append))
  ;;
  (after-load init-rainbow-delimiters
    (add-hook 'text-mode-hook #'rainbow-delimiters-mode            :append))
  ;;
  (after-load init-rainbow-mode
    (add-hook 'text-mode-hook #'rainbow-mode                       :append))
  ;;
  (after-load init-speck
    (add-hook 'text-mode-hook #'speck-mode                         :append)
    ;;
    (after-load speck
      (init-defun init-text-mode-speck-setup ()
        #$
        (setq-local speck-syntactic nil)))
    (add-hook 'text-mode-hook #'init-text-mode-speck-setup         :append))
  ;;
  (after-load init-whitespace
    (add-hook 'text-mode-hook #'whitespace-mode                    :append)))
;;
(init-package "text-mode"
  :built-in
  :commands
  (text-mode))
