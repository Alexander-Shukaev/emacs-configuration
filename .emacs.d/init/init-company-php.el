(eval-when-compile
  (require 'init-package))
;;
(init-handler company-php prepare
  (init-load company     :no-error)
  (init-load ac-php-core :no-error))
;;
(init-handler company-php init)
;;
(init-handler company-php config)
;;
(init-package company-php
  :unless
  (bound-and-true-p init-bb))
