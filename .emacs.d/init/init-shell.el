(eval-when-compile
  (require 'init-package))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Report bug with coloring.  The line with cursor is colored in white always.
;; Only if prompt contains newline, then lines before the line with cursor are
;; colored with `PS1', but the one with cursor still remains white.
;; ---------------------------------------------------------------------------
;;
(init-handler shell init
  ;; TODO: Quick fix against `init-comint':
  (setq-default shell-mode-map (make-sparse-keymap)))
;;
(init-handler shell config
  (when (eq system-type 'windows-nt)
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; Under MSYS2 (Windows) running a login shell ('-l') results in the
    ;; '/etc/post-install/05-home-dir.post' shell script being sourced (by
    ;; '/etc/profile'), which forcibly changes the current working directory
    ;; to '~/' (`HOME') if environment variable `CHERE_INVOKING' is empty
    ;; ('-z').  That's truly annoying, so avoid it:
    ;;
    (setenv "CHERE_INVOKING" "1")
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; On Windows, by default, Emacs will most probably guess the wrong value
    ;; for environment variable `LANG' (for example, `ENU').  Thus, to prevent
    ;; potential problems, set it explicitly:
    ;;
    (setenv "LANG"           "en_US.UTF-8")
    ;; -----------------------------------------------------------------------
    )
  ;;
  (after-load init-bash-completion
    (add-hook 'shell-dynamic-complete-functions
              #'bash-completion-dynamic-complete))
  ;;
  (after-load init-company
    (add-hook 'shell-mode-hook
              #'init-company-idle-delay-setup)))
;;
(init-handler shell mode-map
  (let ((keymap shell-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (set-keymap-parent    keymap comint-mode-map)
    (init-define-keys-kbd keymap
      ;; TODO: `company' instead?
      "TAB" #'completion-at-point)))
;;
(init-package shell
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://www.masteringemacs.org/article/shell-comint-secrets-history-commands'
;;  ==========================================================================
;;  }}} References
