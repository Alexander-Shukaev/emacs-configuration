(eval-when-compile
  (require 'init-package))
;;
(init-handler magit-popup init
  (setq-default magit-popup-display-buffer-action nil))
;;
(init-handler magit-popup config
  (after-load devil
    (devil-set-initial-state 'magit-popup-mode 'emacs))
  ;;
  (after-load evil
    (evil-set-initial-state 'magit-popup-mode 'emacs)))
;;
(init-handler magit-popup mode-map
  (let ((keymap magit-popup-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (set-keymap-parent    keymap button-buffer-map)
    (init-define-keys-kbd keymap
      [remap self-insert-command] #'magit-invoke-popup-action
      ;;
      [?- t]                      #'magit-invoke-popup-switch
      [?= t]                      #'magit-invoke-popup-option
      ;;
      [remap info]                #'magit-popup-info
      "?"                         #'magit-popup-help
      "q"                         #'magit-popup-quit))
  ;;
  (after-load evil
    (evil-make-overriding-map magit-popup-mode-map 'motion))
  ;;
  (setq-default magit-popup-common-commands '(("Info" magit-popup-info)
                                              ("Help" magit-popup-help)
                                              ("Quit" magit-popup-quit))))
;;
(init-package magit-popup)
