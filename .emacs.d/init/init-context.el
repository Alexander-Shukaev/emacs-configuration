(eval-when-compile
  (require 'init-package))
;;
(init-handler context init)
;;
(init-handler context config
  (after-load (init-font-lock font-lock init-tex-mode tex-mode)
    ;; -----------------------------------------------------------------------
    ;; CAUTION:
    ;;
    ;; Somehow it must be strictly `context-mode' here, while `ConTeXt-mode'
    ;; does not apply:
    ;;
    (font-lock-add-keywords 'context-mode
                            init-tex-mode-font-lock-keywords)
    ;; -----------------------------------------------------------------------
    ))
;;
(init-package context
  :ensure auctex
  :mode
  (("\\.context\\'" . ConTeXt-mode)
   ("\\.ConTeXt\\'" . ConTeXt-mode)))
