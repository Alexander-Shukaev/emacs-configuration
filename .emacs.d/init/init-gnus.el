(eval-when-compile
  (require 'init-package))
;;
(init-handler gnus init
  (setq-default
    gnus-home-directory
    (abbreviate-file-name (init-user-expand-directory "gnus/"))
    gnus-default-directory
    (abbreviate-file-name (init-user-expand-directory "gnus/"))))
;;
(init-handler gnus config
  (after-load ffap
    (add-hook 'gnus-article-mode-hook #'ffap-gnus-hook)
    (add-hook 'gnus-summary-mode-hook #'ffap-gnus-hook)))
;;
(init-package gnus
  :built-in)
;;
(init-handler gnus-start init
  (setq-default
    gnus-init-file
    (abbreviate-file-name (expand-file-name "init" gnus-home-directory))))
;;
(init-handler gnus-start config)
;;
(init-package gnus-start
  :built-in)
