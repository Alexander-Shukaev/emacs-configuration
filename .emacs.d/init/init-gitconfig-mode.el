(eval-when-compile
  (require 'init-package))
;;
(init-handler gitconfig-mode init)
;;
(init-handler gitconfig-mode config
  (init-load indent)
  ;;
  (init-assert (fboundp 'gitconfig-indentation-string))
  (init-defad gitconfig-indentation-string
      (:override (&rest ...) init/:override)
    #$
    (eval-when-compile ""))
  ;;
  (init-defun gitconfig-mode-setup ()
    #$
    (setq-local comment-start    "#")
    (setq-local indent-tabs-mode nil)
    (setq-local tab-width        init-indent-width))
  (add-hook 'gitconfig-mode-hook #'gitconfig-mode-setup))
;;
(init-package gitconfig-mode
  :ensure git-modes
  :mode
  (("\\.gitconfig\\'" . gitconfig-mode)))
