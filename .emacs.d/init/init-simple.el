(eval-when-compile
  (require 'init-package))
;;
(init-handler simple init
  (setq-default
    async-shell-command-buffer          'new-buffer
    async-shell-command-display-buffer  t
    ;;
    kill-do-not-save-duplicates         t
    ;;
    line-move-visual                    nil
    ;;
    ;; As per [1]:
    save-interprogram-paste-before-kill t
    ;;
    shell-command-dont-erase-buffer     'beg-last-out))
;;
(init-handler simple config
  (init-defad shell-command
      (:around (function &rest ...) init/:around)
    #$
    (let ((window (apply function ...)))
      (when (window-live-p window)
        (select-window window))
      window))
  ;;
  (init-defad async-shell-command
      (:around (function &rest ...) init/:around)
    #$
    (let ((inhibit-read-only (bound-and-true-p comint-prompt-read-only)))
      (apply function ...)))
  ;;
  (init-defad visual-line-mode
      (:before (&rest ...) init/:before)
    #$
    (user-error "Disabled mode `%s'" 'visual-line-mode))
  ;;
  (after-load init-display-fill-column-indicator
    (init-defun init-fundamental-mode-display-fill-column-indicator-setup ()
      #$
      (when (eq major-mode 'fundamental-mode)
        (display-fill-column-indicator-mode)))
    (add-hook 'after-change-major-mode-hook
              #'init-fundamental-mode-display-fill-column-indicator-setup))
  ;;
  (after-load init-fill-column-indicator
    (init-defun init-fundamental-mode-fill-column-indicator-setup ()
      #$
      (when (eq major-mode 'fundamental-mode)
        (fci-mode)))
    (add-hook 'after-change-major-mode-hook
              #'init-fundamental-mode-fill-column-indicator-setup))
  ;;
  (after-load init-font-lock
    (defconst init-fundamental-mode-font-lock-keywords
      (when (bound-and-true-p init-bb)
        `((,(concat "\\(?:(\\(==>\\|<==\\))\\)")
           (1 font-lock-warning-face              prepend t))
          (,(concat "\\([0-9]+\\)"
                    "\\(=\\)"
                    "\\(?:[^|]*\\)"
                    "\\(\\||\\)")
           (1 font-lock-variable-name-face        prepend t)
           (2 font-lock-operator-face             prepend t)
           (3 font-lock-regexp-grouping-construct prepend t)))))
    (after-load font-lock
      (font-lock-add-keywords 'fundamental-mode
                              init-fundamental-mode-font-lock-keywords)))
  ;;
  (after-init (column-number-mode))
  (after-init (transient-mark-mode)))
;;
(init-package simple
  :built-in)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(emacs) Clipboard'
;;  ==========================================================================
;;  }}} References
