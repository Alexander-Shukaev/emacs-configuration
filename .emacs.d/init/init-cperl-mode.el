(eval-when-compile
  (require 'init-package))
;;
(init-handler cperl-mode init
  (init-load indent)
  ;;
  (setq-default
    cperl-close-paren-offset         (- init-indent-width)
    cperl-continued-statement-offset 0
    cperl-indent-level               (+ init-indent-width)
    cperl-indent-parens-as-block     t
    cperl-label-offset               (- init-indent-width)
    cperl-tab-always-indent          t))
;;
(init-handler cperl-mode config)
;;
(init-package cperl-mode
  :built-in)
