(eval-when-compile
  (require 'init-package))
;;
(init-handler evil-search init
  (setq-default evil-search-module 'isearch))
;;
(init-handler evil-search config)
;;
(init-package evil-search
  :ensure evil)
;; TODO: Use `load-history' to assert that it is never actually loaded!
