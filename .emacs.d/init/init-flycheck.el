(eval-when-compile
  (require 'init-package))
;;
(init-handler flycheck init
  (setq-default
    flycheck-global-modes              '(not emacs-lisp-mode)
    flycheck-standard-error-navigation nil)
  ;;
  (after-init (global-flycheck-mode)))
;;
(init-handler flycheck config)
;;
(init-handler flycheck mode-map
  (init-clear-keymap flycheck-mode-map :recursively-p)
  ;; TODO:
  ;; (init-define-keys-kbd flycheck-mode-map)
  )
;;
(init-package flycheck
  :diminish
  (global-flycheck-mode
   flycheck-mode))
