(eval-when-compile
  (require 'init-package))
;;
(init-handler company-ycmd prepare
  (init-load company :no-error)
  (init-load ycmd    :no-error))
;;
(init-handler company-ycmd init)
;;
(init-handler company-ycmd config)
;;
(init-package company-ycmd
  :commands
  (company-ycmd))
