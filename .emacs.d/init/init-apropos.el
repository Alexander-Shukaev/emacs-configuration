(eval-when-compile
  (require 'init-package))
;;
(init-handler apropos init
  (setq-default
    apropos-do-all                       t
    apropos-documentation-sort-by-scores t
    apropos-sort-by-scores               t))
;;
(init-handler apropos config
  (after-load init-helpful
    (let ((callable #'(lambda
                          (button)
                        (helpful-callable (button-get button
                                                      'apropos-symbol))))
          (variable #'(lambda
                          (button)
                        (helpful-variable (button-get button
                                                      'apropos-symbol)))))
      (define-button-type 'apropos-function
        :supertype        'apropos-function
        'action callable)
      (define-button-type 'apropos-macro
        :supertype        'apropos-macro
        'action callable)
      (define-button-type 'apropos-command
        :supertype        'apropos-command
        'action callable)
      (define-button-type 'apropos-variable
        :supertype        'apropos-variable
        'action variable)
      (define-button-type 'apropos-user-option
        :supertype        'apropos-user-option
        'action variable))))
;;
(init-package apropos
  :built-in)
