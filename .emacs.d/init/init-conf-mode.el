(eval-when-compile
  (require 'init-package))
;;
(init-handler conf-mode init)
;;
(init-handler conf-mode config
  (defcustom conf-mode-hook nil
    "Normal hook run when entering Conf mode and many derived modes."
    :group 'conf
    :type 'hook)
  ;;
  (after-load init-auto-fill
    (add-hook 'conf-mode-hook #'auto-fill-mode                     :append))
  ;;
  (after-load init-display-fill-column-indicator
    (add-hook 'conf-mode-hook #'display-fill-column-indicator-mode :append))
  ;;
  (after-load init-fill-column-indicator
    (add-hook 'conf-mode-hook #'fci-mode                           :append))
  ;;
  (after-load init-bug-reference
    (add-hook 'conf-mode-hook #'bug-reference-prog-mode            :append))
  ;;
  (after-load init-goto-addr
    (add-hook 'conf-mode-hook #'goto-address-prog-mode             :append))
  ;;
  (after-load init-highlight-indentation
    (add-hook 'conf-mode-hook #'highlight-indentation-mode         :append))
  ;;
  (after-load init-highlight-numbers
    (add-hook 'conf-mode-hook #'highlight-numbers-mode             :append))
  ;;
  (after-load init-newcomment
    (init-defun init-conf-mode-auto-fill-setup ()
      #$
      (setq-local comment-auto-fill-only-comments t))
    (add-hook 'conf-mode-hook #'init-conf-mode-auto-fill-setup     :append))
  ;;
  (after-load init-rainbow-delimiters
    (add-hook 'conf-mode-hook #'rainbow-delimiters-mode            :append))
  ;;
  (after-load init-rainbow-mode
    (add-hook 'conf-mode-hook #'rainbow-mode                       :append))
  ;;
  (after-load init-speck
    (add-hook 'conf-mode-hook #'speck-mode                         :append)
    ;;
    (after-load speck
      (init-defun init-conf-mode-speck-setup ()
        #$
        (setq-local speck-syntactic t)))
    (add-hook 'conf-mode-hook #'init-conf-mode-speck-setup         :append))
  ;;
  (after-load init-whitespace
    (add-hook 'conf-mode-hook #'whitespace-mode                    :append)))
;;
(init-handler conf mode-map
  (init-clear-keymap conf-mode-map))
;;
(init-package conf-mode
  :built-in
  :variables
  (conf-mode-hook)
  :mode
  (("\\.\\(\\(auto\\)?mount\\|service\\|socket\\|target\\|timer\\)\\'"
    . conf-unix-mode)
   ("/etc/\\(.+/\\)?\\(.*\\.\\)?\\(crypttab\\(\\.initramfs\\)?\\|fstab\\)\\'"
    . conf-space-mode)
   ("\\.prf\\'"   . conf-mode)
   ("/\\.unison/" . conf-mode)))
