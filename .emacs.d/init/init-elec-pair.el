(eval-when-compile
  (require 'init-package))
;;
(init-handler elec-pair init
  (after-init (electric-pair-mode)))
;;
(init-handler elec-pair config
  (init-defun init-elec-pair-delete/:before
      (&rest ...)
    #$
    (let* ((char-before (char-before))
           (char-after  (char-after))
           (syntax-info (and char-before
                             (electric-pair-syntax-info char-before)))
           (syntax      (car  syntax-info))
           (pair        (cadr syntax-info)))
      (when (and char-after pair
                 (eq pair char-after)
                 (memq syntax '(?\( ?\" ?\$))
                 (if (functionp electric-pair-delete-adjacent-pairs)
                     (funcall   electric-pair-delete-adjacent-pairs)
                   electric-pair-delete-adjacent-pairs))
        (delete-char 1))))
  (dolist (function '(backward-delete-char
                      backward-delete-char-untabify))
    (advice-add function :before #'init-elec-pair-delete/:before)))
;;
(init-handler elec-pair mode-map
  (init-clear-keymap electric-pair-mode-map))
;;
(init-package elec-pair
  :built-in)
