(defcustom init-font-families
  '("Powerline Consolas"
    "Consolas for Powerline"
    "Consolas"
    ;;
    "Powerline Inconsolata-g"
    "Inconsolata-g for Powerline"
    "Inconsolata-g"
    ;;
    "Powerline Source Code Pro"
    "Source Code Pro for Powerline"
    "Source Code Pro"
    ;;
    "Powerline DejaVu Sans Mono"
    "DejaVu Sans Mono for Powerline"
    "DejaVu Sans Mono"
    ;;
    "Monospace")
  "List of font families."
  :group 'init
  :type '(repeat string))
(put 'init-font-families 'safe-local-variable #'listp)
;;
(defcustom init-font-size
  12
  "Size of font."
  :group 'init
  :type 'integer)
(put 'init-font-size     'safe-local-variable #'integerp)
;;
;; ---------------------------------------------------------------------------
;; BUG:
;;
;; As per bug#43177 and bug#40733:
;;
(add-to-list 'face-ignored-fonts "Adobe.*Blank")
;; ---------------------------------------------------------------------------
;;
(provide 'init-font)
