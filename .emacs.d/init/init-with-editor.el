(eval-when-compile
  (require 'init-package))
;;
(init-handler with-editor preface
  (eval-when-compile
    (init-load devil-common :no-error)
    (init-load devil-ex     :no-error)
    ;;
    (require 'devil-common nil :no-error)
    (require 'devil-ex     nil :no-error)))
;;
(init-handler with-editor init)
;;
(init-handler with-editor config
  (after-load (init-devil devil)
    (init-defun init-with-editor-devil-setup ()
      #$
      (if (bound-and-true-p with-editor-mode)
          (devil-ex-define-cmds-local
            "finish"    #'with-editor-finish
            ;;
            "w[rite]"   "finish"
            ;;
            "cancel"    #'with-editor-cancel
            ;;
            "a[bort]"   "cancel"
            "bd[elete]" "cancel"
            "bk[ill]"   "cancel")
        ;; TODO: Clean up `evil-ex-commands'.
        ))
    (add-hook 'with-editor-mode-hook #'init-with-editor-devil-setup)))
;;
(init-handler with-editor mode-map
  (let ((keymap with-editor-mode-map))
    (init-assert (null (keymap-parent keymap)))
    (init-clear-keymap    keymap :recursively-p)
    (init-define-keys-kbd keymap
      [remap server-edit]          #'with-editor-finish
      [remap ido-kill-buffer]      #'with-editor-cancel
      [remap iswitchb-kill-buffer] #'with-editor-cancel
      [remap kill-buffer]          #'with-editor-cancel)))
;;
(init-package with-editor
  :ensure magit
  :diminish
  (with-editor-mode))
