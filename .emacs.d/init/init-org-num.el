(eval-when-compile
  (require 'init-package))
;;
(init-handler org-num init)
;;
(init-handler org-num config)
;;
(init-package org-num
  :built-in
  :diminish
  (org-num-mode))
