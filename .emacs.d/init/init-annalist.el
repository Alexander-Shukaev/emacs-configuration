(eval-when-compile
  (require 'init-package))
;;
(init-handler annalist init
  (setq-default annalist-record nil))
;;
(init-handler annalist config)
;;
(init-package annalist)
