(eval-when-compile
  (require 'init-package))
;;
(init-handler jit-lock init
  (setq-default
    jit-lock-stealth-time 8
    jit-lock-stealth-nice 2
    jit-lock-context-time 2
    jit-lock-defer-time   0))
;;
(init-handler jit-lock config)
;;
(init-package jit-lock
  :built-in)
