(eval-when-compile
  (require 'init-package))
;;
(init-handler whitespace preface
  (eval-when-compile
    (init-load solarized-theme :no-error))
  ;;
  (init-load color))
;;
(init-handler whitespace init
  (setq-default
    whitespace-line-column nil
    whitespace-style       '(face
                             empty
                             indentation
                             space-after-tab
                             space-before-tab
                             spaces
                             tabs
                             trailing
                             lines-tail))
  ;; TODO: Wrong place.  Move it out of here.
  (after-load make-mode
    (init-defun whitespace-makefile-mode-setup ()
      #$
      (setq-local whitespace-style (delq 'space-after-tab
                                         (remq 'indentation
                                               whitespace-style))))
    (add-hook 'makefile-mode-hook #'whitespace-makefile-mode-setup))
  ;; -------------------------------------------------------------------------
  (init-ignore-progn
    (after-init (global-whitespace-mode)))
  ;;
  ;; CAUTION:
  ;;
  ;; This is a bad idea!
  ;; -------------------------------------------------------------------------
  )
;;
(init-handler whitespace config
  (init-defad whitespace-color-on
      (:after (&rest ...) init/:after)
    #$
    (when (whitespace-style-face-p)
      (dolist (keyword whitespace-font-lock-keywords)
        (when (> (length keyword) 3)
          (setf (nth 3 keyword) 'prepend)))
      (font-lock-add-keywords nil whitespace-font-lock-keywords t)
      (font-lock-flush)))
  ;;
  (after-load (init-solarized-theme solarized-theme)
    (init-defun init-whitespace-solarized-theme-setup
        (&optional theme)
      #$
      (interactive)
      (setq theme (or theme init-theme))
      (init-solarized-with-color-variables theme
        (set-face-attribute 'whitespace-line nil
                            :foreground red)))
    (init-whitespace-solarized-theme-setup)
    ;;
    (init-defad enable-theme
        (:after (theme &rest ...)
                init-whitespace-solarized-theme-setup/:after)
      #$
      (init-whitespace-solarized-theme-setup theme))))
;;
(init-package whitespace
  :built-in
  :diminish
  (global-whitespace-mode
   whitespace-mode))
