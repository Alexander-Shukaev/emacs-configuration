(eval-when-compile
  (require 'init-package))
;;
(init-handler rainbow-mode init)
;;
(init-handler rainbow-mode config)
;;
(init-package rainbow-mode
  :diminish
  (rainbow-mode))
