;; (package-initialize)
;; ---------------------------------------------------------------------------
;; NOTE:
;;
;; Whenever composing comments and/or documentation strings, remember to
;; follow best practices summarized in [1].
;; ---------------------------------------------------------------------------
(setq-default
  debug-on-error                 t
  inhibit-compacting-font-caches t
  inhibit-default-init           t
  inhibit-startup-buffer-menu    nil
  inhibit-startup-screen         t)
;; ---------------------------------------------------------------------------
;; CAUTION:
;;
;; The following (additional) call to the `setq' macro is required because
;; `debug-on-error' is let-bound by `startup.el':
;;
(setq
  debug-on-error                 t
  inhibit-compacting-font-caches t
  inhibit-default-init           t
  inhibit-startup-buffer-menu    nil
  inhibit-startup-screen         t)
;; ---------------------------------------------------------------------------
(declare-function debugger-continue "debug")
(eval-after-load 'debug
  '(dolist (key '("q" "<remap top-level>"))
     (define-key (bound-and-true-p debugger-mode-map)
       (kbd key) #'debugger-continue)))
;; ---------------------------------------------------------------------------
(defconst default-force-load-messages
  force-load-messages
  "Default value of variable `force-load-messages'.")
;; ---------------------------------------------------------------------------
(setq-default
  force-load-messages t
  message-log-max     t
  use-dialog-box      nil)
;; ---------------------------------------------------------------------------
(defalias 'yes-or-no-p #'y-or-n-p)
;; ---------------------------------------------------------------------------
(let* ((version-list (split-string (emacs-version)
                                   "[()]"
                                   :omit-nulls
                                   split-string-default-separators))
       (name         (nth 0 version-list))
       (build-info   (nth 1 version-list))
       (build-date   (nth 2 version-list)))
  (when build-date
    (setq build-date (car-safe (last (split-string build-date)))))
  (message "%s" name)
  (message "%s" (make-string (length name) ?=))
  (message "Build:")
  (message "  Date:      %s" (or build-date "N/A"))
  (message "  Info:      %s" (or build-info "N/A"))
  (message "  Version:   %s" emacs-version)
  (message "Daemon:      %s" (if (daemonp)
                                 "Yes" "No"))
  (message "Graphic:     %s" (if (if (< emacs-major-version 23)
                                     (or window-system
                                         (getenv "DISPLAY")
                                         (getenv "WAYLAND_DISPLAY"))
                                   (display-graphic-p))
                                 "Yes" "No"))
  (message "Interactive: %s" (if (not noninteractive)
                                 "Yes" "No"))
  (message "OS:          %s" (or (bound-and-true-p operating-system-release)
                                 "N/A"))
  (message "PID:         %s" (emacs-pid))
  (dolist (attribute (process-attributes (emacs-pid)))
    (message "  %-10s %s"
             (format-message "`%s':" (car attribute)) (cdr attribute)))
  (message "TTY:         %s" (or (tty-type) "N/A"))
  (message "Native:")
  (message "  Comp:      %s" (if (and (fboundp 'native-comp-available-p)
                                      (native-comp-available-p))
                                 "Yes" "No"))
  (message "  JSON:      %s" (if (fboundp 'json-serialize)
                                 "Yes" "No"))
  (message "  Modules:   %s" (if (bound-and-true-p  module-file-suffix)
                                 (format "Yes (%s)" module-file-suffix) "No"))
  (message "  Widgets:   %s" (if (featurep 'xwidget-internal)
                                 "Yes" "No"))
  (message "Window:")
  (message "  PGTK:      %s" (if (bound-and-true-p pgtk-initialized)
                                 "Yes" "No")))
;; ---------------------------------------------------------------------------
(unless noninteractive
  (message "Initializing...")
  (when force-load-messages
    (dolist (file (reverse (mapcar #'car load-history)))
      (message "Loading %s...done" file))))
;; ---------------------------------------------------------------------------
;;;###autoload
(eval-and-compile
  (defconst this-init-file
    (abbreviate-file-name (or #$
                              (and load-in-progress load-file-name)
                              (bound-and-true-p byte-compile-current-file)
                              (buffer-file-name)))
    "Path to this initialization file.")
  ;;
  (defconst this-init-directory
    (abbreviate-file-name (file-name-directory this-init-file))
    "Path to this initialization directory."))
;; ---------------------------------------------------------------------------
;;;###autoload
(eval-when-compile
  (when (equal (bound-and-true-p byte-compile-current-file)
               (expand-file-name this-init-file))
    ;; As per [2]:
    (when byte-compile-verbose
      (message "Compiling %s...denied"
               (bound-and-true-p byte-compile-current-file)))
    (let ((symbol 'no-byte-compile)
          (current no-byte-compile)
          (correct t))
      (if (eq current correct)
          (error "Compiling initialization file `%s' (%s) not allowed"
                 'this-init-file this-init-file)
        (error "Expected the correct value of variable `%s':
- current: %s
- correct: %s"
               symbol
               current
               correct)))))
;; ---------------------------------------------------------------------------
(when noninteractive
  (error "Loading initialization file `%s' (%s) in batch mode not allowed"
         'this-init-file this-init-file))
;; ---------------------------------------------------------------------------
(defconst user-before-init-time
  (current-time)
  "Value of `current-time' before user initialization.")
(let ((symbol  'user-init-file)
      (current (expand-file-name user-init-file))
      (correct (expand-file-name this-init-file)))
  (when (and (not noninteractive) force-load-messages
             (not default-force-load-messages))
    (message "Loading %s..." correct))
  (unless (equal current correct)
    (when (and (not noninteractive) force-load-messages)
      (message "Loading %s...failed" correct))
    (error "Expected the correct value of variable `%s':
- current: %s
- correct: %s"
           symbol
           current
           correct)))
;; ---------------------------------------------------------------------------
(defconst emacs-minimum-version
  "25.1"
  "Version numbers of minimum (required) version of Emacs.")
(when (version< emacs-version emacs-minimum-version)
  (when (and (not noninteractive) force-load-messages)
    (message "Loading %s...failed" (expand-file-name this-init-file)))
  (error "Emacs %s or higher is required.  You are running version %s"
         emacs-minimum-version
         emacs-version))
;; ---------------------------------------------------------------------------
(defconst default-gc-cons-percentage
  gc-cons-percentage
  "Default value of variable `gc-cons-percentage'.")
;;
(defconst default-gc-cons-threshold
  gc-cons-threshold
  "Default value of variable `gc-cons-threshold'.")
;; ---------------------------------------------------------------------------
(defsubst gc-max-cons-threshold
    (&optional value)
  "\
Return maximum value of variable `gc-cons-threshold'."
  (max (or value gc-cons-threshold)
       (max (or (bound-and-true-p init-gc-high-cons-threshold) 0)
            (* 100 (or (bound-and-true-p default-gc-cons-threshold) 0))
            ;; Hexadecimal: 10000000
            (eval-when-compile (* 64 1024 1024)))))
;;
(defsubst gc-min-cons-threshold
    (&optional value)
  "\
Return minimum value of variable `gc-cons-threshold'."
  (min (or value gc-cons-threshold)
       (max (or (bound-and-true-p init-gc-low-cons-threshold) 0)
            (* 10 (or (bound-and-true-p default-gc-cons-threshold) 0))
            ;; Hexadecimal: 1000000
            (eval-when-compile (* 16 1024 1024)))))
;;
(defsubst gc-set-max-cons-threshold
    (&optional value)
  "\
Set `gc-cons-threshold' to the return value of `gc-max-cons-threshold'."
  (setq gc-cons-threshold (gc-max-cons-threshold value)))
;;
(defsubst gc-set-min-cons-threshold
    (&optional value)
  "\
Set `gc-cons-threshold' to the return value of `gc-min-cons-threshold'."
  (setq gc-cons-threshold (gc-min-cons-threshold value)))
;; ---------------------------------------------------------------------------
(setq-default
  gc-cons-percentage 0.1
  gc-cons-threshold  (gc-max-cons-threshold default-gc-cons-threshold))
;; ---------------------------------------------------------------------------
(defconst default-file-name-handler-alist
  file-name-handler-alist
  "Default value of variable `file-name-handler-alist'.")
;; ---------------------------------------------------------------------------
(defsubst expand-directory
    (name &optional default-directory)
  "\
Convert filename NAME to absolute, and canonicalize it.

Return a string representing filename NAME interpreted as a
directory.

See `expand-file-name' and `file-name-as-directory'."
  (file-name-as-directory (expand-file-name name default-directory)))
;;
(defsubst directory-truename
    (filename)
  "\
Return the truename of FILENAME.

Return a string representing filename FILENAME interpreted as a
directory.

See `file-truename' and `file-name-as-directory'."
  (file-name-as-directory (file-truename filename)))
;; ---------------------------------------------------------------------------
(defsubst abbrev-file-name
    (name &optional default-directory)
  "\
Convert filename NAME to absolute, canonicalize, and abbreviate it.

See `expand-file-name' and `abbreviate-file-name'."
  (abbreviate-file-name (expand-file-name name default-directory)))
;;
(defsubst abbrev-directory
    (name &optional default-directory)
  "\
Convert filename NAME to absolute, canonicalize, and abbreviate it.

Return a string representing filename NAME interpreted as a
directory.

See `abbrev-file-name' and `file-name-as-directory'."
  (file-name-as-directory (abbrev-file-name name default-directory)))
;; ---------------------------------------------------------------------------
(defsubst user-expand-file-name
    (name &optional default-directory)
  "\
Convert filename NAME to absolute, and canonicalize it.

DEFAULT-DIRECTORY is directory to start with if NAME is
relative (does not start with slash or tilde); both the directory
name and a directory's file name are accepted.
If DEFAULT-DIRECTORY is nil or missing, the value of
`user-emacs-directory' is used.  NAME should be a string that is
a valid file name for the underlying filesystem.

See `expand-file-name'."
  (expand-file-name name (unless (file-name-absolute-p name)
                           (or default-directory user-emacs-directory))))
;;
(defsubst user-expand-directory
    (name &optional default-directory)
  "\
Convert filename NAME to absolute, and canonicalize it.

DEFAULT-DIRECTORY is directory to start with if NAME is
relative (does not start with slash or tilde); both the directory
name and a directory's file name are accepted.
If DEFAULT-DIRECTORY is nil or missing, the value of
`user-emacs-directory' is used.  NAME should be a string that is
a valid file name for the underlying filesystem.

Return a string representing filename NAME interpreted as a
directory.

See `user-expand-file-name' and `file-name-as-directory'."
  (file-name-as-directory (user-expand-file-name name default-directory)))
;;
(defsubst user-file-truename
    (filename)
  "\
Return the truename of FILENAME.

If FILENAME is not absolute, first expands it against
‘user-emacs-directory’.

See `user-expand-file-name' and `file-truename'."
  (file-truename (if (file-name-absolute-p filename)
                     filename
                   (user-expand-file-name filename))))
;;
(defsubst user-directory-truename
    (filename)
  "\
Return the truename of FILENAME.

If FILENAME is not absolute, first expands it against
‘user-emacs-directory’.

Return a string representing filename FILENAME interpreted as a
directory.

See `user-file-truename' and `file-name-as-directory'."
  (file-name-as-directory (user-file-truename filename)))
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Redefine own function?
;;
(require 'server)
(defalias 'make-safe-directory #'server-ensure-safe-dir)
;; ---------------------------------------------------------------------------
(setq-default
  file-name-handler-alist nil)
;; ---------------------------------------------------------------------------
(let* ((symbol  'default-directory)
       (current (default-value symbol))
       (default "~/"))
  (when current
    (if (equal (expand-directory current)
               (expand-directory default))
        (set-default symbol default)
      (lwarn 'init
             :emergency
             "Changed the default value of variable `%s':
- current: %s
- default: %s"
             symbol
             current
             default))))
;;
(setq-default
  bidi-display-reordering        nil
  byte-compile-debug             t
  byte-compile-verbose           t
  ;; TODO: `locate-file' and everywhere where applies:
  custom-file                    "~/.custom.el"
  delete-by-moving-to-trash      t
  echo-keystrokes                0.1; seconds
  garbage-collection-messages    t
  initial-buffer-choice          #'messages-buffer
  load-dangerous-libraries       nil
  load-prefer-newer              t
  mail-host-address              "Alexander.Shukaev.name"
  max-specpdl-size               (eval-when-compile (* 16 1024))
  truncate-lines                 t
  truncate-partial-width-windows nil
  word-wrap                      nil)
;;
(when (boundp 'w32-get-true-file-attributes)
  ;; As per `W32_OBJ' in `configure.ac' and corresponding `w32.c',
  ;; `w32proc.c', `w32term.c' source files of Emacs:
  (unless (and (eq system-type 'windows-nt)
               (featurep 'w32))
    (lwarn 'init
           :warning
           "Unexpected binding of variable `w32-get-true-file-attributes': %s"
           w32-get-true-file-attributes))
  (setq-default w32-get-true-file-attributes nil))
;;
(when (boundp 'x-wait-for-event-timeout)
  ;; As per `XOBJ' and `W32_OBJ' in `configure.ac' and corresponding
  ;; `frame.c', `xfns.c', `xterm.c', `w32fns.c', `w32term.c' source files of
  ;; Emacs (see `make-frame-visible'):
  (unless (or (featurep 'x)
              (and (memq system-type '(cygwin windows-nt))
                   (featurep 'w32)))
    (lwarn 'init
           :warning
           "Unexpected binding of variable `x-wait-for-event-timeout': %s"
           x-wait-for-event-timeout))
  (setq-default x-wait-for-event-timeout nil))
;; ---------------------------------------------------------------------------
(defconst default-global-map
  (copy-keymap global-map)
  "Default value of variable `global-map'.")
;; ---------------------------------------------------------------------------
(defconst default-pager
  (getenv "PAGER")
  "Default value of environment variable `PAGER'.")
;; ---------------------------------------------------------------------------
(setenv "PAGER" (and (executable-find "cat") "cat"))
;;
(when (eq system-type 'windows-nt)
  ;; -------------------------------------------------------------------------
  ;; CAUTION:
  ;;
  ;; Under MSYS2 (Windows) running a login shell ('-l') results in the
  ;; '/etc/post-install/05-home-dir.post' shell script being sourced (by
  ;; '/etc/profile'), which forcibly changes the current working directory to
  ;; '~/' (`HOME') if environment variable `CHERE_INVOKING' is empty ('-z').
  ;; That's truly annoying, so avoid it:
  ;;
  (setenv "CHERE_INVOKING" "1")
  ;; -------------------------------------------------------------------------
  ;; CAUTION:
  ;;
  ;; On Windows, by default, Emacs will most probably guess the wrong value
  ;; for environment variable `LANG' (for example, `ENU').  Thus, to prevent
  ;; potential problems, set it explicitly:
  ;;
  (setenv "LANG"           "en_US.UTF-8")
  ;; -------------------------------------------------------------------------
  )
;; ---------------------------------------------------------------------------
;; [RESOLVED] bug#583:
;;
;; TODO: Can we do any better with respect to `xdg'?
;; TODO: Review all the files in `user-emacs-directory' and relocate them
;; around according to `xdg'.
;;
(defconst cache-file-directory
  (let ((paths (if (eq system-type 'windows-nt)
                   `(,(getenv "COMMONAPPDATA")
                     ,(getenv "PROGRAMDATA")
                     ,(unless (zerop (length (getenv "ALLUSERSPROFILE")))
                        (substitute-in-file-name
                         "${ALLUSERSPROFILE}\\Application Data")))
                 '("/var/cache"))))
    (catch 'break
      (dolist (path paths)
        (when (and (not (zerop (length path)))
                   (file-accessible-directory-p path))
          (throw 'break
                 (abbreviate-file-name (file-name-as-directory path)))))))
  "Directory for writing cache files.")
;;
(defconst user-cache-directory
  (let ((paths (if (eq system-type 'windows-nt)
                   `(,(getenv "LOCALAPPDATA")
                     ,(unless (zerop (length (getenv "USERPROFILE")))
                        (substitute-in-file-name
                         "${USERPROFILE}\\Local Settings\\Application Data"))
                     ,(unless (zerop (length (getenv "USERPROFILE")))
                        (substitute-in-file-name
                         "${USERPROFILE}\\.cache"))
                     ,(unless (zerop (length (getenv "HOME")))
                        (substitute-in-file-name
                         "${HOME}\\.cache"))
                     ,(unless (or (zerop (length (getenv "HOMEDRIVE")))
                                  (zerop (length (getenv "HOMEPATH"))))
                        (substitute-in-file-name
                         "${HOMEDRIVE}${HOMEPATH}\\.cache"))
                     ,(expand-file-name "~\\.cache"))
                 `(,(getenv "XDG_CACHE_HOME")
                   ,(unless (zerop (length (getenv "HOME")))
                      (substitute-in-file-name "${HOME}/.cache"))
                   ,(expand-file-name "~/.cache")))))
    (catch 'break
      (dolist (path paths)
        (when (and (not (zerop (length path)))
                   (file-accessible-directory-p path))
          (let ((directory (expand-directory "emacs/" path)))
            (condition-case e
                (progn
                  (make-safe-directory directory)
                  (throw 'break (abbreviate-file-name directory)))
              (error (lwarn 'init
                            :warning
                            "Failed to define variable `%s': %s"
                            'user-cache-directory
                            (error-message-string e))
                     nil)))))))
  "Path to user cache directory.")
;;
(defconst user-config-directory
  (let ((paths (if (eq system-type 'windows-nt)
                   `(,(getenv "APPDATA")
                     ,(unless (zerop (length (getenv "USERPROFILE")))
                        (substitute-in-file-name
                         "${USERPROFILE}\\Application Data"))
                     ,(unless (zerop (length (getenv "USERPROFILE")))
                        (substitute-in-file-name
                         "${USERPROFILE}\\.config"))
                     ,(unless (zerop (length (getenv "HOME")))
                        (substitute-in-file-name
                         "${HOME}\\.config"))
                     ,(unless (or (zerop (length (getenv "HOMEDRIVE")))
                                  (zerop (length (getenv "HOMEPATH"))))
                        (substitute-in-file-name
                         "${HOMEDRIVE}${HOMEPATH}\\.config"))
                     ,(expand-file-name "~\\.config"))
                 `(,(getenv "XDG_CONFIG_HOME")
                   ,(unless (zerop (length (getenv "HOME")))
                      (substitute-in-file-name "${HOME}/.config"))
                   ,(expand-file-name "~/.config")))))
    (catch 'break
      (dolist (path paths)
        (when (and (not (zerop (length path)))
                   (file-accessible-directory-p path))
          (let ((directory (expand-directory "emacs/" path)))
            (condition-case e
                (progn
                  (make-safe-directory directory)
                  (throw 'break (abbreviate-file-name directory)))
              (error (lwarn 'init
                            :warning
                            "Failed to define variable `%s': %s"
                            'user-config-directory
                            (error-message-string e))
                     nil)))))))
  "Path to user configuration directory.")
;;
(defconst user-data-directory
  (let ((paths (if (eq system-type 'windows-nt)
                   `(,(getenv "APPDATA")
                     ,(unless (zerop (length (getenv "USERPROFILE")))
                        (substitute-in-file-name
                         "${USERPROFILE}\\Application Data"))
                     ,(unless (zerop (length (getenv "USERPROFILE")))
                        (substitute-in-file-name
                         "${USERPROFILE}\\.local\\share"))
                     ,(unless (zerop (length (getenv "HOME")))
                        (substitute-in-file-name
                         "${HOME}\\.local\\share"))
                     ,(unless (or (zerop (length (getenv "HOMEDRIVE")))
                                  (zerop (length (getenv "HOMEPATH"))))
                        (substitute-in-file-name
                         "${HOMEDRIVE}${HOMEPATH}\\.local\\share"))
                     ,(expand-file-name "~\\.local\\share"))
                 `(,(getenv "XDG_DATA_HOME")
                   ,(unless (zerop (length (getenv "HOME")))
                      (substitute-in-file-name "${HOME}/.local/share"))
                   ,(expand-file-name "~/.local/share")))))
    (catch 'break
      (dolist (path paths)
        (when (and (not (zerop (length path)))
                   (file-accessible-directory-p path))
          (let ((directory (expand-directory "emacs/" path)))
            (condition-case e
                (progn
                  (make-safe-directory directory)
                  (throw 'break (abbreviate-file-name directory)))
              (error (lwarn 'init
                            :warning
                            "Failed to define variable `%s': %s"
                            'user-data-directory
                            (error-message-string e))
                     nil)))))))
  "Path to user data directory.")
;;
(dolist (symbol '(cache-file-directory
                  user-cache-directory
                  user-config-directory
                  user-data-directory))
  (when (boundp symbol)
    (let ((value (symbol-value symbol)))
      (unless (and (not (zerop (length value)))
                   (file-accessible-directory-p value))
        (lwarn 'init
               :emergency
               "Set the non-accessible directory value of variable `%s': %s"
               symbol
               value)))))
;;
(defconst user-temporary-directory
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; Review `server-socket-dir' again, now uses environment variable
  ;; `XDG_RUNTIME_DIR' if set.
  ;; -------------------------------------------------------------------------
  ;; NOTE:
  ;;
  ;; The following path is also intentionally consistent with the default
  ;; `server-socket-dir':
  ;;
  (let ((directory (expand-directory (format "emacs%d/" (user-uid))
                                     temporary-file-directory)))
    (condition-case e
        (progn
          (make-safe-directory directory)
          (abbreviate-file-name directory))
      (error (lwarn 'init
                    :warning
                    "Failed to define variable `%s': %s"
                    'user-temporary-directory
                    (error-message-string e))
             nil)))
  ;; -------------------------------------------------------------------------
  "Path to user temporary directory.")
;;
(defconst user-backup-directory
  (let ((directory (expand-directory "backup/" user-data-directory)))
    (condition-case e
        (progn
          (make-safe-directory directory)
          (abbreviate-file-name directory))
      (error (lwarn 'init
                    :warning
                    "Failed to define variable `%s': %s"
                    'user-backup-directory
                    (error-message-string e))
             nil)))
  "Path to user backup directory.")
;;
(defconst user-init-directory
  (let ((directory (expand-directory "init/" user-emacs-directory))
        (operation nil))
    (unless (file-accessible-directory-p directory)
      (if (file-directory-p directory)
          (setq operation "access")
        (with-file-modes ?\700
          (condition-case nil
              (make-directory directory :parents)
            (error (setq operation "create"))))))
    (if operation
        (progn
          (lwarn 'init
                 :warning
                 "Failed to define variable `%s': Unable to %s directory (%s)"
                 operation
                 'user-init-directory
                 directory)
          nil)
      (abbreviate-file-name directory)))
  "Path to user initialization directory.")
;;
(defconst user-lisp-directory
  (let ((directory (expand-directory "lisp/" user-emacs-directory))
        (operation nil))
    (unless (file-accessible-directory-p directory)
      (if (file-directory-p directory)
          (setq operation "access")
        (with-file-modes ?\700
          (condition-case nil
              (make-directory directory :parents)
            (error (setq operation "create"))))))
    (if operation
        (progn
          (lwarn 'init
                 :warning
                 "Failed to define variable `%s': Unable to %s directory (%s)"
                 operation
                 'user-lisp-directory
                 directory)
          nil)
      (abbreviate-file-name directory)))
  "Path to user Lisp directory.")
;;
(dolist (symbol '(user-temporary-directory
                  user-backup-directory
                  user-init-directory
                  user-lisp-directory))
  (when (boundp symbol)
    (let ((value (symbol-value symbol)))
      (unless (and (not (zerop (length value)))
                   (file-accessible-directory-p value))
        (lwarn 'init
               :emergency
               "Set the non-accessible directory value of variable `%s': %s"
               symbol
               value)))))
;; ---------------------------------------------------------------------------
(defconst byte-compile-load-suffixes
  (cons ".elc" (remove ".elc" load-suffixes))
  "Byte compile time variant of `load-suffixes'.")
;; ---------------------------------------------------------------------------
(defcustom bc-idle-byte-compile-functions
  nil
  "\
List of functions to be called to run byte compilation after `bc-idle-delay'.

Only used by `bc-idle-byte-compile'."
  :type 'hook)
;;
(defcustom bc-idle-delay
  180; seconds
  "Idle time to wait in seconds before running `bc-idle-byte-compile'."
  :group 'init
  :type '(choice (const  :tag "Never" nil)
                 (number :tag "Seconds")))
;;
(defcustom gc-idle-delay
  90; seconds
  "Idle time to wait in seconds before running `gc-idle-garbage-collect'."
  :group 'init
  :type '(choice (const  :tag "Never" nil)
                 (number :tag "Seconds")))
;;
(defcustom locked-buffer-names
  '("scratch.el")
  "List of names of buffers that should be locked.

Used together with `locked-buffer-regexps' by `locked-buffer-p'."
  :group 'init
  :type '(repeat (string :format "%v")))
;;
(defcustom locked-buffer-regexps
  (eval-when-compile
    (mapcar #'(lambda
                  (name)
                (format "^%s\\(?:<[0-9]+>\\)?$" (regexp-quote name)))
            `(,(or (bound-and-true-p byte-compile-log-buffer) "*Compile-Log*")
              "*Messages*"
              "*Warnings*"
              "*compilation*"
              "*scratch*")))
  "List of regular expressions of buffers that should be locked.

Used together with `locked-buffer-names' by `locked-buffer-p'."
  :group 'init
  :type '(repeat (string :format "%v")))
;;
(defcustom user-byte-compile
  t
  "If non-nil, run `user-byte-compile' by `user-byte-compile-maybe'."
  :group 'init
  :type 'boolean)
;; ---------------------------------------------------------------------------
(defun locked-buffer-p
    (&optional buffer)
  "\
Return non-nil if buffer BUFFER should be locked.

See `locked-buffer-names' and `locked-buffer-regexps'."
  (setq buffer (or buffer (current-buffer)))
  (let ((name (if (bufferp buffer) (buffer-name buffer) buffer)))
    (when (stringp name)
      (or (member name locked-buffer-names)
          (catch 'break
            (dolist (regexp locked-buffer-regexps)
              (when (and (stringp regexp)
                         (string-match-p regexp name))
                (throw 'break t))))))))
;; ---------------------------------------------------------------------------
(define-advice autoload-find-generated-file
    (:around (function &rest ...) init/:around)
  (let ((find-file-suppress-same-file-warnings t)
        (find-file-visit-truename              nil))
    (apply function ...)))
;;
(define-advice byte-compile-file
    (:around (function &rest ...) init/:around)
  ;; Optimize the byte compilation process with:
  (let (;; 1.  Preventing calls of file name-specific handlers:
        (file-name-handler-alist nil)
        ;; 2.  Preventing garbage collection:
        (gc-cons-threshold       (gc-max-cons-threshold))
        ;; 3.  Pretending that nothing is being loaded:
        (load-file-name          nil)
        (load-in-progress        nil)
        ;; 4.  Localizing modification of `load-path':
        (load-path               (copy-sequence load-path))
        ;; 5.  Preferring loading of files with `.elc' suffixes:
        (load-suffixes           (or (bound-and-true-p
                                      byte-compile-load-suffixes)
                                     load-suffixes))
        ;; 6.  Preventing messages when saving file(s):
        (save-silently           (bound-and-true-p
                                  inhibit-message)))
    (apply function ...)))
;;
(define-advice byte-recompile-directory
    (:around (function directory &rest ...) init/:around)
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; The following code in `byte-recompile-directory',
  ;;
  ;;   (with-current-buffer (get-buffer-create byte-compile-log-buffer)
  ;;     (setq default-directory (expand-file-name directory))
  ;;     ...)
  ;;
  ;; ... is a genuine bug because it will not work if `directory' is a
  ;; relative path.  That is it will keep on appending `directory' to
  ;; `default-directory' on each subsequent run of `byte-recompile-directory'.
  ;; Fix it by expanding `directory' against the current buffer's
  ;; `default-directory':
  ;;
  (setq directory (expand-directory directory))
  ;; -------------------------------------------------------------------------
  (apply function directory ...))
;;
(define-advice locate-file
    (:around (function &rest ...) init/:around)
  (save-match-data
    (apply function ...)))
;; ---------------------------------------------------------------------------
(if (let (force-load-messages)
      (load custom-file :no-error :no-message :no-suffix :must-suffix))
    (when (and (not noninteractive) force-load-messages)
      (message "Loading %s...done" (expand-file-name custom-file)))
  (when (and (not noninteractive) force-load-messages)
    (message "Loading %s...failed" (expand-file-name custom-file))))
;; ---------------------------------------------------------------------------
(defconst custom-gc-cons-percentage
  gc-cons-percentage
  "Custom value of variable `gc-cons-percentage'.")
;;
(defconst custom-gc-cons-threshold
  gc-cons-threshold
  "Custom value of variable `gc-cons-threshold'.")
;; ---------------------------------------------------------------------------
(setq-default
  gc-cons-percentage 0.1
  gc-cons-threshold  (gc-max-cons-threshold default-gc-cons-threshold))
;; ---------------------------------------------------------------------------
(defvar init-minibuffer-gc-cons-threshold-mode (not gc-idle-delay))
;; ---------------------------------------------------------------------------
(dolist (directory (list user-init-directory
                         user-lisp-directory))
  (unless (zerop (length directory))
    (setq directory (user-expand-directory directory))
    (add-to-list 'load-path (directory-file-name directory))))
(unless (zerop (length user-lisp-directory))
  ;; As per [3]:
  (let ((default-directory user-lisp-directory))
    (setq-default load-path
      (append (let ((load-path (copy-sequence load-path)))
                (normal-top-level-add-subdirs-to-load-path))
              load-path))))
;; ---------------------------------------------------------------------------
;;;###autoload
(eval-when-compile
  (when (equal (bound-and-true-p byte-compile-current-file)
               (expand-file-name this-init-file))
    (let ((directory (user-expand-directory "init/"))
          (load-path (copy-sequence load-path))
          (byte-compile-current-file))
      (add-to-list 'load-path (directory-file-name directory))
      (require 'init-macros))))
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Move this to `after-init-hook' (and optionally but wishfully use [4]) in
;; order to see compilation warnings from `package' in real time rather than
;; delayed:
;;
;; Optimize the initialization process with:
(let (;; 1.  Preventing calls of file name-specific handlers:
      (file-name-handler-alist nil)
      ;; 2.  Preventing garbage collection:
      (gc-cons-threshold       (gc-max-cons-threshold)))
  (require 'init)
  (when file-name-handler-alist
    (lwarn 'init
           :warning
           "Set the non-nil value of variable `file-name-handler-alist': %s"
           file-name-handler-alist)))
(dolist (symbol '(comp-deferred-compilation
                  debug-on-error
                  inhibit-compacting-font-caches
                  inhibit-default-init
                  inhibit-startup-screen
                  force-load-messages
                  native-comp-async-report-warnings-errors
                  user-byte-compile))
  (when (boundp symbol)
    (let ((value (symbol-value symbol)))
      (unless (eq value t)
        (lwarn 'init
               :warning
               "Set the non-t value of variable `%s': %s"
               symbol
               value)))))
(dolist (symbol '(inhibit-startup-buffer-menu))
  (when (boundp symbol)
    (let ((value (symbol-value symbol)))
      (unless (eq value nil)
        (lwarn 'init
               :warning
               "Set the non-nil value of variable `%s': %s"
               symbol
               value)))))
;; ---------------------------------------------------------------------------
(init-negate (boundp 'gc-high-cons-threshold))
(init-negate (boundp 'gc-low-cons-threshold))
;;
(eval-after-load 'init-gc
  '(defvaralias 'gc-high-cons-threshold 'init-gc-high-cons-threshold))
;;
(eval-after-load 'init-gc
  '(defvaralias 'gc-low-cons-threshold 'init-gc-low-cons-threshold))
;; ---------------------------------------------------------------------------
(init-assert (fboundp 'gc-max-cons-threshold))
(init-assert (fboundp 'gc-min-cons-threshold))
;;
(dolist (value `(nil
                 ,gc-cons-threshold
                 ,default-gc-cons-threshold
                 ,custom-gc-cons-threshold))
  (init-assert (>= (gc-max-cons-threshold value)
                   (gc-min-cons-threshold value))))
;;
(declare-function init-gc-max-cons-threshold "init-gc")
(declare-function init-gc-min-cons-threshold "init-gc")
;;
(eval-after-load 'init-gc
  '(dolist (value `(nil
                    ,gc-cons-threshold
                    ,default-gc-cons-threshold
                    ,custom-gc-cons-threshold))
     (init-assert (>= (init-gc-max-cons-threshold value)
                      (init-gc-min-cons-threshold value)))))
;;
(eval-after-load 'init-gc
  '(defalias 'gc-max-cons-threshold #'init-gc-max-cons-threshold))
;;
(eval-after-load 'init-gc
  '(defalias 'gc-min-cons-threshold #'init-gc-min-cons-threshold))
;;
(eval-after-load 'init-gc
  '(dolist (value `(nil
                    ,gc-cons-threshold
                    ,default-gc-cons-threshold
                    ,custom-gc-cons-threshold))
     (init-assert (>= (gc-max-cons-threshold value)
                      (gc-min-cons-threshold value)))))
;; ---------------------------------------------------------------------------
(eval-after-load 'init-package
  '(progn
     (init-assert (boundp 'init-package-byte-compile-commands))
     (add-to-list 'init-package-byte-compile-commands
                  #'byte-recompile-directory)))
;;
(init-negate (fboundp 'cancel-debug-on-error))
(defun cancel-debug-on-error ()
  (interactive)
  (let* ((symbol 'debug-on-error)
         (value  (default-value symbol)))
    (if value
        (toggle-debug-on-error)
      (lwarn 'init
             :warning
             "Expected the non-nil value of variable `%s'"
             symbol))))
;;
(init-negate (fboundp 'enable-file-name-handler))
(defun enable-file-name-handler ()
  (interactive)
  (let* ((symbol 'file-name-handler-alist)
         (value  (default-value symbol)))
    (unless (eq value default-file-name-handler-alist)
      (set-default symbol
                   (delete-dups (nconc value
                                       default-file-name-handler-alist))))
    (message "File Name Handler %s" "enabled globally")))
;;
(init-negate (fboundp 'user-kill-compile-log-buffer-maybe))
(defun user-kill-compile-log-buffer-maybe ()
  (interactive)
  (let ((buffer (get-buffer (or (bound-and-true-p byte-compile-log-buffer)
                                "*Compile-Log*"))))
    (when (and (buffer-live-p buffer) (= (buffer-size buffer) 0))
      (delete-windows-on buffer)
      (let (kill-buffer-query-functions)
        (kill-buffer buffer)))))
;;
(init-negate (fboundp 'user-byte-compile))
(defun user-byte-compile ()
  "\
This function could be on `bc-idle-byte-compile-functions' or bound to a key."
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; In order to make byte compilation display all the warnings in pursuit to
  ;; achieve a truly warning-free configuration, there must be absolute
  ;; isolation of byte compilation process per file.  That is no variables
  ;; and/or functions and/or files (apart from the core ones bundled with the
  ;; Emacs) should be loaded before byte compilation is performed for any file
  ;; as that fools the byte compiler and makes it believe that those (loaded)
  ;; symbols are already defined correctly, i.e. potentially important
  ;; warnings about issues with definitions will not be emitted by the byte
  ;; compiler.  For example,
  ;;
  ;;   Checking ~/.emacs.d/init/...
  ;;   Compiling ~/.emacs.d/init/init-company-flx.el...
  ;;   Compiling package company-flx
  ;;   Loading company-flx...
  ;;   Loading company...
  ;;   Loading pcase...done
  ;;   Loading php-extras...done
  ;;   Loading company...done
  ;;   Loading company-flx...done
  ;;   Compiling ~/.emacs.d/init/init-company-flx.el...done
  ;;   Wrote ~/.emacs.d/init/init-company-flx.elc
  ;;   Checking ~/.emacs.d/init/...
  ;;   Compiling ~/.emacs.d/init/init-company-php.el...
  ;;   Compiling package company-php
  ;;   Loading company-php...
  ;;   Loading company-template...done
  ;;   Loading company-php...done
  ;;   Compiling ~/.emacs.d/init/init-company-php.el...done
  ;;   Wrote ~/.emacs.d/init/init-company-php.elc
  ;;   Checking ~/.emacs.d/init/...
  ;;   Compiling ~/.emacs.d/init/init-company-ycmd.el...
  ;;   Compiling package company-ycmd
  ;;   Loading company-ycmd...
  ;;   Loading deferred...done
  ;;   Loading ycmd...
  ;;   Loading hmac-def...done
  ;;   Loading request...
  ;;   Loading url...
  ;;   Loading mailcap...done
  ;;   Loading url-cookie...
  ;;   Loading url-util...done
  ;;   Loading url-domsuf...done
  ;;   Loading url-cookie...done
  ;;   Loading url-history...done
  ;;   Loading url-expand...
  ;;   Loading url-methods...done
  ;;   Loading url-expand...done
  ;;   Loading url-privacy...done
  ;;   Loading url-proxy...done
  ;;   Loading url...done
  ;;   Loading mail-utils...done
  ;;   Loading request...done
  ;;   Loading request-deferred...done
  ;;   Loading etags...
  ;;   Loading xref...
  ;;   Loading project...done
  ;;   Loading xref...done
  ;;   Loading etags...done
  ;;   Loading diff...done
  ;;   Loading ycmd...done
  ;;   Loading company-ycmd...done
  ;;   Compiling ~/.emacs.d/init/init-company-ycmd.el...done
  ;;   Wrote ~/.emacs.d/init/init-company-ycmd.elc
  ;;   Checking ~/.emacs.d/init/...
  ;;   Compiling ~/.emacs.d/init/init-company.el...
  ;;   Compiling package company
  ;;   Compiling ~/.emacs.d/init/init-company.el... (init-company/:search-map)
  ;;   Loading fill-column-indicator...
  ;;   Configuring package fill-column-indicator...done
  ;;   Loading fill-column-indicator...done
  ;;   Compiling ~/.emacs.d/init/init-company.el...done
  ;;   Wrote ~/.emacs.d/init/init-company.elc
  ;;
  ;; ... where, in fact, `init-company.el' contains many issues with respect
  ;; to used `company' symbols being potentially undefined at run time, but
  ;; all of that goes unnoticed as `company' has already been loaded as (side
  ;; effect) part of byte compiling `init-company-flx.el' earlier.  Perhaps,
  ;; the only way to achieve the aforementioned goal of isolation is to spawn
  ;; a separate (private) Emacs instance for byte compiling (via command-line
  ;; switches) per file.
  ;; -------------------------------------------------------------------------
  (interactive)
  (unless noninteractive
    (message "Compiling packages..."))
  (unwind-protect
      (let ((time (current-time)))
        (condition-case-unless-debug e
            ;; Optimize the byte compilation process with:
            (let (;; 1.  Preventing calls of file name-specific handlers:
                  (file-name-handler-alist nil)
                  ;; 2.  Preventing garbage collection:
                  (gc-cons-threshold       (gc-max-cons-threshold))
                  ;; 3.  Pretending that nothing is being loaded:
                  (load-file-name          nil)
                  (load-in-progress        nil)
                  ;; 4.  Localizing modification of `load-path':
                  (load-path               (copy-sequence load-path))
                  ;; 5.  Preferring loading of files with `.elc' suffixes:
                  (load-suffixes           (or (bound-and-true-p
                                                byte-compile-load-suffixes)
                                               load-suffixes))
                  ;; 6.  Preventing messages when saving file(s):
                  (save-silently           (bound-and-true-p
                                            inhibit-message)))
              ;; -------------------------------------------------------------
              ;; CAUTION:
              ;;
              ;; The `user-init-directory' directory *must* be byte-compiled
              ;; before the `user-lisp-directory' directory because it will
              ;; populate `load-path' with the corresponding ELPA directories,
              ;; and those are definitely needed as dependencies of the
              ;; custom/local/personal packages under the
              ;; `user-lisp-directory' directory.
              ;; -------------------------------------------------------------
              (dolist (directory (list user-init-directory
                                       user-lisp-directory))
                (unless (zerop (length directory))
                  (setq directory (user-expand-directory directory))
                  (when byte-compile-verbose
                    (message "Compiling %s..."
                             (expand-directory directory)))
                  (condition-case-unless-debug e
                      (prog1
                          (byte-recompile-directory directory 0)
                        (when byte-compile-verbose
                          (message "Compiling %s...done"
                                   (expand-directory directory))))
                    (error (when byte-compile-verbose
                             (message "Compiling %s...failed"
                                      (expand-directory directory)))
                           (signal (car e) (cdr e)))))
                (unless noninteractive
                  (message "Compiling packages...done (%.3fs)"
                           (float-time (time-since time))))))
          (error (unless noninteractive
                   (message "Compiling packages...failed (%.3fs)"
                            (float-time (time-since time))))
                 (signal (car e) (cdr e)))))
    (user-kill-compile-log-buffer-maybe)))
(eval-after-load 'init-package
  '(add-to-list 'init-package-byte-compile-commands
                #'user-byte-compile))
;;
(init-negate (fboundp 'user-byte-compile-maybe))
(defun user-byte-compile-maybe ()
  "\
This function could be on `bc-idle-byte-compile-functions' or bound to a key."
  (interactive)
  (if user-byte-compile
      (user-byte-compile)
    (lwarn 'init
           :warning
           "Expected the non-nil value of variable `user-byte-compile'")))
(eval-after-load 'init-package
  '(add-to-list 'init-package-byte-compile-commands
                #'user-byte-compile-maybe))
(add-hook 'bc-idle-byte-compile-functions #'user-byte-compile-maybe)
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Now there is one more argument to perform isolation of byte compilation
;; process per file.  Take the following example, byte compiling some file
;; with `init-' prefix, will trigger preloading (at compile time) of
;;
;; 1.  the corresponding package;
;; 2.  whichever dependent package(s) mentioned in `after-load' form(s).
;;
;; As a result, in both cases the package(s) will be loaded before
;; initializing their custom variables (see `defcustom').  Of course, when
;; files with `init-' prefix are truly loaded later on, those missing
;; initializations (e.g. via `setq-default') will finally happen albeit after
;; their corresponding packages are loaded.  For some packages, this might not
;; be a problem but for some of them it is, e.g. the notorious `evil' with its
;; complex imperative `defcustom' definitions.
;;
;; Now, the solution to this problem consists of two parts.  First of all,
;; either the isolation of byte compilation process per file has to be
;; performed (which would be ideal but we have to postpone its implementation
;; due to lack of time at the moment) and it can happen even before truly
;; loading files with `init-' prefix, or the call
;;
;;   (user-byte-compile)
;;
;; ... should simply be done after truly loading files with `init-' prefix
;; (which is what we will do for the time being).  Secondly, we need to ensure
;; that dependencies of each of the packages do not load before their
;; corresponding files with `init-' prefix are loaded because this could lead
;; to the same issue of not initializing their custom variables properly.  For
;; example, `evil-surround' requires `evil'.  That is if somehow
;; `evil-surround-mode' is enabled (and, as a result, `evil' loaded) before
;; `init-evil' is loaded, `evil' will be left incorrectly customized.  Of
;; course, this could be later on fixed by customizing it (yet) again with
;; `custom' routines but this approach looks even more convoluted, would be a
;; nightmare to maintain, and is simply not worth it.  Anyway, this part of
;; the solution is already implemented by the `init-package' macro and thanks
;; to the `init-load-advice/:before' advice to both `load' and `require'.  The
;; drawback of it remains that in order to make byte compilation display all
;; the warnings in pursuit to achieve a truly warning-free configuration,
;; there must be absolute isolation of byte compilation process per file.
;; That is no variables and/or functions and/or files (apart from the core
;; ones bundled with the Emacs) should be loaded before byte compilation is
;; performed for any file as that fools the byte compiler and makes it believe
;; that those (loaded) symbols are already defined correctly, i.e. potentially
;; important warnings about issues with definitions will not be emitted by the
;; byte compiler.
;; ---------------------------------------------------------------------------
(init-negate (fboundp 'bc-idle-byte-compile))
(defun bc-idle-byte-compile ()
  "\
Run byte compilation after `bc-idle-delay'.

See `bc-idle-byte-compile-functions'."
  (run-hooks 'bc-idle-byte-compile-functions))
;;
(init-negate (fboundp 'gc-idle-garbage-collect))
(defun gc-idle-garbage-collect ()
  "\
Run garbage collection after `gc-idle-delay'.

See `garbage-collect'."
  (when (and (not noninteractive)
             (memq garbage-collection-messages '(idle t)))
    (message "Garbage collecting..."))
  (unwind-protect
      (let ((info nil)
            (time (current-time)))
        (condition-case-unless-debug e
            (prog1
                (setq info (garbage-collect))
              (when (and (not noninteractive)
                         (memq garbage-collection-messages '(idle t)))
                (message "Garbage collecting...done (%.3fs) [idle]"
                         (float-time (time-since time)))
                (when info
                  (let ((current-message (current-message)))
                    (let ((inhibit-message t))
                      (dolist (entry info)
                        (message "- `%s': %s" (car entry) (cdr entry))))
                    (if current-message
                        (let (message-log-max)
                          (message "%s" current-message))
                      (message nil))))))
          (error (when (and (not noninteractive)
                            (memq garbage-collection-messages '(idle t)))
                   (message "Garbage collecting...failed (%.3fs) [idle]"
                            (float-time (time-since time))))
                 (signal (car e) (cdr e)))))
    (gc-set-min-cons-threshold)))
;;
(init-negate (fboundp 'locked-kill-buffer-query-function))
(defun locked-kill-buffer-query-function ()
  "\
Bury the current buffer if `locked-buffer-p' returns non-nil.

Return nil if `locked-buffer-p' returns non-nil, otherwise return non-nil.

This function should be on `kill-buffer-query-functions'."
  (or (not (locked-buffer-p))
      (progn
        (ding)
        (message "Buffer is locked: %S" (current-buffer))
        (and (bury-buffer) nil))))
(add-hook 'kill-buffer-query-functions #'locked-kill-buffer-query-function)
;;
(init-negate (fboundp 'window-setup-finish))
(defun window-setup-finish ()
  (init-negate noninteractive)
  (unless noninteractive
    (message "Initializing...done (%.3fs) [window-setup]"
             (float-time (time-since before-init-time)))))
;;
(init-negate (fboundp 'emacs-startup-finish))
(defun emacs-startup-finish ()
  (init-negate noninteractive)
  (unless noninteractive
    ;; -----------------------------------------------------------------------
    ;; BUG:
    ;;
    ;; For some reason, unless some other hook is run for `emacs-startup-hook'
    ;; after `emacs-startup-finish' was run (i.e. if `emacs-startup-finish' is
    ;; the last hook for `emacs-startup-hook'), then `emacs-startup-finish'
    ;; produces no output (does not write any message to the "*Messages*"
    ;; buffer).  Why?
    ;;
    (message "Initializing...done (%.3fs) [emacs-startup]"
             (float-time (time-since before-init-time)))
    ;; -----------------------------------------------------------------------
    (remove-hook 'window-setup-hook #'window-setup-finish)
    (add-hook    'window-setup-hook #'window-setup-finish      :append)
    (remove-hook 'window-setup-hook #'enable-file-name-handler)
    (add-hook    'window-setup-hook #'enable-file-name-handler :append)
    (remove-hook 'window-setup-hook #'cancel-debug-on-error)
    (add-hook    'window-setup-hook #'cancel-debug-on-error    :append))
  (add-hook 'bc-idle-byte-compile-functions #'user-byte-compile-maybe)
  (if (not bc-idle-delay)
      (lwarn 'init
             :warning
             "No BC idle manager")
    (init-assert (fboundp 'bc-idle-byte-compile))
    (run-with-idle-timer bc-idle-delay nil #'bc-idle-byte-compile)
    (unless noninteractive
      (message "BC idle manager: `%s'" 'bc-idle-byte-compile)))
  (setq-default
    gc-cons-percentage custom-gc-cons-percentage
    gc-cons-threshold  (max custom-gc-cons-threshold (gc-max-cons-threshold)))
  (if (or (bound-and-true-p gcmh-mode)
          (when (and (featurep 'init-gcmh)
                     (fboundp  'gcmh-mode))
            (gcmh-mode)
            (bound-and-true-p gcmh-mode)))
      (progn
        (init-assert (bound-and-true-p gcmh-mode))
        (setq-default gc-idle-delay nil)
        (init-assert (fboundp 'gcmh-idle-garbage-collect))
        (unless noninteractive
          (message "GC idle manager: `%s'" 'gcmh-idle-garbage-collect))
        (lwarn 'init
               :warning
               "Enabled mode `%s'" 'gcmh-mode))
    (init-negate (bound-and-true-p gcmh-mode))
    (if (not gc-idle-delay)
        (lwarn 'init
               :warning
               "No GC idle manager")
      (init-assert (fboundp 'gc-idle-garbage-collect))
      (run-with-idle-timer gc-idle-delay t #'gc-idle-garbage-collect)
      (add-hook 'pre-command-hook #'gc-set-max-cons-threshold)
      (unless noninteractive
        (message "GC idle manager: `%s'" 'gc-idle-garbage-collect))))
  (when (bound-and-true-p minibuffer-gc-cons-threshold-mode)
    (lwarn 'init
           :warning
           "Enabled mode `%s'" 'minibuffer-gc-cons-threshold-mode))
  (when (bound-and-true-p term-setup-hook)
    (lwarn 'init
           :warning
           "Set the non-nil value of variable `term-setup-hook': %s"
           term-setup-hook)))
;;
(init-negate (fboundp 'tty-setup-finish))
(defun tty-setup-finish ()
  (init-negate noninteractive)
  (unless noninteractive
    (message "Initializing...done (%.3fs) [tty-setup]"
             (float-time (time-since before-init-time)))
    (remove-hook 'emacs-startup-hook #'emacs-startup-finish)
    (add-hook    'emacs-startup-hook #'emacs-startup-finish :append)))
;;
(init-negate (fboundp 'delayed-warnings-finish))
(defun delayed-warnings-finish ()
  (unless noninteractive
    (message "Initializing...done (%.3fs) [delayed-warnings]"
             (float-time (time-since before-init-time))))
  (remove-hook 'delayed-warnings-hook #'delayed-warnings-finish))
(remove-hook 'delayed-warnings-hook #'delayed-warnings-finish)
(add-hook    'delayed-warnings-hook #'delayed-warnings-finish :append)
;;
(init-negate (fboundp 'after-init-finish))
(defun after-init-finish ()
  (unless noninteractive
    (message "Initializing...done (%.3fs) [after-init]"
             (float-time (time-since before-init-time)))
    (let (debug-on-error)
      (with-demoted-errors "%s"
        (find-file-noselect (user-expand-file-name "scratch.el"))))
    (remove-hook 'tty-setup-hook     #'tty-setup-finish)
    (add-hook    'tty-setup-hook     #'tty-setup-finish     :append)
    (remove-hook 'emacs-startup-hook #'emacs-startup-finish)
    (add-hook    'emacs-startup-hook #'emacs-startup-finish :append))
  (add-hook 'kill-buffer-query-functions #'locked-kill-buffer-query-function))
(remove-hook 'after-init-hook #'after-init-finish)
(add-hook    'after-init-hook #'after-init-finish :append)
;;
(init-negate (fboundp 'init-finish))
(defun init-finish ()
  (unless noninteractive
    (message "Initializing...done (%.3fs)"
             (float-time (time-subtract after-init-time
                                        before-init-time)))))
(remove-hook 'after-init-hook #'init-finish)
(add-hook    'after-init-hook #'init-finish)
;; ---------------------------------------------------------------------------
(defconst user-after-init-time
  (current-time)
  "Value of `current-time' after user initialization.")
(when (and (not noninteractive) force-load-messages)
  (message "Loading %s...done (%.3fs)"
           (expand-file-name this-init-file)
           (float-time (time-subtract user-after-init-time
                                      user-before-init-time))))
;; ---------------------------------------------------------------------------
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(elisp) Documentation Tips'
;;  [2] Info node `(emacs) Init File'
;;  [3] URL `http://www.emacswiki.org/emacs/LoadPath'
;;  [4] URL `http://emacs.stackexchange.com/questions/13532/warning-bytecomp-is-a-bit-vague-can-i-get-any-more-information'
;;  ==========================================================================
;;  }}} References
;;

;;; File Local Variables {{{
;;  ==========================================================================
;;  Local Variables:
;;  no-byte-compile: t
;;  End:
;;  ==========================================================================
;;  }}} File Local Variables
