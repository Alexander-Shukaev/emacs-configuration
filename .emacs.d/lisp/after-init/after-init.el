(require 'cl-lib)
;;
;;;###autoload
(defsubst eval-after-init
    (form)
  "\
Execute FORM after initializing the Emacs session.

It is run after Emacs loads the init file, `default' library, the
abbrevs file, and additional Lisp packages (if any), and setting
the value of `after-init-time'.

If the Emacs session is already initialized, execute FORM right now.

There is no `condition-case' around the running of this hook;
therefore, if `debug-on-error' is non-nil, an error in FORM
will invoke the debugger.  See `after-init-hook'."
  (declare (compiler-macro (lambda
                               (whole)
                             (if (eq 'quote (car-safe form))
                                 ;; Quote with `lambda' so the compiler can
                                 ;; look inside:
                                 `(eval-after-init #'(lambda ()
                                                       ,(nth 1 form)))
                               whole))))
  (let ((function (if (functionp form)
                      form
                    (eval `(lambda () ,form) lexical-binding))))
    (if after-init-time
        (funcall function)
      (let ((hook-function (cl-gentemp "after-init-")))
        (eval `(progn
                 (defun ,hook-function ()
                   (remove-hook 'after-init-hook #',hook-function)
                   (funcall #',function)
                   (fmakunbound ',hook-function))
                 (add-hook 'after-init-hook #',hook-function :append))
              lexical-binding))
      nil)))
;;
;;;###autoload
(defmacro after-init
    (&rest body)
  "\
Execute BODY after initializing the Emacs session.

It is run after Emacs loads the init file, `default' library, the
abbrevs file, and additional Lisp packages (if any), and setting
the value of `after-init-time'.

If the Emacs session is already initialized, execute BODY right now.

There is no `condition-case' around the running of this hook;
therefore, if `debug-on-error' is non-nil, an error in BODY
will invoke the debugger.  See `after-init-hook'."
  (declare (debug  t)
           (indent defun))
  `(eval-after-init #'(lambda () ,@body)))
;;
(provide 'after-init)
