(eval-when-compile
  (require 'cl-lib))
;; TODO: What's that?
(eval '(eval-when-compile
         (require 'cl-lib)))
;;
;;;###autoload
(defmacro without-eager-macroexpand
    (&rest body)
  (declare (debug  t)
           (indent 1))
  `(cl-letf (((symbol-function 'internal-macroexpand-for-load) nil))
     (fmakunbound 'internal-macroexpand-for-load)
     ,@body))
;;
(provide 'without-eager-macroexpand)
