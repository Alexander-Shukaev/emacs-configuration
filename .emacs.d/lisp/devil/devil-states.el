(require 'devil-core)
;;
(require 'evil-states)
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Fulfill the documentation.
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Get rid of redundant `progn'.
;; ---------------------------------------------------------------------------
;;
(defmacro devil-operator-state-save-excursion
    (&rest body)
  "Execute BODY.
If in Operator-Pending state, protect execution of BODY by `save-excursion'."
  (declare (debug  t)
           (indent defun))
  `(if (evil-operator-state-p)
       (save-excursion ,@body)
     (progn ,@body)))
;;
(defmacro devil-visual-state-save-excursion
    (&rest body)
  "Execute BODY.
If in Visual state, protect execution of BODY by `save-excursion'."
  (declare (debug  t)
           (indent defun))
  `(if (evil-visual-state-p)
       (progn
         (evil-exit-visual-state)
         (unwind-protect
             (save-excursion ,@body)
           (evil-visual-restore)))
     (progn ,@body)))
;;
(defmacro devil-with-extended-operator-state-map
    (keymap &rest body)
  "Execute BODY.
Extend `evil-operator-state-map' with KEYMAP."
  (declare (debug  t)
           (indent defun))
  `(let ((evil-operator-state-map (make-composed-keymap
                                   (list evil-operator-state-map ,keymap))))
     (progn ,@body)))
;;
(defmacro devil-with-operator-shortcut-map
    (keymap &rest body)
  "Execute BODY.
After entering the Operator-Pending state, substitute
`evil-operator-shortcut-map' with KEYMAP."
  (declare (debug  t)
           (indent defun))
  `(let ((evil-operator-state-entry-hook (copy-sequence
                                          evil-operator-state-entry-hook)))
     (add-to-list 'evil-operator-state-entry-hook
                  #'(lambda ()
                      (evil--add-to-alist 'evil-mode-map-alist
                                          'evil-operator-shortcut-mode
                                          ,keymap)))
     (progn ,@body)))
;;
(defun devil-define-operator-shortcut
    (keymap definition)
  "In KEYMAP, define this operator's key sequence(s) as DEFINITION.
KEYMAP is a keymap.
Return KEYMAP."
  (declare (indent defun))
  ;; TODO:
  ;; (unless (evil-operator-state-p)
  ;;   (error "Not in the Operator-Pending state"))
  (let ((keys (listify-key-sequence
               (nth 2 (evil-extract-count (this-command-keys))))))
    (dotimes (var (length keys))
      (define-key keymap (vconcat (nthcdr var keys)) definition)))
  keymap)
;;
(defun devil-replace-state
    (&optional arg)
  "\
Switch to the Replace state at point."
  ;; TODO:
  ;; (eval-when-compile (documentation 'evil-replace-state))
  (interactive "*")
  (devil-call evil-replace-state arg))
(devil-copy-command-properties #'evil-replace-state #'devil-replace-state)
;;
(provide 'devil-states)
