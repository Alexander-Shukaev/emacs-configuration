(require 'devil-common)
;;
(eval-and-compile
  ;; Prevent load of `evil-maps':
  (add-to-list 'features 'evil-maps))
;;
(require 'devil-states)
(require 'devil-ex)
(require 'devil-commands)
(require 'devil-repeat-motion)
;;
(eval-when-compile
  (require 'devil-easy-motion))
;;
(require 'evil-maps)
;;
(eval-and-compile
  (devil-negate (devil-load-history "evil-maps")))
;;
(with-eval-after-load 'evil-maps
  (unless (fboundp 'redo)
    (defalias 'redo #'undo))
  (unless (fboundp 'recentf-open-directories)
    (defalias 'recentf-open-directories #'ignore))
  ;;
  (devil-negate (equal (if (stringp devil-escape-key)
                           (vconcat (kbd devil-escape-key))
                         devil-escape-key)
                       [?\e]))
  ;;
  (setq-default
    evil-ex-commands           nil
    evil-insert-state-bindings nil)
  ;;
  (devil-ex-define-cmds
    "g[lobal]"     #'evil-ex-global
    "s[ubstitute]" #'evil-ex-substitute
    ;;
    "~"            #'evil-ex-repeat-substitute
    "~&"           #'evil-ex-repeat-substitute-with-flags
    "~r"           #'evil-ex-repeat-substitute-with-search
    "~&r"          #'evil-ex-repeat-substitute-with-search-and-flags
    "%~&"          #'evil-ex-repeat-global-substitute)
  ;;
  (devil-ex-define-cmds
    "align-center"              #'evil-align-center
    "align-left"                #'evil-align-left
    "align-right"               #'evil-align-right
    ;;
    "ac[enter]"                 "align-center"
    "al[eft]"                   "align-left"
    "ar[ight]"                  "align-right"
    ;;
    "buffer-other"              #'evil-other-buffer
    "buffer-next"               #'evil-next-buffer
    "buffer-prev[ious]"         #'evil-prev-buffer
    ;;
    "bb[uffer]"                 "buffer-other"
    "bo[ther]"                  "buffer-other"
    "bn[ext]"                   "buffer-next"
    "bp[revious]"               "buffer-prev"
    ;;
    "buffer-delete"             #'evil-delete-buffer
    "buffer-kill"               #'evil-kill-buffer
    "buffer-quit"               #'evil-quit-buffer
    ;;
    "bd[elete]"                 "buffer-delete"
    "bk[ill]"                   "buffer-kill"
    "bq[uit]"                   "buffer-quit"
    ;;
    "process-delete"            #'devil-delete-process
    "process-interrupt"         #'devil-interrupt-process
    "process-kill"              #'devil-kill-process
    ;;
    "pd[elete]"                 "process-delete"
    "pi[nterrupt]"              "process-interrupt"
    "pk[ill]"                   "process-kill"
    ;;
    "process-interrupt-or-kill" #'devil-interrupt-or-kill-process
    ;;
    "k[ill]"                    "process-interrupt-or-kill"
    ;;
    "e[dit]"                    #'devil-edit
    ;;
    "file-info"                 #'evil-show-file-info
    ;;
    "marks"                     #'evil-show-marks
    ;;
    "q[uit]"                    #'evil-quit
    ;;
    "quit-all"                  #'evil-quit-all
    "quit-buffer"               #'evil-quit-buffer
    ;;
    "qa[ll]"                    "quit-all"
    "qb[uffer]"                 "quit-buffer"
    ;;
    "read"                      #'evil-read
    ;;
    "registers"                 #'evil-show-registers
    ;;
    "save"                      #'evil-save
    ;;
    "sudo"                      #'devil-sudo
    ;;
    "t[ail]"                    #'devil-tail
    ;;
    "w[rite]"                   #'evil-write
    ;;
    "write-all"                 #'evil-write-all
    ;;
    "wa[ll]"                    "write-all"
    ;;
    "window-other"              #'evil-window-other
    "window-next"               #'evil-window-next
    "window-prev[ious]"         #'evil-window-prev
    ;;
    "ww[indow]"                 "window-other"
    "wo[ther]"                  "window-other"
    "wn[ext]"                   "window-next"
    "wp[revious]"               "window-prev"
    ;;
    "window-right"              #'evil-window-right
    "window-left"               #'evil-window-left
    "window-down"               #'evil-window-down
    "window-up"                 #'evil-window-up
    ;;
    ;; TODO: Remove?
    ;; "wr[ight]"          "window-right"
    ;; "wl[eft]"           "window-left"
    ;; "wd[own]"           "window-down"
    ;; "wu[p]"             "window-up"
    )
  ;;
  (devil-ex-define-cmds
    "window-split" #'devil-split-window
    ;;
    "ws[plit]"     "window-split"
    ;;
    "d[ired]"      #'devil-dired
    ;;
    "c[ompile]"    #'devil-compile
    ;;
    "m[ake]"       #'devil-make
    ;;
    "&"     #'devil-async-shell-command
    ;;
    "!"     #'devil-shell-command
    ;;
    "shell" #'devil-shell
    "bash"  #'devil-bash
    "csh"   #'devil-csh
    "ksh"   #'devil-ksh
    "zsh"   #'devil-zsh
    "sh"    #'devil-sh
    "cmd"   #'devil-cmd)
  ;;
  (devil-ex-define-cmds
    "buffer-eval"       #'eval-buffer
    ;;
    "be[val]"           "buffer-eval")
  ;;
  (with-eval-after-load "window"
    (devil-ex-define-cmds
      "window-main"         #'window-toggle-side-windows
      ;;
      "wm[ain]"             "window-main"
      ;;
      "window-delete"       #'delete-window
      "window-delete-other" #'delete-other-windows
      "window-quit"         #'quit-window
      ;;
      "wd[elete]"           "window-delete"
      "wdo[ther]"           "window-delete-other"
      "wq[uit]"             "window-quit"
      ;;
      "window-balance"      #'balance-windows
      ;;
      "wb[alance]"          "window-balance"
      "w="                  "window-balance"))
  ;;
  (with-eval-after-load 'help
    (devil-ex-define-cmd "h[elp]" #'help))
  ;;
  (devil-ex-define-cmds
    "grep"      #'grep
    "grep-find" #'grep-find
    "find-grep" #'find-grep
    ;;
    "gf[ind]" "grep-find"
    "fg[rep]" "find-grep"
    ;;
    "grep-local"     #'lgrep
    "grep-recursive" #'rgrep
    ;;
    "gl[ocal]"     "grep-local"
    "gr[ecursive]" "grep-recursive")
  ;;
  ;; TODO: Review [1], especially `evil-insert-digraph',
  ;; `evil-delete-backward-char-and-join', `delete-char',
  ;; `delete-forward-char', `delete-backward-char', and
  ;; `evil-disable-insert-state-bindings'.
  ;;
  (let ((keymap evil-emacs-state-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-toggle-key #'evil-exit-emacs-state))
  ;;
  (let ((keymap evil-insert-state-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-toggle-key #'evil-emacs-state
      devil-escape-key #'evil-normal-state
      ;;
      "<remap delete-backward-char>" #'devil-delete-char-backward-and-unite))
  ;;
  (let ((keymap evil-motion-state-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-toggle-key #'evil-emacs-state
      ;;
      "0" #'digit-argument
      "1" #'digit-argument
      "2" #'digit-argument
      "3" #'digit-argument
      "4" #'digit-argument
      "5" #'digit-argument
      "6" #'digit-argument
      "7" #'digit-argument
      "8" #'digit-argument
      "9" #'digit-argument
      ;;
      "l" #'devil-char-forward
      "h" #'devil-char-backward
      ;;
      "j" #'devil-line-forward
      "k" #'devil-line-backward
      ;;
      "L" #'devil-scroll-right
      "H" #'devil-scroll-left
      ;;
      "J" #'devil-scroll-down
      "K" #'devil-scroll-up
      ;;
      "\/" #'devil-search-forward
      "\\" #'devil-search-backward
      ;;
      "m" #'devil-search-match-next
      "M" #'devil-search-match-previous
      ;;
      "f" #'evil-repeat-find-char
      "F" #'evil-repeat-find-char-reverse
      ;;
      "y" #'evil-yank
      "Y" #'evil-yank-line
      ;;
      "v"   #'evil-visual-char
      "V"   #'evil-visual-line
      "C-v" #'evil-visual-block
      ;;
      ":" #'evil-ex
      ;;
      "\"" #'evil-use-register
      ;;
      ;; TODO: Better through remap:
      "?" #'help-command
      ;;
      "&" #'devil-async-shell-command
      ;;
      "g" nil
      ;;
      "g RET" #'find-file-at-point
      ;;
      "g g"   #'find-file-at-point
      "g /"   #'dired-at-point
      ;;
      ;; TODO: Does [2] make `evil-goto-definition' useful enough now?
      "g d" #'evil-goto-definition
      ;;
      "g f" #'find-function-at-point
      "g s" #'find-variable-at-point
      ;;
      "g l" #'devil-last-non-blank-char
      "g h" #'devil-first-non-blank-char
      ;;
      "g j" #'devil-last-line
      "g k" #'devil-first-line
      ;;
      "g L" #'devil-end-of-visual-line
      "g H" #'devil-beg-of-visual-line
      ;;
      "g J" #'devil-end-of-buffer
      "g K" #'devil-beg-of-buffer
      ;;
      "g $" #'devil-end-of-line
      "g ^" #'devil-beg-of-line
      ;;
      "g i" #'evil-insert-resume
      "g v" #'evil-visual-restore
      ;;
      ;; ---------------------------------------------------------------------
      ;; NOTE:
      ;;
      ;; Not worth reserving "b" and "e" for these:
      ;;
      ;; "g b" nil
      ;; "g e" nil
      ;;
      ;; "g b l" #'devil-beg-of-line
      ;; "g e l" #'devil-end-of-line
      ;;
      ;; "g b b" #'devil-beg-of-buffer
      ;; "g e b" #'devil-end-of-buffer
      ;; ---------------------------------------------------------------------
      ;;
      ">" nil
      "<" nil
      ;;
      "> c" #'evil-find-char
      "< c" #'evil-find-char-backward
      ;;
      "> t" #'evil-find-char-to
      "< t" #'evil-find-char-to-backward
      ;;
      "> b" nil
      "< b" nil
      ;;
      "> B" "> b"
      "< B" "< b"
      ;;
      "> e" nil
      "< e" nil
      ;;
      "> E" "> e"
      "< E" "< e"
      ;;
      "> b w" #'devil-beg-of-word-forward
      "< b w" #'devil-beg-of-word-backward
      ;;
      "> e w" #'devil-end-of-word-forward
      "< e w" #'devil-end-of-word-backward
      ;;
      "> b W" #'devil-beg-of-WORD-forward
      "< b W" #'devil-beg-of-WORD-backward
      ;;
      "> e W" #'devil-end-of-WORD-forward
      "< e W" #'devil-end-of-WORD-backward
      ;;
      "> b s" #'devil-beg-of-symbol-forward
      "< b s" #'devil-beg-of-symbol-backward
      ;;
      "> e s" #'devil-end-of-symbol-forward
      "< e s" #'devil-end-of-symbol-backward
      ;;
      "> b S" #'devil-beg-of-sentence-forward
      "< b S" #'devil-beg-of-sentence-backward
      ;;
      "> e S" #'devil-end-of-sentence-forward
      "< e S" #'devil-end-of-sentence-backward
      ;;
      "> b ." "> b S"
      "< b ." "< b S"
      ;;
      "> e ." "> e S"
      "< e ." "< e S"
      ;;
      "> b p" #'devil-beg-of-paragraph-forward
      "< b p" #'devil-beg-of-paragraph-backward
      ;;
      "> e p" #'devil-end-of-paragraph-forward
      "< e p" #'devil-end-of-paragraph-backward
      ;;
      "> b P" "> b p"
      "< b P" "< b p"
      ;;
      "> e P" "> e p"
      "< e P" "< e p"
      ;;
      "> b f" #'devil-beg-of-defun-forward
      "< b f" #'devil-beg-of-defun-backward
      ;;
      "> e f" #'devil-end-of-defun-forward
      "< e f" #'devil-end-of-defun-backward
      ;;
      "> b F" "> b f"
      "< b F" "< b f"
      ;;
      "> e F" "> e f"
      "< e F" "< e f"
      ;;
      "> w" #'devil-beg-of-word-forward
      "< w" #'devil-beg-of-word-backward
      ;;
      "> W" #'devil-beg-of-WORD-forward
      "< W" #'devil-beg-of-WORD-backward
      ;;
      "> s" #'devil-beg-of-symbol-forward
      "< s" #'devil-beg-of-symbol-backward
      ;;
      "> S" #'devil-beg-of-sentence-forward
      "< S" #'devil-beg-of-sentence-backward
      ;;
      "> ." "> S"
      "< ." "< S"
      ;;
      "> p" #'devil-beg-of-paragraph-forward
      "< p" #'devil-beg-of-paragraph-backward
      ;;
      "> P" "> p"
      "< P" "< p"
      ;;
      "> f" #'devil-beg-of-defun-forward
      "< f" #'devil-beg-of-defun-backward
      ;;
      "> F" "> f"
      "< F" "< f"
      ;;
      "< l" #'devil-beg-of-line
      "> l" #'devil-beg-of-line
      ;;
      "< L" "< l"
      "> L" "> l"
      ;;
      "< %" #'devil-beg-of-buffer
      "> %" #'devil-beg-of-buffer
      ;;
      "w" nil
      ;;
      ;; TODO: Should become `ace-window':
      "w w" #'evil-window-other
      ;;
      "w o" #'evil-window-other
      "w n" #'evil-window-next "<C-tab>    " "w n"
      "w p" #'evil-window-prev "<C-backtab>" "w p"
      ;;
      "w l" #'evil-window-right
      "w h" #'evil-window-left
      "w j" #'evil-window-down
      "w k" #'evil-window-up
      ;;
      "w s" #'devil-split-window
      ;;
      "w m" #'window-toggle-side-windows
      ;;
      "w d" #'delete-window
      "w D" #'delete-other-windows
      "w q" #'quit-window
      ;;
      "w b" #'balance-windows
      "w =" #'balance-windows "C-=" "w ="
      ;;
      "w +" #'evil-window-increase-width  "C-+" "w +"
      "w -" #'evil-window-decrease-width  "C--" "w -"
      "w ^" #'evil-window-increase-height "C-^" "w ^"
      "w _" #'evil-window-decrease-height "C-_" "w _"
      ;;
      "W" #'delete-window
      ;;
      "b" nil
      ;;
      "b e" #'eval-buffer
      ;;
      "b w" #'evil-write
      ;;
      "b C-b" #'switch-to-buffer-other-frame
      "b B"   #'switch-to-buffer-other-window
      "b b"   #'switch-to-buffer
      ;;
      "b o" #'evil-other-buffer
      "b n" #'evil-next-buffer
      "b p" #'evil-prev-buffer
      ;;
      "b l" #'devil-swap-buffer-right
      "b h" #'devil-swap-buffer-left
      "b j" #'devil-swap-buffer-down
      "b k" #'devil-swap-buffer-up
      ;;
      "b d" #'evil-delete-buffer
      ;;
      "b q" #'evil-quit-buffer
      ;;
      "B" #'evil-delete-buffer
      ;;
      "Q" #'evil-quit-buffer
      ;;
      ;; TODO: `devil-leader':
      "SPC" nil
      ;;
      "SPC f" nil
      ;;
      "SPC f C-f" #'ffap-other-frame   "SPC f C-e" "SPC f C-f"
      "SPC f F"   #'ffap-other-window  "SPC f E"   "SPC f F"
      "SPC f f"   #'find-file-at-point "SPC f e"   "SPC f f"
      ;;
      "SPC f A"   #'ffap-alternate-file-other-window
      "SPC f a"   #'ffap-alternate-file
      ;;
      "SPC f l"   #'ffap-literally
      ;;
      "SPC f C-r" #'ffap-read-only-other-frame
      "SPC f R"   #'ffap-read-only-other-window
      "SPC f r"   #'ffap-read-only
      ;;
      "SPC d" nil
      ;;
      "SPC d d"   #'dired
      ;;
      "SPC d C-e" #'ffap-dired-other-frame
      "SPC d E"   #'ffap-dired-other-window
      "SPC d e"   #'dired-at-point
      ;;
      "SPC r" nil
      ;;
      "SPC r f"   #'recentf-open-files
      ;; TODO:
      "SPC r d"   #'recentf-open-directories
      "SPC r r"   #'recentf-open-files ; TODO: Why "SPC r f" hangs?
      ;;
      ;; #'find-file (remap counsel)
      ;; #'dired (remap counsel)
      ;;
      ;;
      ;;
      ","   nil
      ";"   nil
      "`"   nil
      "'"   nil ; avy?
      "|"   nil
      "("   nil
      ")"   nil
      "@"   nil ; mark?
      ;; TODO: As per [3]:
      "t"   nil ; #'(shift (transpose) character right)
      "T"   nil ; #'(shift (transpose) character left)
      ))
  ;;
  (devil-easy-motion-kbd "] c"
    #'evil-repeat-find-char
    :name
    'evilem--motion-function-evil-find-char
    :pre-hook
    (save-excursion
      (setq evil-this-type 'inclusive)
      (call-interactively #'evil-find-char))
    :bind
    ((evil-cross-lines t)))
  (devil-easy-motion-kbd "[ c"
    #'evil-repeat-find-char
    :name
    'evilem--motion-function-evil-find-char-backward
    :pre-hook
    (save-excursion
      (setq evil-this-type 'exclusive)
      (call-interactively #'evil-find-char-backward))
    :bind
    ((evil-cross-lines t)))
  (devil-easy-motion-kbd "] t"
    #'evil-repeat-find-char
    :name
    'evilem--motion-function-evil-find-char-to
    :pre-hook
    (save-excursion
      (setq evil-this-type 'inclusive)
      (call-interactively #'evil-find-char-to))
    :bind
    ((evil-cross-lines t)))
  (devil-easy-motion-kbd "[ t"
    #'evil-repeat-find-char
    :name
    'evilem--motion-function-evil-find-char-to-backward
    :pre-hook
    (save-excursion
      (setq evil-this-type 'exclusive)
      (call-interactively #'evil-find-char-to-backward))
    :bind
    ((evil-cross-lines t)))
  ;;
  (devil-easy-motions-kbd
    "] b w" #'devil-beg-of-word-forward
    "[ b w" #'devil-beg-of-word-backward
    ;;
    "] e w" #'devil-end-of-word-forward
    "[ e w" #'devil-end-of-word-backward
    ;;
    "] b W" #'devil-beg-of-WORD-forward
    "[ b W" #'devil-beg-of-WORD-backward
    ;;
    "] e W" #'devil-end-of-WORD-forward
    "[ e W" #'devil-end-of-WORD-backward
    ;;
    "] b s" #'devil-beg-of-symbol-forward
    "[ b s" #'devil-beg-of-symbol-backward
    ;;
    "] e s" #'devil-end-of-symbol-forward
    "[ e s" #'devil-end-of-symbol-backward
    ;;
    "] b S" #'devil-beg-of-sentence-forward
    "[ b S" #'devil-beg-of-sentence-backward
    ;;
    "] e S" #'devil-end-of-sentence-forward
    "[ e S" #'devil-end-of-sentence-backward
    ;;
    "] b ." #'devil-beg-of-sentence-forward
    "[ b ." #'devil-beg-of-sentence-backward
    ;;
    "] e ." #'devil-end-of-sentence-forward
    "[ e ." #'devil-end-of-sentence-backward
    ;;
    "] b p" #'devil-beg-of-paragraph-forward
    "[ b p" #'devil-beg-of-paragraph-backward
    ;;
    "] e p" #'devil-end-of-paragraph-forward
    "[ e p" #'devil-end-of-paragraph-backward
    ;;
    "] b P" #'devil-beg-of-paragraph-forward
    "[ b P" #'devil-beg-of-paragraph-backward
    ;;
    "] e P" #'devil-end-of-paragraph-forward
    "[ e P" #'devil-end-of-paragraph-backward
    ;;
    "] b f" #'devil-beg-of-defun-forward
    "[ b f" #'devil-beg-of-defun-backward
    ;;
    "] e f" #'devil-end-of-defun-forward
    "[ e f" #'devil-end-of-defun-backward
    ;;
    "] b F" #'devil-beg-of-defun-forward
    "[ b F" #'devil-beg-of-defun-backward
    ;;
    "] e F" #'devil-end-of-defun-forward
    "[ e F" #'devil-end-of-defun-backward)
  ;;
  (devil-easy-motions-kbd
    "] w" #'devil-beg-of-word-forward
    "[ w" #'devil-beg-of-word-backward
    ;;
    "] W" #'devil-beg-of-WORD-forward
    "[ W" #'devil-beg-of-WORD-backward
    ;;
    "] s" #'devil-beg-of-symbol-forward
    "[ s" #'devil-beg-of-symbol-backward
    ;;
    "] S" #'devil-beg-of-sentence-forward
    "[ S" #'devil-beg-of-sentence-backward
    ;;
    "] ." #'devil-beg-of-sentence-forward
    "[ ." #'devil-beg-of-sentence-backward
    ;;
    "] p" #'devil-beg-of-paragraph-forward
    "[ p" #'devil-beg-of-paragraph-backward
    ;;
    "] P" #'devil-beg-of-paragraph-forward
    "[ P" #'devil-beg-of-paragraph-backward
    ;;
    "] f" #'devil-beg-of-defun-forward
    "[ f" #'devil-beg-of-defun-backward
    ;;
    "] F" #'devil-beg-of-defun-forward
    "[ F" #'devil-beg-of-defun-backward)
  ;;
  (devil-repeat-motions-kbd
    ;; -----------------------------------------------------------------------
    ;; #'devil-search-forward  "m" "M"
    ;; #'devil-search-backward "m" "M"
    ;; ;;
    ;; #'devil-search-input-forward  "m" "M"
    ;; #'devil-search-input-backward "m" "M"
    ;; ;;
    ;; #'devil-search-word-forward  "m" "M"
    ;; #'devil-search-word-backward "m" "M"
    ;; ;;
    ;; #'devil-search-WORD-forward  "m" "M"
    ;; #'devil-search-WORD-backward "m" "M"
    ;; ;;
    ;; #'devil-search-symbol-forward  "m" "M"
    ;; #'devil-search-symbol-backward "m" "M"
    ;;
    ;; BUG:
    ;;
    ;; Operators get confused and produce wrong results.
    ;; -----------------------------------------------------------------------
    ;;
    ;; "m" "m" "M"
    ;; "M" "M" "m"
    ;;
    ;; "f" "f" "F"
    ;; "F" "F" "f"
    ;;
    "> c" "f" "F"
    "< c" "f" "F"
    ;;
    "> t" "f" "F"
    "< t" "f" "F"
    ;;
    ;; "> b w" "> b w" "< b w"
    ;; "< b w" "< b w" "> b w"
    ;;
    ;; "> e w" "> e w" "< e w"
    ;; "< e w" "< e w" "> e w"
    ;;
    ;; "> b W" "> b W" "< b W"
    ;; "< b W" "< b W" "> b W"
    ;;
    ;; "> e W" "> e W" "< e W"
    ;; "< e W" "< e W" "> e W"
    ;;
    ;; "> b s" "> b s" "< b s"
    ;; "< b s" "< b s" "> b s"
    ;;
    ;; "> e s" "> e s" "< e s"
    ;; "< e s" "< e s" "> e s"
    ;;
    ;; "> b S" "> b S" "< b S"
    ;; "< b S" "< b S" "> b S"
    ;;
    ;; "> e S" "> e S" "< e S"
    ;; "< e S" "< e S" "> e S"
    ;;
    ;; "> b ." "> b ." "< b ."
    ;; "< b ." "< b ." "> b ."
    ;;
    ;; "> e ." "> e ." "< e ."
    ;; "< e ." "< e ." "> e ."
    ;;
    ;; "> b p" "> b p" "< b p"
    ;; "< b p" "< b p" "> b p"
    ;;
    ;; "> e p" "> e p" "< e p"
    ;; "< e p" "< e p" "> e p"
    ;;
    ;; "> b P" "> b P" "< b P"
    ;; "< b P" "< b P" "> b P"
    ;;
    ;; "> e P" "> e P" "< e P"
    ;; "< e P" "< e P" "> e P"
    ;;
    ;; "> b f" "> b f" "< b f"
    ;; "< b f" "< b f" "> b f"
    ;;
    ;; "> e f" "> e f" "< e f"
    ;; "< e f" "< e f" "> e f"
    ;;
    ;; "> b F" "> b F" "< b F"
    ;; "< b F" "< b F" "> b F"
    ;;
    ;; "> e F" "> e F" "< e F"
    ;; "< e F" "< e F" "> e F"
    ;;
    "> b w" "> b w" "< b w"
    "< b w" "> b w" "< b w"
    ;;
    "> e w" "> e w" "< e w"
    "< e w" "> e w" "< e w"
    ;;
    "> b W" "> b W" "< b W"
    "< b W" "> b W" "< b W"
    ;;
    "> e W" "> e W" "< e W"
    "< e W" "> e W" "< e W"
    ;;
    "> b s" "> b s" "< b s"
    "< b s" "> b s" "< b s"
    ;;
    "> e s" "> e s" "< e s"
    "< e s" "> e s" "< e s"
    ;;
    "> b S" "> b S" "< b S"
    "< b S" "> b S" "< b S"
    ;;
    "> e S" "> e S" "< e S"
    "< e S" "> e S" "< e S"
    ;;
    "> b ." "> b ." "< b ."
    "< b ." "> b ." "< b ."
    ;;
    "> e ." "> e ." "< e ."
    "< e ." "> e ." "< e ."
    ;;
    "> b p" "> b p" "< b p"
    "< b p" "> b p" "< b p"
    ;;
    "> e p" "> e p" "< e p"
    "< e p" "> e p" "< e p"
    ;;
    "> b P" "> b P" "< b P"
    "< b P" "> b P" "< b P"
    ;;
    "> e P" "> e P" "< e P"
    "< e P" "> e P" "< e P"
    ;;
    "> b f" "> b f" "< b f"
    "< b f" "> b f" "< b f"
    ;;
    "> e f" "> e f" "< e f"
    "< e f" "> e f" "< e f"
    ;;
    "> b F" "> b F" "< b F"
    "< b F" "> b F" "< b F"
    ;;
    "> e F" "> e F" "< e F"
    "< e F" "> e F" "< e F")
  ;;
  (devil-repeat-motions-kbd
    "] c" "] c" "[ c"
    "[ c" "] c" "[ c"
    ;;
    "] t" "] t" "[ t"
    "[ t" "] t" "[ t"
    ;;
    "] b w" "] b w" "[ b w"
    "[ b w" "] b w" "[ b w"
    ;;
    "] e w" "] e w" "[ e w"
    "[ e w" "] e w" "[ e w"
    ;;
    "] b W" "] b W" "[ b W"
    "[ b W" "] b W" "[ b W"
    ;;
    "] e W" "] e W" "[ e W"
    "[ e W" "] e W" "[ e W"
    ;;
    "] b s" "] b s" "[ b s"
    "[ b s" "] b s" "[ b s"
    ;;
    "] e s" "] e s" "[ e s"
    "[ e s" "] e s" "[ e s"
    ;;
    "] b S" "] b S" "[ b S"
    "[ b S" "] b S" "[ b S"
    ;;
    "] e S" "] e S" "[ e S"
    "[ e S" "] e S" "[ e S"
    ;;
    "] b ." "] b ." "[ b ."
    "[ b ." "] b ." "[ b ."
    ;;
    "] e ." "] e ." "[ e ."
    "[ e ." "] e ." "[ e ."
    ;;
    "] b p" "] b p" "[ b p"
    "[ b p" "] b p" "[ b p"
    ;;
    "] e p" "] e p" "[ e p"
    "[ e p" "] e p" "[ e p"
    ;;
    "] b P" "] b P" "[ b P"
    "[ b P" "] b P" "[ b P"
    ;;
    "] e P" "] e P" "[ e P"
    "[ e P" "] e P" "[ e P"
    ;;
    "] b f" "] b f" "[ b f"
    "[ b f" "] b f" "[ b f"
    ;;
    "] e f" "] e f" "[ e f"
    "[ e f" "] e f" "[ e f"
    ;;
    "] b F" "] b F" "[ b F"
    "[ b F" "] b F" "[ b F"
    ;;
    "] e F" "] e F" "[ e F"
    "[ e F" "] e F" "[ e F")
  ;;
  (let ((keymap evil-normal-state-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-escape-key #'evil-force-normal-state
      ;;
      ;; "C-e" #'evil-execute-macro
      ;; "C-r" #'evil-record-macro
      ;;
      "  <insert>"     "i"
      "  <insertchar>" "i"
      "<S-insert>"     "I"
      ;;
      "  <delete>    " "x"
      "  <deletechar>" "x"
      "<S-delete>    " "X"
      ;;
      "RET" #'devil-insert-at-eol
      "DEL" #'devil-insert-at-bol
      ;;
      "a" #'devil-amend
      "A" #'devil-amend-to-eol
      ;;
      "c" #'devil-comment
      "C" #'devil-comment-to-eol
      ;;
      "d" #'devil-delete
      "D" #'devil-delete-to-eol
      ;;
      "i" #'devil-insert-at-eop
      "I" #'devil-insert-at-bop
      ;;
      "C-l" #'devil-newline-forward
      "C-j" #'devil-newline-backward
      ;;
      "LFD" nil
      "LFD" #'devil-newline-backward
      ;;
      "o" #'devil-open-line-forward
      "O" #'devil-open-line-backward
      ;;
      "p" #'devil-paste-forward
      "P" #'devil-paste-backward
      ;;
      ;; TODO:
      ;;
      ;; `evil-paste-from-register'
      ;; `evil-paste-last-insertion'
      ;; `evil-paste-pop'
      ;; `evil-paste-pop-next'
      ;;
      "r" #'devil-replace
      "R" #'devil-replace-state
      ;;
      "s" nil
      "S" nil
      ;;
      "u" #'devil-unite-line-forward
      "U" #'devil-unite-line-backward
      ;;
      "x" #'devil-delete-char-forward
      "X" #'devil-delete-char-backward
      ;;
      "z" #'undo-only
      "Z" #'redo
      ;;
      "+" #'devil-increase-indentation
      "-" #'devil-decrease-indentation
      ;;
      "#" #'devil-align-regexp
      ;;
      "*" #'devil-fill
      ;;
      "=" #'devil-format
      ;;
      "$" #'devil-sort
      ;;
      "_" #'devil-downcase
      "^" #'devil-upcase
      "~" #'devil-swapcase
      ;;
      "!" #'devil-shell-command
      ;;
      "  ." #'evil-repeat
      "C-." #'evil-repeat-pop
      "M-." #'evil-repeat-pop-next
      ;;
      "%" #'evil-jump-item
      ;;
      "g ." #'evil-repeat-pop
      "g :" #'evil-repeat-pop-next
      ;;
      "g ~" #'evil-ex-repeat-global-substitute
      ;;
      "g c" #'goto-last-change
      "g C" #'goto-last-change-reverse))
  ;;
  (let ((keymap evil-operator-state-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-escape-key #'keyboard-quit
      ;;
      "b" #'evil-buffer
      ;;
      " T" #'evil-inner-tag
      " t" #'evil-inner-tag
      " P" #'evil-inner-paragraph
      " p" #'evil-inner-paragraph
      " ." #'evil-inner-sentence
      " (" #'evil-inner-paren
      " )" #'evil-inner-paren
      " <" #'evil-inner-angle
      " >" #'evil-inner-angle
      " [" #'evil-inner-bracket
      " ]" #'evil-inner-bracket
      " {" #'evil-inner-curly
      " }" #'evil-inner-curly
      " G" #'evil-inner-grave-quote
      " g" #'evil-inner-grave-quote
      " Q" #'evil-inner-grave-quote
      " q" #'evil-inner-grave-quote
      " '" #'evil-inner-single-quote
      "\"" #'evil-inner-double-quote
      " `" #'evil-inner-back-quote
      ;;
      "i" evil-inner-text-objects-map
      "o" evil-outer-text-objects-map))
  ;;
  (let ((keymap evil-replace-state-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-escape-key #'evil-normal-state
      ;;
      "DEL" #'evil-replace-backspace))
  ;;
  ;; -------------------------------------------------------------------------
  (let ((keymap evil-visual-state-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-escape-key #'evil-exit-visual-state
      ;;
      "R" #'devil-amend
      ;;
      "i" evil-inner-text-objects-map
      "o" evil-outer-text-objects-map
      ;;
      "<remap evil-repeat>" #'undefined))
  ;;
  ;; BUG:
  ;;
  ;; `C-v k ]', then try to undo.  On the first line entered text remains.
  ;; Possibly `keep-visual' again?
  ;; -------------------------------------------------------------------------
  ;;
  (let ((keymap evil-read-key-map))
    (devil-clear-keymap keymap)
    (devil-define-keys-kbd keymap
      "ESC" #'keyboard-quit
      "C-[" #'keyboard-quit
      "C-]" #'keyboard-quit
      "C-g" #'keyboard-quit
      ;;
      "C-d" #'evil-read-digraph-char
      "C-q" #'evil-read-quoted-char
      ;;
      "RET" "LFD"))
  ;;
  (let ((keymap evil-inner-text-objects-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      ;; TODO: `m'
      " b" #'evil-inner-buffer
      " c" #'devil-inner-comment
      " W" #'evil-inner-WORD
      " w" #'evil-inner-word
      " s" #'evil-inner-symbol
      " T" #'evil-inner-tag
      " t" #'evil-inner-tag
      " P" #'evil-inner-paragraph
      " p" #'evil-inner-paragraph
      " ." #'evil-inner-sentence
      " S" #'evil-inner-sentence
      " (" #'evil-inner-paren
      " )" #'evil-inner-paren
      " <" #'evil-inner-angle
      " >" #'evil-inner-angle
      " [" #'evil-inner-bracket
      " ]" #'evil-inner-bracket
      " {" #'evil-inner-curly
      " }" #'evil-inner-curly
      "\/" #'evil-inner-slash
      "\\" #'evil-inner-backslash
      " G" #'evil-inner-grave-quote
      " g" #'evil-inner-grave-quote
      " Q" #'evil-inner-grave-quote
      " q" #'evil-inner-grave-quote
      " '" #'evil-inner-single-quote
      "\"" #'evil-inner-double-quote
      " `" #'evil-inner-back-quote))
  ;;
  (let ((keymap evil-outer-text-objects-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      ;; TODO: `m'
      " b" #'evil-a-buffer
      " W" #'evil-a-WORD
      " w" #'evil-a-word
      " s" #'evil-a-symbol
      " T" #'evil-a-tag
      " t" #'evil-a-tag
      " P" #'evil-a-paragraph
      " p" #'evil-a-paragraph
      " ." #'evil-a-sentence
      " S" #'evil-a-sentence
      " (" #'evil-a-paren
      " )" #'evil-a-paren
      " <" #'evil-an-angle
      " >" #'evil-an-angle
      " [" #'evil-a-bracket
      " ]" #'evil-a-bracket
      " {" #'evil-a-curly
      " }" #'evil-a-curly
      "\/" #'evil-a-slash
      "\\" #'evil-a-backslash
      " G" #'evil-a-grave-quote
      " g" #'evil-a-grave-quote
      " Q" #'evil-a-grave-quote
      " q" #'evil-a-grave-quote
      " '" #'evil-a-single-quote
      "\"" #'evil-a-double-quote
      " `" #'evil-a-back-quote))
  ;;
  (let ((keymap evil-window-map))
    (devil-clear-keymap keymap)
    (define-prefix-command 'evil-window-map))
  ;;
  (let ((keymap evil-ex-map))
    (devil-clear-keymap keymap))
  ;;
  (let ((keymap evil-ex-completion-map))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      devil-escape-key #'abort-recursive-edit
      ;;
      "<remap completion-at-point>" #'evil-ex-completion
      "TAB"                         #'evil-ex-completion
      ;;
      "<down>" #'next-complete-history-element
      "<up>"   #'previous-complete-history-element
      ;;
      "<S-down>" #'next-matching-history-element
      "<S-up>  " #'previous-matching-history-element
      ;;
      "<C-down>" #'next-matching-history-element
      "<C-up>  " #'previous-matching-history-element
      ;;
      "RET" #'exit-minibuffer))
  ;;
  (with-eval-after-load 'evil-core
    (devil-define-keys-kbd evil-ex-completion-map
      :motion
      ;; TODO: Does not seem to work anymore:
      "q" devil-escape-key))
  ;;
  (let ((keymap evil-ex-search-keymap))
    (devil-clear-keymap    keymap)
    (devil-define-keys-kbd keymap
      "<down>" #'next-history-element
      "<up>"   #'previous-history-element
      ;;
      "TAB"       #'next-history-element
      "<backtab>" #'previous-history-element))
  ;;
  (let ((keymap evil-command-window-mode-map))
    (devil-clear-keymap keymap))
  ;;
  ;; TODO: `ace-buffer'?
  ;; TODO: `devil-eval' - operator
  )
;;
(provide 'devil-maps)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://github.com/noctuid/evil-guide#use-some-emacs-keybindings'
;;  [2] URL `http://github.com/emacs-evil/evil/pull/873'
;;  [3] Info node `(emacs) Transpose'
;;  ==========================================================================
;;  }}} References
