(require 'devil-common)
(require 'devil-states)
;;
(require 'evil-macros)
;;
(put 'evil-define-motion      'lisp-indent-function 2)
(put 'evil-define-text-object 'lisp-indent-function 2)
(put 'evil-define-operator    'lisp-indent-function 2)
;;
(provide 'devil-macros)
