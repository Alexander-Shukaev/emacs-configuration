(require 'devil-core)
;;
(eval-and-compile
  ;; Prevent load of `evil-keybindings':
  (add-to-list 'features 'evil-keybindings))
;;
(require 'devil-macros)
(require 'devil-types)
(require 'devil-maps)
;;
(require 'evil-keybindings)
;;
(eval-and-compile
  (devil-negate (devil-load-history "evil-keybindings")))
;;
(provide 'devil-keybindings)
