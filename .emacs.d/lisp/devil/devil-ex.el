(require 'devil-common)
(require 'devil-states)
;;
(require 'evil-ex)
;;
;; TODO: Needed for `comint', for example:
(put 'evil-ex-commands 'permanent-local t)
;;
(let ((binding (cdr (assoc 'binding evil-ex-grammar))))
  (setcar binding (concat (if (equal (car binding)
                                     "[~&*@<>=:]+\\|[[:alpha:]-]+\\|!")
                              "[[:alpha:]-:]+\\|"
                            (devil-assert
                              (equal (car binding)
                                     "[~&*@<>=:]+\\|[[:alpha:]_]+\\|!"))
                            "[[:alpha:]_:]+\\|")
                          (car binding))))
;;
(eval-and-compile
  (defalias 'devil-ex-define-cmd #'evil-ex-define-cmd))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Of course completions don't work for locally defined Ex commands.  Most
;; probably the reason is that when point is in minibuffer, the buffer-local
;; binding of `evil-ex-commands' is no longer visible.
;;
(defun devil-ex-define-cmd-local
    (cmd function)
  (eval-when-compile
    (documentation 'devil-ex-define-cmd))
  (unless (local-variable-p 'evil-ex-commands)
    (setq-local evil-ex-commands (copy-alist evil-ex-commands)))
  (devil-ex-define-cmd cmd function))
;; ---------------------------------------------------------------------------
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; The following macros result in a dependency hell.  Turn them into
;; functions.
;;
(defmacro devil-ex-define-cmds
    (&rest ...)
  "Apply `devil-ex-define-cmd' to each two consecutive arguments.
Return list of results where each element is the return value of the
corresponding `devil-ex-define-cmd' application."
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-ex-define-cmd ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(list ,@body)))
;;
(defmacro devil-ex-define-cmds-local
    (&rest ...)
  "Apply `devil-ex-define-cmd-local' to each two consecutive arguments.
Return list of results where each element is the return value of the
corresponding `devil-ex-define-cmd-local' application."
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-ex-define-cmd-local ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(list ,@body)))
;; ---------------------------------------------------------------------------
;;
(provide 'devil-ex)
