(require 'devil-common)
(require 'devil-states)
(require 'devil-macros)
(require 'devil-search)
(require 'devil-isearch)
(require 'devil-types)
;;
(eval-when-compile
  (require 'cl-lib))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Fulfill the documentation.
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Redesign `evil-fill' because it does not respect `fill-paragraph-function'
;; and so, for instance, `lisp-fill-paragraph' does not work in
;; `emacs-lisp-mode' when `evil-fill' is invoked.
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Add the `devil-add-hjkl-bindings' function.
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Keep in mind that all copying (`devil-copy-command'), moving
;; (`devil-move-command'), aliasing (`devil-define-alias' or `defalias'), and
;; forwarding (`devil-call') of `evil' commands into `devil' commands that we
;; perform here should ideally be well-behaved and bug-free.  However, side
;; effects might still arise from usage (e.g. by `evil' internally) of
;; `last-command', `this-command', `evil-this-motion', and
;; `evil-this-operator' variables, unless `devil-call' already mitigates this
;; issue (for what there is no formal proof just yet).
;; ---------------------------------------------------------------------------
;;
(with-eval-after-load 'evil-commands
  (devil-remove-commands
    ;; -----------------------------------------------------------------------
    #'evil-next-visual-line
    #'evil-previous-visual-line
    ;;
    ;; BUG:
    ;;
    ;; These commands are severely bugged on first and last lines of buffer.
    ;; Furthermore, they behave incorrectly when horizontal scrolling is
    ;; involved.
    ;; -----------------------------------------------------------------------
    #'evil-next-match
    #'evil-previous-match
    ;;
    #'evil-search-forward
    #'evil-search-backward
    ;;
    #'evil-search-next
    #'evil-search-previous
    ;;
    #'evil-search-word-forward
    #'evil-search-word-backward
    ;;
    #'evil-search-unbounded-word-forward
    #'evil-search-unbounded-word-backward
    ;;
    #'evil-window-split
    #'evil-window-vsplit
    ;;
    #'evil-delete-buffer
    ;;
    #'evil-split-buffer
    #'evil-split-next-buffer
    #'evil-split-prev-buffer
    ;;
    #'evil-ret-gen
    #'evil-ret
    #'evil-ret-and-indent
    ;;
    #'evil-fill-and-move
    ;;
    #'evil-find-file-at-point-with-line
    ;;
    #'evil-indent-line
    ;;
    #'evil-join
    #'evil-join-whitespace
    ;;
    #'evil-make
    ;;
    #'evil-shift-right-line
    #'evil-shift-left-line)
  ;;
  (devil-move-commands
    #'evil-buffer 'evil-other-buffer
    ;;
    #'evil-delete                        'devil-delete
    #'evil-delete-line                   'devil-delete-to-eol
    ;;
    #'evil-delete-char                   'devil-delete-char-forward
    #'evil-delete-backward-char          'devil-delete-char-backward
    #'evil-delete-backward-char-and-join 'devil-delete-char-backward-and-unite
    ;;
    #'evil-edit 'devil-edit
    ;;
    #'evil-fill 'devil-fill
    ;;
    #'evil-indent 'devil-indent
    ;;
    #'evil-paste-after  'devil-paste-forward
    #'evil-paste-before 'devil-paste-backward
    ;;
    #'evil-replace 'devil-replace
    ;;
    #'evil-shell-command 'devil-shell-command
    ;;
    #'evil-shift-right 'devil-increase-indentation
    #'evil-shift-left  'devil-decrease-indentation
    ;;
    #'evil-window-mru 'evil-window-other)
  ;;
  (devil-copy-commands
    #'evil-forward-char  'devil-char-forward
    #'evil-backward-char 'devil-char-backward
    ;;
    #'evil-next-line     'devil-line-forward
    #'evil-previous-line 'devil-line-backward
    ;;
    #'evil-next-line-first-non-blank     'devil-line-forward-first-non-blank-char
    #'evil-previous-line-first-non-blank 'devil-line-backward-first-non-blank-char
    ;;
    #'evil-beginning-of-line 'devil-beg-of-line
    #'evil-end-of-line       'devil-end-of-line
    ;;
    #'evil-beginning-of-visual-line 'devil-beg-of-visual-line
    #'evil-end-of-visual-line       'devil-end-of-visual-line
    #'evil-middle-of-visual-line    'devil-mid-of-visual-line
    ;;
    #'evil-first-non-blank 'devil-first-non-blank-char
    #'evil-last-non-blank  'devil-last-non-blank-char
    ;;
    #'evil-forward-word-begin  'devil-beg-of-word-forward
    #'evil-backward-word-begin 'devil-beg-of-word-backward
    ;;
    #'evil-forward-word-end  'devil-end-of-word-forward
    #'evil-backward-word-end 'devil-end-of-word-backward
    ;;
    #'evil-forward-WORD-begin  'devil-beg-of-WORD-forward
    #'evil-backward-WORD-begin 'devil-beg-of-WORD-backward
    ;;
    #'evil-forward-WORD-end  'devil-end-of-WORD-forward
    #'evil-backward-WORD-end 'devil-end-of-WORD-backward
    ;;
    #'evil-forward-section-begin  'devil-beg-of-defun-forward
    #'evil-backward-section-begin 'devil-beg-of-defun-backward
    ;;
    #'evil-forward-section-end  'devil-end-of-defun-forward
    #'evil-backward-section-end 'devil-end-of-defun-backward
    ;;
    #'evil-change      'devil-amend
    #'evil-change-line 'devil-amend-to-eol
    ;;
    #'evil-insert 'devil-insert-at-bop
    #'evil-append 'devil-insert-at-eop)
  ;;
  (add-to-list 'evil-change-commands 'devil-amend)
  ;;
  (evil-set-command-property #'evil-yank-line :motion 'evil-end-of-line)
  ;;
  (evil-define-text-object evil-buffer
      (count &optional beg end type)
    ;; -----------------------------------------------------------------------
    ;; TODO:
    ;;
    ;; When applied to the `evil-format' operator (anything else?), save
    ;; excursion.  This might also be the wrong place to do that.
    ;; -----------------------------------------------------------------------
    "Select buffer."
    :type line
    (save-restriction
      (widen)
      (evil-range (point-min) (point-max))))
  (devil-copy-command #'evil-buffer 'evil-a-buffer     "Select a buffer.")
  (devil-copy-command #'evil-buffer 'evil-inner-buffer "Select inner buffer.")
  ;;
  (evil-define-text-object evil-a-slash
      (count &optional beg end type)
    "Select a slash."
    :extend-selection nil
    (evil-select-quote ?/ beg end type count t))
  ;;
  (evil-define-text-object evil-inner-slash
      (count &optional beg end type)
    "Select inner slash."
    :extend-selection nil
    (evil-select-quote ?/ beg end type count))
  ;;
  (evil-define-text-object evil-a-backslash
      (count &optional beg end type)
    "Select a backslash."
    :extend-selection nil
    (evil-select-quote ?\\ beg end type count t))
  ;;
  (evil-define-text-object evil-inner-backslash
      (count &optional beg end type)
    "Select inner backslash."
    :extend-selection nil
    (evil-select-quote ?\\ beg end type count))
  ;;
  (evil-define-text-object evil-a-grave-quote
      ;; ---------------------------------------------------------------------
      ;; NOTE:
      ;;
      ;; Name `grave' is inspired by the corresponding value of variable
      ;; `text-quoting-style' that reflects this quoting style of grave
      ;; accents (`) and apostrophes (') as per [1].
      ;; ---------------------------------------------------------------------
      (count &optional beg end type)
    "Select a grave-quoted expression."
    :extend-selection nil
    (evil-select-paren ?\` ?' beg end type count t))
  ;;
  (evil-define-text-object evil-inner-grave-quote
      ;; ---------------------------------------------------------------------
      ;; NOTE:
      ;;
      ;; Name `grave' is inspired by the corresponding value of variable
      ;; `text-quoting-style' that reflects this quoting style of grave
      ;; accents (`) and apostrophes (') as per [1].
      ;; ---------------------------------------------------------------------
      (count &optional beg end type)
    "Select inner grave-quoted expression."
    :extend-selection nil
    (evil-select-paren ?\` ?' beg end type count))
  ;;
  (evil-define-command evil-kill-buffer
      (buffer &optional bang)
    "\
Kill buffer BUFFER.

See the Info node `(emacs) Emacs Server'."
    :repeat nil
    (interactive "<b><!>")
    (with-current-buffer (or buffer (current-buffer))
      (when bang
        (set-buffer-modified-p nil)
        ;; TODO: Why not `(get-buffer-process (current-buffer))'?
        (dolist (process (process-list))
          (when (eq (process-buffer process) (current-buffer))
            (set-process-query-on-exit-flag process nil))))
      (let ((tramp-mode (bound-and-true-p tramp-mode)))
        (when bang
          (setq tramp-mode nil))
        (if (and (bound-and-true-p server-buffer-clients)
                 (fboundp 'server-edit))
            ;; If buffer BUFFER was initiated by 'emacsclient' (see the Info
            ;; node `(emacs) Emacs Server'), call `server-edit' to avoid the
            ;; message: "Buffer still has clients".
            (server-edit)
          (kill-buffer)))))
  (devil-define-alias #'evil-delete-buffer #'evil-kill-buffer)
  ;;
  (evil-define-command evil-kill-buffer-and-delete-windows
      (buffer &optional bang)
    "\
Kill buffer BUFFER and delete windows displaying it.

All windows currently displaying buffer BUFFER will be deleted
except for the last window in each frame.

See the Info node `(emacs) Emacs Server'."
    :repeat nil
    (interactive "<b><!>")
    (let ((window-list (get-buffer-window-list (or buffer (current-buffer))
                                               nil
                                               'all-frames)))
      (devil-call evil-kill-buffer buffer bang)
      (mapc #'(lambda
                  (window)
                (ignore-errors (delete-window window)))
            window-list)))
  (devil-define-aliases
    'evil-delete-buffer-and-windows #'evil-kill-buffer-and-delete-windows
    'evil-quit-buffer-and-windows   #'evil-kill-buffer-and-delete-windows
    'evil-quit-buffer               #'evil-kill-buffer-and-delete-windows))
;;
(require 'evil-commands)
;;
;; ---------------------------------------------------------------------------
(evil-define-motion devil-visual-line-forward
    (count)
  "Move point COUNT screen lines forward."
  :type exclusive
  (devil-without-window-hscroll
    (let ((line-move-visual t))
      (evil-line-move (+ (or count 1))))))
(devil-remove-command #'devil-visual-line-forward)
;;
(evil-define-motion devil-visual-line-backward
    (count)
  "Move point COUNT screen lines backward."
  :type exclusive
  (devil-without-window-hscroll
    (let ((line-move-visual t))
      (evil-line-move (- (or count 1))))))
(devil-remove-command #'devil-visual-line-backward)
;;
;; BUG:
;;
;; These commands are severely bugged on first and last lines of buffer,
;; similar to `evil-next-visual-line' and `evil-previous-visual-line'.
;; ---------------------------------------------------------------------------
;;
(evil-define-motion devil-first-line ()
  "Move point to the first (top) line of buffer."
  :jump t
  :type line
  (unwind-protect
      (devil-signal-at-first-line)
    (ignore-errors (evil-previous-line)))
  (evil-save-goal-column (goto-char (point-min)))
  (let ((last-command #'previous-line))
    (ignore-errors (evil-previous-line))))
(devil-define-alias 'devil-top-of-buffer #'devil-first-line)
;;
(evil-define-motion devil-last-line ()
  "Move point to the last (bottom) line of buffer."
  :jump t
  :type line
  (unwind-protect
      (devil-signal-at-last-line)
    (ignore-errors (evil-next-line)))
  (evil-save-goal-column (goto-char (point-max)))
  (let ((last-command #'next-line))
    (ignore-errors (evil-next-line))))
(devil-define-alias 'devil-bot-of-buffer #'devil-last-line)
;;
(evil-define-motion devil-beg-of-buffer ()
  "Move point to the beginning of buffer."
  :jump t
  :type exclusive
  (devil-signal-at-bob)
  (goto-char (point-min)))
;;
(evil-define-motion devil-end-of-buffer ()
  "Move point to the end of buffer."
  :jump t
  :type inclusive
  (devil-signal-at-eob)
  (goto-char (point-max)))
;;
(evil-define-motion devil-beg-of-symbol-forward
    (count)
  "Move point to the beginning of COUNT-th symbol forward."
  :type exclusive
  (evil-signal-at-bob-or-eob count)
  (evil-forward-beginning 'evil-symbol count))
;;
(evil-define-motion devil-beg-of-symbol-backward
    (count)
  "Move point to the beginning of COUNT-th symbol backward."
  :type exclusive
  (evil-signal-at-bob-or-eob (- (or count 1)))
  (evil-backward-beginning 'evil-symbol count))
;;
(evil-define-motion devil-end-of-symbol-forward
    (count)
  "Move point to the end of COUNT-th symbol forward."
  :type inclusive
  (evil-signal-at-bob-or-eob count)
  (evil-forward-end 'evil-symbol count))
;;
(evil-define-motion devil-end-of-symbol-backward
    (count)
  "Move point to the end of COUNT-th symbol backward."
  :type inclusive
  (evil-signal-at-bob-or-eob (- (or count 1)))
  (evil-backward-end 'evil-symbol count))
;;
(evil-define-motion devil-beg-of-sentence-forward
    (count)
  "Move point to the beginning of COUNT-th sentence forward."
  :jump t
  :type exclusive
  (evil-signal-at-bob-or-eob count)
  (evil-forward-beginning 'evil-sentence count))
;;
(evil-define-motion devil-beg-of-sentence-backward
    (count)
  "Move point to the beginning of COUNT-th sentence backward."
  :jump t
  :type exclusive
  (evil-signal-at-bob-or-eob (- (or count 1)))
  (evil-backward-beginning 'evil-sentence count))
;;
(evil-define-motion devil-end-of-sentence-forward
    (count)
  "Move point to the end of COUNT-th sentence forward."
  :jump t
  :type inclusive
  (evil-signal-at-bob-or-eob count)
  (evil-forward-end 'evil-sentence count))
;;
(evil-define-motion devil-end-of-sentence-backward
    (count)
  "Move point to the end of COUNT-th sentence backward."
  :jump t
  :type inclusive
  (evil-signal-at-bob-or-eob (- (or count 1)))
  (evil-backward-end 'evil-sentence count))
;;
(evil-define-motion devil-beg-of-paragraph-forward
    (count)
  "Move point to the beginning of COUNT-th paragraph forward."
  :jump t
  :type exclusive
  (evil-signal-at-bob-or-eob count)
  (evil-forward-beginning 'evil-paragraph count))
;;
(evil-define-motion devil-beg-of-paragraph-backward
    (count)
  "Move point to the beginning of COUNT-th paragraph backward."
  :jump t
  :type exclusive
  (evil-signal-at-bob-or-eob (- (or count 1)))
  (evil-backward-beginning 'evil-paragraph count))
;;
(evil-define-motion devil-end-of-paragraph-forward
    (count)
  "Move point to the end of COUNT-th paragraph forward."
  :jump t
  :type inclusive
  (evil-signal-at-bob-or-eob count)
  (when (and (not (eolp)) (devil-eol-p)) (forward-char))
  (evil-forward-end 'evil-paragraph count))
;;
(evil-define-motion devil-end-of-paragraph-backward
    (count)
  "Move point to the end of COUNT-th paragraph backward."
  :jump t
  :type inclusive
  (evil-signal-at-bob-or-eob (- (or count 1)))
  (evil-backward-end 'evil-paragraph count))
;;
(defun devil-ret
    (count indent-p)
  "\
If point is on a widget or a button, click on it.
If in Insert state, insert COUNT newlines (and indent).
"
  (let* ((field  (get-char-property (point) 'field))
         (button (get-char-property (point) 'button))
         (doc    (get-char-property (point) 'widget-doc))
         (widget (or field button doc)))
    (cond ((and widget
                (fboundp 'widget-type)
                (fboundp 'widget-button-press)
                (or (and (symbolp widget)
                         (get widget 'widget-type))
                    (and (consp widget)
                         (get (widget-type widget) 'widget-type))))
           (when (evil-operator-state-p)
             (setq evil-inhibit-operator t))
           (when (fboundp 'widget-button-press)
             (widget-button-press (point)))
           t)
          ((and (fboundp 'button-at)
                (fboundp 'push-button)
                (button-at (point)))
           (when (evil-operator-state-p)
             (setq evil-inhibit-operator t))
           (push-button)
           t)
          ((or (evil-emacs-state-p)
               (and (evil-insert-state-p)
                    (not buffer-read-only)))
           (if (not indent-p)
               (newline count)
             (delete-horizontal-space :backward-only)
             (newline count)
             (indent-according-to-mode))
           t)
          (t
           nil))))
;;
(defun devil-visual-count-lines ()
  "\
Return number of lines between point and mark in the Visual state.
"
  (when (evil-visual-state-p)
    (save-excursion
      (let ((mark (mark)))
        (unwind-protect
            (progn
              (evil-visual-rotate 'upper-left)
              (count-lines evil-visual-beginning
                           evil-visual-end))
          (set-mark mark))))))
;;
(evil-define-motion devil-insert-at-bol
    (count &optional vcount)
  ;; TODO:
  ;; (eval-when-compile (documentation 'evil-insert-line))
  :type line
  (unless (devil-ret count :indent-p)
    (if (or buffer-read-only
            (not (or (evil-normal-state-p)
                     (evil-visual-state-p))))
        (if (devil-bol-p)
            (devil-line-backward-first-non-blank-char count)
          (devil-beg-of-line))
      (when (evil-visual-state-p)
        (unless (memq (evil-visual-type) '(line block))
          (evil-visual-select evil-visual-beginning
                              evil-visual-end
                              evil-visual-line))
        (evil-visual-rotate 'upper-left))
      (evil-insert-line count (if (called-interactively-p 'any)
                                  (devil-visual-count-lines)
                                vcount)))))
;;
(evil-define-motion devil-insert-at-eol
    (count &optional vcount)
  ;; TODO:
  ;; (eval-when-compile (documentation 'evil-append-line))
  :type line
  (unless (devil-ret count :indent-p)
    (if (or buffer-read-only
            (not (or (evil-normal-state-p)
                     (evil-visual-state-p))))
        (if (devil-eol-p)
            (devil-line-forward-first-non-blank-char count)
          (devil-end-of-line))
      (when (evil-visual-state-p)
        (unless (memq (evil-visual-type) '(line block))
          (evil-visual-select evil-visual-beginning
                              evil-visual-end
                              evil-visual-line))
        (evil-visual-rotate 'upper-left))
      (evil-append-line count (if (called-interactively-p 'any)
                                  (devil-visual-count-lines)
                                vcount)))))
;;
(defun devil-select-inner-comment
    (beg end type)
  ;; -------------------------------------------------------------------------
  ;; BUG:
  ;;
  ;; `evil-in-comment-p' fails in the following situations ("|" denotes
  ;; `point' position):
  ;;
  ;; /* Hello, *|//* world! */
  ;; /* Hello, *|/// world!
  ;;
  ;; In other words, when there is a block comment immediately followed by
  ;; either another block comment or a trailing line comment, and `point' is
  ;; at "|" (see above example), then `evil-in-comment-p' gets confused and
  ;; does NOT determine that the `point' is actually in the first block
  ;; comment, i.e. `evil-in-comment-p' simply returns nil in this case.  The
  ;; source of confusion are probably two adjacent forward slashes "//" which
  ;; actually correspond to two different comments accordingly.
  ;;
  ;; NOTE:
  ;;
  ;; This corner case is insanely rare to happen in real life.
  ;; -------------------------------------------------------------------------
  "Return inner comment range."
  (save-excursion
    (goto-char beg)
    (let ((beg-in-comment-p (evil-in-comment-p)))
      (when beg-in-comment-p
        (setq beg beg-in-comment-p))
      (if (> (- end beg) 1)
          (progn
            (goto-char (1- end))
            (when (evil-in-comment-p)
              (while (not (evil-looking-at-end-comment t))
                (forward-char))
              (setq end (point))))
        (when beg-in-comment-p
          (while (not (evil-looking-at-end-comment t))
            (forward-char))
          (setq end (point))))))
  (evil-range beg end type))
;;
(evil-define-text-object devil-inner-comment
    (count &optional beg end type)
  ;; -------------------------------------------------------------------------
  ;; TODO:
  ;;
  ;; Works in the Visual state, but fails in the Operator state.
  ;; -------------------------------------------------------------------------
  "Select inner comment."
  (devil-select-inner-comment beg end nil))
;;
(evil-define-text-object devil-search-match-next
    (count &optional beg end type)
  "Select next search match."
  :extend-selection nil
  :jump t
  :type inclusive
  (when (memq evil-this-operator devil-search-operators)
    (setq evil-inhibit-operator t))
  (devil-operator-state-save-excursion
    (devil-visual-state-save-excursion
      (let (range)
        (dotimes (var (or count 1) range)
          (setq range
            (let ((devil-isearch-forward devil-isearch-forward))
              (devil-isearch (or (devil-search-ring devil-search-regexp-p) "")
                             devil-isearch-forward
                             devil-search-regexp-p))))))))
;;
(evil-define-text-object devil-search-match-previous
    (count &optional beg end type)
  "Select previous search match."
  :extend-selection nil
  :jump t
  :type inclusive
  (when (memq evil-this-operator devil-search-operators)
    (setq evil-inhibit-operator t))
  (devil-operator-state-save-excursion
    (devil-visual-state-save-excursion
      (let (range)
        (dotimes (var (or count 1) range)
          (setq range
            (let ((devil-isearch-forward (not devil-isearch-forward)))
              (devil-isearch (or (devil-search-ring devil-search-regexp-p) "")
                             devil-isearch-forward
                             devil-search-regexp-p))))))))
;;
(cl-macrolet
    ((define-search-thing (thing forward)
       (let* ((direction (if forward 'forward 'backward))
              (name (intern (format "devil-search-%s-%s"
                                    thing
                                    direction))))
         `(evil-define-text-object ,name
              (count &optional beg end type)
            ,(format "\
Search and select `%s' at point %s."
                     thing
                     direction)
            :extend-selection nil
            :jump t
            :type inclusive
            (when (memq evil-this-operator devil-search-operators)
              (setq evil-inhibit-operator t))
            (devil-operator-state-save-excursion
              (devil-visual-state-save-excursion
                (devil-isearch-thing ',thing
                                     ,forward
                                     devil-search-thing-bounded-p
                                     devil-search-history-p)))))))
  (define-search-thing evil-word   t)
  (define-search-thing evil-word   nil)
  ;;
  (define-search-thing evil-WORD   t)
  (define-search-thing evil-WORD   nil)
  ;;
  (define-search-thing evil-symbol t)
  (define-search-thing evil-symbol nil))
;;
(devil-define-aliases
  'devil-search-word-forward    #'devil-search-evil-word-forward
  'devil-search-word-backward   #'devil-search-evil-word-backward
  ;;
  'devil-search-WORD-forward    #'devil-search-evil-WORD-forward
  'devil-search-WORD-backward   #'devil-search-evil-WORD-backward
  ;;
  'devil-search-symbol-forward  #'devil-search-evil-symbol-forward
  'devil-search-symbol-backward #'devil-search-evil-symbol-backward)
;;
(evil-define-operator devil-comment
    (beg end type register yank-handler)
  "\
Comment text from BEG to END with TYPE."
  (interactive "*<R><x><y>")
  (unless register
    (let ((text (filter-buffer-substring beg end)))
      (unless (string-match-p "\n" text)
        (evil-set-register ?- text))))
  (let ((evil-was-yanked-without-register nil))
    (evil-yank beg end type register yank-handler))
  (let ((range (devil-select-inner-comment beg end type)))
    (setq beg (evil-range-beginning range))
    (setq end (evil-range-end       range))
    (comment-normalize-vars)
    (if (comment-only-p   beg end)
        (uncomment-region beg end)
      (if (eq type 'block)
          (devil-apply-on-block #'(lambda
                                      (beg end)
                                    (ignore-errors (comment-region beg end)))
                                beg
                                end
                                nil)
        (comment-region beg end))))
  (when (and (called-interactively-p 'any)
             (eq type 'line))
    (evil-first-non-blank)))
;;
(evil-define-operator devil-comment-to-eol
    (beg end type register yank-handler)
  "\
Comment text to the end of current line."
  :motion evil-end-of-line
  (interactive "*<R><x><y>")
  (devil-comment beg end type register yank-handler))
;;
(defalias 'devil-search-backward--yank
  ;; TODO: `defcustom' in `devil-search':
  #'evil-inner-symbol
  "\
Pseudo motion used as operator shortcut by `devil-search-backward'.

Forwards to interactively call `devil-search-input-backward'.")
;;
(defalias 'devil-search-forward--yank
  ;; TODO: `defcustom' in `devil-search':
  #'evil-inner-symbol
  "\
Pseudo motion used as operator shortcut by `devil-search-forward'.

Forwards to interactively call `devil-search-input-forward'.")
;;
;; TODO: Repeat breaks editing repeat:
(evil-define-operator devil-search-backward
    (beg end)
  "\
Search for text from BEG to END backward."
  (interactive
   (let (evil-surround-mode)
     (devil-with-extended-operator-state-map devil-search-backward-map
       (let ((keymap (make-sparse-keymap)))
         (devil-define-operator-shortcut   keymap #'ignore)
         (define-key                       keymap (kbd "y")
           #'devil-search-backward--yank)
         (devil-with-operator-shortcut-map keymap
           (evil-operator-range))))))
  (if (and (called-interactively-p 'any)
           (or (= beg end)
               (eq evil-this-motion #'devil-search-backward--yank)))
      (let ((devil-inhibit-interactive t)
            (devil-search-range        (evil-range beg end)))
        (devil-call devil-search-input-backward beg end))
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; It is not a hard requirement to exit the Visual state, but rather a
    ;; common practice for operators.
    ;;
    (when (evil-visual-state-p) (evil-exit-visual-state))
    ;; -----------------------------------------------------------------------
    (devil-isearch-region beg
                          end
                          nil
                          devil-search-regexp-p
                          devil-search-history-p)))
;; (evil-set-command-property #'devil-search-backward :suppress-operator nil)
;;
;; TODO: Repeat breaks editing repeat:
(evil-define-operator devil-search-forward
    (beg end)
  "\
Search for text from BEG to END forward."
  (interactive
   (let (evil-surround-mode)
     (devil-with-extended-operator-state-map devil-search-forward-map
       (let ((keymap (make-sparse-keymap)))
         (devil-define-operator-shortcut   keymap #'ignore)
         (define-key                       keymap (kbd "y")
           #'devil-search-forward--yank)
         (devil-with-operator-shortcut-map keymap
           (evil-operator-range))))))
  (if (and (called-interactively-p 'any)
           (or (= beg end)
               (eq evil-this-motion #'devil-search-forward--yank)))
      (let ((devil-inhibit-interactive t)
            (devil-search-range        (evil-range beg end)))
        (devil-call devil-search-input-forward beg end))
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; It is not a hard requirement to exit the Visual state, but rather a
    ;; common practice for operators.
    ;;
    (when (evil-visual-state-p) (evil-exit-visual-state))
    ;; -----------------------------------------------------------------------
    (devil-isearch-region beg
                          end
                          t
                          devil-search-regexp-p
                          devil-search-history-p)))
(devil-ignore-progn
  ;; TODO:
  (evil-set-command-property #'devil-search-forward :suppress-operator nil))
;;
(evil-define-operator devil-search-input-backward
    (beg end)
  "\
Search for text from BEG to END as input backward."
  ;; TODO:
  ;; :repeat evil-repeat-search
  :repeat nil
  (interactive
   (if (bound-and-true-p devil-inhibit-interactive)
       (bound-and-true-p devil-search-range)
     (let (evil-surround-mode)
       (devil-with-extended-operator-state-map devil-search-input-backward-map
         (let ((keymap (make-sparse-keymap)))
           (devil-define-operator-shortcut   keymap #'ignore)
           (devil-with-operator-shortcut-map keymap
             (evil-operator-range)))))))
  ;; -------------------------------------------------------------------------
  ;; NOTE:
  ;;
  ;; It is not a hard requirement to exit the Visual state, but rather a
  ;; common practice for operators.
  ;;
  (when (evil-visual-state-p) (evil-exit-visual-state))
  ;; -------------------------------------------------------------------------
  (devil-isearch-input-region beg
                              end
                              nil
                              devil-search-regexp-p
                              devil-search-history-p))
(devil-ignore-progn
  ;; TODO:
  (evil-set-command-property #'devil-search-input-backward :suppress-operator nil))
;;
(evil-define-operator devil-search-input-forward
    (beg end)
  "\
Search for text from BEG to END as input forward."
  ;; TODO:
  ;; :repeat evil-repeat-search
  :repeat nil
  (interactive
   (if (bound-and-true-p devil-inhibit-interactive)
       (bound-and-true-p devil-search-range)
     (let (evil-surround-mode)
       (devil-with-extended-operator-state-map devil-search-input-forward-map
         (let ((keymap (make-sparse-keymap)))
           (devil-define-operator-shortcut   keymap #'ignore)
           (devil-with-operator-shortcut-map keymap
             (evil-operator-range)))))))
  ;; -------------------------------------------------------------------------
  ;; NOTE:
  ;;
  ;; It is not a hard requirement to exit the Visual state, but rather a
  ;; common practice for operators.
  ;;
  (when (evil-visual-state-p) (evil-exit-visual-state))
  ;; -------------------------------------------------------------------------
  (devil-isearch-input-region beg
                              end
                              t
                              devil-search-regexp-p
                              devil-search-history-p))
(devil-ignore-progn
  ;; TODO:
  (evil-set-command-property #'devil-search-input-forward :suppress-operator nil))
;;
(evil-define-operator devil-align-regexp
    (beg end regexp &optional group spacing repeat)
  "\
Align lines from BEG to END according to the regular expression REGEXP."
  :move-point nil
  :type line
  (devil-save-excursion
    (evil-with-state 'normal
      (goto-char beg)
      (evil-visual-state)
      (goto-char end)
      ;; TODO: When entering a regular expression in minibuffer, the shape of
      ;; the cursor does not reflect the Insert state:
      (let ((current-prefix-arg t))
        (devil-call align-regexp beg end regexp group spacing repeat)))))
;;
(evil-define-operator devil-format
    (beg end)
  "\
Format (indent) lines from BEG to END."
  :move-point nil
  :type line
  ;; In the Visual state saves column and in the Operator state saves
  ;; position:
  (devil-save-excursion
    ;; TODO: Looks like `devil-save-excursion' causes that weird jumping upon
    ;; formatting!
    (funcall devil-format-region-function beg end)))
;;
(evil-define-operator devil-sort
    (beg end)
  "\
Sort lines from BEG to END."
  :move-point nil
  :type line
  (devil-save-excursion
    (sort-lines nil beg end)))
;;
(defun devil-unite-line
    (beg end direction)
  "\
Unite current line with adjacent line in DIRECTION.

If selection from BEG to END contains more than one line, unite selected lines."
  (let ((arg (cond ((eq direction 'forward)
                    1)
                   ((eq direction 'backward)
                    nil)
                   (t
                    (error "Unknown direction: %s" direction))))
        (count (count-lines beg end)))
    (when (> count 1)
      (setq arg 1)
      (setq count (1- count)))
    (dotimes (var count)
      (join-line arg))))
;;
(evil-define-operator devil-unite-line-forward
    (beg end)
  "\
Unite current line with next line.

If selection from BEG to END contains more than one line, unite selected lines."
  :motion evil-line
  :move-point nil
  (interactive "*<r>")
  (devil-unite-line beg end 'forward))
;;
(evil-define-operator devil-unite-line-backward
    (beg end)
  "\
Unite current line with previous line.

If selection from BEG to END contains more than one line, unite selected lines."
  :motion evil-line
  :move-point nil
  (interactive "*<r>")
  (devil-unite-line beg end 'backward))
;;
(evil-define-operator devil-upcase
    (beg end type)
  "\
Convert text from BEG to END with TYPE to uppercase."
  (interactive
   (let ((keymap (make-sparse-keymap)))
     (devil-define-operator-shortcut   keymap #'evil-forward-char)
     (devil-with-operator-shortcut-map keymap
       (evil-operator-range :return-type))))
  (evil-upcase beg end type)
  (when evil-this-motion
    (goto-char end)
    (when (and evil-cross-lines
               evil-move-cursor-back
               (not (evil-visual-state-p))
               (not (evil-operator-state-p))
               (eolp)
               (not (bolp))
               (not (eobp)))
      (forward-char))))
;;
(evil-define-operator devil-downcase
    (beg end type)
  "\
Convert text from BEG to END with TYPE to lowercase."
  (interactive
   (let ((keymap (make-sparse-keymap)))
     (devil-define-operator-shortcut   keymap #'evil-forward-char)
     (devil-with-operator-shortcut-map keymap
       (evil-operator-range :return-type))))
  (evil-downcase beg end type)
  (when evil-this-motion
    (goto-char end)
    (when (and evil-cross-lines
               evil-move-cursor-back
               (not (evil-visual-state-p))
               (not (evil-operator-state-p))
               (eolp)
               (not (bolp))
               (not (eobp)))
      (forward-char))))
;;
(evil-define-operator devil-swapcase
    (beg end type)
  "\
Invert case of text from BEG to END with TYPE."
  (interactive
   (let ((keymap (make-sparse-keymap)))
     (devil-define-operator-shortcut   keymap #'evil-forward-char)
     (devil-with-operator-shortcut-map keymap
       (evil-operator-range :return-type))))
  (evil-invert-case beg end type)
  (when evil-this-motion
    (goto-char end)
    (when (and evil-cross-lines
               evil-move-cursor-back
               (not (evil-visual-state-p))
               (not (evil-operator-state-p))
               (eolp)
               (not (bolp))
               (not (eobp)))
      (forward-char))))
;;
;; ---------------------------------------------------------------------------
;; DEPRECATED:
;;
;; Because of `evil-kill-on-visual-paste':
;;
(devil-ignore-progn
  (define-advice devil-delete
      (:before (beg end type &optional register yank-handler) set-register)
    (when (and (not register)
               (not (eq this-command #'evil-visual-paste)))
      (evil-set-register ?0 (filter-buffer-substring beg end)))))
;; ---------------------------------------------------------------------------
;;
;; ---------------------------------------------------------------------------
;; BUG:
;;
;; Appears to misbehave and maybe does not work properly in the terminal
;; variant of Emacs.  Reconsider whether to improve further or drop:
;;
(devil-ignore-progn
  (devil-move-command #'evil-paste-after 'devil--paste-forward)
  (evil-define-command devil-paste-forward
      (count &optional register yank-handler)
    "\
Paste last copied text just after (below) point.

Return value is the copied text."
    ;; TODO:
    ;; (eval-when-compile (documentation 'evil-paste-after))
    (interactive "*i")
    (let ((evil-this-register evil-this-register))
      (when (and (called-interactively-p 'any)
                 (evil-visual-state-p))
        (let ((  primary-text (evil-get-register ?* 'noerror))
              (clipboard-text (evil-get-register ?+ 'noerror)))
          (when (equal primary-text clipboard-text)
            (evil-set-register ?0 primary-text)
            (evil-set-register ?* nil)))
        (setq evil-this-register ?0))
      (devil-call devil--paste-forward count register yank-handler)))
  (devil-define-alias #'evil-paste-after #'devil-paste-forward))
;;
(devil-ignore-progn
  (devil-move-command #'evil-paste-before 'devil--paste-backward)
  (evil-define-command devil-paste-backward
      (count &optional register yank-handler)
    "\
Paste last copied text just before (above) point.

Return value is the copied text."
    ;; TODO:
    ;; (eval-when-compile (documentation 'evil-paste-before))
    (interactive "*i")
    (let ((evil-this-register evil-this-register))
      (when (and (called-interactively-p 'any)
                 (evil-visual-state-p))
        (let ((  primary-text (evil-get-register ?* 'noerror))
              (clipboard-text (evil-get-register ?+ 'noerror)))
          (when (equal primary-text clipboard-text)
            (evil-set-register ?0 primary-text)
            (evil-set-register ?* nil)))
        (setq evil-this-register ?0))
      (devil-call devil--paste-backward count register yank-handler)))
  (devil-define-alias #'evil-paste-before #'devil-paste-backward))
;; ---------------------------------------------------------------------------
;;
(evil-define-command devil-open-line-forward
    (count)
  "\
Open COUNT new lines just below point."
  ;; TODO:
  ;; (eval-when-compile (documentation 'evil-open-below))
  :suppress-operator t
  (interactive "*i")
  (evil-save-state
    (devil-call evil-open-below count)))
(devil-copy-command-properties
  #'evil-open-below #'devil-open-line-forward :add-p)
;;
(evil-define-command devil-open-line-backward
    (count)
  "\
Open COUNT new lines just above point."
  ;; TODO:
  ;; (eval-when-compile (documentation 'evil-open-above))
  :suppress-operator t
  (interactive "*i")
  (evil-save-state
    (devil-call evil-open-above count)))
(devil-copy-command-properties
  #'evil-open-above #'devil-open-line-backward :add-p)
;;
(evil-define-command devil-newline-forward
    (count)
  "\
Insert COUNT newlines just after point."
  :suppress-operator t
  (interactive "*<c>")
  (save-excursion
    (forward-char)
    (delete-horizontal-space 'backward-only)
    (newline count)
    (indent-according-to-mode)))
;;
(evil-define-command devil-newline-backward
    (count)
  "\
Insert COUNT newlines just before point."
  :suppress-operator t
  (interactive "*<c>")
  (delete-horizontal-space 'backward-only)
  (newline count)
  (indent-according-to-mode))
;;
(evil-define-command devil-repeat
    (count &optional save-point-p)
  "\
Repeat last editing command COUNT times.

If SAVE-POINT-P is non-nil, do not move point."
  ;; TODO:
  ;; (eval-when-compile (documentation 'evil-repeat))
  (interactive "*i")
  (devil-call evil-repeat count save-point-p))
(devil-copy-command-properties #'evil-repeat #'devil-repeat)
;;
(defun devil-swap-buffer
    (window &optional buffer-or-name)
  "\
Swap BUFFER-OR-NAME with the buffer displayed in window WINDOW.

If BUFFER-OR-NAME is omitted or nil, it defaults to the current buffer.

Return nil."
  (let ((buffer (window-buffer window)))
    (set-window-buffer window (or buffer-or-name (current-buffer)))
    (condition-case e
        (set-window-buffer (selected-window) buffer)
      (error (set-window-buffer window buffer)
             (signal (car e) (cdr e))))))
;;
(evil-define-command devil-swap-buffer-right
    (count)
  "\
Swap the buffer with the COUNT-th buffer right of the current one."
  :repeat nil
  (interactive "p")
  (unless (window-minibuffer-p)
    (let ((window (save-selected-window
                    (evil-window-right count)
                    (while (window-minibuffer-p)
                      (evil-window-right 1))
                    (selected-window))))
      (devil-swap-buffer window)
      (select-window window))))
;;
(evil-define-command devil-swap-buffer-left
    (count)
  "\
Swap the buffer with the COUNT-th buffer left of the current one."
  :repeat nil
  (interactive "p")
  (unless (window-minibuffer-p)
    (let ((window (save-selected-window
                    (evil-window-left count)
                    (while (window-minibuffer-p)
                      (evil-window-left 1))
                    (selected-window))))
      (devil-swap-buffer window)
      (select-window window))))
;;
(evil-define-command devil-swap-buffer-down
    (count)
  "\
Swap the buffer with the COUNT-th buffer below of the current one."
  :repeat nil
  (interactive "p")
  (unless (window-minibuffer-p)
    (let ((window (save-selected-window
                    (evil-window-down count)
                    (while (window-minibuffer-p)
                      (evil-window-down 1))
                    (selected-window))))
      (devil-swap-buffer window)
      (select-window window))))
;;
(evil-define-command devil-swap-buffer-up
    (count)
  "\
Swap the buffer with the COUNT-th buffer above of the current one."
  :repeat nil
  (interactive "p")
  (unless (window-minibuffer-p)
    (let ((window (save-selected-window
                    (evil-window-up count)
                    (while (window-minibuffer-p)
                      (evil-window-up 1))
                    (selected-window))))
      (devil-swap-buffer window)
      (select-window window))))
;;
(evil-define-command devil-scroll-right
    (count)
  "\
Scroll window and point COUNT columns rightward.
If COUNT is nil, use `devil-scroll-right-default-count-function'."
  (interactive "P")
  (devil-signal-at-eol)
  (setq count (or count (funcall devil-scroll-right-default-count-function)))
  (let ((column (+ (current-column) count)))
    (when (and truncate-lines (< (window-body-width) column))
      (save-excursion (scroll-left count)))
    (move-to-column column)))
(devil-copy-command-properties #'evil-scroll-right #'devil-scroll-right)
;;
(evil-define-command devil-scroll-left
    (count)
  "\
Scroll window and point COUNT columns leftward.
If COUNT is nil, use `devil-scroll-left-default-count-function'."
  (interactive "P")
  (devil-signal-at-bol)
  (setq count (or count (funcall devil-scroll-left-default-count-function)))
  (let ((column (- (current-column) count)))
    (when (and truncate-lines (> (window-hscroll) 0))
      (save-excursion (scroll-right count)))
    (move-to-column (max column 0))))
(devil-copy-command-properties #'evil-scroll-left #'devil-scroll-left)
;;
;; TODO: Review the latest variant of `evil-scroll-down':
(evil-define-command devil-scroll-down
    (count)
  "\
Scroll window and point COUNT lines downward.
If COUNT is nil, use `devil-scroll-down-default-count-function'."
  (interactive "P")
  (unwind-protect
      (progn
        (devil-signal-at-last-line)
        (setq count
          (or count (funcall devil-scroll-down-default-count-function)))
        (let ((arg (- count (save-excursion (forward-line count)))))
          (save-excursion (scroll-up arg))))
    (ignore-errors (evil-next-line count))
    ;; If we're at the end of buffer, let the last line be at the bottom:
    (let ((beg (window-start))
          (end (window-end nil 'update)))
      (when (= end (point-max))
        (scroll-down (1- (- (1- (window-height)) (count-lines beg end))))
        (ignore-errors (evil-next-line 0))))))
(devil-copy-command-properties #'evil-scroll-down #'devil-scroll-down)
;;
;; TODO: Review the latest variant of `evil-scroll-up':
(evil-define-command devil-scroll-up
    (count)
  "\
Scroll window and point COUNT lines upward.
If COUNT is nil, use `devil-scroll-up-default-count-function'."
  (interactive "P")
  (unwind-protect
      (progn
        (devil-signal-at-first-line)
        (setq count
          (or count (funcall devil-scroll-up-default-count-function)))
        (let ((arg (max 0 (+ count (save-excursion
                                     (goto-char (window-start))
                                     (forward-line (- count)))))))
          (save-excursion (scroll-down arg))))
    (ignore-errors (evil-previous-line count))))
(devil-copy-command-properties #'evil-scroll-up #'devil-scroll-up)
;;
(evil-define-command devil-split-window-sensibly
    (&optional file)
  "\
Split the current window sensibly, editing file FILE.

If `evil-auto-balance-windows' is non-nil, all the children windows of the
parent window of the split window are rebalanced (with `balance-windows')."
  :repeat nil
  (interactive "<f>")
  (split-window-sensibly)
  (when evil-auto-balance-windows
    (balance-windows (window-parent)))
  (when file
    (devil-edit file)))
(devil-define-alias 'devil-split-window #'devil-split-window-sensibly)
;;
(evil-define-command devil-sudo
    (file &optional bang)
  "Edit FILE with root privileges."
  (interactive "<f><!>")
  (let (method)
    (if file
        (setq method (file-remote-p file 'method))
      (setq file (or (buffer-file-name)
                     (file-name-as-directory default-directory)))
      (when (equal (setq method (file-remote-p file 'method)) "sudo")
        (setq file nil)))
    (when file
      (setq file (expand-file-name file))
      (unless (equal method "sudo")
        (let ((buffer (get-file-buffer file)))
          (when buffer
            (with-current-buffer buffer
              (devil-call evil-kill-buffer buffer bang))))
        (let ((remote (file-remote-p file)))
          (unless (zerop (length remote))
            (aset remote (1- (length remote)) ?|))
          (setq file (concat (or remote "/")
                             "sudo:"
                             (file-remote-p file 'host)
                             ":"
                             (or (file-remote-p file 'localname) file))))))
    (devil-edit file bang)))
;;
(eval-when-compile
  (require 'dired nil :noerror)
  (require 'ffap  nil :noerror)
  (require 'itail nil :noerror))
(evil-define-command devil-tail
    (file &optional count)
  "Run tail on FILE."
  :repeat nil
  (interactive "<f>P")
  (setq file (or file (or (and (eq major-mode 'dired-mode)
                               (fboundp 'dired-get-filename)
                               (dired-get-filename nil
                                                   :no-error-if-not-filep))
                          (when (or (fboundp 'ffap-file-at-point)
                                    (and (require 'ffap nil :noerror)
                                         (fboundp 'ffap-file-at-point)))
                            (ffap-file-at-point)))))
  (if (and (not (zerop (length file)))
           (file-directory-p file))
      (devil-call itail file count)
    (itail file count)))
;;
(eval-when-compile
  (require 'dired-x nil :noerror))
(evil-define-command devil-dired
    (file &optional other-window-p)
  "\
Run `dired-jump' for FILE."
  :repeat nil
  (interactive "<f><!>")
  (dired-jump other-window-p file))
;;
(evil-define-command devil-compile
    (command &optional new-p)
  "\
Call compilation command in the current directory.

If COMMAND is nil, call `recompile', otherwise call `compile' passing COMMAND
as the first argument."
  (interactive "<sh><!>")
  (when new-p
    (let ((buffer (get-buffer "*compilation*")))
      (when buffer
        (with-current-buffer buffer
          (rename-uniquely)))))
  (if (and (fboundp 'recompile) (not command))
      (recompile)
    (compile (or command compile-command "make -k "))))
(devil-define-alias 'devil-make #'devil-compile)
;;
(eval-when-compile
  (require 'tramp     nil :noerror)
  (require 'tramp-adb nil :noerror))
(evil-define-command devil-async-shell-command
    (command &optional previous)
  "\
Run `async-shell-command'."
  (interactive "<sh><!>")
  (if (and (called-interactively-p 'any)
           (not (evil-ex-p)))
      (let ((evil-ex-initial-input (or (car (rassoc this-command
                                                    evil-ex-commands))
                                       (format "%s " this-command))))
        (call-interactively 'evil-ex))
    (when command
      (setq command (evil-ex-replace-special-filenames command)))
    (if (zerop (length command))
        (when previous
          (setq command evil-previous-shell-command))
      (setq evil-previous-shell-command command))
    (when (zerop (length command))
      (if previous
          (user-error "No previous shell command")
        (user-error "No shell command")))
    (if (and (file-remote-p default-directory)
             (or (and (or (boundp 'tramp-version)
                          (and (require 'tramp nil :no-error)
                               (boundp  'tramp-version)))
                      (version< tramp-version "2.4.2"))
                 (and (or (fboundp 'tramp-adb-handle-shell-command)
                          (and (require 'tramp-adb nil :no-error)
                               (fboundp 'tramp-adb-handle-shell-command))))))
        ;; -------------------------------------------------------------------
        ;; NOTE:
        ;;
        ;; Seamless multi-processing with (many) asynchronous shell commands
        ;; running in parallel was not supported by TRAMP at least until
        ;; version 2.4.2, where the culprit function
        ;; `tramp-adb-handle-shell-command' was supposedly removed.  That's
        ;; super annoying, so let's roll our own implementation by
        ;; synthesizing the relevant code from function `shell-command' with a
        ;; call to function `make-comint-in-buffer' as it already can
        ;; establish yet another asynchronous process via `start-file-process'
        ;; (see `comint-exec-1'):
        ;;
        (let* ((buffer         (get-buffer-create "*Async Shell Command*"))
               (buffer-name    (buffer-name buffer))
               (buffer-process (get-buffer-process buffer))
               (directory      default-directory)
               (filter         nil))
          ;; -----------------------------------------------------------------
          ;; NOTE:
          ;;
          ;; Always behave as if `devil-async-shell-command-buffer' would be
          ;; set to `new-buffer':
          ;;
          (when buffer-process
            (setq buffer (generate-new-buffer buffer-name)))
          ;; -----------------------------------------------------------------
          (with-current-buffer buffer
            (if (fboundp 'shell-command-save-pos-or-erase)
                (shell-command-save-pos-or-erase)
              (shell-command--save-pos-or-erase))
            (setq default-directory directory))
          ;; -----------------------------------------------------------------
          ;; NOTE:
          ;;
          ;; It would definitely be wrong to use variables `shell-file-name'
          ;; and `shell-command-switch' here directly (like in
          ;; `shell-command') as they are only meaningful at the local host:
          ;;
          (make-comint-in-buffer command buffer "/bin/sh" nil "-c" command)
          ;; -----------------------------------------------------------------
          (setq buffer-process (get-buffer-process buffer))
          (setq filter         (process-filter buffer-process))
          (devil-assert (eq filter #'comint-output-filter))
          (with-current-buffer buffer
            (unless (fboundp 'shell-mode)
              (require 'shell))
            (shell-mode)
            (set-process-sentinel buffer-process #'shell-command-sentinel)
            (set-process-filter   buffer-process #'comint-output-filter)
            ;; ---------------------------------------------------------------
            ;; TODO:
            ;;
            ;; Deferring displaying buffer until first process output based on
            ;; the value of variable `async-shell-command-display-buffer'
            ;; (like in `shell-command') somehow didn't work as expected
            ;; (investigate later), so unconditionally display buffer
            ;; immediately for now:
            ;;
            (display-buffer buffer '(nil (allow-no-window . t)))
            ;; ---------------------------------------------------------------
            )
          ;; -----------------------------------------------------------------
          )
      (let ((async-shell-command-buffer
             (or (bound-and-true-p devil-async-shell-command-buffer)
                 'new-buffer)))
        (async-shell-command command)))))
;;
(evil-define-command devil-shell
    (&optional buffer new-p)
  :repeat nil
  (interactive "<b><!>")
  (let ((name (or explicit-shell-file-name
                  (getenv "ESHELL")
                  shell-file-name
                  (getenv "SHELL")
                  (and (memq system-type '(cygwin windows-nt))
                       (getenv "COMSPEC")))))
    (setq
      buffer (and (bufferp buffer) (buffer-name buffer))
      buffer (or buffer (and name (concat "*shell: " name "*")))))
  (when buffer
    (setq buffer (if new-p
                     (generate-new-buffer-name buffer)
                   (catch 'break
                     (dolist (b (buffer-list (and (bound-and-true-p
                                                   devil-selected-frame-p)
                                                  (selected-frame)))
                                buffer)
                       (setq b (buffer-name b))
                       (when (string-match-p (format "^%s<[0-9]+>$"
                                                     (regexp-quote buffer))
                                             b)
                         (throw 'break b)))))))
  (shell buffer))
;;
(evil-define-command devil-bash
    (&optional buffer new-p)
  :repeat nil
  (interactive "<b><!>")
  (let ((explicit-shell-file-name (or (executable-find "bash")
                                      "/bin/bash"))
        (explicit-bash-args '("--noediting" "-i" "-l")))
    (devil-shell buffer new-p)))
;;
(evil-define-command devil-csh
    (&optional buffer new-p)
  :repeat nil
  (interactive "<b><!>")
  (let ((explicit-shell-file-name (or (executable-find "csh")
                                      "/bin/csh"))
        (explicit-csh-args '("-i" "-l")))
    (when (eq system-type 'hpux)
      (add-to-list 'explicit-csh-args "-T"))
    (devil-shell buffer new-p)))
;;
(evil-define-command devil-ksh
    (&optional buffer new-p)
  :repeat nil
  (interactive "<b><!>")
  (let ((explicit-shell-file-name (or (executable-find "ksh")
                                      "/bin/ksh"))
        (explicit-ksh-args '("-i")))
    (devil-shell buffer new-p)))
;;
(evil-define-command devil-zsh
    (&optional buffer new-p)
  :repeat nil
  (interactive "<b><!>")
  (let ((explicit-shell-file-name (or (executable-find "zsh")
                                      "/bin/zsh"))
        (explicit-zsh-args '("-i" "-l")))
    (devil-shell buffer new-p)))
;;
(evil-define-command devil-sh
    (&optional buffer new-p)
  :repeat nil
  (interactive "<b><!>")
  (let ((explicit-shell-file-name (or (executable-find "sh")
                                      "/bin/sh"))
        (explicit-sh-args '("-i")))
    (devil-shell buffer new-p)))
;;
(evil-define-command devil-cmd
    (&optional buffer new-p)
  :repeat nil
  (interactive "<b><!>")
  (let ((explicit-shell-file-name (or (executable-find "cmd")
                                      (getenv "COMSPEC"))))
    (devil-shell buffer new-p)))
;;
(evil-define-command devil-interrupt-process
    (buffer &optional bang)
  "\
Interrupt process running in buffer BUFFER."
  :repeat nil
  (interactive "<b><!>")
  (with-current-buffer (or buffer (current-buffer))
    (unless bang
      (when buffer-read-only
        (signal 'buffer-read-only (list (current-buffer)))))
    (let ((process (get-buffer-process (current-buffer))))
      (if process
          (progn
            (interrupt-process process)
            (sit-for 1)
            (delete-process process)
            (when (setq process (get-buffer-process (current-buffer)))
              (interrupt-process process t)
              (sit-for 1)
              (delete-process process)))
        (error "The %s process is not running" (downcase mode-name)))
      (when (setq process (get-buffer-process (current-buffer)))
        (error "The %s process is not interrupted" (downcase mode-name))))))
;;
(evil-define-command devil-kill-process
    (buffer &optional bang)
  "\
Kill process running in buffer BUFFER."
  :repeat nil
  (interactive "<b><!>")
  (with-current-buffer (or buffer (current-buffer))
    (unless bang
      (when buffer-read-only
        (signal 'buffer-read-only (list (current-buffer)))))
    (let ((process (get-buffer-process (current-buffer))))
      (if process
          (progn
            (kill-process process)
            (sit-for 1)
            (delete-process process)
            (when (setq process (get-buffer-process (current-buffer)))
              (kill-process process t)
              (sit-for 1)
              (delete-process process)))
        (error "The %s process is not running" (downcase mode-name)))
      (when (setq process (get-buffer-process (current-buffer)))
        (error "The %s process is not killed" (downcase mode-name))))))
;;
(evil-define-command devil-interrupt-or-kill-process
    (buffer &optional bang)
  "\
Interrupt or kill process running in buffer BUFFER."
  :repeat nil
  (interactive "<b><!>")
  (condition-case nil
      (devil-interrupt-process buffer bang)
    (error
     (devil-kill-process buffer bang))))
(devil-define-alias 'devil-delete-process #'devil-interrupt-or-kill-process)
;;
(provide 'devil-commands)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(elisp) Text Quoting Style'
;;  ==========================================================================
;;  }}} References
