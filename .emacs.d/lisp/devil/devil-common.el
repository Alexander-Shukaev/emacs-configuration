(require 'devil-vars)
;;
(require 'evil-common)
;;
(eval-when-compile
  (require 'cl-lib))
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Fulfill the documentation.
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Report bug about COUNT becoming `&optional' after
;; `evil-define-text-object'.
;; ---------------------------------------------------------------------------
;;;###autoload
(eval-and-compile
  (define-advice eval-when-compile
      (:filter-return (expression) devil/:filter-return)
    (if (and (listp             expression)
             (eq      (car-safe expression) 'quote)
             (stringp (cadr     expression)))
        (cadr expression)
      expression)))
;;
;; NOTE:
;;
;; The above advice is useful to, for example, allow `eval-when-compile' as
;; DOCSTRING for `defun', `defmacro', etc.
;; ---------------------------------------------------------------------------
;;
(put 'evil-define-command 'lisp-indent-function 2)
;;
(defmacro devil-assert
    (form)
  "\
Signal `error' if FORM evaluates to nil."
  (declare (debug  t)
           (indent defun))
  (macroexpand `(cl-assert (identity ,form) :show-args)))
;;
(defmacro devil-reject
    (form)
  "\
Signal `error' if FORM evaluates to t."
  (declare (debug  t)
           (indent defun))
  (macroexpand `(cl-assert (not      ,form) :show-args)))
;; ---------------------------------------------------------------------------
;; TODO:
;;
;; The following `eval-and-compile' form was needed due to `native-comp', why?
;;
(eval-and-compile
  (defalias 'devil-negate #'devil-reject))
;; ---------------------------------------------------------------------------
;;
(defmacro devil-display-graphic-p ()
  "\
Compatibility wrapper for `display-graphic-p'.

In older versions of Emacs, `display-graphic-p' did not exist.
In Emacs 23 and newer, it does."
  (if (< emacs-major-version 23)
      '(or window-system (getenv "DISPLAY") (getenv "WAYLAND_DISPLAY"))
    '(display-graphic-p)))
;;
(defmacro devil-ignore-progn
    (&rest body)
  "\
Ignore BODY."
  (declare (debug  t)
           (indent defun)))
;;
(defsubst devil-backtrace-to-string ()
  "\
Return a trace of (Lisp) function calls currently active as string.

See `devil-backtrace-to-string-function'."
  (when devil-backtrace-to-string-function
    (funcall devil-backtrace-to-string-function)))
;;
(defun devil-warning
    (level format-string &rest args)
  "\
Display a warning message with trace of (Lisp) function calls.

See `lwarn' and `devil-backtrace-to-string'."
  (setq format-string (concat format-string "\n  %S"))
  (setq args          (nconc args (list (current-buffer))))
  (let ((backtrace-string (devil-backtrace-to-string)))
    (unless (zerop (length backtrace-string))
      (setq format-string (concat format-string "\n%s"))
      (setq args          (nconc args (list backtrace-string)))))
  (apply #'lwarn 'devil level format-string args))
;;
(defsubst devil-load-history
    (&optional library no-suffix path)
  "\
Return mapping(s) of loaded file name to symbols and features."
  (if library
      (let ((file (locate-library library no-suffix path)))
        (when file
          (setq file (expand-file-name file))
          (or (when (string-match-p "\\.el\\'" file)
                (assoc (concat file "c") load-history))
              (assoc file load-history))))
    load-history))
;;
(defmacro devil-save-column
    (pos &rest body)
  "\
Execute BODY, then restore the previous column at POS."
  (declare (debug  t)
           (indent defun))
  `(let ((devil--column (evil-column ,pos)))
     (unwind-protect
         (save-current-buffer ,@body)
       (beginning-of-line)
       (move-to-column devil--column))))
;;
(defmacro devil-save-line
    (pos &rest body)
  "\
Execute BODY, then restore the previous line at POS."
  (declare (debug  t)
           (indent defun))
  `(let ((devil--line (line-number-at-pos ,pos)))
     (unwind-protect
         (save-current-buffer ,@body)
       (goto-char (point-min))
       (forward-line (1- devil--line)))))
;;
(defmacro devil-save-excursion
    (&rest body)
  "\
Execute BODY, then restore the previous point and mark."
  (declare (debug  t)
           (indent defun))
  `(devil-save-column (point)
     (devil-save-line (point)
       (unwind-protect
           (devil-save-column (mark :force)
             (devil-save-line (mark :force)
               ,@body))
         (set-marker (mark-marker) (point))))))
;;
(defmacro devil-save-selected-window
    (&rest body)
  "\
Execute BODY, then select the previously selected window."
  (declare (debug  t)
           (indent defun))
  `(let ((inhibit-redisplay t))
     (devil-without-force-mode-line-update
       (save-window-excursion
         (save-selected-window
           ,@body)))))
;;
;; TODO: Can be more generic function disable/substitution macro:
(defmacro devil-without-messages
    (&rest body)
  "\
Execute BODY without displaying and logging of messages."
  (declare (debug  t)
           (indent defun))
  `(let* ((devil--symbol   'message)
          (devil--function (symbol-function devil--symbol))
          (message-log-max nil))
     (fset devil--symbol #'ignore)
     (unwind-protect
         (progn ,@body)
       (fset devil--symbol devil--function))))
(defalias 'devil-ignore-messages   #'devil-without-messages)
(defalias 'devil-suppress-messages #'devil-without-messages)
;;
;; TODO: Can be more generic function disable/substitution macro:
(defmacro devil-without-force-mode-line-update
    (&rest body)
  "\
Execute BODY without force redisplay of the current buffer's mode line.

See `force-mode-line-update'."
  (declare (debug  t)
           (indent defun))
  (let* ((         symbol 'force-mode-line-update)
         (  symbol-symbol (cl-gentemp (format "%s--symbol-"   symbol)))
         (function-symbol (cl-gentemp (format "%s--function-" symbol))))
    `(let* ((  ,symbol-symbol ',symbol)
            (,function-symbol (symbol-function ,symbol-symbol)))
       (fset ,symbol-symbol #'ignore)
       (unwind-protect
           (progn ,@body)
         (fset ,symbol-symbol ,function-symbol)))))
;;
(defmacro devil-with-window-hscroll
    (&rest body)
  "\
Execute BODY with horizontal scrolling in the selected window."
  (declare (debug  t)
           (indent defun))
  `(let ((devil--hscroll (window-hscroll)))
     (if (not (zerop devil--hscroll))
         (progn ,@body)
       (set-window-hscroll (selected-window) 1)
       (unwind-protect
           (progn ,@body)
         (set-window-hscroll (selected-window) 0)))))
;;
(defmacro devil-without-window-hscroll
    (&rest body)
  "\
Execute BODY without horizontal scrolling in the selected window."
  (declare (debug  t)
           (indent defun))
  `(let ((devil--hscroll (window-hscroll)))
     (if (zerop devil--hscroll)
         (progn ,@body)
       (set-window-hscroll (selected-window) 0)
       (unwind-protect
           (progn ,@body)
         (set-window-hscroll (selected-window) devil--hscroll)))))
;;
(defmacro devil-without-timers
    (&rest body)
  "\
Execute BODY without running timers.

See `timer-list', `timer-idle-list', and `timer-event-handler'."
  (declare (debug  t)
           (indent defun))
  `(let (timer-list timer-idle-list)
     (unwind-protect
         (progn ,@body)
       (dolist (symbol '(timer-list timer-idle-list))
         (let ((value (symbol-value symbol)))
           (when value
             (devil-warning :warning
                            "Set the non-nil value of variable `%s': %s"
                            symbol
                            value)))))))
;;
(defmacro devil-call
    (function &rest arguments)
  "\
Call FUNCTION with ARGUMENTS."
  (declare (debug t))
  (when (and (listp function)
             (eq (car-safe function) 'quote))
    (setq function (cadr function)))
  `(if (called-interactively-p 'any)
       (or (and (symbolp          ',function)
                (bound-and-true-p  ,function)
                (functionp         ,function)
                (prog1 t (call-interactively ,function)))
           (call-interactively #',function))
     (or (and (symbolp          ',function)
              (bound-and-true-p  ,function)
              (functionp         ,function)
              (prog1 t (funcall ,function ,@arguments)))
         (funcall #',function ,@arguments))))
;;
(defmacro devil-without-keymap-parent
    (keymap &rest body)
  "\
Execute BODY without the parent keymap of keymap KEYMAP."
  (declare (debug  t)
           (indent defun))
  (let* ((           symbol 'devil-without-keymap-parent)
         (       map-symbol (cl-gentemp (format "%s--map-"        symbol)))
         (parent-map-symbol (cl-gentemp (format "%s--parent-map-" symbol))))
    `(let* ((       ,map-symbol ,keymap)
            (,parent-map-symbol (keymap-parent ,map-symbol)))
       (set-keymap-parent ,map-symbol nil)
       (unwind-protect
           (progn ,@body)
         (set-keymap-parent ,map-symbol ,parent-map-symbol)
         (devil-assert
           (eq (keymap-parent ,map-symbol) ,parent-map-symbol))))))
;;
(defun devil-clear-keymap
    (keymap &optional recursively-p)
  "\
Clear keymap KEYMAP.

If RECURSIVELY-P is non-nil, remove the parent keymap of keymap KEYMAP too.

Return KEYMAP."
  (unless (keymapp keymap)
    (signal 'wrong-type-argument (list 'keymapp keymap)))
  (if recursively-p
      (progn
        (setcdr keymap nil)
        (devil-assert (null (keymap-parent keymap))))
    (devil-without-keymap-parent keymap
      (setcdr keymap nil)))
  keymap)
;;
(defun devil-define-alias
    (old new &optional docstring)
  "\
Alias OLD's command definition to NEW's command definition.

Return OLD's command definition if any or nil."
  (declare (doc-string 3)
           (indent     defun))
  (prog1
      (devil-remove-command old)
    (devil-assert (symbolp old))
    (defalias old new docstring)
    (when (symbolp new) (devil-copy-command-properties new old))))
;;
(defmacro devil-define-aliases
    (&rest ...)
  "Apply `devil-define-alias' to each two consecutive arguments.
Return list of results where each element is the return value of the
corresponding `devil-define-alias' application.

\(fn [OLD NEW]...)"
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-define-alias ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(list ,@body)))
;;
(defun devil-define-key
    (keymap &rest ...)
  "\
Apply `define-key'.

\(fn KEYMAP [STATE] KEY [DEF])"
  (when (or (< (length ...) 1)
            (> (length ...) 3))
    (signal 'wrong-number-of-arguments
            (list 'devil-define-key (length ...))))
  (let ((state (when (or (null     (car-safe ...))
                         (keywordp (car-safe ...)))
                 (pop ...))))
    (when state
      (setq
        state  (intern-soft (substring (symbol-name state) 1))
        ;; -------------------------------------------------------------------
        keymap (devil-without-keymap-parent keymap
                 (evil-get-auxiliary-keymap keymap state :create))
        ;;
        ;; NOTE:
        ;;
        ;; Prevent `evil-get-auxiliary-keymap' from searching in parent
        ;; keymaps by wrapping its call into `devil-without-keymap-parent'.
        ;; -------------------------------------------------------------------
        )))
  (let ((key (pop      ...))
        (def (car-safe ...)))
    (dolist (arg (list key def))
      (when (or (stringp arg)
                (vectorp arg))
        (devil-negate (zerop (length arg)))))
    (when (and def (not (eq def #'undefined)))
      (let ((command (devil-without-keymap-parent keymap
                       (devil-key-command key keymap))))
        (devil-assert (or (null  command)
                          (eq    command #'undefined)
                          (equal command def)))))
    (define-key keymap key def))
  (evil-set-keymap-prompt keymap (keymap-prompt keymap))
  keymap)
;;
(defun devil-define-key-kbd
    (keymap &rest ...)
  "\
Apply `devil-define-key'.

\(fn KEYMAP [STATE] KEY [DEF])"
  (when (or (< (length ...) 1)
            (> (length ...) 3))
    (signal 'wrong-number-of-arguments
            (list 'devil-define-key-kbd (length ...))))
  (let ((state (when (or (null     (car-safe ...))
                         (keywordp (car-safe ...)))
                 (pop ...)))
        (key   (pop      ...))
        (def   (car-safe ...)))
    (dolist (arg (list key def))
      (when (or (stringp arg)
                (vectorp arg))
        (devil-negate (zerop (length arg))))
      (when (stringp arg)
        (devil-negate (string-match-p "^[\s\t\n\f]+$" arg))))
    (devil-define-key keymap
                      state
                      (if (stringp key) (kbd key) key)
                      (if (stringp def) (kbd def) def)))
  keymap)
;;
(defmacro devil-define-keys
    (keymap &rest ...)
  "\
Apply `devil-define-key' to KEYMAP and each two consecutive arguments.
KEYMAP is a keymap.
Return KEYMAP.

\(fn KEYMAP [[STATE] KEY DEF]...)"
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-define-key keymap
                               ,(if (keywordp (car-safe ...))
                                    `(setq state ,(pop ...))
                                  'state)
                               ,(pop ...)
                               ,(pop ...))
            body))
    (setq body (nreverse body))
    `(let ((keymap ,keymap)
           (state))
       ,@body
       keymap)))
;;
(defmacro devil-define-keys-with-prefix
    (keymap &rest ...)
  "\
Apply `devil-define-keys' to a keymap with prefix KEY in KEYMAP.
If no such keymap exists, make new sparse keymap with prefix KEY
in KEYMAP and apply `devil-define-keys' to it instead.
KEYMAP is a keymap.
Return a keymap with prefix KEY in KEYMAP.

\(fn KEYMAP [STATE] KEY [[STATE] KEY DEF]...)"
  (declare (debug  t)
           (indent defun))
  (when (< (length ...) 1)
    (signal 'wrong-number-of-arguments
            (list 'devil-define-keys-with-prefix (length ...))))
  (let* ((    symbol 'devil-define-keys-with-prefix)
         (map-symbol (cl-gentemp (format "%s--map-" symbol)))
         (state      (when (or (null     (car-safe ...))
                               (keywordp (car-safe ...)))
                       (pop ...)))
         (key        (pop ...)))
    `(let* ((keymap      ,keymap)
            (,map-symbol (or (lookup-key keymap ,key)
                             (make-sparse-keymap))))
       (devil-define-key keymap ,state ,key
                         (devil-define-keys ,map-symbol
                           ,@...))
       ,map-symbol)))
;;
(defmacro devil-define-keys-kbd
    (keymap &rest ...)
  "\
Apply `devil-define-key-kbd' to KEYMAP and each two consecutive arguments.
KEYMAP is a keymap.
Return KEYMAP.

\(fn KEYMAP [[STATE] KEY DEF]...)"
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-define-key-kbd keymap
                                   ,(if (keywordp (car-safe ...))
                                        `(setq state ,(pop ...))
                                      'state)
                                   ,(pop ...)
                                   ,(pop ...))
            body))
    (setq body (nreverse body))
    `(let ((keymap ,keymap)
           (state))
       ,@body
       keymap)))
;;
(defmacro devil-define-keys-with-prefix-kbd
    (keymap &rest ...)
  "\
Apply `devil-define-keys-kbd' to a keymap with prefix KEY in KEYMAP.
If no such keymap exists, make new sparse keymap with prefix KEY
in KEYMAP and apply `devil-define-keys-kbd' to it instead.
KEYMAP is a keymap.
Return a keymap with prefix KEY in KEYMAP.

\(fn KEYMAP [STATE] KEY [[STATE] KEY DEF]...)"
  (declare (debug  t)
           (indent defun))
  (when (< (length ...) 1)
    (signal 'wrong-number-of-arguments
            (list 'devil-define-keys-with-prefix-kbd (length ...))))
  (let* ((    symbol 'devil-define-keys-with-prefix-kbd)
         (map-symbol (cl-gentemp (format "%s--map-" symbol)))
         (state      (when (or (null     (car-safe ...))
                               (keywordp (car-safe ...)))
                       (pop ...)))
         (key        (pop ...)))
    `(let* ((keymap      ,keymap)
            (,map-symbol (or (lookup-key keymap ,(cond ((stringp key)
                                                        (kbd key))
                                                       ((vectorp key)
                                                        key)
                                                       (t
                                                        `(if (stringp ,key)
                                                             (kbd ,key)
                                                           ,key))))
                             (make-sparse-keymap))))
       (devil-define-key-kbd keymap ,state ,key
                             (devil-define-keys-kbd ,map-symbol
                               ,@...))
       ,map-symbol)))
;;
(defun devil-key-command
    (key-or-command &optional keymap)
  "Return command for KEY-OR-COMMAND."
  (cond ((and (listp key-or-command)
              (eq (car-safe key-or-command) 'quote))
         (cadr key-or-command))
        ((or (stringp key-or-command)
             (vectorp key-or-command))
         ;; TODO: `evil-lookup-key' everywhere:
         ;; TODO:
         ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Searching-Keymaps.html
         ;; `local-key-binding'
         ;; `global-key-binding'
         ;; `minor-mode-key-binding'
         (let ((key (lookup-key keymap key-or-command)))
           (unless (or (integerp key)
                       (keymapp  key))
             (devil-key-command key keymap))))
        ((or (symbolp  key-or-command)
             (commandp key-or-command))
         key-or-command)
        (t
         (signal 'wrong-type-argument (list 'commandp key-or-command)))))
;;
(defun devil-command-keys
    (command &optional keymap)
  "Return keys for COMMAND."
  (and (fboundp command)
       (or (where-is-internal command keymap))))
;;
(defun devil-copy-command-function
    (old new &optional docstring)
  "Copy OLD's command function to NEW's command function.
Return OLD's command function."
  (declare (doc-string 3)
           (indent     defun))
  (unless (functionp old)
    (signal 'wrong-type-argument (list 'functionp old)))
  (unless (symbolp   new)
    (signal 'wrong-type-argument (list 'symbolp   new)))
  (let ((function (if (symbolp old) (indirect-function old) old)))
    (devil-assert (functionp function))
    (devil-negate (symbolp   function))
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; Keep in mind that `defalias' is used over `fset' in this case only
    ;; because it provides optional documentation string support (which I was
    ;; lazy to implement around `fset').  However, it is important to remember
    ;; the (potential) side effect with the `defalias-fset-function' property.
    ;;
    (defalias new function docstring)
    ;; -----------------------------------------------------------------------
    function))
;;
(defun devil-remove-command-function
    (symbol)
  "Remove SYMBOL's command function.
Return SYMBOL's command function."
  (declare (indent defun))
  (unless (symbolp symbol)
    (signal 'wrong-type-argument (list 'symbolp symbol)))
  (prog1 (indirect-function symbol) (fmakunbound symbol)))
;;
(defun devil-copy-command-properties
    (old new &optional add-p)
  "Copy OLD's command properties to NEW's command properties.
Return OLD's command properties."
  (declare (indent defun))
  (unless (symbolp old)
    (signal 'wrong-type-argument (list 'symbolp old)))
  (unless (symbolp new)
    (signal 'wrong-type-argument (list 'symbolp new)))
  (let ((properties (evil-get-command-properties old)))
    (when properties
      (apply (if add-p
                 #'evil-add-command-properties
               #'evil-set-command-properties)
             new
             properties))
    properties))
;;
(defun devil-remove-command-properties
    (symbol)
  "Remove SYMBOL's command properties.
Return SYMBOL's command properties."
  (declare (indent defun))
  (unless (symbolp symbol)
    (signal 'wrong-type-argument (list 'symbolp symbol)))
  (prog1
      (evil-get-command-properties symbol)
    (evil-remove-command-properties symbol)))
;;
(defun devil-copy-command
    (old new &optional docstring)
  "Copy OLD's command definition to NEW's command definition.
Return OLD's command definition."
  (declare (doc-string 3)
           (indent     defun))
  (list (devil-copy-command-function old new docstring)
        (when (symbolp old) (devil-copy-command-properties old new))))
;;
(defmacro devil-copy-commands
    (&rest ...)
  "Apply `devil-copy-command' to each two consecutive arguments.
Return list of results where each element is the return value of the
corresponding `devil-copy-command' application.

\(fn [OLD NEW]...)"
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-copy-command ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(list ,@body)))
;;
(defun devil-remove-command
    (symbol)
  "Remove SYMBOL's command definition.
Return SYMBOL's command definition."
  (declare (indent defun))
  (list (devil-remove-command-function   symbol)
        (devil-remove-command-properties symbol)))
;;
(defun devil-remove-commands
    (&rest ...)
  "Apply `devil-remove-command' to each one consecutive argument.
Return list of results where each element is the return value of the
corresponding `devil-remove-command' application.

\(fn SYMBOL...)"
  (declare (indent defun))
  (mapcar #'devil-remove-command ...))
;;
(defun devil-move-command
    (old new &optional docstring no-alias-p)
  "Move OLD's command definition to NEW's command definition.
Return OLD's command definition."
  (declare (doc-string 3)
           (indent     defun))
  (prog1
      (devil-copy-command old new docstring)
    (devil-assert (symbolp new))
    (when (symbolp old)
      (let ((function (symbol-function old)))
        (while (symbolp function)
          (devil-assert (symbolp old))
          (unless (eq old new)
            (unless no-alias-p
              (devil-copy-command-properties new old)))
          (setq
            old      function
            function (symbol-function old))))
      (devil-assert (symbolp old))
      (unless (eq old new)
        (devil-remove-command old)
        (unless no-alias-p
          (defalias old new)
          (devil-copy-command-properties new old))))))
;;
(defmacro devil-move-commands
    (&rest ...)
  "Apply `devil-move-command' to each two consecutive arguments.
Return list of results where each element is the return value of the
corresponding `devil-move-command' application.

\(fn [OLD NEW]...)"
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-move-command ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(list ,@body)))
;;
(defun devil-replace-in-string
    (exp rep string)
  "\
Replace all matches for EXP with REP in STRING and return it."
  (replace-regexp-in-string (regexp-quote exp) rep string nil :literal))
;;
(defun devil-fontify-string
    (string &optional mode beg end)
  "\
Fontify STRING according to MODE and return fontified STRING."
  (let ((mode (or mode major-mode)))
    ;; TODO: Create buffer once and reuse it (also prevent `prog-mode'):
    (with-temp-buffer
      (insert string)
      (setq-local enable-local-variables     nil)
      (setq-local enable-dir-local-variables nil)
      (funcall mode)
      (funcall font-lock-fontify-buffer-function)
      (setq beg (cond ((null beg)
                       (point-min))
                      ((and (number-or-marker-p beg) (>= beg 0))
                       (1+ beg))
                      ((and (number-or-marker-p beg) (<  beg 0))
                       (+ (point-max) beg))
                      (t
                       (signal 'wrong-type-argument
                               (list 'number-or-marker-p beg)))))
      (setq end (cond ((null end)
                       (point-max))
                      ((and (number-or-marker-p end) (>= end 0))
                       (1+ end))
                      ((and (number-or-marker-p end) (<  end 0))
                       (+ (point-max) end))
                      (t
                       (signal 'wrong-type-argument
                               (list 'number-or-marker-p end)))))
      (buffer-substring beg end))))
;;
(defun devil-format-quote
    (string)
  "\
Substitute all % with %% in STRING and return it."
  (devil-replace-in-string "%" "%%" string))
;;
(defun devil-current-quit-char ()
  "\
Return the character currently used for quitting."
  (nth 3 (current-input-mode)))
;;
(defun devil-current-quit-key ()
  "\
Return the key currently used for quitting."
  (char-to-string (devil-current-quit-char)))
;;
(defun devil-interesting-buffer-p
    (&optional buffer)
  "\
Return t if BUFFER is a live buffer whose name does not start with a space."
  (and (buffer-live-p (or buffer (current-buffer)))
       (or (zerop (length (buffer-name buffer)))
           (/=    (aref   (buffer-name buffer) 0) ?\s))
       (not (minibufferp buffer))))
;;
(defun devil-first-line-p ()
  "\
Return t if point is at the first (top) line of buffer."
  (save-excursion
    (let ((inhibit-field-text-motion t))
      (beginning-of-line))
    (bobp)))
;;
(defun devil-last-line-p ()
  "\
Return t if point is at the last (bottom) line of buffer."
  (save-excursion
    (let ((inhibit-field-text-motion t))
      (end-of-line))
    (eobp)))
;;
(defun devil-bol-p ()
  "\
Return t if point is at the beginning of line."
  (bolp))
;;
(defun devil-eol-p ()
  "\
Return t if point is at the end of line."
  (or (eolp)
      (and (bound-and-true-p evil-mode)
           (save-excursion
             (forward-char)
             (eolp)))))
;;
(defun devil-bob-p ()
  "\
Return t if point is at the beginning of buffer."
  (bobp))
;;
(defun devil-eob-p ()
  "\
Return t if point is at the end of buffer."
  (or (eobp)
      (and (bound-and-true-p evil-mode)
           (save-excursion
             (forward-char)
             (eobp)))))
;;
(defmacro devil-signal-at-first-line ()
  "\
Signal error if point is at the first (top) line of buffer."
  `(when (devil-first-line-p) (signal 'beginning-of-buffer nil)))
;;
(defmacro devil-signal-at-last-line ()
  "\
Signal error if point is at the last (bottom) line of buffer."
  `(when (devil-last-line-p) (signal 'end-of-buffer nil)))
;;
(defmacro devil-signal-at-bol ()
  "\
Signal error if point is at the beginning of line."
  `(when (devil-bol-p) (signal 'beginning-of-line nil)))
;;
(defmacro devil-signal-at-eol ()
  "\
Signal error if point is at the end of line."
  `(when (devil-eol-p) (signal 'end-of-line nil)))
;;
(defmacro devil-signal-at-bob ()
  "\
Signal error if point is at the beginning of buffer."
  `(when (devil-bob-p) (signal 'beginning-of-buffer nil)))
;;
(defmacro devil-signal-at-eob ()
  "\
Signal error if point is at the end of buffer."
  `(when (devil-eob-p) (signal 'end-of-buffer nil)))
;;
(defun devil-bounds-of-thing-at-point
    (thing &optional forward)
  "Return the beginning and the end positions of THING at point.
If FORWARD is non-nil, search forward, otherwise backward.
THING should be a symbol understood by `bounds-of-thing-at-point',
where in addition to the default possibilities listed,
THING can also be `evil-string', `evil-comment', `evil-empty-line',
`evil-space', `evil-word', `evil-WORD', `evil-symbol', `evil-defun',
`evil-sentence', `evil-paragraph', `evil-quote', and `evil-quote-simple'.
Return nil if nothing is found."
  (let ((move-function (if forward #'forward-char #'backward-char))
        (endp-function (if forward #'eobp         #'bobp))
        (bounds))
    (save-excursion
      (setq bounds (bounds-of-thing-at-point thing))
      ;; ---------------------------------------------------------------------
      ;; If there is nothing at point, move forward (or backward) to find it:
      ;;
      (while (and (null bounds) (not (funcall endp-function)))
        (funcall move-function)
        (setq bounds (bounds-of-thing-at-point thing)))
      ;; ---------------------------------------------------------------------
      )
    bounds))
;;
(defun devil-thing-at-point
    (thing &optional forward properties-p)
  "Return THING at point (as a string).
If FORWARD is non-nil, search forward, otherwise backward.
THING should be a symbol understood by `thing-at-point',
where in addition to the default possibilities listed,
THING can also be `evil-string', `evil-comment', `evil-empty-line',
`evil-space', `evil-word', `evil-WORD', `evil-symbol', `evil-defun',
`evil-sentence', `evil-paragraph', `evil-quote', and `evil-quote-simple'.
If PROPERTIES-P is non-nil, include text properties in the return value.
Return nil if nothing is found."
  (let ((move-function (if forward #'forward-char #'backward-char))
        (endp-function (if forward #'eobp         #'bobp))
        (string))
    (save-excursion
      (setq string (thing-at-point thing (not properties-p)))
      ;; ---------------------------------------------------------------------
      ;; If there is nothing at point, move forward (or backward) to find it:
      ;;
      (while (and (null string) (not (funcall endp-function)))
        (funcall move-function)
        (setq string (thing-at-point thing (not properties-p))))
      ;; ---------------------------------------------------------------------
      )
    (unless (zerop (length string))
      string)))
;;
(defun devil-word-beginning ()
  "Return regular expression for the beginning of word."
  "\\<")
(defalias 'devil-word-beg #'devil-word-beginning)
;;
(defun devil-word-end ()
  "Return regular expression for the end of a word."
  "\\>")
;;
(defun devil-WORD-beginning ()
  "Return regular expression for the beginning of WORD."
  "\\(^\\|\\s-+?\\)")
(defalias 'devil-WORD-beg #'devil-WORD-beginning)
;;
(defun devil-WORD-end ()
  "Return regular expression for the end of a WORD."
  "\\($\\|\\s-+?\\)")
;;
(defun devil-symbol-beginning ()
  "Return regular expression for the beginning of a symbol."
  "\\_<")
(defalias 'devil-symbol-beg #'devil-symbol-beginning)
;;
(defun devil-symbol-end ()
  "Return regular expression for the end of a symbol."
  "\\_>")
;;
(defun devil-sentence-beginning ()
  "Return regular expression for the beginning of a sentence."
  (sentence-end))
(defalias 'devil-sentence-beg #'devil-sentence-beginning)
;;
(defun devil-sentence-end ()
  "Return regular expression for the end of a sentence."
  (sentence-end))
;;
(defun devil-scroll-horizontally-default-count ()
  "\
Return the default amount to scroll horizontally."
  (max (/ (min (save-excursion
                 (end-of-visual-line)
                 (current-column))
               (window-body-width))
          2)
       1))
;;
(defun devil-scroll-vertically-default-count ()
  "\
Return the default amount to scroll vertically."
  (max (/ (window-body-height)
          ;; -----------------------------------------------------------------
          ;; (min (count-screen-lines (window-start) (window-end))
          ;;      (window-body-height))
          ;;
          ;; NOTE:
          ;;
          ;; It turns out that `count-screen-lines' slows down vertical
          ;; scrolling in certain cases by stuttering it.  Is it a
          ;; computationally intensive function?
          ;; -----------------------------------------------------------------
          2)
       1))
;;
(defun devil-apply-on-block
    (function beg end pass-columns &rest args)
  "\
Call FUNCTION for each line of a block selection.

See `evil-apply-on-block'."
  (let ((eol-col (and (memq last-command '(next-line previous-line))
                      (numberp temporary-goal-column)
                      temporary-goal-column))
        begcol begpt endcol endpt)
    (save-excursion
      (goto-char beg)
      (setq begcol (current-column))
      (beginning-of-line)
      (setq begpt (point))
      (goto-char end)
      (setq endcol (current-column))
      (forward-line 1)
      (setq endpt (point-marker))
      ;; Ensure the beginning column is the left one:
      (evil-sort begcol endcol)
      ;; Maybe find maximal column:
      (when eol-col
        (setq eol-col 0)
        (goto-char endpt)
        (while (> (point) begpt)
          (forward-line -1)
          (setq eol-col (max eol-col
                             (evil-column (line-end-position)))))
        (setq endcol (max endcol
                          (min eol-col
                               (1+ (min (1- most-positive-fixnum)
                                        temporary-goal-column))))))
      ;; Begin looping over lines:
      (goto-char endpt)
      (while (> (point) begpt)
        (forward-line -1)
        (if pass-columns
            (apply function begcol endcol args)
          (apply function
                 (save-excursion (evil-move-to-column begcol))
                 (save-excursion (evil-move-to-column endcol t))
                 args))))))
;;
(provide 'devil-common)
