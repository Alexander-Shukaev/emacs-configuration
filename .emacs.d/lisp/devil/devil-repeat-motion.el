(require 'devil-core)
(require 'devil-states)
;;
(defgroup devil-repeat-motion
  nil
  "Devil repeat motion."
  :group 'devil
  :prefix 'devil-repeat-motion-)
;;
(defface devil-repeat-motion-lighter
  '((t
     (:inherit error)))
  "\
Face used to highlight `devil-repeat-motion-lighter'.")
;;
(defcustom devil-repeat-motion-lighter
  ;; As per [1]:
  " [::]"
  "\
Lighter for `devil-repeat-motion-mode'."
  :type 'string)
;;
(defvaralias 'devil-repeat-motion-key-next 'devil-repeat-motion-key)
(defcustom devil-repeat-motion-key
  (kbd "n")
  "Key used to repeat (last) motion."
  :type 'key-sequence)
;;
(defvaralias
  'devil-repeat-motion-key-previous
  'devil-repeat-motion-key-reverse)
(defcustom devil-repeat-motion-key-reverse
  (kbd "N")
  "Key used to repeat (last) motion in reverse direction."
  :type 'key-sequence)
;;
(defun devil-repeat-motion-lighter ()
  "\
Return lighter for `devil-repeat-motion-mode'."
  (unless (zerop (length devil-repeat-motion-lighter))
    (propertize devil-repeat-motion-lighter
                'face
                'devil-repeat-motion-lighter)))
;;
;;;###autoload
(defun devil-repeat-motion
    (key-or-command
     key-or-command-next
     key-or-command-previous
     &optional keymap)
  "\
Make KEY-OR-COMMAND repeatable."
  (setq keymap (if keymap
                   (or (evil-get-auxiliary-keymap keymap 'motion) keymap)
                 evil-motion-state-map))
  (let ((command          (devil-key-command key-or-command          keymap))
        (command-next     (devil-key-command key-or-command-next     keymap))
        (command-previous (devil-key-command key-or-command-previous keymap)))
    (eval
     `(defadvice ,command
          (before
           ,(intern (format "devil-repeat-motion--%s" (symbol-name command)))
           activate)
        ,(format "\
Make `%s' repeatable.
Repeatable with `%s'.
Repeatable with `%s' in reverse direction."
                 command
                 command-next
                 command-previous)
        (when (called-interactively-p 'any)
          (unless (eq last-command #',command)
            ,@(unless (eq keymap evil-motion-state-map)
                `((devil-repeat-motion--make-local-mode-overriding-map)))
            (evil-define-key 'motion devil-repeat-motion-mode-map
              devil-repeat-motion-key-next     #',command-next
              devil-repeat-motion-key-previous #',command-previous)
            (evil-normalize-keymaps)))))
    (list command
          command-next
          command-previous)))
;;
;;;###autoload
(defun devil-repeat-motion-kbd
    (key-or-command
     key-or-command-next
     key-or-command-previous
     &optional keymap)
  "\
Make KEY-OR-COMMAND repeatable."
  (devil-repeat-motion (if (stringp key-or-command)
                           (kbd key-or-command)
                         key-or-command)
                       (if (stringp key-or-command-next)
                           (kbd key-or-command-next)
                         key-or-command-next)
                       (if (stringp key-or-command-previous)
                           (kbd key-or-command-previous)
                         key-or-command-previous)
                       keymap))
;;
;;;###autoload
(defmacro devil-repeat-motions
    (&rest ...)
  "Apply `devil-repeat-motion' to each three consecutive arguments.
Return list of results where each element is the return value of the
corresponding `devil-repeat-motion' application."
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (when (eq (car-safe ...) :keymap)
        (pop ...)
        (push `(setq keymap ,(pop ...)) body))
      (push `(devil-repeat-motion ,(pop ...)
                                  ,(pop ...)
                                  ,(pop ...)
                                  ,(if (eq (car-safe ...) :keymap)
                                       (progn
                                         (pop ...)
                                         `(setq keymap ,(pop ...)))
                                     'keymap))
            body))
    (setq body (nreverse body))
    `(let (keymap)
       (list ,@body))))
;;
;;;###autoload
(defmacro devil-repeat-motions-kbd
    (&rest ...)
  "Apply `devil-repeat-motion-kbd' to each three consecutive arguments.
Return list of results where each element is the return value of the
corresponding `devil-repeat-motion-kbd' application."
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (when (eq (car-safe ...) :keymap)
        (pop ...)
        (push `(setq keymap ,(pop ...)) body))
      (push `(devil-repeat-motion-kbd ,(pop ...)
                                      ,(pop ...)
                                      ,(pop ...)
                                      ,(if (eq (car-safe ...) :keymap)
                                           (progn
                                             (pop ...)
                                             `(setq keymap ,(pop ...)))
                                         'keymap))
            body))
    (setq body (nreverse body))
    `(let (keymap)
       (list ,@body))))
;;
;;;###autoload(autoload 'devil-repeat-motion-mode "devil" nil t)
(define-minor-mode devil-repeat-motion-mode
  "\
Devil repeat motion mode."
  :global t
  :keymap (make-sparse-keymap)
  :lighter (:eval (devil-repeat-motion-lighter))
  ;; -------------------------------------------------------------------------
  (require 'devil)
  ;;
  ;; CAUTION:
  ;;
  ;; Similar to `evil-mode', `devil-repeat-motion-mode' is annotated with the
  ;; weird `autoload' cookie.  That is instead of loading the
  ;; `devil-repeat-motion' feature it performs indirection and loads the
  ;; `devil' feature.  On the one hand, this is nice and we indeed rely on
  ;; this fact (as a usual practice) in the `init-devil' feature.  On the
  ;; other hand, if the `devil-repeat-motion' feature had already been loaded
  ;; (for whatever reason) *before* `devil-repeat-motion-mode' has been called
  ;; for the first time, then when `devil-repeat-motion-mode' is actually
  ;; called, the corresponding `autoload' is ultimately ignored and the
  ;; `devil' feature is never loaded.  As a result, in order to avoid this
  ;; corner case, it is a good idea to explicitly try to load the `devil'
  ;; feature in the beginning of `devil-repeat-motion-mode'.
  ;; -------------------------------------------------------------------------
  (evil-normalize-keymaps))
;;
(defun devil-repeat-motion--make-local-mode-overriding-map ()
  "\
Make and return buffer-local overriding `devil-repeat-motion-mode-map'.

See `minor-mode-overriding-map-alist'."
  (let ((mode     'devil-repeat-motion-mode)
        (mode-map 'devil-repeat-motion-mode-map))
    (unless (local-variable-p mode-map)
      (set-keymap-parent
       (set (make-local-variable mode-map)
            (or (cdr (assoc mode minor-mode-overriding-map-alist))
                (let ((keymap (make-sparse-keymap)))
                  (push `(,mode . ,keymap) minor-mode-overriding-map-alist)
                  keymap)))
       (or (cdr (assoc mode minor-mode-map-alist))
           (default-value mode-map))))
    (symbol-value mode-map)))
;;
(provide 'devil-repeat-motion)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] URL `http://en.wikipedia.org/wiki/Repeat_sign'
;;  ==========================================================================
;;  }}} References
