(require 'devil-common)
;;
(require 'evil-core)
;;
(put 'evil-without-input-method-hooks 'lisp-indent-function 'defun)
;;
(defun devil-set-initial-state
    (symbol-or-regexp state)
  "\
Set the initial state for major mode(s) matching SYMBOL-OR-REGEXP to STATE.

This is the state the buffer comes up in.

Return the corresponding list of major mode symbols."
  (if (stringp symbol-or-regexp)
      (let (symbols)
        (mapatoms #'(lambda
                        (symbol)
                      (when (and (fboundp symbol)
                                 (not (memq symbol minor-mode-list))
                                 (string-match-p symbol-or-regexp
                                                 (symbol-name symbol)))
                        (evil-set-initial-state symbol state)
                        (push symbol symbols))))
        symbols)
    (when (and (fboundp symbol-or-regexp)
               (not (memq symbol-or-regexp minor-mode-list)))
      (evil-set-initial-state symbol-or-regexp state)
      (list symbol-or-regexp))))
;;
(provide 'devil-core)
