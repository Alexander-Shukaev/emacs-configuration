(require 'devil-core)
(require 'devil-search)
;;
(eval-when-compile
  (require 'cl-lib))
;;
(defvar devil-isearch-forward
  t
  "Direction of last search; if non-nil, forward, otherwise backward.")
;;
(defvar devil-isearch-lazy-highlight-buffer-list
  nil
  "List of currently lazy-highlighted buffers.")
;;
(defvar-local devil-isearch-echo-timer
  nil
  "Timer for displaying of search message.")
(put 'devil-isearch-echo-timer 'permanent-local t)
;;
(defvar-local devil-isearch-highlight-timer
  nil
  "Timer for highlighting of current search match.")
(put 'devil-isearch-highlight-timer 'permanent-local t)
;;
(defvar-local devil-isearch-lazy-highlight-timer
  nil
  "Timer for lazy-highlighting of all search matches.")
(put 'devil-isearch-lazy-highlight-timer 'permanent-local t)
;;
(define-advice isearch-del-char
    (:around (function &rest ...) devil-isearch)
  "\
Exit search if `devil-isearch-p' is non-nil and all input is deleted."
  (if (and (bound-and-true-p devil-isearch-p) (zerop (length isearch-string)))
      (let (search-nonincremental-instead)
        (setq isearch-success nil)
        (isearch-exit))
    (apply function ...)))
;;
(define-advice isearch-delete-char
    (:around (function &rest ...) devil-isearch)
  "\
Call `isearch-del-char' if `devil-isearch-p' is non-nil."
  (if (bound-and-true-p devil-isearch-p)
      (isearch-del-char)
    (apply function ...)))
;;
(define-advice isearch-done
    (:around (function &rest ...) devil-isearch)
  "\
Call `isearch-done' without displaying and logging of messages."
  (devil-without-messages
    (apply function ...)))
;;
(define-advice isearch-lazy-highlight-search
    (:around (function &rest ...) devil-isearch)
  "\
Call `isearch-lazy-highlight-search' without wrapping search."
  (let (devil-search-wrap-p)
    (apply function ...)))
;;
(define-advice isearch-message-prefix
    (:around (function &rest ...) devil-isearch)
  "\
Return `devil-search-prompt' if `devil-isearch-p' is non-nil."
  (if (bound-and-true-p devil-isearch-p)
      (or (devil-search-prompt isearch-forward) "")
    (apply function ...)))
;;
(defun devil-isearch-search-fun ()
  "\
Return Isearch-compatible search function.
Based on `isearch-forward', `isearch-regexp', and `devil-search-wrap-p'."
  (devil-search-function isearch-forward
                         isearch-regexp
                         devil-search-wrap-p
                         (bound-and-true-p isearch-filter-predicate)))
;;
(defun devil-isearch-silence
    (&optional force)
  "\
Disable displaying of search message.

If FORCE is non-nil, disable displaying unconditionally.

If `this-command' is a search command, i.e. belongs to
`devil-search-commands', do nothing (to avoid flicker)."
  (remove-hook 'pre-command-hook #'devil-isearch-silence :local)
  (when (or force (not (memq this-command devil-search-commands)))
    (when devil-isearch-echo-timer
      (cancel-timer devil-isearch-echo-timer))
    (devil-without-timers
      (evil-echo-area-restore))))
(put 'devil-isearch-silence 'permanent-local-hook t)
;;
(defun devil-isearch-dehighlight
    (&optional force)
  "\
Disable highlighting of current search match.

If FORCE is non-nil, disable highlighting unconditionally.

If `this-command' is a search command, i.e. belongs to
`devil-search-commands', do nothing (to avoid flicker)."
  (remove-hook 'pre-command-hook #'devil-isearch-dehighlight :local)
  (when (or force (not (memq this-command devil-search-commands)))
    (when devil-isearch-highlight-timer
      (cancel-timer devil-isearch-highlight-timer))
    (devil-without-timers
      (isearch-dehighlight))))
(put 'devil-isearch-dehighlight 'permanent-local-hook t)
;;
(defun devil-isearch-lazy-dehighlight
    (&optional force)
  "\
Disable lazy-highlighting of all search matches.

If FORCE is non-nil, disable lazy-highlighting unconditionally.

If `this-command' is a search command, i.e. belongs to
`devil-search-commands', do nothing (to avoid flicker)."
  (remove-hook 'pre-command-hook #'devil-isearch-lazy-dehighlight :local)
  (when (or force (not (memq this-command devil-search-commands)))
    (when devil-isearch-lazy-highlight-timer
      (cancel-timer devil-isearch-lazy-highlight-timer))
    (devil-without-timers
      (save-current-buffer
        (dolist (buffer (devil-isearch-lazy-highlight-buffer-list))
          (when (buffer-live-p buffer)
            (set-buffer buffer)
            (setq isearch-lazy-highlight-last-string nil)
            (lazy-highlight-cleanup :force))))
      (setq devil-isearch-lazy-highlight-buffer-list nil))))
(put 'devil-isearch-lazy-dehighlight 'permanent-local-hook t)
;;
(cl-macrolet
    ((define-hook-forced (name)
       (let ((name-forced (intern (format "%s-forced" name))))
         `(progn
            (defun ,name-forced ()
              ,(format "\
Call `%s' with FORCE as non-nil."
                       name)
              (remove-hook 'isearch-mode-hook #',name-forced :local)
              (,name :force))
            (put ',name-forced 'permanent-local-hook t)))))
  (define-hook-forced devil-isearch-silence)
  (define-hook-forced devil-isearch-dehighlight)
  (define-hook-forced devil-isearch-lazy-dehighlight))
;;
(defun devil-isearch-clean-overlays ()
  "\
Clean overlays produced by Isearch.

If `this-command' is a search command, i.e. belongs to
`devil-search-commands', do nothing (to avoid flicker)."
  (dolist (hook '(isearch-mode-hook pre-command-hook))
    (remove-hook hook #'devil-isearch-clean-overlays :local))
  (unless (memq this-command devil-search-commands)
    (isearch-clean-overlays)))
(put 'devil-isearch-clean-overlays 'permanent-local-hook t)
;;
(defun devil-isearch-echo
    (message)
  "\
Enable displaying of search message.

Duration for displaying is determined by `devil-search-echo-duration'."
  (unless (zerop (length message))
    (when devil-isearch-echo-timer
      (cancel-timer devil-isearch-echo-timer))
    (evil-echo-area-save)
    ;; -----------------------------------------------------------------------
    (cl-macrolet
        ((define-echo (message)
           (if (< emacs-major-version 25)
               '(evil-echo "%s" message)
             '(let ((text-quoting-style 'grave))
                (evil-echo (devil-format-quote message))))))
      (devil-without-timers
        (define-echo message)))
    ;;
    ;; BUG:
    ;;
    ;; Since Emacs 25, `message' and `format' functions no longer correctly
    ;; preserve text properties (e.g. faces) when used with the %s control
    ;; sequence.  However, incidentally also since Emacs 25, `format-message',
    ;; `message', and `error' functions translate quoting style of grave
    ;; accents (`) and apostrophes (') according to the value of variable
    ;; `text-quoting-style' as per [1, 2].
    ;; -----------------------------------------------------------------------
    (add-hook 'isearch-mode-hook #'devil-isearch-silence-forced nil :local)
    (add-hook 'pre-command-hook  #'devil-isearch-silence        nil :local)
    (when devil-search-echo-duration
      (if (numberp devil-search-echo-duration)
          (setq devil-isearch-echo-timer
            (run-at-time devil-search-echo-duration
                         nil
                         #'devil-isearch-silence-forced))
        (signal 'wrong-type-argument
                (list 'numberp devil-search-echo-duration))))))
;;
(defun devil-isearch-highlight
    (&optional beg end)
  "\
Enable highlighting of current search match.

Duration for highlighting is determined by `devil-search-highlight-duration'."
  (setq
    beg (or beg (match-beginning (or devil-search-regexp-group 0)))
    end (or end (match-end       (or devil-search-regexp-group 0))))
  (when (/= beg end)
    (when devil-isearch-highlight-timer
      (cancel-timer devil-isearch-highlight-timer))
    (isearch-highlight beg end)
    (let ((hook 'isearch-mode-hook))
      (add-hook hook #'devil-isearch-dehighlight-forced nil :local)
      (add-hook hook #'devil-isearch-clean-overlays     nil :local))
    (let ((hook 'pre-command-hook))
      (add-hook hook #'devil-isearch-dehighlight        nil :local)
      (add-hook hook #'devil-isearch-clean-overlays     nil :local))
    (when devil-search-highlight-duration
      (if (numberp devil-search-highlight-duration)
          (setq devil-isearch-highlight-timer
            (run-at-time devil-search-highlight-duration
                         nil
                         #'devil-isearch-dehighlight-forced))
        (signal 'wrong-type-argument
                (list 'numberp devil-search-highlight-duration))))))
;;
(defun devil-isearch-lazy-highlight
    (&optional beg end)
  "\
Enable lazy-highlighting of all search matches.

BEG and END specify the bounds within which lazy-highlighting should occur.

Duration for lazy-highlighting is determined by
`devil-search-lazy-highlight-duration'."
  (when devil-isearch-lazy-highlight-timer
    (cancel-timer devil-isearch-lazy-highlight-timer))
  (setq devil-isearch-lazy-highlight-buffer-list nil)
  (devil-save-selected-window
    (dolist (window (window-list-1 nil
                                   :no-minibuf
                                   devil-search-lazy-highlight-all-frames))
      (let ((buffer (window-buffer window)))
        (when (and (bufferp buffer)
                   (not (memq buffer
                              devil-isearch-lazy-highlight-buffer-list)))
          (push buffer devil-isearch-lazy-highlight-buffer-list)
          (select-window window :norecord)
          (let ((lazy-highlight-initial-delay 0)
                ;; -----------------------------------------------------------
                ;; [RESOLVED] bug#25751:
                ;;
                (lazy-highlight-interval      0)
                (lazy-highlight-max-at-a-time nil)
                ;; -----------------------------------------------------------
                (isearch-case-fold-search     case-fold-search)
                (isearch-search-fun-function  #'devil-isearch-search-fun))
            (dolist (variable '(isearch-lazy-highlight-overlays
                                isearch-lazy-highlight-wrapped
                                isearch-lazy-highlight-start-limit
                                isearch-lazy-highlight-end-limit
                                isearch-lazy-highlight-start
                                isearch-lazy-highlight-end
                                isearch-lazy-highlight-timer
                                isearch-lazy-highlight-last-string
                                isearch-lazy-highlight-window
                                isearch-lazy-highlight-window-group
                                isearch-lazy-highlight-window-start
                                isearch-lazy-highlight-window-start-changed
                                isearch-lazy-highlight-window-end
                                isearch-lazy-highlight-window-end-changed
                                isearch-lazy-highlight-point-min
                                isearch-lazy-highlight-point-max
                                isearch-lazy-highlight-buffer
                                isearch-lazy-highlight-case-fold-search
                                isearch-lazy-highlight-regexp
                                isearch-lazy-highlight-lax-whitespace
                                isearch-lazy-highlight-regexp-lax-whitespace
                                isearch-lazy-highlight-regexp-function
                                isearch-lazy-highlight-forward
                                isearch-lazy-highlight-error
                                isearch-lazy-count-current
                                isearch-lazy-count-total
                                isearch-lazy-count-hash))
              (make-local-variable variable))
            (setq
              isearch-lazy-highlight-start   (point)
              isearch-lazy-highlight-end     (point)
              isearch-lazy-highlight-wrapped nil)
            (when (and (if (isearch-lazy-highlight-new-loop beg end)
                           (if (fboundp 'isearch-lazy-highlight-start)
                               (isearch-lazy-highlight-start)
                             (devil-negate isearch-lazy-highlight-overlays)
                             (lazy-highlight-cleanup :force)
                             (isearch-lazy-highlight-update))
                         (unless isearch-lazy-highlight-overlays
                           (isearch-lazy-highlight-update)))
                       (> emacs-major-version 26))
              (devil-assert isearch-lazy-highlight-timer))
            (when (< emacs-major-version 27)
              (devil-negate isearch-lazy-highlight-timer)))))))
  (let ((hook 'isearch-mode-hook))
    (add-hook hook #'devil-isearch-lazy-dehighlight-forced nil :local))
  (let ((hook 'pre-command-hook))
    (add-hook hook #'devil-isearch-lazy-dehighlight        nil :local))
  (when devil-search-lazy-highlight-duration
    (if (numberp devil-search-lazy-highlight-duration)
        (setq devil-isearch-lazy-highlight-timer
          (run-at-time devil-search-lazy-highlight-duration
                       nil
                       #'devil-isearch-lazy-dehighlight-forced))
      (signal 'wrong-type-argument
              (list 'numberp devil-search-lazy-highlight-duration)))))
;;
(defun devil-isearch-input
    (&optional string forward regexp-p history-p)
  "\
Search for (user-entered) input in specified direction."
  (setq devil-isearch-forward forward)
  (let ((devil-isearch-p t)
        (isearch-lazy-highlight (and devil-search-incremental-lazy-highlight-p
                                     (or isearch-lazy-highlight t)))
        (isearch-search-fun-function #'devil-isearch-search-fun)
        ;; -------------------------------------------------------------------
        ;; Avoid flicker:
        ;;
        (lazy-highlight-cleanup (not devil-search-lazy-highlight-p))
        ;; -------------------------------------------------------------------
        (point (point))
        (search-highlight devil-search-incremental-highlight-p)
        (search-nonincremental-instead (not devil-search-incremental-p)))
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; Set the input method locally rather than globally to ensure that
    ;; Isearch clears the input method when it's finished.
    ;;
    (evil-without-input-method-hooks
      (setq current-input-method evil-input-method)
      (let ((isearch-mode-hook        (copy-sequence isearch-mode-hook))
            (isearch-hide-immediately devil-search-hide-immediately-p)
            (search-invisible         devil-search-invisible-p))
        (unless (zerop (length string))
          (add-to-list 'isearch-mode-hook
                       #'(lambda ()
                           ;; ------------------------------------------------
                           ;; NOTE:
                           ;;
                           ;; The value of `isearch-regexp' is equal to the
                           ;; value of `regexp-p' since the former is set by
                           ;; either `isearch-forward' or `isearch-backward'
                           ;; (which are called further).
                           ;;
                           (when isearch-regexp
                             (setq string (regexp-quote string)))
                           ;; ------------------------------------------------
                           (setq
                             isearch-message string
                             isearch-string  string)
                           (ignore-errors (isearch-search-and-update)))
                       ;; ----------------------------------------------------
                       ;; NOTE:
                       ;;
                       ;; The following `lambda' seems to deliver the same
                       ;; result.
                       ;;
                       ;; #'(lambda ()
                       ;;     (ignore-errors (isearch-yank-string string)))
                       ;; ----------------------------------------------------
                       ))
        (if forward (isearch-forward regexp-p) (isearch-backward regexp-p)))
      (when history-p
        (devil-search-history-push isearch-string))
      (setq current-input-method nil))
    ;; -----------------------------------------------------------------------
    (when (/= (point) point)
      (let (beg end)
        ;; -------------------------------------------------------------------
        ;; NOTE:
        ;;
        ;; Move the point to the beginning of match only if Isearch has really
        ;; moved the point.  Isearch doesn't move the point only if <C-g> is
        ;; hit twice to exit the search, in which case we shouldn't move the
        ;; point either:
        ;;
        (when isearch-other-end
          (if forward
              (setq
                beg isearch-other-end
                end (point))
            (setq
              beg (point)
              end isearch-other-end))
          (goto-char beg))
        (when (and (= (point) point) (not (zerop (length isearch-string))))
          (if forward (isearch-repeat-forward) (isearch-repeat-backward))
          (isearch-exit)
          (when isearch-other-end
            (if forward
                (setq
                  beg isearch-other-end
                  end (point))
              (setq
                beg (point)
                end isearch-other-end))
            (goto-char beg)))
        ;; -------------------------------------------------------------------
        (when (and beg end)
          (when devil-search-echo-p
            (devil-isearch-echo (devil-search-format-message isearch-string
                                                             point
                                                             beg
                                                             forward)))
          (when devil-search-highlight-p
            (devil-isearch-highlight beg end))
          (when devil-search-lazy-highlight-p
            (devil-isearch-lazy-highlight))
          (evil-range beg end))))))
;;
(defun devil-isearch-input-region
    (beg end &optional forward regexp-p history-p)
  "\
Search for text from BEG to END as input in specified direction."
  (setq devil-isearch-forward forward)
  (let ((string (buffer-substring-no-properties beg end)))
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; Check for empty string:
    ;;
    ;; (when (zerop (length string))
    ;;   (user-error "No %s selected" (if regexp-p "pattern" "string")))
    ;;
    ;; ... is unnecessary here because this would simply mean that search
    ;; starts from scratch, without any initial input (see
    ;; `devil-isearch-input').
    (devil-isearch-input string forward regexp-p history-p)))
;;
(defun devil-isearch
    (string &optional forward regexp-p history-p origin)
  "\
Search for STRING in specified direction.
If FORWARD is non-nil, search forward, otherwise backward.
If REGEXP-P is non-nil, STRING is treated as regular expression.
ORIGIN is a position to search from, and if omitted, it is one more
than the current position."
  (unless (stringp string)
    (signal 'wrong-type-argument (list 'stringp string)))
  (setq devil-isearch-forward forward)
  (when (zerop (length string))
    (user-error "%s is empty" (if regexp-p "Pattern" "String")))
  ;; -------------------------------------------------------------------------
  ;; NOTE:
  ;;
  ;; Discard all text properties from the search string with
  ;;
  (setq string (substring-no-properties string))
  ;;
  ;; ... rather than with
  ;;
  ;; (set-text-properties 0 (length string) nil string)
  ;;
  ;; ... because we don't want to modify the original search string passed
  ;; into this function.
  ;; -------------------------------------------------------------------------
  (let* ((point  (point))
         (origin (or origin (if forward
                                (min (point-max) (1+ point))
                              (if (bound-and-true-p evil-mode)
                                  point
                                (max (point-min) (1- point))))))
         ;; ------------------------------------------------------------------
         (isearch-forward             forward)
         (isearch-regexp              regexp-p)
         (isearch-search-fun-function #'devil-isearch-search-fun)
         (isearch-string              string)
         ;; ------------------------------------------------------------------
         (case-fold-search (when (or (not search-upper-case)
                                     (isearch-no-upper-case-p string
                                                              regexp-p))
                             isearch-case-fold-search))
         (search-invisible devil-search-invisible-p))
    (isearch-update-ring string regexp-p)
    (when history-p
      (devil-search-history-push string))
    (goto-char origin)
    (condition-case nil
        (funcall (funcall isearch-search-fun-function) string)
      ((search-failed)
       (goto-char point)
       (user-error "%s not found: %s"
                   (if regexp-p "Pattern" "String")
                   ;; --------------------------------------------------------
                   (devil-search-pattern string)
                   ;;
                   ;; NOTE:
                   ;;
                   ;; Unfortunately, `user-error' seems to discard all the
                   ;; text properties.  Thus, `devil-search-pattern' is
                   ;; useless here.
                   ;; --------------------------------------------------------
                   )))
    (let ((beg (match-beginning (or devil-search-regexp-group 0)))
          (end (match-end       (or devil-search-regexp-group 0))))
      (goto-char beg)
      (when devil-search-echo-p
        (devil-isearch-echo (devil-search-format-message string
                                                         origin
                                                         beg
                                                         forward)))
      (when devil-search-highlight-p
        (devil-isearch-highlight beg end))
      (when devil-search-lazy-highlight-p
        (devil-isearch-lazy-highlight))
      (evil-range beg end))))
;;
(defun devil-isearch-region
    (beg end &optional forward regexp-p history-p bounds)
  "\
Search for text from BEG to END in specified direction."
  (setq devil-isearch-forward forward)
  (let ((string (buffer-substring-no-properties beg end)))
    (when (zerop (length string))
      (user-error "No %s selected" (if regexp-p "pattern" "string")))
    (when regexp-p
      (setq string (regexp-quote string))
      (when bounds
        (setq string (concat (car bounds)
                             ;; TODO:
                             ;; "\\(?1:"
                             string
                             ;; "\\)"
                             (cdr bounds)))))
    (let ((origin (if forward beg end)))
      (devil-isearch string forward regexp-p history-p origin))))
;;
;; TODO:
;; (defun devil-isearch-input-thing
;;     (thing &optional forward bounded history-p)
;;   "Search for THING at point as input in specified direction.
;; If FORWARD is non-nil, search forward, otherwise backward.
;; If BOUNDED is non-nil, the search pattern is surrounded with
;; bound patterns according to `devil-search-thing-bound-patterns-alist'."
;;   )
;;
(defun devil-isearch-thing
    (thing &optional forward bounded history-p)
  "\
Search for THING at point in specified direction.
If FORWARD is non-nil, search forward, otherwise backward.
If BOUNDED is non-nil, the search pattern is surrounded with
bound patterns according to `devil-search-thing-bound-patterns-alist'."
  (let ((bounds   (devil-bounds-of-thing-at-point thing forward))
        (patterns (when bounded
                    (assoc-default thing
                                   devil-search-thing-bound-patterns-alist
                                   #'eq))))
    (or (condition-case nil
            (when bounds
              (devil-isearch-region (car bounds)
                                    (cdr bounds)
                                    forward
                                    :regexp-p
                                    history-p
                                    (when patterns
                                      (cons (funcall (car patterns))
                                            (funcall (cdr patterns))))))
          (user-error nil))
        (user-error "No `%s' at point" thing))))
;;
(defun devil-isearch-lazy-highlight-buffer-list
    (&optional buffer)
  "\
Return list of currently lazy-highlighted buffers or BUFFER."
  (setq buffer (or buffer (current-buffer)))
  (unless (bufferp buffer)
    (signal 'wrong-type-argument (list 'bufferp buffer)))
  (or devil-isearch-lazy-highlight-buffer-list (list buffer)))
;;
(provide 'devil-isearch)
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(elisp) Formatting Strings'
;;  [2] Info node `(elisp) Text Quoting Style'
;;  ==========================================================================
;;  }}} References
