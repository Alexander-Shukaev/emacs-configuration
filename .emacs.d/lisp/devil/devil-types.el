(require 'devil-common)
(require 'devil-macros)
;;
(require 'evil-types)
;;
(evil-define-interactive-code "i"
  "Ignore argument, i.e. always nil."
  (list nil))
;;
(provide 'devil-types)
