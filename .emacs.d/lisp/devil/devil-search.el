(require 'devil-common)
;;
;; ---------------------------------------------------------------------------
;; TODO:
;;
(devil-ignore-progn
  (eval-and-compile
    ;; Prevent load of `evil-search':
    (add-to-list 'features 'evil-search))
  ;;
  (require 'evil-search)
  ;;
  (eval-and-compile
    (devil-negate (devil-load-history "evil-search"))))
;; ---------------------------------------------------------------------------
;;
(require 'memoize)
;;
(defgroup devil-search
  nil
  "Devil search."
  :group 'devil
  :prefix 'devil-search-)
;;
(defface devil-search-string
  '((t
     (:inherit font-lock-string-face)))
  "Face used to highlight search string.")
;;
(defface devil-search-backward-prompt
  '((t
     (:inherit font-lock-warning-face)))
  "Face used to highlight backward search prompt.")
;;
(defface devil-search-forward-prompt
  '((t
     (:inherit font-lock-warning-face)))
  "Face used to highlight forward search prompt.")
;;
(defcustom devil-search-commands
  '(devil-search-forward
    devil-search-backward
    devil-search-input-forward
    devil-search-input-backward
    devil-search-match-next
    devil-search-match-previous
    devil-search-word-forward
    devil-search-word-backward
    devil-search-WORD-forward
    devil-search-WORD-backward
    devil-search-symbol-forward
    devil-search-symbol-backward
    devil-search-evil-word-forward
    devil-search-evil-word-backward
    devil-search-evil-WORD-forward
    devil-search-evil-WORD-backward
    devil-search-evil-symbol-forward
    devil-search-evil-symbol-backward)
  "List of search commands."
  :type '(repeat symbol))
;;
(defcustom devil-search-operators
  '(devil-search-forward
    devil-search-backward
    devil-search-input-forward
    devil-search-input-backward)
  "List of search operators."
  :type '(repeat symbol))
;;
(defcustom devil-search-thing-bound-patterns-alist
  '((evil-word     . (devil-word-beg     . devil-word-end    ))
    (evil-WORD     . (devil-WORD-beg     . devil-WORD-end    ))
    (evil-symbol   . (devil-symbol-beg   . devil-symbol-end  ))
    (evil-sentence . (devil-sentence-beg . devil-sentence-end)))
  "Association list of bound patterns for thing at point."
  :type '(alist :key-type   symbol
                :value-type (cons symbol symbol)))
;;
(defcustom devil-search-backward-prompt
  "\\"
  "String to use for backward search prompt."
  :type 'string)
;;
(defcustom devil-search-forward-prompt
  "\/"
  "String to use for forward search prompt."
  :type 'string)
;;
(defcustom devil-search-echo-duration
  10
  "Duration (seconds) for displaying of search message."
  :type '(choice (const  :tag "Never" nil)
                 (number :tag "Seconds")))
;;
(defcustom devil-search-echo-p
  t
  "Whether to display search message."
  :type 'boolean)
;;
(defcustom devil-search-highlight-duration
  10
  "Duration (seconds) for highlighting of current search match."
  :type '(choice (const  :tag "Never" nil)
                 (number :tag "Seconds")))
;;
(defcustom devil-search-highlight-p
  t
  "Whether to highlight current search match."
  :type 'boolean)
;;
(defcustom devil-search-lazy-highlight-all-frames
  nil
  "Specification of the frames to consider for lazy-highlighting:

- nil means consider all windows on the selected frame only.

- t means consider all windows on all existing frames.

- `visible' means consider all windows on all visible frames on
  the current terminal.

- 0 (the number zero) means consider all windows on all visible
  and iconified frames on the current terminal."
  :type `(choice (const :tag "All windows on the selected frame only"
                        nil)
                 (const :tag "All windows on all existing frames"
                        t)
                 (const :tag ,(concat "All windows on all visible frames"
                                      " "
                                      "on the current terminal")
                        visible)
                 (const :tag ,(concat "All windows on all visible and"
                                      " "
                                      "iconified frames"
                                      " "
                                      "on the current terminal")
                        0)))
;;
(defcustom devil-search-lazy-highlight-duration
  5
  "Duration (seconds) for lazy-highlighting of all search matches."
  :type '(choice (const  :tag "Never" nil)
                 (number :tag "Seconds")))
;;
(defcustom devil-search-lazy-highlight-p
  t
  "Whether to lazy-highlight all search matches."
  :type 'boolean)
;;
(defcustom devil-search-incremental-highlight-p
  t
  "Whether to incrementally highlight current search match."
  :type 'boolean)
;;
(defcustom devil-search-incremental-lazy-highlight-p
  t
  "Whether to incrementally lazy-highlight all search matches."
  :type 'boolean)
;;
(defcustom devil-search-incremental-p
  t
  "Whether search is incremental."
  :type 'boolean)
;;
(with-eval-after-load (eval-when-compile
                        (if (< emacs-major-version 26) "isearch" 'isearch))
  (defcustom devil-search-invisible-p
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; Prefer to keep custom variables `devil-search-invisible-p' and
    ;; `search-invisible' separate, i.e. without alias binding via
    ;; `defvaralias':
    ;;
    search-invisible
    ;; -----------------------------------------------------------------------
    "See `search-invisible'."
    :type '(choice (const :tag "Match hidden text"       t)
                   (const :tag "Open overlays"           open)
                   (const :tag "Don't match hidden text" nil)))
  ;;
  (defcustom devil-search-hide-immediately-p
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; Prefer to keep custom variables `devil-search-hide-immediately-p' and
    ;; `isearch-hide-immediately' separate, i.e. without alias binding via
    ;; `defvaralias':
    ;;
    isearch-hide-immediately
    ;; -----------------------------------------------------------------------
    "See `isearch-hide-immediately'."
    :type 'boolean))
;;
(defcustom devil-search-regexp-group
  0
  "Number used to match concrete group in search pattern."
  :type 'number)
;;
(defcustom devil-search-regexp-p
  t
  "Whether search is based on regular expressions."
  :type 'boolean)
;;
(defcustom devil-search-wrap-p
  t
  "Whether search wraps around top or bottom of buffer."
  :type 'boolean)
;;
(defcustom devil-search-history-p
  t
  "Whether search history can be recorded."
  :type 'boolean)
;;
(defcustom devil-search-thing-bounded-p
  t
  "Whether search for thing at point should be bounded."
  :type 'boolean)
;;
(defcustom devil-search-wrap-message-bottom
  (concat "Search wrapped around"
          " "
          (propertize "Bottom"
                      'face
                      'link)
          " "
          "of buffer")
  "Message to display when search wraps around bottom of buffer."
  :type 'string)
;;
(defcustom devil-search-wrap-message-top
  (concat "Search wrapped around"
          " "
          (propertize "Top"
                      'face
                      'link)
          " "
          "of buffer")
  "Message to display when search wraps around top of buffer."
  :type 'string)
;;
(defcustom devil-search-format-message-function
  #'devil-search-format-message-default
  "Function to format search message."
  :type 'function)
;;
(defvar devil-search-history
  nil
  "Search history.")
;;
(defvar devil-search-forward-map
  (let ((keymap (devil-define-keys-kbd (make-sparse-keymap)
                  "c" #'devil-inner-comment
                  "w" #'evil-inner-word
                  "W" #'evil-inner-WORD
                  "s" #'evil-inner-symbol
                  "S" #'evil-inner-sentence)))
    (devil-define-keys-with-prefix-kbd keymap "r"
      "w" #'devil-search-word-forward
      "W" #'devil-search-WORD-forward
      "s" #'devil-search-symbol-forward)
    keymap)
  "Keymap for the `devil-search-forward' operator.")
;;
(defvar devil-search-backward-map
  (let ((keymap (devil-define-keys-kbd (make-sparse-keymap)
                  "c" #'devil-inner-comment
                  "w" #'evil-inner-word
                  "W" #'evil-inner-WORD
                  "s" #'evil-inner-symbol
                  "S" #'evil-inner-sentence)))
    (devil-define-keys-with-prefix-kbd keymap "r"
      "w" #'devil-search-word-backward
      "W" #'devil-search-WORD-backward
      "s" #'devil-search-symbol-backward)
    keymap)
  "Keymap for the `devil-search-backward' operator.")
;;
(defvar devil-search-input-forward-map
  (copy-keymap devil-search-forward-map)
  "Keymap for the `devil-search-input-forward' operator.")
;;
(defvar devil-search-input-backward-map
  (copy-keymap devil-search-backward-map)
  "Keymap for the `devil-search-input-backward' operator.")
;;
(defun devil-search-history-push
    (string)
  "Push STRING into search history."
  (unless (equal (car-safe devil-search-history) string)
    (push string devil-search-history)))
;;
(defun devil-search-wrap-message
    (beg end &optional forward)
  "Return appropriate wrap message if applicable."
  (cond ((and forward       (<  end beg))
         devil-search-wrap-message-bottom)
        ((and (not forward) (>= end beg))
         devil-search-wrap-message-top)))
;;
(defun devil-search-pattern
    (string)
  "Return fontified search pattern."
  (let ((print-escape-newlines        t)
        (prog-mode-hook               nil)
        (emacs-lisp-mode-hook         nil)
        (minor-mode-list              nil)
        (after-change-major-mode-hook nil))
    (devil-fontify-string (prin1-to-string string) #'emacs-lisp-mode +1 -1)))
;;
(defun devil-search-prompt
    (&optional forward)
  "Return fontified search prompt for specified direction."
  (if forward
      (propertize devil-search-forward-prompt
                  'face
                  'devil-search-forward-prompt)
    (propertize devil-search-backward-prompt
                'face
                'devil-search-backward-prompt)))
;;
(defun devil-search-format-message-default
    (string beg end &optional forward)
  "Return formatted (by default) search message for specified direction."
  (let ((prompt       (devil-search-prompt       forward))
        (pattern      (devil-search-pattern      string))
        (wrap-message (devil-search-wrap-message beg
                                                 end
                                                 forward)))
    (concat prompt
            pattern
            (unless (zerop (length wrap-message))
              (let ((width (string-width wrap-message)))
                (unless (devil-display-graphic-p)
                  (1+ width))
                (format-mode-line (propertize " "
                                              'display
                                              `((space :align-to
                                                       (- right
                                                          right-fringe
                                                          right-margin
                                                          ,width)))))))
            wrap-message)))
;;
(defun devil-search-format-message
    (string beg end &optional forward)
  "Return formatted search message for specified direction."
  (funcall (or devil-search-format-message-function
               #'devil-search-format-message-default)
           string
           beg
           end
           forward))
;;
(defun devil-search-ring
    (&optional regexp-p)
  "Return the value of either `search-ring' or `regexp-search-ring'.
If REGEXP-P is non-nil, return the value of `regexp-search-ring',
otherwise `search-ring'."
  (if regexp-p (car-safe regexp-search-ring) (car-safe search-ring)))
;;
(defun devil-search-call-until
    (function string &optional bound noerror count predicate)
  "Repeatedly call search FUNCTION until PREDICATE call returns non-nil.
FUNCTION is search function that modifies `match-data' (e.g. one
of `re-search-backward', `re-search-forward', `search-backward',
`search-forward').
STRING, BOUND, NOERROR, and COUNT arguments are forwarded to
search FUNCTION calls.
If PREDICATE is non-nil, must be function as per
`isearch-filter-predicate'(e.g. `isearch-filter-visible').
Return the result of the last search FUNCTION call as soon as the
corresponding PREDICATE call returns non-nil (for the last search
match) for the very first time if PREDICATE is non-nil, otherwise
return the result of the very first search FUNCTION call."
  (let ((result (funcall function string bound noerror count)))
    (when predicate
      (while (when result
               (let ((beg (match-beginning (or devil-search-regexp-group 0)))
                     (end (match-end       (or devil-search-regexp-group 0))))
                 (not (funcall predicate beg end))))
        (setq result (funcall function string bound noerror count))))
    result))
;;
(defmemoize devil-search-function
    (&optional forward regexp-p wrap-p predicate)
  "Return search function.
If FORWARD is non-nil, search forward, otherwise backward.
If REGEXP-P is non-nil, input is treated as regular expression.
If WRAP-P is non-nil, search wraps around top or bottom of buffer.
If PREDICATE is non-nil, must be function as per
`isearch-filter-predicate' (e.g. `isearch-filter-visible')."
  (unless (functionp predicate)
    (setq predicate (symbol-value predicate)))
  (let ((function (if regexp-p
                      (if forward
                          #'re-search-forward
                        #'re-search-backward)
                    (if forward
                        #'search-forward
                      #'search-backward))))
    `(lambda
         (string &optional bound noerror count)
       (let* ((point  (point))
              (result (devil-search-call-until #',function
                                               string
                                               bound
                                               ,(if wrap-p t :noerror)
                                               count
                                               #',predicate)))
         (unless result
           ,@(if wrap-p
                 `((goto-char ,(if forward '(point-min) '(point-max)))
                   (unwind-protect
                       (setq result
                         (devil-search-call-until #',function
                                                  string
                                                  ,(if forward
                                                       '(max (or bound
                                                                 (point-min))
                                                             (1- point))
                                                     '(min (or bound
                                                               (point-max))
                                                           (1+ point)))
                                                  noerror
                                                  count
                                                  #',predicate))
                     (unless result
                       (goto-char point))))
               '((goto-char point))))
         (devil-assert (or result (= (point) point)))
         result))))
;;
(provide 'devil-search)
