(require 'devil-common)
;;
(require 'evil-easymotion)
;;
(require 'avy)
;;
(require 'macroexp)
;;
;;;###autoload
(defmacro devil-easy-motion-kbd
    (key motion &rest ...)
  (declare (debug  t)
           (indent defun))
  (macroexpand-all `(evilem-define ,(cond ((stringp key)
                                           (kbd key))
                                          ((vectorp key)
                                           key)
                                          (t
                                           `(if (stringp ,key)
                                                (kbd ,key)
                                              ,key)))
                                   ,motion
                                   ,@...)))
;;
;;;###autoload
(defmacro devil-easy-motions-kbd
    (&rest ...)
  (declare (debug  t)
           (indent defun))
  (let (body)
    (while ...
      (push `(devil-easy-motion-kbd ,(pop ...) ,(pop ...)) body))
    (setq body (nreverse body))
    `(list ,@body)))
;;
(provide 'devil-easy-motion)
