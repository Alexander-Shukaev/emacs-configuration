(eval-and-compile
  (unless (fboundp 'evil-update-insert-state-bindings)
    (fset 'evil-update-insert-state-bindings #'ignore))
  ;;
  (with-eval-after-load 'evil-vars
    (dolist (feature '(;; TODO:
                       ;; evil-search
                       evil-maps
                       evil-keybindings))
      (add-to-list 'features feature))))
;;
(require 'evil-vars)
;;
(defgroup devil
  nil
  "Dandy extensible vi layer (Devil)."
  :group 'convenience
  :group 'emulations
  :group 'evil
  :prefix 'devil-)
;;
(when (bound-and-true-p devil-toggle-key)
  (set 'evil-toggle-key devil-toggle-key))
(defvaralias 'devil-toggle-key 'evil-toggle-key)
;;
(defcustom devil-escape-char
  ?\e
  "Character used to escape the current state."
  :type 'character)
;;
(defcustom devil-escape-key
  (kbd "<escape>")
  "Key used to escape the current state."
  :type 'key-sequence)
;;
(defcustom devil-quit-char
  ?\3
  "Character used to signal the `quit' condition."
  :type 'character)
;;
(defcustom devil-quit-key
  (kbd (char-to-string devil-quit-char))
  "Key used to signal the `quit' condition."
  :type 'key-sequence)
;;
(defcustom devil-backtrace-to-string-function
  nil
  "Function to return a trace of function calls currently active as string."
  :type 'function)
;;
(defcustom devil-scroll-right-default-count-function
  #'devil-scroll-horizontally-default-count
  "Function to return the default amount to scroll right."
  :type 'function)
;;
(defcustom devil-scroll-left-default-count-function
  #'devil-scroll-horizontally-default-count
  "Function to return the default amount to scroll left."
  :type 'function)
;;
(defcustom devil-scroll-down-default-count-function
  #'devil-scroll-vertically-default-count
  "Function to return the default amount to scroll down."
  :type 'function)
;;
(defcustom devil-scroll-up-default-count-function
  #'devil-scroll-vertically-default-count
  "Function to return the default amount to scroll up."
  :type 'function)
;;
(defcustom devil-selected-frame-p
  nil
  "Whether to consider only selected frame."
  :type 'boolean)
;;
(with-eval-after-load 'simple
  (defcustom devil-async-shell-command-buffer
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; Prefer to keep custom variables `devil-async-shell-command-buffer' and
    ;; `async-shell-command-buffer' separate, i.e. without alias binding via
    ;; `defvaralias':
    ;;
    async-shell-command-buffer
    ;; -----------------------------------------------------------------------
    "See `async-shell-command-buffer'."
    :type (get 'async-shell-command-buffer 'custom-type)))
;;
(defvar-local devil-format-region-function
  #'devil-indent
  "Function to format region.")
;;
(provide 'devil-vars)
