;; damned
;; dandy
;; deprecated
;; deviant
;; doomed
;; ---------------------------------------------------------------------------
;;
(require 'devil-vars)
(require 'devil-common)
(require 'devil-core)
(require 'devil-states)
(require 'devil-repeat-motion)
(require 'devil-macros)
(require 'devil-search)
(require 'devil-ex)
(require 'devil-types)
(require 'devil-commands)
;;
(devil-negate (featurep 'devil-easy-motion))
;;
(require 'devil-maps)
(require 'devil-integration)
(require 'devil-keybindings)
;;
(require 'evil)
;;
(provide 'devil)
