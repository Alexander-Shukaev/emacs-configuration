(require 'devil-core)
(require 'devil-macros)
(require 'devil-types)
(require 'devil-maps)
;;
(require 'evil-integration)
;;
(defsubst devil--delete-process-keys ()
  (devil-command-keys #'devil-delete-process))
;;
(defsubst devil--force-normal-state-keys ()
  (and (evil-normal-state-p)
       (or (and (get-buffer-process (current-buffer))
                (devil--delete-process-keys))
           (devil-command-keys #'evil-force-normal-state
                               (list evil-normal-state-map)))))
;;
(defsubst devil--normal-state-keys ()
  (or (and (evil-insert-state-p)
           (devil-command-keys #'evil-normal-state
                               (list evil-insert-state-map)))
      (and (evil-replace-state-p)
           (devil-command-keys #'evil-normal-state
                               (list evil-replace-state-map)))))
;;
(defsubst devil--exit-visual-state-keys ()
  (and (evil-visual-state-p)
       (devil-command-keys #'evil-exit-visual-state
                           (list evil-visual-state-map))))
;;
(defsubst devil--company-search-abort-keys ()
  (when (fboundp 'company-search-abort)
    (let ((command #'company-search-abort))
      (or (and (bound-and-true-p company-search-map)
               (or (and (bound-and-true-p company-my-keymap)
                        (eq company-my-keymap company-search-map))
                   (eq overriding-local-map          company-search-map)
                   (eq overriding-terminal-local-map company-search-map))
               (devil-command-keys command
                                   (list company-search-map)))
          (and (bound-and-true-p company-mode-map)
               (or (and (bound-and-true-p company-my-keymap)
                        (eq company-my-keymap company-mode-map))
                   (eq overriding-local-map          company-mode-map)
                   (eq overriding-terminal-local-map company-mode-map))
               (devil-command-keys command
                                   (list company-mode-map)))))))
;;
(defsubst devil--company-abort-keys ()
  (when (fboundp 'company-abort)
    (let ((command #'company-abort))
      (or (and (bound-and-true-p company-active-map)
               (or (and (bound-and-true-p company-my-keymap)
                        (eq company-my-keymap company-active-map))
                   (eq overriding-local-map          company-active-map)
                   (eq overriding-terminal-local-map company-active-map))
               (devil-command-keys command
                                   (list company-active-map)))
          (and (bound-and-true-p company-mode-map)
               (or (and (bound-and-true-p company-my-keymap)
                        (eq company-my-keymap company-mode-map))
                   (eq overriding-local-map          company-mode-map)
                   (eq overriding-terminal-local-map company-mode-map))
               (devil-command-keys command
                                   (list company-mode-map)))))))
;;
(defsubst devil--isearch-abort-keys ()
  (when (fboundp 'isearch-abort)
    (let ((command #'isearch-abort))
      (and (bound-and-true-p isearch-mode-map)
           (or (eq overriding-local-map          isearch-mode-map)
               (eq overriding-terminal-local-map isearch-mode-map))
           (devil-command-keys command
                               (list isearch-mode-map))))))
;;
(defsubst devil--isearch-cancel-keys ()
  (when (fboundp 'isearch-cancel)
    (let ((command #'isearch-cancel))
      (and (bound-and-true-p isearch-mode-map)
           (or (eq overriding-local-map          isearch-mode-map)
               (eq overriding-terminal-local-map isearch-mode-map))
           (devil-command-keys command
                               (list isearch-mode-map))))))
;;
(defsubst devil--transient-quit-seq-keys ()
  (when (fboundp 'transient-quit-seq)
    (let ((command #'transient-quit-seq))
      (or (and (bound-and-true-p transient--redisplay-map)
               (or (eq overriding-local-map          transient--redisplay-map)
                   (eq overriding-terminal-local-map transient--redisplay-map)
                   (bound-and-true-p transient--showp)
                   (bound-and-true-p transient--helpp)
                   (bound-and-true-p transient--editp))
               (devil-command-keys command
                                   (list transient--redisplay-map)))
          (and (bound-and-true-p transient--transient-map)
               (or (eq overriding-local-map          transient--transient-map)
                   (eq overriding-terminal-local-map transient--transient-map)
                   (bound-and-true-p transient--showp)
                   (bound-and-true-p transient--helpp)
                   (bound-and-true-p transient--editp))
               (devil-command-keys command
                                   (list transient--transient-map)))))))
;;
(defsubst devil--transient-quit-one-keys ()
  (when (fboundp 'transient-quit-one)
    (let ((command #'transient-quit-one))
      (or (and (bound-and-true-p transient--redisplay-map)
               (or (eq overriding-local-map          transient--redisplay-map)
                   (eq overriding-terminal-local-map transient--redisplay-map)
                   (bound-and-true-p transient--showp)
                   (bound-and-true-p transient--helpp)
                   (bound-and-true-p transient--editp))
               (devil-command-keys command
                                   (list transient--redisplay-map)))
          (and (bound-and-true-p transient--transient-map)
               (or (eq overriding-local-map          transient--transient-map)
                   (eq overriding-terminal-local-map transient--transient-map)
                   (bound-and-true-p transient--showp)
                   (bound-and-true-p transient--helpp)
                   (bound-and-true-p transient--editp))
               (devil-command-keys command
                                   (list transient--transient-map)))))))
;;
(defsubst devil--transient-quit-all-keys ()
  (when (fboundp 'transient-quit-all)
    (let ((command #'transient-quit-all))
      (or (and (bound-and-true-p transient--redisplay-map)
               (or (eq overriding-local-map          transient--redisplay-map)
                   (eq overriding-terminal-local-map transient--redisplay-map)
                   (bound-and-true-p transient--showp)
                   (bound-and-true-p transient--helpp)
                   (bound-and-true-p transient--editp))
               (devil-command-keys command
                                   (list transient--redisplay-map)))
          (and (bound-and-true-p transient--transient-map)
               (or (eq overriding-local-map          transient--transient-map)
                   (eq overriding-terminal-local-map transient--transient-map)
                   (bound-and-true-p transient--showp)
                   (bound-and-true-p transient--helpp)
                   (bound-and-true-p transient--editp))
               (devil-command-keys command
                                   (list transient--transient-map)))))))
;;
(defsubst devil--minibuffer-keyboard-quit-keys ()
  (and (window-minibuffer-p)
       (devil-command-keys #'minibuffer-keyboard-quit)))
;;
(defsubst devil--abort-recursive-edit-keys ()
  (and (window-minibuffer-p)
       (devil-command-keys #'abort-recursive-edit)))
;;
(defsubst devil--keyboard-quit-keys ()
  (devil-command-keys #'keyboard-quit))
;;
(defun devil-keyboard-quit
    (prompt)
  ;; TODO:
  (message "%s" (this-command-keys))
  (unless (or (eq this-command #'quoted-insert)
              (devil-key-command (this-command-keys)))
    (or (and (or (eq overriding-local-map          evil-read-key-map)
                 (eq overriding-terminal-local-map evil-read-key-map))
             (when (fboundp 'keyboard-quit)
               (keyboard-quit))
             nil)
        (catch 'break
          ;; Order of values for `command' matters significantly:
          (dolist (command '(delete-process
                             company-search-abort
                             company-abort
                             exit-visual-state
                             normal-state
                             isearch-abort
                             isearch-cancel
                             transient-quit-seq
                             transient-quit-one
                             transient-quit-all
                             minibuffer-keyboard-quit
                             abort-recursive-edit
                             force-normal-state
                             keyboard-quit))
            ;; TODO: Implement as custom hook instead:
            (let ((keys (funcall (intern (format "devil--%s-keys"
                                                 (symbol-name command))))))
              (when keys
                (throw 'break (car keys)))))))))
;;
(devil-define-keys-kbd key-translation-map
  (devil-current-quit-key) #'devil-keyboard-quit
  devil-quit-key           #'devil-keyboard-quit)
(devil-define-keys-kbd evil-operator-state-map
  devil-quit-key #'keyboard-quit)
(set-quit-char devil-quit-char)
;;
(eval-when-compile
  (require 'evil-common))
(with-eval-after-load 'evil-common
  (evil-declare-motion #'beginning-of-line)
  (evil-declare-motion #'end-of-line)
  ;;
  (evil-declare-motion #'beginning-of-visual-line)
  (evil-declare-motion #'end-of-visual-line)
  ;;
  (evil-declare-motion #'beginning-of-buffer)
  (evil-declare-motion #'end-of-buffer))
;;
(with-eval-after-load 'evil-states
  ;; Imagine the following scenario.  One wants to paste some previously
  ;; copied (from application other than Emacs) text to the system's clipboard
  ;; in place of some contiguous block of text in a buffer.  Hence, one
  ;; switches to `evil-visual-state' and selects the corresponding block of
  ;; text to be replaced.  However, one either pastes some (previously killed)
  ;; text from `kill-ring' or (if `kill-ring' is empty) receives the error:
  ;; "Kill ring is empty"; see `evil-visual-paste' and `current-kill'
  ;; respectively.  The reason why `current-kill' does not return the desired
  ;; text from the system's clipboard is because
  ;; `evil-visual-update-x-selection' is being run by
  ;; `evil-visual-pre-command' before `evil-visual-paste'.  That is
  ;; `x-select-text' is being run (by `evil-visual-update-x-selection') before
  ;; `evil-visual-paste'.  As a result, `x-select-text' copies the selected
  ;; block of text to the system's clipboard as long as
  ;; `x-select-enable-clipboard' is non-nil (and in this scenario we assume
  ;; that it is).  According to the documentation of
  ;; `interprogram-paste-function', it should not return the text from the
  ;; system's clipboard if it was last provided by Emacs (e.g. with
  ;; `x-select-text').  Thus, one ends up with the problem described above.
  ;; To solve it, simply make `evil-visual-update-x-selection' do nothing:
  (fset 'evil-visual-update-x-selection #'ignore))
;;
(with-eval-after-load 'wgrep
  (declare-function devil-delete@wgrep-toggle-readonly-area
                    #$
                    (function &rest ...))
  (define-advice devil-delete
      (:around (function &rest ...) wgrep-toggle-readonly-area)
    (when (and (bound-and-true-p wgrep-prepared)
               (fboundp 'wgrep-toggle-readonly-area))
      (wgrep-toggle-readonly-area))
    (unwind-protect
        (apply function ...)
      (when (and (bound-and-true-p wgrep-prepared)
                 (fboundp 'wgrep-toggle-readonly-area))
        (wgrep-toggle-readonly-area)))))
;;
(provide 'devil-integration)
