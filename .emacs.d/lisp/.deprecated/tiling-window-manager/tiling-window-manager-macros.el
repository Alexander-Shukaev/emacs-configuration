(defmacro twm-without-split-window
    (&rest body)
  "Execute BODY with `twm-split-window' being temporarily removed."
  (declare (indent defun)
           (debug  t))
  (advice-remove 'split-window #'twm-split-window)
  `(unwind-protect
       (progn ,@body)
     (advice-add #'split-window :around #'twm-split-window)))

(defmacro twm-keep-balanced-windows
    (&rest body)
  "Execute BODY, then call `balance-windows'."
  (declare (indent defun)
           (debug  t))
  `(unwind-protect
       (progn ,@body)
     (balance-windows)))

(provide 'tiling-window-manager-macros)
