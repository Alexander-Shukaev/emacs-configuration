(defgroup tiling-window-manager
  nil
  "Customization group for TWM."
  :tag "Tiling Window Manager (TWM)"
  :group 'convenience
  :prefix "twm-")

(require 'tiling-window-manager-variables)
(require 'tiling-window-manager-functions)

(advice-add #'split-window           :around #'twm-split-window)
(advice-add #'delete-window          :around #'twm-delete-window)
(advice-add #'unrecord-window-buffer :around #'twm-unrecord-window-buffer)

(provide 'tiling-window-manager)
