(require 'tiling-window-manager-functions)

(defcustom twm-master-window-side
  'left
  "Specifies on what side master window should be located."
  :tag "TWM Master Window Side"
  :group 'tiling-window-manager
  :type '(choice (const :tag "Left"  left)
                 (const :tag "Right" right))
  :set #'(lambda
             (symbol value)
           (let* ((twm-master-window-side (if (boundp symbol)
                                              (symbol-value symbol)
                                            'left))
                  (master-window (twm-master-window)))
             (set-default symbol value)
             ;; --------------------------------------------------------------
             (twm-set-window-master master-window)
             ;;
             ;; NOTE:
             ;;
             ;; Ideally, one would prefer to restore the selected window after
             ;; mirroring.  However, since `twm-set-window-master' wipes out
             ;; all windows and recreates them, we don't know which window to
             ;; select.  One possible workaround would be to count how many
             ;; windows are above (below) the selected window (before
             ;; mirroring) in stack with `twm-select-window-above'
             ;; (`twm-select-window-below') and then (after mirroring) to move
             ;; selection through exactly this number of windows above (below)
             ;; in stack.
             ;; --------------------------------------------------------------
             )))

(provide 'tiling-window-manager-variables)
