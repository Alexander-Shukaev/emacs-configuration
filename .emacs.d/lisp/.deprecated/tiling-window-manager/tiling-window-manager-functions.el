(require 'tiling-window-manager-macros)

(eval-when-compile
  (defvar twm-master-window-side))

(defsubst twm-toggle-master-window-side ()
  "Toggle master window side."
  (interactive)
  (twm-check-customs)
  (cond ((eq twm-master-window-side 'left)
         (customize-set-variable 'twm-master-window-side 'right))
        ((eq twm-master-window-side 'right)
         (customize-set-variable 'twm-master-window-side 'left)))
  twm-master-window-side)

(defsubst twm-select-window-next
    (&optional window)
  "Select window after WINDOW in the cyclic ordering of windows."
  (interactive)
  (select-window (next-window window)))

(defsubst twm-select-window-previous
    (&optional window)
  "Select window before WINDOW in the cyclic ordering of windows."
  (interactive)
  (select-window (previous-window window)))

(defsubst twm-select-window-uppermost-leftmost ()
  "Select uppermost-leftmost window."
  (interactive)
  (while (or (ignore-errors (windmove-up))
             (ignore-errors (windmove-left))))
  (selected-window))

(defsubst twm-select-window-uppermost-rightmost ()
  "Select uppermost-rightmost window."
  (interactive)
  (while (or (ignore-errors (windmove-up))
             (ignore-errors (windmove-right))))
  (selected-window))

(defsubst twm-select-window-lowermost-leftmost ()
  "Select lowermost-leftmost window."
  (interactive)
  (while (or (ignore-errors (windmove-down))
             (ignore-errors (windmove-left))))
  (selected-window))

(defsubst twm-select-window-lowermost-rightmost ()
  "Select lowermost-rightmost window."
  (interactive)
  (while (or (ignore-errors (windmove-down))
             (ignore-errors (windmove-right))))
  (selected-window))

(defsubst twm-select-window-upper ()
  "Select window above current window."
  (interactive)
  (ignore-errors
    (windmove-up)
    (selected-window)))

(defalias 'twm-select-window-above 'twm-select-window-upper)

(defsubst twm-select-window-lower ()
  "Select window below current window."
  (interactive)
  (ignore-errors
    (windmove-down)
    (selected-window)))

(defalias 'twm-select-window-below 'twm-select-window-lower)

(defsubst twm-select-window-left ()
  "Select window left of current window."
  (interactive)
  (ignore-errors
    (windmove-left)
    (selected-window)))

(defsubst twm-select-window-right ()
  "Select window right of current window."
  (interactive)
  (ignore-errors
    (windmove-right)
    (selected-window)))

(defsubst twm-select-window-master ()
  "Select master window."
  (interactive)
  (twm-check-customs)
  (cond ((eq twm-master-window-side 'left)
         (twm-select-window-uppermost-leftmost))
        ((eq twm-master-window-side 'right)
         (twm-select-window-uppermost-rightmost))))

(defsubst twm-select-window-stack-top ()
  "Select top stack window."
  (interactive)
  (twm-check-customs)
  (cond ((eq twm-master-window-side 'left)
         (twm-select-window-uppermost-rightmost))
        ((eq twm-master-window-side 'right)
         (twm-select-window-uppermost-leftmost))))

(defsubst twm-select-window-stack-bottom ()
  "Select bottom stack window."
  (interactive)
  (twm-check-customs)
  (cond ((eq twm-master-window-side 'left)
         (twm-select-window-lowermost-rightmost))
        ((eq twm-master-window-side 'right)
         (twm-select-window-lowermost-leftmost))))

(defun twm-set-window-master
    (&optional window)
  "Set WINDOW as master window."
  (interactive)
  (twm-check-customs)
  (unless (one-window-p 'nomini)
    (let* ((window-list     (cdr (window-list nil nil window)))
           (buffer-list     (twm-buffer-list     window-list))
           (dedicated?-list (twm-dedicated?-list window-list)))
      ;; Use `delete-other-windows-internal' instead of `delete-other-windows'
      ;; in order to delete dedicated windows too:
      (delete-other-windows-internal window)
      (twm-without-split-window
        (twm-keep-balanced-windows
          (save-selected-window
            (cond ((eq twm-master-window-side 'left)
                   (select-window (split-window window nil 'right) 'norecord))
                  ((eq twm-master-window-side 'right)
                   (select-window (split-window window nil 'left)  'norecord)))
            (set-window-buffer      nil (pop buffer-list))
            (set-window-dedicated-p nil (pop dedicated?-list))
            (while buffer-list
              ;; Maximize to increase probability of successful split.
              (ignore-errors
                (maximize-window))
              (condition-case nil
                  (select-window (split-window nil nil 'below) 'norecord)
                (error
                 (user-error "twm: error: %s"
                             "Stack of windows has reached maximum size")))
              (set-window-buffer      nil (pop buffer-list))
              (set-window-dedicated-p nil (pop dedicated?-list))))))))
  (or window (selected-window)))

(defsubst twm-master-window ()
  "Return master window."
  (save-selected-window
    (twm-select-window-master)))

(defsubst twm-stack-window-top ()
  "Return top stack window."
  (save-selected-window
    (twm-select-window-stack-top)))

(defsubst twm-stack-window-bottom ()
  "Return bottom stack window."
  (save-selected-window
    (twm-select-window-stack-bottom)))

(defsubst twm-window-upper ()
  "Return window above current window."
  (save-selected-window
    (twm-select-window-above)))

(defalias 'twm-window-above 'twm-window-upper)

(defsubst twm-window-lower ()
  "Return window below current window."
  (save-selected-window
    (twm-select-window-below)))

(defalias 'twm-window-below 'twm-window-lower)

(defsubst twm-window-left ()
  "Return window left of current window."
  (save-selected-window
    (twm-select-window-left)))

(defsubst twm-window-right ()
  "Return window right of current window."
  (save-selected-window
    (twm-select-window-right)))

(defsubst twm-selected-window?
    (&optional window)
  "Check whether WINDOW is selected window."
  (eq (or window (selected-window)) (selected-window)))

(defsubst twm-master-window?
    (&optional window)
  "Check whether WINDOW is master window."
  (eq (or window (selected-window)) (twm-master-window)))

(defsubst twm-stack-window?
    (&optional window)
  "Check whether WINDOW is stack window."
  (not (twm-master-window? window)))

(defsubst twm-stack-window-top?
    (&optional window)
  "Check whether WINDOW is top stack window."
  (eq (or window (selected-window)) (twm-stack-window-top)))

(defsubst twm-stack-window-bottom?
    (&optional window)
  "Check whether WINDOW is bottom stack window."
  (eq (or window (selected-window)) (twm-stack-window-bottom)))

(defun twm-buffer-list
    (window-list)
  "Return list of `window-buffer' values for each window in WINDOW-LIST."
  (let (buffer-list)
    (while window-list
      (push (window-buffer (pop window-list)) buffer-list))
    (nreverse buffer-list)))

(defun twm-dedicated?-list
    (window-list)
  "Return list of `window-dedicated-p' values for each window in WINDOW-LIST."
  (let (dedicated?-list)
    (while window-list
      (push (window-dedicated-p (pop window-list)) dedicated?-list))
    (nreverse dedicated?-list)))

(defun twm-split-window
    (split-window &rest arguments)
  "Advice for `split-window'."
  (twm-check-customs)
  (let* ((window (nth 0 arguments))
         (side   (nth 2 arguments))
         (buffer (window-buffer window)))
    ;; ─────────────────────────────────────────────────────────────────────────
    ;; From `split-window':
    (setq side (cond ((not side) 'below)
                     ((memq side '(below above right left)) side)
                     (t          'right)))
    ;; ─────────────────────────────────────────────────────────────────────────
    (if (one-window-p 'nomini)
        (cond ((eq twm-master-window-side 'left)
               (setq side 'right))
              ((eq twm-master-window-side 'right)
               (setq side 'left)))
      (if (twm-stack-window? window)
          (when (memq side '(left right))
            (setq side (if (eq side twm-master-window-side)
                           'above
                         'below)))
        (when (memq side '(left right))
          (setq side (if (eq side twm-master-window-side)
                         'below
                       'above)))
        (setq window (if (eq side 'below)
                         (twm-stack-window-bottom)
                       (twm-stack-window-top)))))
    (twm-without-split-window
      (twm-keep-balanced-windows
        ;; Maximize to increase probability of successful split.
        (ignore-errors
          (maximize-window window))
        (condition-case nil
            ;; TODO:
            ;; How about `split-window-internal' instead?
            (setq window (funcall split-window window nil side))
          (error
           (user-error "twm: error: %s"
                       "Stack of windows has reached maximum size")))))
    (set-window-buffer window buffer)
    window))

(defun twm-delete-window
    (delete-window &rest arguments)
  "Advice for `delete-window'."
  (if (one-window-p 'nomini)
      (apply delete-window arguments)
    (let ((window (car arguments)))
      (if (twm-master-window? window)
          (let* ((stack-window (twm-stack-window-top))
                 ;; TODO:
                 ;; How about `twm-stack-buffer-top' helper (pure cosmetics)?
                 (stack-buffer (window-buffer stack-window)))
            (set-window-dedicated-p window nil)
            (set-window-buffer      window stack-buffer)
            (set-window-dedicated-p window (window-dedicated-p stack-window))
            (setq window stack-window))
        (when (twm-selected-window? window)
          ;; Remember that `window' could still be nil here, so assign it to
          ;; `selected-window' for sure.
          (setq window (selected-window))
          (let ((window (or (twm-window-below)
                            (twm-window-above))))
            (when window
              (select-window window)))))
      (twm-keep-balanced-windows
        (funcall delete-window window)))))

(defun twm-unrecord-window-buffer
    (unrecord-window-buffer &rest arguments)
  "Advice for `unrecord-window-buffer'."
  (if (window-live-p (car arguments))
      (apply unrecord-window-buffer arguments)))

(defsubst twm-check-customs ()
  (unless (memq twm-master-window-side '(left right))
    (user-error "twm: error: %s: `%s'"
                "Unrecognized value of `twm-master-window-side'"
                twm-master-window-side)))

(provide 'tiling-window-manager-functions)
