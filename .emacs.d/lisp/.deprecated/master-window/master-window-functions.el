(require 'master-window-variables)
(eval-when-compile
  (require 'master-window-macros))
;;
(defsubst master-window-select-window-uppermost-leftmost ()
  "Select uppermost-leftmost window."
  (interactive)
  (master-window-ignore-messages
    (while (or (ignore-errors (windmove-up))
               (ignore-errors (windmove-left)))))
  (selected-window))
;;
(defsubst master-window-select-window-uppermost-rightmost ()
  "Select uppermost-rightmost window."
  (interactive)
  (master-window-ignore-messages
    (while (or (ignore-errors (windmove-up))
               (ignore-errors (windmove-right)))))
  (selected-window))
;;
(defsubst master-window-select-window-lowermost-leftmost ()
  "Select lowermost-leftmost window."
  (interactive)
  (master-window-ignore-messages
    (while (or (ignore-errors (windmove-down))
               (ignore-errors (windmove-left)))))
  (selected-window))
;;
(defsubst master-window-select-window-lowermost-rightmost ()
  "Select lowermost-rightmost window."
  (interactive)
  (master-window-ignore-messages
    (while (or (ignore-errors (windmove-down))
               (ignore-errors (windmove-right)))))
  (selected-window))
;;
(defsubst master-window-select-window-upper ()
  "Select window above current window."
  (interactive)
  (ignore-errors
    (master-window-ignore-messages
      (windmove-up))
    (selected-window)))
(defalias
  'master-window-select-window-above
  'master-window-select-window-upper)
;;
(defsubst master-window-select-window-lower ()
  "Select window below current window."
  (interactive)
  (ignore-errors
    (master-window-ignore-messages
      (windmove-down))
    (selected-window)))
(defalias
  'master-window-select-window-below
  'master-window-select-window-lower)
;;
(defsubst master-window-select-window-left ()
  "Select window left of current window."
  (interactive)
  (ignore-errors
    (master-window-ignore-messages
      (windmove-left))
    (selected-window)))
;;
(defun master-window-select-window-right ()
  "Select window right of current window."
  (interactive)
  (ignore-errors
    (master-window-ignore-messages
      (windmove-right))
    (selected-window)))
;;
(defsubst master-window-select ()
  "Select master window."
  (interactive)
  (master-window--check-customs)
  (cond ((eq master-window-side 'left)
         (master-window-select-window-uppermost-leftmost))
        ((eq master-window-side 'right)
         (master-window-select-window-uppermost-rightmost))))
;;
(defsubst master-window ()
  "Return master window."
  (save-selected-window
    (master-window-select)))
;;
(defsubst master-window-p
    (&optional window)
  "Check whether WINDOW is master window."
  (eq (or window (selected-window)) (master-window)))
;;
(defsubst master-window-min-size
    (window-min-size-function &rest arguments)
  "Advice for `window-min-size'."
  (master-window--check-customs)
  (let ((window (car arguments)))
    (master-window--with-min-size-maybe window
      (apply window-min-size-function arguments))))
;;
(defsubst master-window-resize
    (window-resize-function &rest arguments)
  "Advice for `window-resize'."
  (master-window--check-customs)
  (let ((window (car arguments)))
    (master-window--with-min-size-maybe window
      (apply window-resize-function arguments))))
;;
(defsubst master-window-split
    (split-window-function &rest arguments)
  "Advice for `split-window'."
  (master-window--check-customs)
  (let ((window (car arguments)))
    (master-window--with-min-size-maybe window
      (apply split-window-function arguments))))
;;
(defsubst master-window-split-sensibly
    (split-window-sensibly-function &rest arguments)
  "Advice for `split-window-sensibly'."
  (master-window--check-customs)
  (let ((window (car arguments)))
    (master-window--with-split-size-threshold-maybe window
      (apply split-window-sensibly-function arguments))))
;;
(defsubst master-window-splittable-p
    (window-splittable-p-function &rest arguments)
  "Advice for `window-splittable-p'."
  (master-window--check-customs)
  (let ((window (car arguments)))
    (master-window--with-min-size-maybe window
      (master-window--with-split-size-threshold-maybe window
        (apply window-splittable-p-function arguments)))))
;;
(defsubst master-window--check-customs ()
  (unless (memq master-window-side '(left right))
    (user-error "master-window: error: %s: `%s'"
                "Unrecognized value of `master-window-side'"
                master-window-side)))
;;
(defsubst master-window-min-height ()
  (or master-window-min-height window-min-height))
;;
(defsubst master-window-min-width ()
  (or master-window-min-width window-min-width))
;;
(defsubst master-window-split-height-threshold ()
  (or master-window-split-height-threshold split-height-threshold))
;;
(defsubst master-window-split-width-threshold ()
  (or master-window-split-width-threshold split-width-threshold))
;;
(provide 'master-window-functions)
