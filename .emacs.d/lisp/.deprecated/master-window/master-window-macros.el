(defmacro master-window-without-messages
    (&rest body)
  "\
Execute BODY without displaying and logging of messages."
  (declare (debug  t)
           (indent defun))
  `(let ((message-function (symbol-function 'message))
         (message-log-max  nil))
     (fset 'message #'ignore)
     (unwind-protect
         (progn ,@body)
       (fset 'message message-function))))
(defalias 'master-window-ignore-messages   #'master-window-without-messages)
(defalias 'master-window-suppress-messages #'master-window-without-messages)
;;
(defmacro master-window--with-min-size-maybe
    (window &rest body)
  "\
Execute BODY with WINDOW."
  (declare (debug  t)
           (indent defun))
  `(if (master-window-p window)
       (let ((window-min-height (master-window-min-height))
             (window-min-width  (master-window-min-width)))
         ,@body)
     ,@body))
;;
(defmacro master-window--with-split-size-threshold-maybe
    (window &rest body)
  "\
Execute BODY with WINDOW."
  (declare (debug  t)
           (indent defun))
  `(if (master-window-p window)
       (let ((split-height-threshold (master-window-split-height-threshold))
             (split-width-threshold  (master-window-split-width-threshold)))
         ,@body)
     ,@body))
;;
(provide 'master-window-macros)
