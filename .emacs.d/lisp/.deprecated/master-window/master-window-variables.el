(defcustom master-window-side
  'left
  "\
Specifies on what side master window should be located."
  :tag "Master-Window Side"
  :group 'master-window
  :type '(choice (const :tag "Left"  left)
                 (const :tag "Right" right)))
;;
(defcustom master-window-min-height
  nil
  "\
The minimum total height, in lines, of master window.

The value has to accommodate one text line, a mode and header
line, a horizontal scroll bar and a bottom divider, if present.
A value less than `window-safe-min-height' is ignored.  The value
of this variable is honored when master window is resized or split."
  :group 'master-window
  :type 'integer)
;;
(defcustom master-window-min-width
  nil
  "\
The minimum total width, in columns, of master window.

The value has to accommodate two text columns as well as margins,
fringes, a vertical scroll bar and a right divider, if present.
A value less than `window-safe-min-width' is ignored.  The value
of this variable is honored when master window is resized or split."
  :group 'master-window
  :type 'integer)
;;
(defcustom master-window-split-height-threshold
  nil
  "\
Minimum height for splitting master window sensibly.

See `split-height-threshold'."
  :group 'master-window
  :type '(choice (const nil) (integer :tag "lines")))
;;
(defcustom master-window-split-width-threshold
  nil
  "\
Minimum width for splitting master window sensibly.

See `split-width-threshold'."
  :group 'master-window
  :type '(choice (const nil) (integer :tag "columns")))
;;
(provide 'master-window-variables)
