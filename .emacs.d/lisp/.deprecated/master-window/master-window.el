;; TODO: Should be minor mode (global)!
;;
(defgroup master-window
  nil
  "Customization group for Master-Window."
  :tag "Master-Window (MW)"
  :group 'convenience
  :prefix "master-window-")
;;
(require 'master-window-variables)
(require 'master-window-functions)
;;
;; TODO: Slow!
;; ;;;###autoload
;; (advice-add #'window-min-size       :around #'master-window-min-size)
;;
;; ;;;###autoload
;; (advice-add #'window-resize         :around #'master-window-resize)
;;
;; TODO: Global minor mode?
;;;###autoload
(advice-add #'split-window          :around #'master-window-split)
;;
;;;###autoload
(advice-add #'split-window-sensibly :around #'master-window-split-sensibly)
;;
;;;###autoload
(advice-add #'window-splittable-p   :around #'master-window-splittable-p)
;;
(provide 'master-window)
