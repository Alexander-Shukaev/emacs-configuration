(defgroup uncrustify
  nil
  "Customization group for Uncrustify."
  :tag "Uncrustify"
  :group 'external
  :prefix "uncrustify-")

(defcustom uncrustify-configuration-path
  "~/.uncrustify/default.cfg"
  "Path to Uncrustify configuration file."
  :tag "Uncrustify Configuration File Path"
  :group 'uncrustify
  :type  'file)

(defcustom uncrustify-executable-path
  (or (executable-find "uncrustify")
      (executable-find "uncrustify.exe"))
  "Path to Uncrustify executable file."
  :tag "Uncrustify Executable File Path"
  :group 'uncrustify
  :type  'file)

(defcustom uncrustify-verbose?
  t
  "Whether to provide additional warnings."
  :tag "Uncrustify Verbose"
  :group 'uncrustify
  :type  'boolean)

(defun uncrustify-executable-exists? ()
  "Check whether Uncrustify executable file exists."
  (interactive)
  (file-executable-p uncrustify-executable-path))

(defcustom uncrustify-args
  '("-q")
  "List of arguments to Uncrustify."
  :group 'uncrustify
  :type  'list)

(defcustom uncrustify-before-format-hook
  nil
  "Hook called before running Uncrustify."
  :group 'uncrustify
  :type  'hook)

(defcustom uncrustify-after-format-hook
  nil
  "Hook called after running Uncrustify."
  :group 'uncrustify
  :type  'hook)

(defmacro uncrustify-save-excursion (&rest body)
  "Execute BODY, then restore previous point."
  `(let ((top-visible-line-number (line-number-at-pos (window-start)))
         (line-number   (line-number-at-pos))
         (column-number (current-column)))
     (unwind-protect
         (progn ,@body)
       (scroll-down (- (line-number-at-pos (window-start))
                       top-visible-line-number))
       (goto-line      line-number)
       (move-to-column column-number))))

(defun uncrustify-infer-language ()
  "Infer language to be used by Uncrustify based on the current Major Mode."
  (case major-mode
    ('c-mode      "C")
    ('c++-mode    "CPP")
    ('csharp-mode "CS")
    ('d-mode      "D")
    ('java-mode   "JAVA")
    (t            nil)))

(defun uncrustify-format-region (beg end)
  "Call Uncrustify to format region."
  (run-hooks 'uncrustify-before-format-hook)
  (unless uncrustify-executable
    (signal
     'error
     '("uncrustify: error: Executable not found.")))
  (unless (uncrustify-infer-language)
    (signal
     'error
     '("uncrustify: error: Language not supported.")))
  (let ((command (format "%s -c %s -l %s %s"
                         uncrustify-executable
                         (expand-file-name uncrustify-configuration-file)
                         (uncrustify-infer-language)
                         (mapconcat 'identity uncrustify-args " "))))
    (uncrustify-save-excursion
     (shell-command-on-region beg
                              end
                              command
                              nil
                              t
                              null-device
                              nil)))
  (run-hooks 'uncrustify-after-format-hook))

(defun uncrustify-format-selection ()
  "Call Uncrustify to format selection."
  (interactive)
  (unwind-protect
      (condition-case e
          (progn
            (uncrustify-format-region (region-beginning) (region-end))
            nil)
        (error
         (ding)
         (message "%s" (error-message-string e))
         t))))

(defun uncrustify-format-buffer ()
  "Call Uncrustify to format buffer."
  (interactive)
  (unwind-protect
      (condition-case e
          (progn
            (uncrustify-format-region (point-min) (point-max))
            nil)
        (error
         (ding)
         (message "%s" (error-message-string e))
         t))))

(provide 'uncrustify)
