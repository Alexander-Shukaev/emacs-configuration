(require 'longlines)
;;
(defcustom global-longlines-functions
  nil
  "\
List of functions to be called to try to apply `longlines-mode'.

Only used by `global-longlines-p'.  If none of them returns
nil, try to apply `longlines-mode'."
  :group 'longlines
  :type 'hook)
;;
(defcustom global-longlines-threshold
  line-number-display-limit-width
  "\
Maximum line length permitted before applying `longlines-mode'.

See `global-longlines-p'."
  :group 'longlines
  :type 'integer)
;;
(defun global-longlines-p
    (&optional buffer)
  "\
Test all lines of buffer BUFFER for `global-longlines-threshold'.

Return t if `global-longlines-threshold' is exceeded."
  (with-current-buffer (or buffer (current-buffer))
    (when (and (run-hook-with-args-until-failure 'global-longlines-functions)
               (integerp global-longlines-threshold))
      (save-restriction
        (widen)
        (save-excursion
          (goto-char (point-min))
          (let ((inhibit-field-text-motion t))
            (catch 'break
              (while (unless (eobp)
                       (or (> (+ (point) global-longlines-threshold)
                              (progn (forward-line 1) (point)))
                           (throw 'break t)))))))))))
;;
;;;###autoload(autoload 'turn-on-longlines-mode-maybe "global-longlines" nil t)
(defun turn-on-longlines-mode-maybe ()
  (interactive)
  (when (global-longlines-p)
    (longlines-mode)))
;;
;;;###autoload(autoload 'global-longlines-mode "global-longlines" nil t)
(define-globalized-minor-mode global-longlines-mode longlines-mode
  turn-on-longlines-mode-maybe
  :group 'longlines)
;;
(provide 'global-longlines)
