;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Fulfill the documentation.
;; ---------------------------------------------------------------------------
(defvar ssh--auth-sock
  (getenv "SSH_AUTH_SOCK")
  "Value of environment variable `SSH_AUTH_SOCK'.")
;;
(defvar ssh--agent-pid
  (getenv "SSH_AGENT_PID")
  "Value of environment variable `SSH_AGENT_PID'.")
;;
(defmacro ssh--called-interactively-p ()
  "\
Compatibility wrapper for `called-interactively-p'.

In older versions of Emacs, `called-interactively-p' takes no arguments.
In Emacs 23.2 and newer, it takes one argument."
  (if (version< emacs-version "23.2")
      '(called-interactively-p)
    '(called-interactively-p 'any)))
;;
(defmacro ssh--with-askpass
    (&rest body)
  "\
Execute BODY with `SSH_ASKPASS' set."
  (declare (debug  t)
           (indent defun))
  `(if (eq system-type 'windows-nt)
       (let ((--askpass (getenv "SSH_ASKPASS"))
             (--cmd-script (make-temp-file "askpass" nil ".cmd"))
             (--vbs-script (make-temp-file "askpass" nil ".vbs")))
         (setenv "SSH_ASKPASS" --cmd-script)
         (unwind-protect
             (progn
               (with-temp-file --cmd-script
                 (insert (format "\
@echo off
::
setlocal
::
\"cscript.exe\" //nologo \"%s\" %s
"
                                 (convert-standard-filename --vbs-script)
                                 "%*")))
               (with-temp-file --vbs-script
                 (insert "\
WScript.StdOut.WriteLine(InputBox(WScript.Arguments.Item(0), \"SSH AskPass\"))
"))
               ,@body)
           (setenv "SSH_ASKPASS" --askpass)
           (delete-file --cmd-script)
           (delete-file --vbs-script)))
     ,@body))
;;
(defmacro ssh--with-display
    (value &rest body)
  "\
Execute BODY with DISPLAY set to VALUE."
  (declare (debug  t)
           (indent defun))
  `(let ((display (getenv "DISPLAY")))
     (setenv "DISPLAY" ,value)
     (unwind-protect
         (progn ,@body)
       (setenv "DISPLAY" display))))
;;
(defmacro ssh--without-agent
    (&rest body)
  "\
Execute BODY with `SSH_AUTH_SOCK' and `SSH_AGENT_PID' unset."
  (declare (debug  t)
           (indent defun))
  `(let ((auth-sock (getenv "SSH_AUTH_SOCK"))
         (agent-pid (getenv "SSH_AGENT_PID")))
     (setenv "SSH_AUTH_SOCK" nil)
     (setenv "SSH_AGENT_PID" nil)
     (unwind-protect
         (progn ,@body)
       (setenv "SSH_AUTH_SOCK" auth-sock)
       (setenv "SSH_AGENT_PID" agent-pid))))
;;
(defun ssh--call-process
    (program &rest args)
  (let* ((status)
         (stderr-file (make-temp-file program)))
    (unwind-protect
        (with-temp-buffer
          (setq status (apply #'call-process
                              program nil (list t stderr-file) nil args))
          (if (= status 0)
              (when (> (buffer-size) 0)
                (buffer-substring-no-properties (point-min) (point-max)))
            (erase-buffer)
            (insert-file-contents stderr-file)
            (user-error "%s"
                        (or (when (> (buffer-size) 0)
                              (goto-char (point-min))
                              (buffer-substring-no-properties
                               (line-beginning-position)
                               (line-end-position)))
                            (format (if (stringp status)
                                        "killed with signal %s"
                                      "failed with status %d")
                                    status)))))
      (delete-file stderr-file))))
;;
;;;###autoload
(defun ssh-add
    (passphrase &optional file)
  (interactive (list nil
                     (when current-prefix-arg
                       (let ((file (catch 'break
                                     (dolist (file '("~/.ssh/id_rsa"
                                                     "~/.ssh/id_dsa"
                                                     "~/.ssh/id_ecdsa"
                                                     "~/.ssh/id_ed25519"
                                                     "~/.ssh/identity"))
                                       (when (file-exists-p file)
                                         (throw 'break file))))))
                         (read-file-name (if file
                                             (format "File (%s): " file)
                                           "File: ")
                                         "~/.ssh/"
                                         file)))))
  (let ((called-interactively (ssh--called-interactively-p))
        (process)
        (process-connection-type t))
    (unwind-protect
        (ssh--with-display ":0"
          (ssh--with-askpass
            (setq process (apply #'start-process "ssh-add" nil "ssh-add"
                                 (delq nil (list (when (stringp file)
                                                   (expand-file-name file))))))
            (set-process-filter process #'ssh-add-process-filter)
            (while (accept-process-output process))))
      (when (eq (process-status process) 'run)
        (kill-process process)))))
;;
(defun ssh-add-process-filter
    (process string)
  (if (string-match-p (format "^\\(?:%s\\|%s\\) for .*: $"
                              "Enter passphrase"
                              "Bad passphrase, try again")
                      string)
      (process-send-string process
                           (concat (if (bound-and-true-p called-interactively)
                                       (read-passwd string)
                                     (bound-and-true-p passphrase))
                                   "\n"))
    (message "%s" (substring string 0 -1))))
;;
;;;###autoload
(defun ssh-agent ()
  (interactive)
  (ssh-kill-agent-safe)
  (save-match-data
    (let ((stdout (ssh--call-process "ssh-agent" "-s")))
      (add-hook 'kill-emacs-hook #'ssh-kill-agent-safe)
      (dolist (variable '("SSH_AUTH_SOCK" "SSH_AGENT_PID"))
        (when (string-match (concat variable "=\\([^;]+\\);") stdout)
          (setenv variable (substring-no-properties stdout
                                                    (match-beginning 1)
                                                    (match-end       1)))))))
  (message "Agent pid %s" (getenv "SSH_AGENT_PID")))
;;
;;;###autoload
(defun ssh-kill-agent ()
  (interactive)
  (if (and (equal (getenv "SSH_AUTH_SOCK") ssh--auth-sock)
           (equal (getenv "SSH_AGENT_PID") ssh--agent-pid))
      (ssh--without-agent
        (ssh--call-process "ssh-agent" "-k"))
    (let ((pid (getenv "SSH_AGENT_PID")))
      (prog1
          (unwind-protect
              (ssh--call-process "ssh-agent" "-k")
            (setenv "SSH_AUTH_SOCK" ssh--auth-sock)
            (setenv "SSH_AGENT_PID" ssh--agent-pid))
        (message "Agent pid %s killed" pid)))))
;;
(defun ssh-kill-agent-safe ()
  (ignore-errors (ssh-kill-agent)))
;;
(provide 'ssh)
