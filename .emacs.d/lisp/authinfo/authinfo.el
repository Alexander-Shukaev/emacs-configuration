(eval-when-compile
  (require 'font-lock))
;;
(defconst authinfo-keywords
  '("account"
    "default"
    "host"
    "login"
    "macdef"
    "machine"
    "password"
    "port"
    "user"))
;;
(defconst authinfo-font-lock-keywords
  `((,(mapconcat #'identity authinfo-keywords "\\|")
     . font-lock-keyword-face)
    ("^#.+"
     . font-lock-comment-face)))
;;
;;;###autoload
(define-derived-mode authinfo-mode fundamental-mode "authinfo"
  "Major mode to edit '.authinfo' and '.netrc' files."
  (setq-local font-lock-defaults    '(authinfo-font-lock-keywords))
  (setq-local font-lock-string-face nil)
  (setq-local mode-name             "authinfo")
  (setq-local comment-start         "#")
  (setq-local comment-end           ""))
;;
;;;###autoload
(add-to-list 'auto-mode-alist
             '("\\.\\(authinfo\\|netrc\\)\\(\\.gpg\\|\\.asc\\)?\\'"
               . authinfo-mode))
;;
(provide 'authinfo)
