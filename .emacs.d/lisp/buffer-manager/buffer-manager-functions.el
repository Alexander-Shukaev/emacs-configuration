(require 'buffer-manager-variables)
(require 'buffer-manager-macros)

(require 'tabulated-list)

(eval-when-compile
  (defvar bm--marked-or-kill)
  (defvar bm--save))

;; TODO:
;; (with-current-buffer (or (buffer-base-buffer (current-buffer))
;;                          (current-buffer))

(defun bm-check-customs ()
  (unless (memq bm-display-buffer-side '(left right))
    (user-error "bm: error: %s: `%s'"
                "Unrecognized value of `bm-display-buffer-side'"
                bm-display-buffer-side)))

(defun bm-toggle-display-buffer-side ()
  "Toggle master window side."
  (interactive)
  (bm-check-customs)
  (cond ((eq bm-display-buffer-side 'left)
         (customize-set-variable 'bm-display-buffer-side 'right))
        ((eq bm-display-buffer-side 'right)
         (customize-set-variable 'bm-display-buffer-side 'left)))
  bm-display-buffer-side)

(defun bm-switch-to-buffer-at
    (&optional pos bm-buffer)
  "Switch to BM buffer."
  (interactive)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (let* ((buffer (bm-invalid-entry-error pos bm-buffer
                   (bm-buffer-at pos bm-buffer)))
         (buffer (switch-to-buffer buffer)))
    (bm-update-entry-at pos bm-buffer)
    buffer))

(defun bm-switch-to-buffer-other-window-at
    (&optional pos bm-buffer)
  "Switch to BM buffer."
  (interactive)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (let* ((buffer (bm-invalid-entry-error pos bm-buffer
                   (bm-buffer-at pos bm-buffer)))
         (buffer (switch-to-buffer-other-window buffer)))
    (bm-update-entry-at pos bm-buffer)
    buffer))

(defun bm-switch-to-buffer-other-frame-at
    (&optional pos bm-buffer)
  "Switch to BM buffer."
  (interactive)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (let* ((buffer (bm-invalid-entry-error pos bm-buffer
                   (bm-buffer-at pos bm-buffer)))
         (buffer (switch-to-buffer-other-frame buffer)))
    (bm-update-entry-at pos bm-buffer)
    buffer))

(defun bm-display-buffer-at
    (&optional pos bm-buffer)
  "Switch to BM buffer."
  (interactive)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (let* ((buffer (bm-invalid-entry-error pos bm-buffer
                   (bm-buffer-at pos bm-buffer)))
         (window (display-buffer buffer t)))
    (bm-update-entry-at pos bm-buffer)
    window))

;;;###autoload(autoload 'bm-switch-to-bm-buffer "buffer-manager" nil t)
(defun bm-switch-to-bm-buffer
    (&optional files-only buffer-list)
  "Switch to BM buffer."
  (interactive "P")
  (setq files-only (when files-only
                     (> (prefix-numeric-value files-only) 0)))
  (let* ((bm-buffer (bm-get-bm-buffer-create files-only buffer-list))
         (bm-buffer (switch-to-buffer bm-buffer)))
    (bm-update bm-buffer)
    bm-buffer))

;;;###autoload(autoload 'list-buffers "buffer-manager" nil t)
(defalias 'list-buffers #'bm-switch-to-bm-buffer)

;;;###autoload(autoload 'bm-buffer "buffer-manager" nil t)
(defalias 'bm-buffer    #'bm-switch-to-bm-buffer)

;;;###autoload(autoload 'bm-switch-to-bm-buffer-other-window "buffer-manager"
;;;###autoload nil t)
(defun bm-switch-to-bm-buffer-other-window
    (&optional files-only buffer-list)
  "Switch to BM buffer in another window."
  (interactive "P")
  (setq files-only (when files-only
                     (> (prefix-numeric-value files-only) 0)))
  (let* ((bm-buffer (bm-get-bm-buffer-create files-only buffer-list))
         (bm-buffer (switch-to-buffer-other-window bm-buffer)))
    (bm-update bm-buffer)
    bm-buffer))

;;;###autoload(autoload 'bm-switch-to-bm-buffer-other-frame "buffer-manager"
;;;###autoload nil t)
(defun bm-switch-to-bm-buffer-other-frame
    (&optional files-only buffer-list)
  "Switch to BM buffer in another frame."
  (interactive "P")
  (setq files-only (when files-only
                     (> (prefix-numeric-value files-only) 0)))
  (let* ((bm-buffer (bm-get-bm-buffer-create files-only buffer-list))
         (bm-buffer (switch-to-buffer-other-frame bm-buffer)))
    (bm-update bm-buffer)
    bm-buffer))

;;;###autoload(autoload 'bm-display-bm-buffer "buffer-manager" nil t)
(defun bm-display-bm-buffer
    (&optional files-only buffer-list)
  "Display BM buffer."
  (interactive "P")
  (setq files-only (when files-only
                     (> (prefix-numeric-value files-only) 0)))
  (let* ((bm-buffer (bm-get-bm-buffer-create files-only buffer-list))
         (bm-window (display-buffer bm-buffer t)))
    (bm-update bm-buffer)
    bm-window))

(defun bm-get-bm-buffer-create
    (&optional files-only buffer-list previous-buffer)
  "Return the BM buffer, creating a new one if needed."
  (setq previous-buffer (or previous-buffer (current-buffer)))
  (let ((bm-buffer (get-buffer-create bm-buffer-name)))
    (with-current-buffer bm-buffer
      (bm-mode)
      (setq
        bm-files-only      files-only
        bm-buffer-list     buffer-list
        bm-previous-buffer previous-buffer))
    bm-buffer))

(defun bm-bm-buffer-p
    (&optional buffer)
  "Return t if BUFFER is the BM buffer."
  (setq buffer (or buffer (current-buffer)))
  (and (string-prefix-p bm-buffer-name (buffer-name buffer))
       (with-current-buffer buffer
         (eq major-mode 'bm-mode))))

(defun bm-interesting-buffer-p
    (&optional buffer)
  "\
Return t if BUFFER is a live buffer whose name does not start with a space."
  (and (buffer-live-p (or buffer (current-buffer)))
       (or (zerop (length (buffer-name buffer)))
           (/=    (aref   (buffer-name buffer) 0) ?\s))
       (not (bm-bm-buffer-p buffer))
       (not (minibufferp    buffer))))

(defun bm-toggle-files-only
    (&optional files-only bm-buffer)
  "Toggle whether to display only file-visiting buffers in BM."
  (interactive "P")
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (setq bm-files-only (if files-only
                            (> (prefix-numeric-value files-only) 0)
                          (not bm-files-only)))
    (message (if bm-files-only
                 "bm: Displaying only file-visiting buffers"
               "bm: Displaying all non-internal buffers"))
    (bm-update)
    bm-files-only))

;; TODO: Wrap like `bm-update' does (use `pos' and `bm-buffer' as optional
;; arguments):
(defalias 'bm-sort #'tabulated-list-sort)

(defun bm-buffer-at
    (&optional pos bm-buffer)
  "Return buffer of the BM entry at POS."
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (let ((buffer (tabulated-list-get-id pos)))
      (if (buffer-live-p buffer)
          buffer
        ;; TODO: Define the `bm-invalid-entry-error' error symbol.
        (error "bm: error: Invalid entry (%s): Buffer not found"
               (bm-line-number pos))))))

(defun bm-flags-at
    (&optional pos bm-buffer)
  "Return flag in index of the BM entry at POS."
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (let ((columns (tabulated-list-get-entry pos)))
      (if columns
          (bm--string-to-flags (aref columns bm-flags-string-column-number))
        ;; TODO: Define the `bm-invalid-entry-error' error symbol.
        (error "bm: error: Invalid entry (%s): Flags not found"
               (bm-line-number pos))))))

(defun bm-toggle-flag-at
    (flag &optional value pos bm-buffer)
  "Toggle whether to display (later) buffer of the BM entry at POS."
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (let ((buffer)
          (flags))
      (bm-invalid-entry-error pos bm-buffer
        (setq
          buffer (bm-buffer-at pos)
          flags  (bm-flags-at  pos)))
      ;; NOTE:
      ;; This will not only handle inconsistency of flags, but also
      ;; inconsistency of characters which might happen in case of dynamic
      ;; character customization (and as long as `bm--string-to-flags'
      ;; implements `character-flag-alist' generation).
      (if (equal flags (bm-flags buffer))
          (progn
            (funcall (get flag 'set)
                     ;; TODO:
                     ;; Only when `called-interactively-p'.
                     (if value
                         (> (prefix-numeric-value value) 0)
                       (not (funcall (get flag 'get) buffer)))
                     buffer)
            (setq flags (bm-flags buffer))
            (bm-save-excursion
              (when pos
                (goto-char pos))
              (tabulated-list-set-col bm-flags-string-column-number
                                      (bm--flags-to-string flags)
                                      'change-entry-data)))
        ;; TODO:
        ;; Create macro `bm-obsolete-entry-error'.
        (save-excursion
          (when pos
            (goto-char pos))
          (let ((inhibit-read-only t))
            (add-face-text-property (line-beginning-position)
                                    (line-end-position)
                                    'bm-obsolete-entry)))
        (if (yes-or-no-p (format "bm: prompt: Obsolete entry (%s): Update? "
                                 (bm-line-number pos)))
            (let ((entry (bm-update-entry-at pos)))
              (bm--assert
                (eq buffer (bm-buffer-at pos)))
              (bm--assert
                (equal (setq flags (bm-flags buffer)) (bm-flags-at pos))))
          (error "bm: error: Obsolete entry (%s): Entry is out of date"
                 (bm-line-number pos))))
      (aref flags (get flag 'index)))))

(defun bm-update-entry-at
    (&optional pos bm-buffer)
  "Update the BM entry at POS."
  (interactive)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (let ((buffer)
          (columns))
      (bm-invalid-entry-error pos bm-buffer
        (setq buffer (bm-buffer-at pos)))
      (setq columns (bm--columns buffer))
      (bm-save-excursion
        (when pos
          (goto-char pos))
        (tabulated-list-delete-entry)
        (tabulated-list-print-entry buffer columns))
      (list buffer columns))))

(defun bm-update
    (&optional bm-buffer)
  "display buffers marked with `bm-marked-flag'."
  (interactive)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (revert-buffer)))

(defun bm-display
    (&optional bm-buffer)
  "display buffers marked with `bm-marked-flag'."
  (interactive)
  (bm-check-customs)
  (let ((buffer-list (bm-buffer-list 'bm-marked-flag bm-buffer))
        (buffer)
        (window))
    (when buffer-list
      (delete-other-windows)
      (setq
        buffer (pop buffer-list)
        window (split-window nil nil bm-display-buffer-side))
      (set-window-buffer window buffer)
      ;; (funcall (get 'bm-marked-flag 'set) nil buffer)
      )
    (bm-keep-balanced-windows
      (while buffer-list
        (setq buffer (pop buffer-list))
        ;; Maximize to increase probability of successful split.
        (ignore-errors (maximize-window window))
        (condition-case nil
            (setq window (split-window window nil 'below))
          (error
           (user-error "bm: error: %s"
                       "Stack of windows has reached maximum size")))
        (set-window-buffer window buffer)
        ;; (funcall (get 'bm-marked-flag 'set) nil buffer)
        )))
  (bm-update bm-buffer))

(defun bm-kill
    (&optional bm-buffer)
  "display buffers marked with `bm-display-flag'."
  (interactive)
  (let ((buffer-list (bm-buffer-list 'bm-kill-flag bm-buffer))
        (buffer))
    (while buffer-list
      (setq buffer (pop buffer-list))
      (let (debug-on-error)
        (with-demoted-errors "bm: error: %s"
          (when (funcall (get 'bm-save-flag 'get) buffer bm-buffer)
            (with-current-buffer buffer
              (save-buffer))
            (funcall (get 'bm-save-flag 'set) nil buffer))
          (kill-buffer buffer)))))
  (bm-update bm-buffer))

(defun bm-save
    (&optional bm-buffer)
  "display buffers marked with `bm-display-flag'."
  (interactive)
  (let ((buffer-list (bm-buffer-list 'bm-save-flag bm-buffer))
        (buffer))
    (while buffer-list
      (setq buffer (pop buffer-list))
      (let (debug-on-error)
        (with-demoted-errors "bm: error: %s"
          (with-current-buffer buffer
            (save-buffer))
          (funcall (get 'bm-save-flag 'set) nil buffer)))))
  (bm-update bm-buffer))

(defun bm-line-number
    (&optional pos)
  "Return the horizontal position of POS.
POS defaults to point."
  (line-number-at-pos pos))

(defun bm-column-number
    (&optional pos)
  "Return the horizontal position of POS.
POS defaults to point."
  (save-excursion
    (when pos
      (goto-char pos))
    (current-column)))

;; ---------------------------------------------------------------------------
;; TODO:
;;
;; Generate this function. Priority is deduced from order of flags'
;; definition.  Position is determined by index.
(defun bm-flags
    (&optional buffer bm-buffer)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (vector (cond ((bm--buffer-marked-p    buffer) 'bm-marked-flag)
                  ((bm--buffer-kill-p      buffer) 'bm-kill-flag)
                  ((bm--buffer-previous-p  buffer) 'bm-previous-flag))
            (cond ((bm--buffer-save-p      buffer) 'bm-save-flag)
                  ((bm--buffer-modified-p  buffer) 'bm-modified-flag))
            (cond ((bm--buffer-read-only-p buffer) 'bm-read-only-flag))
            (cond ((bm--buffer-displayed-p buffer) 'bm-displayed-flag)))))
;; ---------------------------------------------------------------------------

(defun bm-flag-set-p
    (flag flags)
  (funcall (intern (format "%s-set-p" (symbol-name flag))) flags))

(defun bm-buffer-list
    (&optional flag bm-buffer)
  (setq bm-buffer (bm--ensure-buffer bm-buffer))
  (with-current-buffer bm-buffer
    (delq nil
          (mapcar #'(lambda
                        (buffer)
                      (and (bm-interesting-buffer-p buffer)
                           (or (buffer-file-name buffer)
                               (not bm-files-only))
                           (or (null flag)
                               (bm-flag-set-p flag (bm-flags buffer)))
                           buffer))
                  (or bm-buffer-list
                      (buffer-list (when bm-selected-frame-only
                                     (selected-frame))))))))

(defun bm--entries ()
  (bm--ensure-buffer)
  (let (entries)
    (dolist (buffer (bm-buffer-list))
      (push (bm--entry buffer) entries))
    (nreverse entries)))

(defun bm--entry
    (&optional buffer bm-buffer)
  (list buffer (bm--columns buffer bm-buffer)))

(defun bm--columns
    (&optional buffer bm-buffer)
  (vector (bm--flags-to-string    (bm-flags buffer bm-buffer))
          (bm--pretty-buffer-name buffer)
          (bm--pretty-mode-name   buffer)
          (bm--pretty-file-path   buffer)))

(defun bm--ensure-buffer
    (&optional buffer)
  (unless (bm-bm-buffer-p buffer)
    (error "bm: error: Not the BM buffer: %s" buffer))
  (or buffer (current-buffer)))

(defun bm--flags-to-string
    (flags)
  (mapconcat #'(lambda
                   (flag)
                 (propertize (string (symbol-value (get flag 'character)))
                             'font-lock-face flag))
             flags
             nil))

(defun bm--string-to-flags
    (string)
  ;; TODO:
  ;; Generate `character-flag-alist' here to allow for dynamic character
  ;; customization.
  (vconcat (mapcar #'(lambda
                         (character)
                       (cdr (assoc character bm--character-flag-alist)))
                   string)))

(defun bm--pretty-buffer-name
    (&optional buffer)
  (propertize (buffer-name buffer)
              'font-lock-face 'bm-buffer-name
              'mouse-face     'highlight))

(defun bm--pretty-mode-name
    (&optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (with-current-buffer buffer
    (concat (format-mode-line mode-name
                              nil
                              nil
                              buffer)
            (when (bound-and-true-p mode-line-process)
              (format-mode-line mode-line-process
                                nil
                                nil
                                buffer)))))

(defun bm--pretty-file-path
    (&optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (with-current-buffer buffer
    (let ((path buffer-file-name))
      (cond (path
             (abbreviate-file-name path))
            ((eq major-mode 'Info-mode)
             (bm--pretty-Info-node-description))
            ((bound-and-true-p list-buffers-directory)
             list-buffers-directory)
            (t
             "")))))

(defun bm--pretty-Info-node-description
    (&optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (with-current-buffer buffer
    (let ((path Info-current-file))
      (cond ((equal path "dir")    "*Info Directory*")
            ((eq    path 'apropos) "*Info Apropos*")
            ((eq    path 'history) "*Info History*")
            ((eq    path 'toc)     "*Info TOC*")
            ((not (stringp path))  "")
            (t                     (concat "("
                                           (file-name-nondirectory path)
                                           ")"
                                           " "
                                           Info-current-node))))))

(defsubst bm-nil-flag-p
    (flag)
  "Return t if FLAG is nil."
  (null flag))

(defsubst bm-nil-flag-set-p
    (flags index)
  "Return t if FLAGS contains nil at INDEX."
  (bm-nil-flag-p (aref flags index)))

(bm--define-flag-get-function displayed
  (not (null (get-buffer-window buffer t))))

(bm--define-flag-get-function marked
  (with-current-buffer buffer
    (unless (local-variable-p 'bm--marked-or-kill)
      (setq-local bm--marked-or-kill nil))
    (eq bm--marked-or-kill 'marked)))

(bm--define-flag-get-function kill
  (with-current-buffer buffer
    (unless (local-variable-p 'bm--marked-or-kill)
      (setq-local bm--marked-or-kill nil))
    (eq bm--marked-or-kill 'kill)))

(bm--define-flag-get-function previous
  (eq buffer (with-current-buffer bm-buffer
               bm-previous-buffer)))

(bm--define-flag-get-function modified
  (buffer-modified-p buffer))

(bm--define-flag-get-function save
  (with-current-buffer buffer
    (unless (local-variable-p 'bm--save)
      (setq-local bm--save nil))
    (not (null bm--save))))

(bm--define-flag-get-function read-only
  (with-current-buffer buffer
    (not (null buffer-read-only))))

(bm--define-flag-set-function marked
  (with-current-buffer buffer
    (setq-local bm--marked-or-kill (when value
                                     'marked))))

(bm--define-flag-set-function kill
  (with-current-buffer buffer
    (setq-local bm--marked-or-kill (when value
                                     'kill))))

(bm--define-flag-set-function modified
  (with-current-buffer buffer
    (set-buffer-modified-p value))
  (not (null value)))

(bm--define-flag-set-function save
  (with-current-buffer buffer
    (setq-local bm--save (not (null value)))))

(bm--define-flag-set-function read-only
  (with-current-buffer buffer
    (setq-local buffer-read-only (not (null value)))))

(bm--define-toggle-flag-at-function marked)
(bm--define-toggle-flag-at-function kill)
(bm--define-toggle-flag-at-function modified)
(bm--define-toggle-flag-at-function save)
(bm--define-toggle-flag-at-function read-only)

(defun bm-make-mode-map ()
  "Construct and return a new BM mode keymap."
  (let ((keymap (make-sparse-keymap)))
    (define-key keymap (kbd "<follow-link>") 'mouse-face)

    (define-key keymap (kbd "<mouse-2>") #'bm-switch-to-buffer-at)

    (define-key keymap (kbd "U") #'bm-update)
    (define-key keymap (kbd "u") #'bm-update-entry-at)

    (define-key keymap (kbd "D") #'bm-display)
    (define-key keymap (kbd "d") #'bm-display-buffer-at)

    ;; (define-key keymap (kbd "a m") #'bm-toggle-marked-flag-all)
    ;; (define-key keymap (kbd "DEL m") #'bm-unmark)

    (define-key keymap (kbd "x d") #'bm-display)
    (define-key keymap (kbd "x k") #'bm-kill)
    (define-key keymap (kbd "x s") #'bm-save)

    (define-key keymap (kbd "K") #'bm-kill)
    (define-key keymap (kbd "S") #'bm-save)

    (define-key keymap (kbd ".") #'bm-toggle-marked-flag-at)
    (define-key keymap (kbd "m") #'bm-toggle-marked-flag-at)
    (define-key keymap (kbd "k") #'bm-toggle-kill-flag-at)
    (define-key keymap (kbd "s") #'bm-toggle-save-flag-at)
    (define-key keymap (kbd "*") #'bm-toggle-modified-flag-at)
    (define-key keymap (kbd "r") #'bm-toggle-read-only-flag-at)

    (define-key keymap (kbd "f") #'bm-toggle-files-only)

    (define-key keymap (kbd "$") #'bm-sort)

    ;; TODO: [remaps]
    ;; (define-key keymap (kbd "C-SPC") #'bm-switch-to-buffer-other-window-at)
    (define-key keymap (kbd "RET")   #'bm-switch-to-buffer-at)

    keymap))

(defvar bm-mode-map
  (bm-make-mode-map)
  "Local keymap for `bm-mode' buffers.")

(define-derived-mode bm-mode
  tabulated-list-mode
  "Buffer-Manager"
  "Major mode for BM."
  (setq-local buffer-stale-function #'(lambda
                                          (&optional _noconfirm)
                                        'fast))
  (setq tabulated-list-use-header-line bm-header-line
    tabulated-list-padding 1
    tabulated-list-format (vector `(""       4                     nil)
                                  `("Buffer" ,bm-buffer-name-width t)
                                  `("Mode"   ,bm-mode-name-width   t)
                                  `("File"   ,bm-file-path-width   t))
    tabulated-list-entries #'bm--entries)
  (tabulated-list-init-header))

(provide 'buffer-manager-functions)
