(require 'buffer-manager-macros)
;;
(defgroup buffer-manager
  nil
  "Customization group for Buffer Manager (BM)."
  :tag "Buffer Manager (BM)"
  :group 'convenience
  :group 'tools
  :prefix 'bm-)
;;
(defcustom bm-display-buffer-side
  'right
  "Specifies on what side display buffer should be located."
  :tag "BM Display Buffer Side"
  :type '(choice (const :tag "Left"  left)
                 (const :tag "Right" right)))
;;
(defvaralias 'bm-column-titles 'bm-header-line)
(defcustom bm-header-line
  t
  "Whether to display header line (column titles) in BM."
  :tag "BM Header Line"
  :type 'boolean)
;;
(defcustom bm-selected-frame-only
  t
  "Whether to display buffers from currently selected frame's buffer list in
BM.  Buffers that were never selected in that frame are listed at the very
end."
  :tag "BM Current Frame Buffer List"
  :type 'boolean)
;;
(defcustom bm-buffer-name-width
  31
  "Width of buffer name column in BM."
  :tag "BM Buffer Name Width"
  :type 'number)
;;
(defcustom bm-mode-name-width
  23
  "Width of mode name column in BM."
  :tag "BM Mode Name Width"
  :type 'number)
;;
(defcustom bm-file-path-width
  0
  "Width of file path column in BM."
  :tag "BM File Path Width"
  :type 'number)
;;
(put nil 'character 'bm-nil-flag-character)
;;
(defcustom bm-nil-flag-character
  ?\s
  "Character for the 'nil' flag."
  :tag "BM Nil Flag Character"
  :type 'character)
;;
(defconst bm--character-flag-alist
  nil
  "Association list of character flags in BM.")
;;
(require 'buffer-manager-faces)
;;
;; TODO: `view':
(bm--define-flags
  marked    0 ?> nil
  kill      0 ?K "#FF0000"
  previous  0 ?P nil
  modified  1 ?* "#FFFF00"
  save      1 ?S "#00FFFF"
  read-only 2 ?# "#FF0000"
  displayed 3 ?D "#00FF00")
;;
(defconst bm-buffer-name
  "*Buffer-Manager*"
  "Name of the BM buffer.")
;;
(defconst bm-flags-string-column-number
  0
  "Column number for flags' string.")
;;
(defvar-local bm-files-only
  nil
  "Whether to display only file-visiting buffers in BM.")
(put 'bm-files-only 'permanent-local t)
;;
(defvar-local bm-buffer-list
  nil
  "Whether to display only file-visiting buffers in BM.")
(put 'bm-buffer-list 'permanent-local t)
;;
(defvar-local bm-previous-buffer
  nil
  "Whether to display only file-visiting buffers in BM.")
(put 'bm-previous-buffer 'permanent-local t)
;;
(provide 'buffer-manager-variables)
