(defgroup buffer-manager-faces
  nil
  "Faces for BM."
  :group 'buffer-manager
  :group 'faces
  :link '(custom-group-link "buffer-manager")
  :prefix 'bm-)
;;
(defface bm-buffer-name
  '((t :inherit mode-line-buffer-id))
  "Face for buffer name in BM."
  :tag "BM Buffer Name Face")
;;
(defface bm-invalid-entry
  '((t :strike-through "#FF0000"))
  "Face for invalid entry in BM."
  :tag "BM Invalid Entry Face")
;;
(defface bm-obsolete-entry
  '((t :strike-through "#FFFF00"))
  "Face for obsolete entry in BM."
  :tag "BM Obsolete Entry Face")
;;
;; TODO:
;; (defface bm-nil-flag
;;   '((t :inherit default))
;;   "Face for the nil flag."
;;   :tag "BM Nil Flag Face")
;;
(provide 'buffer-manager-faces)
