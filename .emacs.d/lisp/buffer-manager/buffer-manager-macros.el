(eval-when-compile
  (require 'cl-lib))

(defmacro bm-invalid-entry-error
    ;; TODO: Make `pos' and `bm-buffer' optional.
    (pos bm-buffer &rest body)
  "Execute BODY and if there is error, add text property `bm-invalid-entry'."
  (declare (indent defun)
           (debug  t))
  `(condition-case e
       (progn ,@body)
     ;; TODO: Rather catch `bm-invalid-entry-error' specifically.
     (error
      ;; TODO: Only when `called-interactively-p'.
      (with-current-buffer (bm--ensure-buffer ,bm-buffer)
        ;; TODO: `bm-with-current-point'.
        (save-excursion
          (when ,pos
            (goto-char ,pos))
          (let ((inhibit-read-only t))
            (add-face-text-property (line-beginning-position)
                                    (line-end-position)
                                    'bm-invalid-entry))))
      (signal (car e) (cdr e)))))

(defmacro bm-keep-balanced-windows
    (&rest body)
  "Execute BODY, then call `balance-windows'."
  (declare (indent defun)
           (debug  t))
  `(unwind-protect
       (progn ,@body)
     (balance-windows)))

(defmacro bm-ignore-messages
    (&rest body)
  "Execute BODY without displaying and logging of messages."
  (declare (debug  t)
           (indent defun))
  `(let ((message-function (symbol-function 'message))
         (message-log-max  nil))
     (fset 'message #'ignore)
     (unwind-protect
         (progn ,@body)
       (fset 'message message-function))))

(defmacro bm-save-column
    (pos &rest body)
  "\
Execute BODY, then restore the previous column at POS."
  (declare (debug  t)
           (indent defun))
  `(let ((bm--column (bm-column-number ,pos)))
     (unwind-protect
         (save-current-buffer ,@body)
       (beginning-of-line)
       (move-to-column bm--column))))

(defmacro bm-save-line
    (pos &rest body)
  "\
Execute BODY, then restore the previous line at POS."
  (declare (debug  t)
           (indent defun))
  `(let ((bm--line (bm-line-number ,pos)))
     (unwind-protect
         (save-current-buffer ,@body)
       (goto-char (point-min))
       (forward-line (1- bm--line)))))

(defmacro bm-save-excursion
    (&rest body)
  "\
Execute BODY, then restore the previous point and mark."
  (declare (debug  t)
           (indent defun))
  `(bm-save-column (point)
     (bm-save-line (point)
       (unwind-protect
           (bm-save-column (mark :force)
             (bm-save-line (mark :force)
               ,@body))
         (set-marker (mark-marker) (point))))))

;; (defmacro bm--define-flag-accessors
;;     (name &rest keywords)
;;   (declare (indent defun)
;;            (debug  t))
;;   (let ((flag-var (intern (format "bm--%s" (symbol-name name))))
;;         (keyword-plist)
;;         (get-function)
;;         (set-function))
;;     (while (keywordp (car-safe keywords))
;;       (setq keyword-plist (plist-put keyword-plist
;;                                      (pop keywords)
;;                                      (pop keywords))))
;;     (setq get-function (plist-get keyword-plist :get)
;;           set-function (plist-get keyword-plist :set))
;;     `(progn
;;        (put ',flag-var 'set (or #',set-function
;;                                 #'(lambda
;;                                       (&optional buffer)
;;                                     (with-current-buffer buffer
;;                                       ,flag-var))))
;;        ;; (funcall ,(plist-get keyword-plist :set) ',symbol)
;;        (defvar-local ,flag-var
;;          nil
;;          ,(format "Variable for the state of the '%s' flag."
;;                   (symbol-name name)))
;;        (put ',flag-var 'permanent-local t))))

;; (bm--define-flag-var display)
;; (bm--define-flag-var kill)
;; (bm--define-flag-var save)

;; (defmacro bm--define-flag-alias
;;     (name base-var)
;;   (declare (indent defun)
;;            (debug  t))
;;   (let ((flag-var (intern (format "bm--%s" (symbol-name name)))))
;;     `(defvaralias ',flag-var ',base-var
;;        ,(format "Variable for the state of the '%s' flag."
;;                 (symbol-name name)))))

;; (bm--define-flag-alias read-only buffer-read-only)

;; TODO:
;; Rename to something like `bm--define-flag-predicate-get-function', but even
;; better move that all to `bm--define-flag' with `:set' and `:get'
;; keywords. `bm-buffer' argument should only be left for
;; `bm--buffer-previous-p'.
(defmacro bm--define-flag-get-function
    (name &rest body)
  (declare (indent defun)
           (debug  t))
  (let ((symbol   (intern (format "bm-%s-flag"      (symbol-name name))))
        (function (intern (format "bm--buffer-%s-p" (symbol-name name)))))
    `(progn
       (defun ,function
           (&optional buffer bm-buffer)
         ,(format "Return the state of the '%s' flag in BUFFER."
                  (symbol-name name))
         (setq buffer    (or buffer (current-buffer))
           bm-buffer (bm--ensure-buffer bm-buffer))
         ,(if body
              `(progn ,@body)
            `(with-current-buffer buffer
               ,symbol)))
       (put ',symbol 'get #',function))))

(defmacro bm--define-flag-set-function
    (name &rest body)
  (declare (indent defun)
           (debug  t))
  (let ((symbol (intern (format "bm-%s-flag" (symbol-name name)))))
    `(put ',symbol 'set #'(lambda
                              (value &optional buffer)
                            (setq buffer (or buffer (current-buffer)))
                            ,(if body
                                 `(progn ,@body)
                               `(with-current-buffer buffer
                                  (setq-local ,symbol value)))))))

;; (defmacro bm--define-flag-set-fun
;;     (name)
;;   (declare (indent defun)
;;            (debug  t))
;;   (let ((flag-set-fun (intern (format "bm--%s-flag-set" (symbol-name name))))
;;         (flag-var     (intern (format "bm--%s-flag"     (symbol-name name)))))
;;     `(defun ,flag-set-fun
;;          (value &optional buffer)
;;        ,(format "Set the state of the '%s' flag to VALUE in BUFFER."
;;                 (symbol-name name))
;;        (with-current-buffer buffer
;;          (setq ,flag-var value)))))

(defmacro bm--define-toggle-flag-at-function
    (name)
  (declare (indent defun)
           (debug  t))
  (let ((flag     (intern (format "bm-%s-flag"           (symbol-name name))))
        (function (intern (format "bm-toggle-%s-flag-at" (symbol-name name)))))
    `(defun ,function
         (&optional ,name pos bm-buffer)
       ,(format "Toggle the '%s' flag at POS."
                (symbol-name name))
       (interactive "P")
       (bm-toggle-flag-at ',flag ,name pos bm-buffer))))

(defmacro bm--assert
    (form)
  "\
Signal `error' if FORM evaluates to nil."
  (declare (debug  t)
           (indent defun))
  (macroexpand `(cl-assert (identity ,form) :show-args)))

(defmacro bm--define-flag
    (name index character foreground)
  (declare (indent defun)
           (debug  t))
  (let* ((symbol           (intern (format "bm-%s-flag"
                                           (symbol-name name))))
         (symbol-p         (intern (format "%s-p"
                                           (symbol-name symbol))))
         (symbol-set       (intern (format "%s-set"
                                           (symbol-name symbol))))
         (symbol-unset     (intern (format "%s-unset"
                                           (symbol-name symbol))))
         (symbol-set-p     (intern (format "%s-set-p"
                                           (symbol-name symbol))))
         (symbol-toggle    (intern (format "%s-toggle"
                                           (symbol-name symbol))))
         (symbol-character (intern (format "%s-character"
                                           (symbol-name symbol)))))
    `(progn
       (bm--assert (featurep 'buffer-manager-faces))

       (defun ,symbol-p
           (flag)
         ,(format "Return t if FLAG is `eq' to the symbol `%s'."
                  (symbol-name symbol))
         (eq flag ',symbol))

       (defun ,symbol-set
           (flags)
         ,(format "Set the symbol `%s' in FLAGS and return it."
                  (symbol-name symbol))
         (aset flags (get ',symbol 'index) ',symbol))

       (defun ,symbol-unset
           (flags)
         ,(format "Unset the symbol `%s' in FLAGS and return nil."
                  (symbol-name symbol))
         (aset flags (get ',symbol 'index) nil))

       (defun ,symbol-set-p
           (flags)
         ,(format "Return t if FLAGS contains the symbol `%s'."
                  (symbol-name symbol))
         (,symbol-p (aref flags (get ',symbol 'index))))

       (defun ,symbol-toggle
           (flags &optional how)
         ,(format "Toggle the symbol `%s' in FLAGS."
                  (symbol-name symbol))
         (unless (or (null how)
                     (memq how '(set unset)))
           (error "bm: error: %s: `%s'"
                  "Unrecognized value of `how'"
                  how))
         (if how
             (funcall (intern (format "%s-%s"
                                      (symbol-name ',symbol)
                                      (symbol-name how)))
                      flags)
           (if (,symbol-set-p flags)
               (,symbol-unset flags)
             (,symbol-set flags))))

       (put ',symbol 'index     ,index)
       (put ',symbol 'character ',symbol-character)

       (defcustom ,symbol-character
         ,character
         ,(format "Character for the '%s' flag."
                  (symbol-name name))
         :tag ,(format "BM %s Flag Character"
                       (capitalize (symbol-name name)))
         :type 'character
         :set #'(lambda
                    (symbol value)
                  (when (boundp symbol)
                    (delete (cons (symbol-value symbol) ',symbol)
                            bm--character-flag-alist))
                  (push (cons value ',symbol) bm--character-flag-alist)
                  (set-default symbol value)))
       (bm--assert (member (cons ,symbol-character ',symbol)
                           bm--character-flag-alist))

       (defface ,symbol
         '((t :foreground ,foreground
              :weight     bold))
         ,(format "Face for the '%s' flag."
                  (symbol-name name))
         :tag ,(format "BM %s Flag Face"
                       (capitalize (symbol-name name)))))))

(defmacro bm--define-flags
    (name index character foreground &rest ...)
  (declare (indent defun)
           (debug  t))
  (let (body)
    (while name
      (push `(bm--define-flag ,name ,index ,character ,foreground) body)
      (setq
        name       (pop ...)
        index      (pop ...)
        character  (pop ...)
        foreground (pop ...)))
    `(progn ,@body)))

(provide 'buffer-manager-macros)
