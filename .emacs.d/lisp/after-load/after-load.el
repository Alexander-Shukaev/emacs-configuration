(eval-when-compile
  (require 'cl-lib))
;;
;;;###autoload
(defconst after-load-font-lock-keywords
  '(("\\(?:^\\|[^`']\\)(after-load\\_>"
     ("\\(?:\\sw\\|\\s_\\)+"
      nil
      nil
      (0 font-lock-constant-face nil t)))))
;;
;;;###autoload
(font-lock-add-keywords 'emacs-lisp-mode after-load-font-lock-keywords)
;;
(defvar after-load--files    nil)
(defvar after-load--features nil)
;;
(unless (fboundp 'with-eval-after-load)
  (defmacro with-eval-after-load
      (file &rest body)
    "\
Execute BODY after FILE is loaded.
FILE is normally a feature name, but it can also be a file name,
in case that file does not provide any feature.  See `eval-after-load'
for more details about the different forms of FILE and their semantics."
    (declare (debug  t)
             (indent 1))
    ;; -----------------------------------------------------------------------
    ;; NOTE:
    ;;
    ;; This definition is a bit different from that in Emacs 24.4: an extra
    ;; `funcall' is needed for older versions of Emacs.
    ;; -----------------------------------------------------------------------
    `(eval-after-load ,file
       `(funcall (function #',(lambda () ,@body))))))
;;
(defsubst after-load-as-string
    (string-or-symbol)
  "\
If STRING-OR-SYMBOL is already a string, return it.
Otherwise convert it to a string and return that."
  (if (stringp string-or-symbol)
      string-or-symbol
    (symbol-name string-or-symbol)))
;;
(defsubst after-load-as-symbol
    (string-or-symbol)
  "\
If STRING-OR-SYMBOL is already a symbol, return it.
Otherwise convert it to a symbol and return that."
  (if (symbolp string-or-symbol)
      string-or-symbol
    (intern string-or-symbol)))
;;
(defun after-load-load-form
    (file-or-feature)
  (when (and (listp file-or-feature)
             (eq (car-safe file-or-feature) 'quote))
    (setq file-or-feature (cadr file-or-feature)))
  (let ((string (after-load-as-string file-or-feature))
        (symbol (after-load-as-symbol file-or-feature)))
    `(if (or (member  ,string after-load--files)
             (memq   ',symbol after-load--features))
         (when (and (not noninteractive) force-load-messages)
           (message "%s" ,(format "Loading %s...skipped" file-or-feature)))
       ;; --------------------------------------------------------------------
       ;; CAUTION:
       ;;
       ;; Locally binding `after-load-alist' to nil speeds up byte compilation
       ;; but makes no sense functionally as it results in potentially broken
       ;; post-load initialization (see `eval-after-load') for some packages.
       ;; Not worth it.
       ;; --------------------------------------------------------------------
       (let (debug-on-error)
         (with-demoted-errors
             ,(format "Cannot load %s: %%S" file-or-feature)
           ,(cond ((stringp file-or-feature)
                   `(let* ((after-load--file     ,file-or-feature)
                           (after-load--files    (cons after-load--file
                                                       after-load--files)))
                      (unless (after-load-load-history after-load--file)
                        (load after-load--file nil :no-message))))
                  ((symbolp file-or-feature)
                   `(let* ((after-load--feature ',file-or-feature)
                           (after-load--features (cons after-load--feature
                                                       after-load--features)))
                      (require after-load--feature)))
                  (t
                   (signal 'wrong-type-argument
                           (list #'or
                                 (list #'stringp file-or-feature)
                                 (list #'symbolp file-or-feature))))))))))
;;
(defun after-load-load-forms
    (file-or-feature-list)
  (cl-loop for     file-or-feature
           in      file-or-feature-list
           for     form = (after-load-load-form file-or-feature)
           when    form
           collect form))
;;
(defun after-load-body
    (file-or-feature-list body)
  (if (null file-or-feature-list)
      body
    `((with-eval-after-load ',(car file-or-feature-list)
        ,@(after-load-body (cdr file-or-feature-list) body)))))
;;
(defsubst after-load-load-history
    (&optional library no-suffix path)
  "\
Return mapping(s) of loaded file name to symbols and features."
  (if library
      (let ((file (locate-library library no-suffix path)))
        (when file
          (setq file (expand-file-name file))
          (or (when (string-match-p "\\.el\\'" file)
                (assoc (concat file "c") load-history))
              (assoc file load-history))))
    load-history))
;;
(defsubst after-load-load-p ()
  "\
Return t if both `load-in-progress' and `load-file-name' are non-nil."
  (and load-in-progress load-file-name t))
;;
(defsubst after-load-byte-compile-p ()
  "\
Return t if `after-load-load-p' is nil and either `byte-compile-current-file'
or `comp-native-compiling' is non-nil."
  (unless (after-load-load-p)
    (and (or (bound-and-true-p byte-compile-current-file)
             (bound-and-true-p comp-native-compiling))
         t)))
(defalias 'after-load-compile-p #'after-load-byte-compile-p)
;;
;;;###autoload
(defmacro after-load
    (file-or-feature &rest body)
  (declare (debug  t)
           (indent 1))
  (when (and (listp file-or-feature)
             (eq (car-safe file-or-feature) 'quote))
    (setq file-or-feature (cdr file-or-feature)))
  (let ((file-or-feature-list (if (listp file-or-feature)
                                  file-or-feature
                                (list file-or-feature))))
    `(progn
       ,@(when (after-load-compile-p)
           `((lambda ()
               (eval-and-compile
                 (let ((byte-compile-current-file)
                       (comp-native-compiling))
                   ,@(after-load-load-forms file-or-feature-list))))))
       ,@(after-load-body file-or-feature-list body))))
;;
(provide 'after-load)
