(defgroup inhibit-set-auto-mode
  nil
  "Customization group for Inhibit `set-auto-mode'."
  :tag "Inhibit `set-auto-mode'"
  :group 'files
  :prefix 'inhibit-set-auto-mode-)
;;
(defcustom inhibit-set-auto-mode-functions
  nil
  "\
List of functions to be called to try to inhibit `set-auto-mode'.

Only used by `inhibit-set-auto-mode'.  If one of them returns
non-nil, inhibit `set-auto-mode' and the rest are not called."
  :type 'hook)
;;
(defun inhibit-set-auto-mode--around-advice
    (function &rest ...)
  (if (run-hook-with-args-until-success 'inhibit-set-auto-mode-functions)
      (let (enable-local-variables
            interpreter-mode-alist
            magic-mode-alist
            auto-mode-alist
            magic-fallback-mode-alist)
        (apply function ...))
    (apply function ...)))
;;
;;;###autoload(autoload 'inhibit-set-auto-mode "inhibit-set-auto-mode" nil t)
(define-minor-mode inhibit-set-auto-mode
  "\
Toggle inhibiting `set-auto-mode'.

With a prefix argument ARG, enable the mode if ARG is positive,
and disable it otherwise.  If called from Lisp, enable the mode
if ARG is omitted or nil.

See `inhibit-set-auto-mode-functions'."
  :group 'inhibit-set-auto-mode
  :global t
  (if inhibit-set-auto-mode
      (advice-add #'set-auto-mode
                  :around
                  #'inhibit-set-auto-mode--around-advice
                  '((depth . 100)))
    (advice-remove #'set-auto-mode
                   #'inhibit-set-auto-mode--around-advice)))
;;
(provide 'inhibit-set-auto-mode)
