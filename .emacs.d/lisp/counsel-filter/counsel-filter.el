(require 'counsel)
;;
(defgroup counsel-filter
  nil
  "Customization group for Counsel Filter (CF)."
  :tag "Counsel Filter (CF)"
  :group 'counsel
  :prefix 'counsel-filter-)
;;
(defun counsel--projectile-project-root ()
  "Return root of current project or nil on failure.
Use `projectile-project-root' to determine the root."
  (and (fboundp 'projectile-project-p)
       (fboundp 'projectile-project-root)
       (projectile-project-p)
       (projectile-project-root)))
;;
(defvar counsel-filter-file-root-functions
  '(counsel--projectile-project-root
    counsel--project-current
    counsel--git-root
    counsel--configure-root
    counsel--dir-locals-root)
  "Special hook to find the project root for `counsel-filter-file'.
Each function on this hook is called in turn with no arguments
and should return either a directory, or nil if no root was
found.")
;;
(defun counsel-filter-file-root ()
  "Return root of current project or `default-directory'.
The root is determined by `counsel-filter-file-root-functions'."
  (or (run-hook-with-args-until-success 'counsel-filter-file-root-functions)
      default-directory))
;;
(defcustom counsel-filter-file-dir-function
  #'counsel-filter-file-root
  "Function that returns a directory for `counsel-filter-file'."
  :type 'function)
;;
(defcustom counsel-filter-file-find-args
  "-name '.git' -prune -o -type f -print"
  "Arguments for the `find-command' for `counsel-filter-file'."
  :type 'string)
;;
(defcustom counsel-filter-file-grep-args
  "-E -e %s"
  "Arguments for the `grep-command' for `counsel-filter-file'."
  :type 'string)
;;
(defvar counsel-filter-file-command
  nil
  "Command for `counsel-filter-file'.")
;;
(defvar counsel-filter-file-dir
  nil
  "Directory for `counsel-filter-file'.")
;;
(defun counsel-filter-file-function
    (string)
  (or (ivy-more-chars)
      (let ((default-directory counsel-filter-file-dir)
            (regex (replace-regexp-in-string "\n" ""
                                             (counsel--grep-regex string))))
        (counsel--async-command (format counsel-filter-file-command
                                        (shell-quote-argument regex)))
        nil)))
;;
(defun counsel-filter-file-action
    (f)
  (with-ivy-window
    (let ((default-directory counsel-filter-file-dir))
      (find-file f))))
;;
;;;###autoload
(defun counsel-filter-file
    (&optional initial-input initial-directory prompt action)
  (interactive (list nil
                     (when current-prefix-arg
                       (read-directory-name "From directory: "))))
  (counsel-require-program find-program)
  (counsel-require-program grep-program)
  (setq counsel-filter-file-command
    (mapconcat #'identity
               (list find-program
                     "."
                     counsel-filter-file-find-args
                     (when (fboundp 'make-process)
                       (concat "2> " (shell-quote-argument null-device)))
                     "| cut -c 3- |"
                     grep-program
                     counsel-filter-file-grep-args
                     (unless (string-match-p "%s"
                                             counsel-filter-file-grep-args)
                       "%s"))
               " "))
  (setq counsel-filter-file-dir
    (or initial-directory
        (and (functionp counsel-filter-file-dir-function)
             (funcall counsel-filter-file-dir-function))
        (and (eq major-mode 'dired-mode)
             (fboundp 'dired-current-directory)
             (dired-current-directory))
        default-directory))
  (let ((default-directory counsel-filter-file-dir))
    (ivy-read (or prompt "Filter file: ")
              #'counsel-filter-file-function
              :initial-input initial-input
              :dynamic-collection t
              :preselect (counsel--preselect-file)
              :require-match 'confirm-after-completion
              :history 'file-name-history
              :keymap counsel-find-file-map
              :action (or action #'counsel-filter-file-action)
              :caller 'counsel-filter-file)))
(ivy-configure 'counsel-filter-file
  :unwind-fn #'counsel--grep-unwind
  :occur #'counsel-filter-file-occur
  :exit-codes '(1 ""))
;;
(defvar counsel-filter-file-occur-command
  "%s | xargs -d '\\n' ls -d --group-directories-first"
  "Command for `counsel-filter-file-occur'.")
;;
(defun counsel-filter-file-occur
    (&optional _cands)
  (cd counsel-filter-file-dir)
  (let* ((regex ivy--old-re)
         (regex (replace-regexp-in-string "\n" ""
                                          (counsel--elisp-to-pcre regex))))
    (counsel-cmd-to-dired
     (counsel--expand-ls
      (format counsel-filter-file-occur-command
              (format counsel-filter-file-command
                      (shell-quote-argument regex)))))))
;;
(defcustom counsel-filter-directory-find-args
  "-name '.git' -prune -o -type d -print"
  "Arguments for the `find-command' for `counsel-filter-directory'."
  :type 'string)
;;
(defcustom counsel-filter-directory-grep-args
  "-E -e %s"
  "Arguments for the `grep-command' for `counsel-filter-directory'."
  :type 'string)
;;
(defun counsel-filter-directory-action
    (d)
  (with-ivy-window
    (let ((default-directory counsel-filter-file-dir))
      (dired-jump nil (expand-file-name d)))))
;;
;;;###autoload
(defun counsel-filter-directory
    (&optional initial-input initial-directory prompt action)
  (interactive (list nil
                     (when current-prefix-arg
                       (read-directory-name "From directory: "))))
  (let ((counsel-filter-file-find-args counsel-filter-directory-find-args)
        (counsel-filter-file-grep-args counsel-filter-directory-grep-args))
    (counsel-filter-file initial-input
                         initial-directory
                         (or prompt "Filter directory: ")
                         (or action #'counsel-filter-directory-action))))
;;
(provide 'counsel-filter)
