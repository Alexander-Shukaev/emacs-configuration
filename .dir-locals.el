;;; Directory Local Variables {{{
;;  ==========================================================================
((nil . ((eval . (when (bound-and-true-p init-user-url)
                   (setq-local init-user-email init-user-url))))))
;;  ==========================================================================
;;  }}} Directory Local Variables
;;
;;; References {{{
;;  ==========================================================================
;;  [1] Info node `(emacs) Directory Variables'
;;  ==========================================================================
;;  }}} References
